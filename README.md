# Dreamscapes

Dreamscapes is an immersive AR and VR experience for the iPad designed for the Philadelphia Art Museum. It is the submission by Day Bakers (an alias of Night Kitchen Interactive) for Hackathon 2.0.

## How to Run

In order to run, install the newest version of xCode on a machine running Mac OS, and open "Unity-iPhone.xcodeproj". This should open the project in xCode. In the project settings, under "Signing", sign in with a valid Apple Developer Profile. This is required to be able to deploy the app to a device. 
In order to run, install the newest version of xCode on a machine running Mac OS, and open "Unity-iPhone.xcodeproj". This should open the project in xCode. In the project settings, under "Signing", sign in with a valid Apple Developer Profile. This is required to be able to deploy the app to a device. 

Plug in an iPad running iOS 10.3 by USB. Select the iPod as the target device in the toolbar at the top of the xCode window. Press the Build & Run button in the top left. This should take a few minutes to build. Make sure that your iPad is unlocked. When the app is ready, it will open on the iPad. You can then eject the iPad and open the app again.

*If there's any trouble running the app, or if you need to see any other related material, please let us know by contacting us through Devpost, or emailing james@whatscookin.com.*

### How to Play

You'll be given a walkthrough when you play the game, but here's a quick rundown: 

The game starts in the North Wing on the First Floor. Players are asked to scan paintings using the iPad camera, looking for ones that remind Diana of her dreams about the museum!  

Those that do will have a message pop up beside them in the camera, notifying the player that they have discovered a clue. Players can see these paintings come to life, and become three-dimensional, appearing to recede into the frame and acquire an new element of depth to the piece. 

Players can walk around this 3D display and view it at different angles, watching the elements of the painting move around in space. 

After discovering a clue, Players are notified that they can view their log to get a hint about where to go next. Each hint refers to a room on the Second floor that players can visit to unlock Diana's dreams!

When they're ready, the player moves on to the Second Floor, looking for the rooms that match the clues in their log. When they think they've found one, the player can tap the room on their map. If they were right, their camera will display an image of an object from the room. 

The player is instructed to line up that image with the object in the room, and tap the screen.

Once they do, the screen displays a VR Dreamscape of that room - a panorama of the space that has been painted in a style inspired by the painting from the first floor whose clue led there. 

Tapping the screen again closes the Dreamscape. Players are then free to explore the rest of the dreamscapes! Tap rooms you've already visited to see them again.
