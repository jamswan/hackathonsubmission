﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Gvr_Internal_MNAPGJPEPGN721519217.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// System.Single[]
struct SingleU5BU5D_t577127397;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.OAPLNBCDLIB
struct  OAPLNBCDLIB_t4108484976  : public MNAPGJPEPGN_t721519217
{
public:
	// System.Single[] Gvr.Internal.OAPLNBCDLIB::IPFDHFHGFNL
	SingleU5BU5D_t577127397* ___IPFDHFHGFNL_23;
	// System.Single[] Gvr.Internal.OAPLNBCDLIB::LPFIMOFHBAG
	SingleU5BU5D_t577127397* ___LPFIMOFHBAG_24;
	// System.Single[] Gvr.Internal.OAPLNBCDLIB::GMDNALCDCCP
	SingleU5BU5D_t577127397* ___GMDNALCDCCP_25;
	// UnityEngine.Matrix4x4 Gvr.Internal.OAPLNBCDLIB::ELLHLOEBGKN
	Matrix4x4_t2933234003  ___ELLHLOEBGKN_26;
	// UnityEngine.Matrix4x4 Gvr.Internal.OAPLNBCDLIB::KNIEEAMILHE
	Matrix4x4_t2933234003  ___KNIEEAMILHE_27;
	// UnityEngine.Matrix4x4 Gvr.Internal.OAPLNBCDLIB::HHBEFPMOEIK
	Matrix4x4_t2933234003  ___HHBEFPMOEIK_28;
	// System.Boolean Gvr.Internal.OAPLNBCDLIB::JJDIKBCLNGI
	bool ___JJDIKBCLNGI_29;
	// System.Boolean Gvr.Internal.OAPLNBCDLIB::PLANKBDNDHL
	bool ___PLANKBDNDHL_30;

public:
	inline static int32_t get_offset_of_IPFDHFHGFNL_23() { return static_cast<int32_t>(offsetof(OAPLNBCDLIB_t4108484976, ___IPFDHFHGFNL_23)); }
	inline SingleU5BU5D_t577127397* get_IPFDHFHGFNL_23() const { return ___IPFDHFHGFNL_23; }
	inline SingleU5BU5D_t577127397** get_address_of_IPFDHFHGFNL_23() { return &___IPFDHFHGFNL_23; }
	inline void set_IPFDHFHGFNL_23(SingleU5BU5D_t577127397* value)
	{
		___IPFDHFHGFNL_23 = value;
		Il2CppCodeGenWriteBarrier(&___IPFDHFHGFNL_23, value);
	}

	inline static int32_t get_offset_of_LPFIMOFHBAG_24() { return static_cast<int32_t>(offsetof(OAPLNBCDLIB_t4108484976, ___LPFIMOFHBAG_24)); }
	inline SingleU5BU5D_t577127397* get_LPFIMOFHBAG_24() const { return ___LPFIMOFHBAG_24; }
	inline SingleU5BU5D_t577127397** get_address_of_LPFIMOFHBAG_24() { return &___LPFIMOFHBAG_24; }
	inline void set_LPFIMOFHBAG_24(SingleU5BU5D_t577127397* value)
	{
		___LPFIMOFHBAG_24 = value;
		Il2CppCodeGenWriteBarrier(&___LPFIMOFHBAG_24, value);
	}

	inline static int32_t get_offset_of_GMDNALCDCCP_25() { return static_cast<int32_t>(offsetof(OAPLNBCDLIB_t4108484976, ___GMDNALCDCCP_25)); }
	inline SingleU5BU5D_t577127397* get_GMDNALCDCCP_25() const { return ___GMDNALCDCCP_25; }
	inline SingleU5BU5D_t577127397** get_address_of_GMDNALCDCCP_25() { return &___GMDNALCDCCP_25; }
	inline void set_GMDNALCDCCP_25(SingleU5BU5D_t577127397* value)
	{
		___GMDNALCDCCP_25 = value;
		Il2CppCodeGenWriteBarrier(&___GMDNALCDCCP_25, value);
	}

	inline static int32_t get_offset_of_ELLHLOEBGKN_26() { return static_cast<int32_t>(offsetof(OAPLNBCDLIB_t4108484976, ___ELLHLOEBGKN_26)); }
	inline Matrix4x4_t2933234003  get_ELLHLOEBGKN_26() const { return ___ELLHLOEBGKN_26; }
	inline Matrix4x4_t2933234003 * get_address_of_ELLHLOEBGKN_26() { return &___ELLHLOEBGKN_26; }
	inline void set_ELLHLOEBGKN_26(Matrix4x4_t2933234003  value)
	{
		___ELLHLOEBGKN_26 = value;
	}

	inline static int32_t get_offset_of_KNIEEAMILHE_27() { return static_cast<int32_t>(offsetof(OAPLNBCDLIB_t4108484976, ___KNIEEAMILHE_27)); }
	inline Matrix4x4_t2933234003  get_KNIEEAMILHE_27() const { return ___KNIEEAMILHE_27; }
	inline Matrix4x4_t2933234003 * get_address_of_KNIEEAMILHE_27() { return &___KNIEEAMILHE_27; }
	inline void set_KNIEEAMILHE_27(Matrix4x4_t2933234003  value)
	{
		___KNIEEAMILHE_27 = value;
	}

	inline static int32_t get_offset_of_HHBEFPMOEIK_28() { return static_cast<int32_t>(offsetof(OAPLNBCDLIB_t4108484976, ___HHBEFPMOEIK_28)); }
	inline Matrix4x4_t2933234003  get_HHBEFPMOEIK_28() const { return ___HHBEFPMOEIK_28; }
	inline Matrix4x4_t2933234003 * get_address_of_HHBEFPMOEIK_28() { return &___HHBEFPMOEIK_28; }
	inline void set_HHBEFPMOEIK_28(Matrix4x4_t2933234003  value)
	{
		___HHBEFPMOEIK_28 = value;
	}

	inline static int32_t get_offset_of_JJDIKBCLNGI_29() { return static_cast<int32_t>(offsetof(OAPLNBCDLIB_t4108484976, ___JJDIKBCLNGI_29)); }
	inline bool get_JJDIKBCLNGI_29() const { return ___JJDIKBCLNGI_29; }
	inline bool* get_address_of_JJDIKBCLNGI_29() { return &___JJDIKBCLNGI_29; }
	inline void set_JJDIKBCLNGI_29(bool value)
	{
		___JJDIKBCLNGI_29 = value;
	}

	inline static int32_t get_offset_of_PLANKBDNDHL_30() { return static_cast<int32_t>(offsetof(OAPLNBCDLIB_t4108484976, ___PLANKBDNDHL_30)); }
	inline bool get_PLANKBDNDHL_30() const { return ___PLANKBDNDHL_30; }
	inline bool* get_address_of_PLANKBDNDHL_30() { return &___PLANKBDNDHL_30; }
	inline void set_PLANKBDNDHL_30(bool value)
	{
		___PLANKBDNDHL_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
