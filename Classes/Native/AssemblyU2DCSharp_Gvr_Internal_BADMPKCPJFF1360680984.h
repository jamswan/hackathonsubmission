﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Collections.Generic.List`1<Gvr.Internal.BADMPKCPJFF/FIDMFOFBGKL>
struct List_1_t3343883158;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.BADMPKCPJFF
struct  BADMPKCPJFF_t1360680984 
{
public:
	// System.Int32 Gvr.Internal.BADMPKCPJFF::ANNIDELAEEC
	int32_t ___ANNIDELAEEC_0;
	// System.Int32 Gvr.Internal.BADMPKCPJFF::OPIBJJMGAOD
	int32_t ___OPIBJJMGAOD_1;
	// System.Collections.Generic.List`1<Gvr.Internal.BADMPKCPJFF/FIDMFOFBGKL> Gvr.Internal.BADMPKCPJFF::JKCJGOGMNAF
	List_1_t3343883158 * ___JKCJGOGMNAF_2;

public:
	inline static int32_t get_offset_of_ANNIDELAEEC_0() { return static_cast<int32_t>(offsetof(BADMPKCPJFF_t1360680984, ___ANNIDELAEEC_0)); }
	inline int32_t get_ANNIDELAEEC_0() const { return ___ANNIDELAEEC_0; }
	inline int32_t* get_address_of_ANNIDELAEEC_0() { return &___ANNIDELAEEC_0; }
	inline void set_ANNIDELAEEC_0(int32_t value)
	{
		___ANNIDELAEEC_0 = value;
	}

	inline static int32_t get_offset_of_OPIBJJMGAOD_1() { return static_cast<int32_t>(offsetof(BADMPKCPJFF_t1360680984, ___OPIBJJMGAOD_1)); }
	inline int32_t get_OPIBJJMGAOD_1() const { return ___OPIBJJMGAOD_1; }
	inline int32_t* get_address_of_OPIBJJMGAOD_1() { return &___OPIBJJMGAOD_1; }
	inline void set_OPIBJJMGAOD_1(int32_t value)
	{
		___OPIBJJMGAOD_1 = value;
	}

	inline static int32_t get_offset_of_JKCJGOGMNAF_2() { return static_cast<int32_t>(offsetof(BADMPKCPJFF_t1360680984, ___JKCJGOGMNAF_2)); }
	inline List_1_t3343883158 * get_JKCJGOGMNAF_2() const { return ___JKCJGOGMNAF_2; }
	inline List_1_t3343883158 ** get_address_of_JKCJGOGMNAF_2() { return &___JKCJGOGMNAF_2; }
	inline void set_JKCJGOGMNAF_2(List_1_t3343883158 * value)
	{
		___JKCJGOGMNAF_2 = value;
		Il2CppCodeGenWriteBarrier(&___JKCJGOGMNAF_2, value);
	}
};

struct BADMPKCPJFF_t1360680984_StaticFields
{
public:
	// System.Int32 Gvr.Internal.BADMPKCPJFF::NONPECLJJJB
	int32_t ___NONPECLJJJB_3;
	// System.Int32 Gvr.Internal.BADMPKCPJFF::GIEEDIKMGIA
	int32_t ___GIEEDIKMGIA_4;
	// System.Int32 Gvr.Internal.BADMPKCPJFF::OAOGMCMCNNK
	int32_t ___OAOGMCMCNNK_5;

public:
	inline static int32_t get_offset_of_NONPECLJJJB_3() { return static_cast<int32_t>(offsetof(BADMPKCPJFF_t1360680984_StaticFields, ___NONPECLJJJB_3)); }
	inline int32_t get_NONPECLJJJB_3() const { return ___NONPECLJJJB_3; }
	inline int32_t* get_address_of_NONPECLJJJB_3() { return &___NONPECLJJJB_3; }
	inline void set_NONPECLJJJB_3(int32_t value)
	{
		___NONPECLJJJB_3 = value;
	}

	inline static int32_t get_offset_of_GIEEDIKMGIA_4() { return static_cast<int32_t>(offsetof(BADMPKCPJFF_t1360680984_StaticFields, ___GIEEDIKMGIA_4)); }
	inline int32_t get_GIEEDIKMGIA_4() const { return ___GIEEDIKMGIA_4; }
	inline int32_t* get_address_of_GIEEDIKMGIA_4() { return &___GIEEDIKMGIA_4; }
	inline void set_GIEEDIKMGIA_4(int32_t value)
	{
		___GIEEDIKMGIA_4 = value;
	}

	inline static int32_t get_offset_of_OAOGMCMCNNK_5() { return static_cast<int32_t>(offsetof(BADMPKCPJFF_t1360680984_StaticFields, ___OAOGMCMCNNK_5)); }
	inline int32_t get_OAOGMCMCNNK_5() const { return ___OAOGMCMCNNK_5; }
	inline int32_t* get_address_of_OAOGMCMCNNK_5() { return &___OAOGMCMCNNK_5; }
	inline void set_OAOGMCMCNNK_5(int32_t value)
	{
		___OAOGMCMCNNK_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Gvr.Internal.BADMPKCPJFF
struct BADMPKCPJFF_t1360680984_marshaled_pinvoke
{
	int32_t ___ANNIDELAEEC_0;
	int32_t ___OPIBJJMGAOD_1;
	List_1_t3343883158 * ___JKCJGOGMNAF_2;
};
// Native definition for COM marshalling of Gvr.Internal.BADMPKCPJFF
struct BADMPKCPJFF_t1360680984_marshaled_com
{
	int32_t ___ANNIDELAEEC_0;
	int32_t ___OPIBJJMGAOD_1;
	List_1_t3343883158 * ___JKCJGOGMNAF_2;
};
