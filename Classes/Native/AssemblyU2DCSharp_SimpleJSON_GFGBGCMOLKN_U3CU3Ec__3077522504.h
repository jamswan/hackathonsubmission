﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.IEnumerator`1<SimpleJSON.GFGBGCMOLKN>
struct IEnumerator_1_t709296976;
// SimpleJSON.GFGBGCMOLKN
struct GFGBGCMOLKN_t3233773149;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.GFGBGCMOLKN/<>c__Iterator1
struct  U3CU3Ec__Iterator1_t3077522504  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerator`1<SimpleJSON.GFGBGCMOLKN> SimpleJSON.GFGBGCMOLKN/<>c__Iterator1::HCHAHDKBNGC
	Il2CppObject* ___HCHAHDKBNGC_0;
	// SimpleJSON.GFGBGCMOLKN SimpleJSON.GFGBGCMOLKN/<>c__Iterator1::PAONNDEKLLE
	GFGBGCMOLKN_t3233773149 * ___PAONNDEKLLE_1;
	// System.Collections.Generic.IEnumerator`1<SimpleJSON.GFGBGCMOLKN> SimpleJSON.GFGBGCMOLKN/<>c__Iterator1::FKBOJIFPHNA
	Il2CppObject* ___FKBOJIFPHNA_2;
	// SimpleJSON.GFGBGCMOLKN SimpleJSON.GFGBGCMOLKN/<>c__Iterator1::LAMPMLAEPCJ
	GFGBGCMOLKN_t3233773149 * ___LAMPMLAEPCJ_3;
	// SimpleJSON.GFGBGCMOLKN SimpleJSON.GFGBGCMOLKN/<>c__Iterator1::AOOLEAHHMIH
	GFGBGCMOLKN_t3233773149 * ___AOOLEAHHMIH_4;
	// SimpleJSON.GFGBGCMOLKN SimpleJSON.GFGBGCMOLKN/<>c__Iterator1::LGBFNMECDHC
	GFGBGCMOLKN_t3233773149 * ___LGBFNMECDHC_5;
	// System.Boolean SimpleJSON.GFGBGCMOLKN/<>c__Iterator1::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_6;
	// System.Int32 SimpleJSON.GFGBGCMOLKN/<>c__Iterator1::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_7;

public:
	inline static int32_t get_offset_of_HCHAHDKBNGC_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3077522504, ___HCHAHDKBNGC_0)); }
	inline Il2CppObject* get_HCHAHDKBNGC_0() const { return ___HCHAHDKBNGC_0; }
	inline Il2CppObject** get_address_of_HCHAHDKBNGC_0() { return &___HCHAHDKBNGC_0; }
	inline void set_HCHAHDKBNGC_0(Il2CppObject* value)
	{
		___HCHAHDKBNGC_0 = value;
		Il2CppCodeGenWriteBarrier(&___HCHAHDKBNGC_0, value);
	}

	inline static int32_t get_offset_of_PAONNDEKLLE_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3077522504, ___PAONNDEKLLE_1)); }
	inline GFGBGCMOLKN_t3233773149 * get_PAONNDEKLLE_1() const { return ___PAONNDEKLLE_1; }
	inline GFGBGCMOLKN_t3233773149 ** get_address_of_PAONNDEKLLE_1() { return &___PAONNDEKLLE_1; }
	inline void set_PAONNDEKLLE_1(GFGBGCMOLKN_t3233773149 * value)
	{
		___PAONNDEKLLE_1 = value;
		Il2CppCodeGenWriteBarrier(&___PAONNDEKLLE_1, value);
	}

	inline static int32_t get_offset_of_FKBOJIFPHNA_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3077522504, ___FKBOJIFPHNA_2)); }
	inline Il2CppObject* get_FKBOJIFPHNA_2() const { return ___FKBOJIFPHNA_2; }
	inline Il2CppObject** get_address_of_FKBOJIFPHNA_2() { return &___FKBOJIFPHNA_2; }
	inline void set_FKBOJIFPHNA_2(Il2CppObject* value)
	{
		___FKBOJIFPHNA_2 = value;
		Il2CppCodeGenWriteBarrier(&___FKBOJIFPHNA_2, value);
	}

	inline static int32_t get_offset_of_LAMPMLAEPCJ_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3077522504, ___LAMPMLAEPCJ_3)); }
	inline GFGBGCMOLKN_t3233773149 * get_LAMPMLAEPCJ_3() const { return ___LAMPMLAEPCJ_3; }
	inline GFGBGCMOLKN_t3233773149 ** get_address_of_LAMPMLAEPCJ_3() { return &___LAMPMLAEPCJ_3; }
	inline void set_LAMPMLAEPCJ_3(GFGBGCMOLKN_t3233773149 * value)
	{
		___LAMPMLAEPCJ_3 = value;
		Il2CppCodeGenWriteBarrier(&___LAMPMLAEPCJ_3, value);
	}

	inline static int32_t get_offset_of_AOOLEAHHMIH_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3077522504, ___AOOLEAHHMIH_4)); }
	inline GFGBGCMOLKN_t3233773149 * get_AOOLEAHHMIH_4() const { return ___AOOLEAHHMIH_4; }
	inline GFGBGCMOLKN_t3233773149 ** get_address_of_AOOLEAHHMIH_4() { return &___AOOLEAHHMIH_4; }
	inline void set_AOOLEAHHMIH_4(GFGBGCMOLKN_t3233773149 * value)
	{
		___AOOLEAHHMIH_4 = value;
		Il2CppCodeGenWriteBarrier(&___AOOLEAHHMIH_4, value);
	}

	inline static int32_t get_offset_of_LGBFNMECDHC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3077522504, ___LGBFNMECDHC_5)); }
	inline GFGBGCMOLKN_t3233773149 * get_LGBFNMECDHC_5() const { return ___LGBFNMECDHC_5; }
	inline GFGBGCMOLKN_t3233773149 ** get_address_of_LGBFNMECDHC_5() { return &___LGBFNMECDHC_5; }
	inline void set_LGBFNMECDHC_5(GFGBGCMOLKN_t3233773149 * value)
	{
		___LGBFNMECDHC_5 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_5, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3077522504, ___IMAGLIMLPFK_6)); }
	inline bool get_IMAGLIMLPFK_6() const { return ___IMAGLIMLPFK_6; }
	inline bool* get_address_of_IMAGLIMLPFK_6() { return &___IMAGLIMLPFK_6; }
	inline void set_IMAGLIMLPFK_6(bool value)
	{
		___IMAGLIMLPFK_6 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3077522504, ___EPCFNNGDBGC_7)); }
	inline int32_t get_EPCFNNGDBGC_7() const { return ___EPCFNNGDBGC_7; }
	inline int32_t* get_address_of_EPCFNNGDBGC_7() { return &___EPCFNNGDBGC_7; }
	inline void set_EPCFNNGDBGC_7(int32_t value)
	{
		___EPCFNNGDBGC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
