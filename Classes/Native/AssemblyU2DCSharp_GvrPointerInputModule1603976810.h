﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM1295781545.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerInputModule
struct  GvrPointerInputModule_t1603976810  : public BaseInputModule_t1295781545
{
public:
	// System.Boolean GvrPointerInputModule::vrModeOnly
	bool ___vrModeOnly_8;
	// UnityEngine.EventSystems.PointerEventData GvrPointerInputModule::LHLLOCFLGCJ
	PointerEventData_t1599784723 * ___LHLLOCFLGCJ_9;
	// UnityEngine.Vector2 GvrPointerInputModule::OIJLKMOAKHM
	Vector2_t2243707579  ___OIJLKMOAKHM_10;
	// UnityEngine.Vector2 GvrPointerInputModule::LLNECJFHFCK
	Vector2_t2243707579  ___LLNECJFHFCK_11;
	// System.Boolean GvrPointerInputModule::DEJGPGIIABI
	bool ___DEJGPGIIABI_12;
	// System.Boolean GvrPointerInputModule::OOHKMKEDDKP
	bool ___OOHKMKEDDKP_13;
	// System.Boolean GvrPointerInputModule::AKNJNLJHIGB
	bool ___AKNJNLJHIGB_14;

public:
	inline static int32_t get_offset_of_vrModeOnly_8() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t1603976810, ___vrModeOnly_8)); }
	inline bool get_vrModeOnly_8() const { return ___vrModeOnly_8; }
	inline bool* get_address_of_vrModeOnly_8() { return &___vrModeOnly_8; }
	inline void set_vrModeOnly_8(bool value)
	{
		___vrModeOnly_8 = value;
	}

	inline static int32_t get_offset_of_LHLLOCFLGCJ_9() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t1603976810, ___LHLLOCFLGCJ_9)); }
	inline PointerEventData_t1599784723 * get_LHLLOCFLGCJ_9() const { return ___LHLLOCFLGCJ_9; }
	inline PointerEventData_t1599784723 ** get_address_of_LHLLOCFLGCJ_9() { return &___LHLLOCFLGCJ_9; }
	inline void set_LHLLOCFLGCJ_9(PointerEventData_t1599784723 * value)
	{
		___LHLLOCFLGCJ_9 = value;
		Il2CppCodeGenWriteBarrier(&___LHLLOCFLGCJ_9, value);
	}

	inline static int32_t get_offset_of_OIJLKMOAKHM_10() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t1603976810, ___OIJLKMOAKHM_10)); }
	inline Vector2_t2243707579  get_OIJLKMOAKHM_10() const { return ___OIJLKMOAKHM_10; }
	inline Vector2_t2243707579 * get_address_of_OIJLKMOAKHM_10() { return &___OIJLKMOAKHM_10; }
	inline void set_OIJLKMOAKHM_10(Vector2_t2243707579  value)
	{
		___OIJLKMOAKHM_10 = value;
	}

	inline static int32_t get_offset_of_LLNECJFHFCK_11() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t1603976810, ___LLNECJFHFCK_11)); }
	inline Vector2_t2243707579  get_LLNECJFHFCK_11() const { return ___LLNECJFHFCK_11; }
	inline Vector2_t2243707579 * get_address_of_LLNECJFHFCK_11() { return &___LLNECJFHFCK_11; }
	inline void set_LLNECJFHFCK_11(Vector2_t2243707579  value)
	{
		___LLNECJFHFCK_11 = value;
	}

	inline static int32_t get_offset_of_DEJGPGIIABI_12() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t1603976810, ___DEJGPGIIABI_12)); }
	inline bool get_DEJGPGIIABI_12() const { return ___DEJGPGIIABI_12; }
	inline bool* get_address_of_DEJGPGIIABI_12() { return &___DEJGPGIIABI_12; }
	inline void set_DEJGPGIIABI_12(bool value)
	{
		___DEJGPGIIABI_12 = value;
	}

	inline static int32_t get_offset_of_OOHKMKEDDKP_13() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t1603976810, ___OOHKMKEDDKP_13)); }
	inline bool get_OOHKMKEDDKP_13() const { return ___OOHKMKEDDKP_13; }
	inline bool* get_address_of_OOHKMKEDDKP_13() { return &___OOHKMKEDDKP_13; }
	inline void set_OOHKMKEDDKP_13(bool value)
	{
		___OOHKMKEDDKP_13 = value;
	}

	inline static int32_t get_offset_of_AKNJNLJHIGB_14() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t1603976810, ___AKNJNLJHIGB_14)); }
	inline bool get_AKNJNLJHIGB_14() const { return ___AKNJNLJHIGB_14; }
	inline bool* get_address_of_AKNJNLJHIGB_14() { return &___AKNJNLJHIGB_14; }
	inline void set_AKNJNLJHIGB_14(bool value)
	{
		___AKNJNLJHIGB_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
