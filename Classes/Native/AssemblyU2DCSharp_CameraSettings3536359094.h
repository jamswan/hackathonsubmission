﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer1654543970.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSettings
struct  CameraSettings_t3536359094  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean CameraSettings::FCLGCBDLJAG
	bool ___FCLGCBDLJAG_2;
	// System.Boolean CameraSettings::OFPCBIFHDJL
	bool ___OFPCBIFHDJL_3;
	// System.Boolean CameraSettings::JMKLIMBIJFA
	bool ___JMKLIMBIJFA_4;
	// Vuforia.CameraDevice/CameraDirection CameraSettings::GDLJHPPEELM
	int32_t ___GDLJHPPEELM_5;

public:
	inline static int32_t get_offset_of_FCLGCBDLJAG_2() { return static_cast<int32_t>(offsetof(CameraSettings_t3536359094, ___FCLGCBDLJAG_2)); }
	inline bool get_FCLGCBDLJAG_2() const { return ___FCLGCBDLJAG_2; }
	inline bool* get_address_of_FCLGCBDLJAG_2() { return &___FCLGCBDLJAG_2; }
	inline void set_FCLGCBDLJAG_2(bool value)
	{
		___FCLGCBDLJAG_2 = value;
	}

	inline static int32_t get_offset_of_OFPCBIFHDJL_3() { return static_cast<int32_t>(offsetof(CameraSettings_t3536359094, ___OFPCBIFHDJL_3)); }
	inline bool get_OFPCBIFHDJL_3() const { return ___OFPCBIFHDJL_3; }
	inline bool* get_address_of_OFPCBIFHDJL_3() { return &___OFPCBIFHDJL_3; }
	inline void set_OFPCBIFHDJL_3(bool value)
	{
		___OFPCBIFHDJL_3 = value;
	}

	inline static int32_t get_offset_of_JMKLIMBIJFA_4() { return static_cast<int32_t>(offsetof(CameraSettings_t3536359094, ___JMKLIMBIJFA_4)); }
	inline bool get_JMKLIMBIJFA_4() const { return ___JMKLIMBIJFA_4; }
	inline bool* get_address_of_JMKLIMBIJFA_4() { return &___JMKLIMBIJFA_4; }
	inline void set_JMKLIMBIJFA_4(bool value)
	{
		___JMKLIMBIJFA_4 = value;
	}

	inline static int32_t get_offset_of_GDLJHPPEELM_5() { return static_cast<int32_t>(offsetof(CameraSettings_t3536359094, ___GDLJHPPEELM_5)); }
	inline int32_t get_GDLJHPPEELM_5() const { return ___GDLJHPPEELM_5; }
	inline int32_t* get_address_of_GDLJHPPEELM_5() { return &___GDLJHPPEELM_5; }
	inline void set_GDLJHPPEELM_5(int32_t value)
	{
		___GDLJHPPEELM_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
