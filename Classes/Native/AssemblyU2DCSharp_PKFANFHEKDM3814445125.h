﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_MMINFHHJGIF384011353.h"

// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Object
struct Il2CppObject;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PKFANFHEKDM
struct  PKFANFHEKDM_t3814445125  : public MMINFHHJGIF_t384011353
{
public:
	// System.Action`1<System.Object> PKFANFHEKDM::ANNIDELAEEC
	Action_1_t2491248677 * ___ANNIDELAEEC_4;
	// System.Object PKFANFHEKDM::MHAHJONGPBJ
	Il2CppObject * ___MHAHJONGPBJ_5;
	// UnityEngine.Texture2D PKFANFHEKDM::IDBKNFKOEBF
	Texture2D_t3542995729 * ___IDBKNFKOEBF_6;

public:
	inline static int32_t get_offset_of_ANNIDELAEEC_4() { return static_cast<int32_t>(offsetof(PKFANFHEKDM_t3814445125, ___ANNIDELAEEC_4)); }
	inline Action_1_t2491248677 * get_ANNIDELAEEC_4() const { return ___ANNIDELAEEC_4; }
	inline Action_1_t2491248677 ** get_address_of_ANNIDELAEEC_4() { return &___ANNIDELAEEC_4; }
	inline void set_ANNIDELAEEC_4(Action_1_t2491248677 * value)
	{
		___ANNIDELAEEC_4 = value;
		Il2CppCodeGenWriteBarrier(&___ANNIDELAEEC_4, value);
	}

	inline static int32_t get_offset_of_MHAHJONGPBJ_5() { return static_cast<int32_t>(offsetof(PKFANFHEKDM_t3814445125, ___MHAHJONGPBJ_5)); }
	inline Il2CppObject * get_MHAHJONGPBJ_5() const { return ___MHAHJONGPBJ_5; }
	inline Il2CppObject ** get_address_of_MHAHJONGPBJ_5() { return &___MHAHJONGPBJ_5; }
	inline void set_MHAHJONGPBJ_5(Il2CppObject * value)
	{
		___MHAHJONGPBJ_5 = value;
		Il2CppCodeGenWriteBarrier(&___MHAHJONGPBJ_5, value);
	}

	inline static int32_t get_offset_of_IDBKNFKOEBF_6() { return static_cast<int32_t>(offsetof(PKFANFHEKDM_t3814445125, ___IDBKNFKOEBF_6)); }
	inline Texture2D_t3542995729 * get_IDBKNFKOEBF_6() const { return ___IDBKNFKOEBF_6; }
	inline Texture2D_t3542995729 ** get_address_of_IDBKNFKOEBF_6() { return &___IDBKNFKOEBF_6; }
	inline void set_IDBKNFKOEBF_6(Texture2D_t3542995729 * value)
	{
		___IDBKNFKOEBF_6 = value;
		Il2CppCodeGenWriteBarrier(&___IDBKNFKOEBF_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
