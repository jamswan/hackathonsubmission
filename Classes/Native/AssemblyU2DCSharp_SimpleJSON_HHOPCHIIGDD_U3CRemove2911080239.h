﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SimpleJSON.GFGBGCMOLKN
struct GFGBGCMOLKN_t3233773149;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.HHOPCHIIGDD/<Remove>c__AnonStorey2
struct  U3CRemoveU3Ec__AnonStorey2_t2911080239  : public Il2CppObject
{
public:
	// SimpleJSON.GFGBGCMOLKN SimpleJSON.HHOPCHIIGDD/<Remove>c__AnonStorey2::MANBNKHPPAC
	GFGBGCMOLKN_t3233773149 * ___MANBNKHPPAC_0;

public:
	inline static int32_t get_offset_of_MANBNKHPPAC_0() { return static_cast<int32_t>(offsetof(U3CRemoveU3Ec__AnonStorey2_t2911080239, ___MANBNKHPPAC_0)); }
	inline GFGBGCMOLKN_t3233773149 * get_MANBNKHPPAC_0() const { return ___MANBNKHPPAC_0; }
	inline GFGBGCMOLKN_t3233773149 ** get_address_of_MANBNKHPPAC_0() { return &___MANBNKHPPAC_0; }
	inline void set_MANBNKHPPAC_0(GFGBGCMOLKN_t3233773149 * value)
	{
		___MANBNKHPPAC_0 = value;
		Il2CppCodeGenWriteBarrier(&___MANBNKHPPAC_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
