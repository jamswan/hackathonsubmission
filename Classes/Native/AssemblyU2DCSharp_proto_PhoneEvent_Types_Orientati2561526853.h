﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Gene1462349001.h"

// proto.PhoneEvent/Types/OrientationEvent
struct OrientationEvent_t2038376807;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/OrientationEvent/Builder
struct  Builder_t2561526853  : public GeneratedBuilderLite_2_t1462349001
{
public:
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::LCFNHIBHFMO
	bool ___LCFNHIBHFMO_0;
	// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::JOMIOHAFLNI
	OrientationEvent_t2038376807 * ___JOMIOHAFLNI_1;

public:
	inline static int32_t get_offset_of_LCFNHIBHFMO_0() { return static_cast<int32_t>(offsetof(Builder_t2561526853, ___LCFNHIBHFMO_0)); }
	inline bool get_LCFNHIBHFMO_0() const { return ___LCFNHIBHFMO_0; }
	inline bool* get_address_of_LCFNHIBHFMO_0() { return &___LCFNHIBHFMO_0; }
	inline void set_LCFNHIBHFMO_0(bool value)
	{
		___LCFNHIBHFMO_0 = value;
	}

	inline static int32_t get_offset_of_JOMIOHAFLNI_1() { return static_cast<int32_t>(offsetof(Builder_t2561526853, ___JOMIOHAFLNI_1)); }
	inline OrientationEvent_t2038376807 * get_JOMIOHAFLNI_1() const { return ___JOMIOHAFLNI_1; }
	inline OrientationEvent_t2038376807 ** get_address_of_JOMIOHAFLNI_1() { return &___JOMIOHAFLNI_1; }
	inline void set_JOMIOHAFLNI_1(OrientationEvent_t2038376807 * value)
	{
		___JOMIOHAFLNI_1 = value;
		Il2CppCodeGenWriteBarrier(&___JOMIOHAFLNI_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
