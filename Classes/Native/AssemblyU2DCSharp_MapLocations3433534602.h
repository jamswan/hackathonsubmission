﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Singleton_1_gen314451693.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// MapLocation[]
struct MapLocationU5BU5D_t2165601514;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>
struct Dictionary_2_t894930024;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapLocations
struct  MapLocations_t3433534602  : public Singleton_1_t314451693
{
public:
	// UnityEngine.Transform MapLocations::playerMarker
	Transform_t3275118058 * ___playerMarker_5;
	// UnityEngine.Transform MapLocations::hintMarker
	Transform_t3275118058 * ___hintMarker_6;
	// MapLocation[] MapLocations::setMapLocations
	MapLocationU5BU5D_t2165601514* ___setMapLocations_7;
	// MapLocation[] MapLocations::HAOAJEOLMNG
	MapLocationU5BU5D_t2165601514* ___HAOAJEOLMNG_8;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform> MapLocations::IBIJEOFAMJA
	Dictionary_2_t894930024 * ___IBIJEOFAMJA_9;

public:
	inline static int32_t get_offset_of_playerMarker_5() { return static_cast<int32_t>(offsetof(MapLocations_t3433534602, ___playerMarker_5)); }
	inline Transform_t3275118058 * get_playerMarker_5() const { return ___playerMarker_5; }
	inline Transform_t3275118058 ** get_address_of_playerMarker_5() { return &___playerMarker_5; }
	inline void set_playerMarker_5(Transform_t3275118058 * value)
	{
		___playerMarker_5 = value;
		Il2CppCodeGenWriteBarrier(&___playerMarker_5, value);
	}

	inline static int32_t get_offset_of_hintMarker_6() { return static_cast<int32_t>(offsetof(MapLocations_t3433534602, ___hintMarker_6)); }
	inline Transform_t3275118058 * get_hintMarker_6() const { return ___hintMarker_6; }
	inline Transform_t3275118058 ** get_address_of_hintMarker_6() { return &___hintMarker_6; }
	inline void set_hintMarker_6(Transform_t3275118058 * value)
	{
		___hintMarker_6 = value;
		Il2CppCodeGenWriteBarrier(&___hintMarker_6, value);
	}

	inline static int32_t get_offset_of_setMapLocations_7() { return static_cast<int32_t>(offsetof(MapLocations_t3433534602, ___setMapLocations_7)); }
	inline MapLocationU5BU5D_t2165601514* get_setMapLocations_7() const { return ___setMapLocations_7; }
	inline MapLocationU5BU5D_t2165601514** get_address_of_setMapLocations_7() { return &___setMapLocations_7; }
	inline void set_setMapLocations_7(MapLocationU5BU5D_t2165601514* value)
	{
		___setMapLocations_7 = value;
		Il2CppCodeGenWriteBarrier(&___setMapLocations_7, value);
	}

	inline static int32_t get_offset_of_HAOAJEOLMNG_8() { return static_cast<int32_t>(offsetof(MapLocations_t3433534602, ___HAOAJEOLMNG_8)); }
	inline MapLocationU5BU5D_t2165601514* get_HAOAJEOLMNG_8() const { return ___HAOAJEOLMNG_8; }
	inline MapLocationU5BU5D_t2165601514** get_address_of_HAOAJEOLMNG_8() { return &___HAOAJEOLMNG_8; }
	inline void set_HAOAJEOLMNG_8(MapLocationU5BU5D_t2165601514* value)
	{
		___HAOAJEOLMNG_8 = value;
		Il2CppCodeGenWriteBarrier(&___HAOAJEOLMNG_8, value);
	}

	inline static int32_t get_offset_of_IBIJEOFAMJA_9() { return static_cast<int32_t>(offsetof(MapLocations_t3433534602, ___IBIJEOFAMJA_9)); }
	inline Dictionary_2_t894930024 * get_IBIJEOFAMJA_9() const { return ___IBIJEOFAMJA_9; }
	inline Dictionary_2_t894930024 ** get_address_of_IBIJEOFAMJA_9() { return &___IBIJEOFAMJA_9; }
	inline void set_IBIJEOFAMJA_9(Dictionary_2_t894930024 * value)
	{
		___IBIJEOFAMJA_9 = value;
		Il2CppCodeGenWriteBarrier(&___IBIJEOFAMJA_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
