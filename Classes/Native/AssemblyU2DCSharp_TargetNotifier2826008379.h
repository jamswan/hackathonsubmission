﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1779888572;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TargetNotifier
struct  TargetNotifier_t2826008379  : public MonoBehaviour_t1158329972
{
public:
	// Vuforia.TrackableBehaviour TargetNotifier::CHPIGKLDJDB
	TrackableBehaviour_t1779888572 * ___CHPIGKLDJDB_2;

public:
	inline static int32_t get_offset_of_CHPIGKLDJDB_2() { return static_cast<int32_t>(offsetof(TargetNotifier_t2826008379, ___CHPIGKLDJDB_2)); }
	inline TrackableBehaviour_t1779888572 * get_CHPIGKLDJDB_2() const { return ___CHPIGKLDJDB_2; }
	inline TrackableBehaviour_t1779888572 ** get_address_of_CHPIGKLDJDB_2() { return &___CHPIGKLDJDB_2; }
	inline void set_CHPIGKLDJDB_2(TrackableBehaviour_t1779888572 * value)
	{
		___CHPIGKLDJDB_2 = value;
		Il2CppCodeGenWriteBarrier(&___CHPIGKLDJDB_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
