﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SimpleJSON_GFGBGCMOLKN3233773149.h"

// SimpleJSON.GFGBGCMOLKN
struct GFGBGCMOLKN_t3233773149;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.IOLAFEHAHBI
struct  IOLAFEHAHBI_t963357276  : public GFGBGCMOLKN_t3233773149
{
public:
	// SimpleJSON.GFGBGCMOLKN SimpleJSON.IOLAFEHAHBI::CDGLKIHLGKA
	GFGBGCMOLKN_t3233773149 * ___CDGLKIHLGKA_0;
	// System.String SimpleJSON.IOLAFEHAHBI::LCHMOLBOCAN
	String_t* ___LCHMOLBOCAN_1;

public:
	inline static int32_t get_offset_of_CDGLKIHLGKA_0() { return static_cast<int32_t>(offsetof(IOLAFEHAHBI_t963357276, ___CDGLKIHLGKA_0)); }
	inline GFGBGCMOLKN_t3233773149 * get_CDGLKIHLGKA_0() const { return ___CDGLKIHLGKA_0; }
	inline GFGBGCMOLKN_t3233773149 ** get_address_of_CDGLKIHLGKA_0() { return &___CDGLKIHLGKA_0; }
	inline void set_CDGLKIHLGKA_0(GFGBGCMOLKN_t3233773149 * value)
	{
		___CDGLKIHLGKA_0 = value;
		Il2CppCodeGenWriteBarrier(&___CDGLKIHLGKA_0, value);
	}

	inline static int32_t get_offset_of_LCHMOLBOCAN_1() { return static_cast<int32_t>(offsetof(IOLAFEHAHBI_t963357276, ___LCHMOLBOCAN_1)); }
	inline String_t* get_LCHMOLBOCAN_1() const { return ___LCHMOLBOCAN_1; }
	inline String_t** get_address_of_LCHMOLBOCAN_1() { return &___LCHMOLBOCAN_1; }
	inline void set_LCHMOLBOCAN_1(String_t* value)
	{
		___LCHMOLBOCAN_1 = value;
		Il2CppCodeGenWriteBarrier(&___LCHMOLBOCAN_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
