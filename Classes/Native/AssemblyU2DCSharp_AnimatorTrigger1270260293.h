﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// UnityEngine.Animator
struct Animator_t69676727;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimatorTrigger
struct  AnimatorTrigger_t1270260293  : public NarrativeEffect_t3336735121
{
public:
	// UnityEngine.Animator AnimatorTrigger::animator
	Animator_t69676727 * ___animator_3;
	// System.String AnimatorTrigger::triggerName
	String_t* ___triggerName_4;

public:
	inline static int32_t get_offset_of_animator_3() { return static_cast<int32_t>(offsetof(AnimatorTrigger_t1270260293, ___animator_3)); }
	inline Animator_t69676727 * get_animator_3() const { return ___animator_3; }
	inline Animator_t69676727 ** get_address_of_animator_3() { return &___animator_3; }
	inline void set_animator_3(Animator_t69676727 * value)
	{
		___animator_3 = value;
		Il2CppCodeGenWriteBarrier(&___animator_3, value);
	}

	inline static int32_t get_offset_of_triggerName_4() { return static_cast<int32_t>(offsetof(AnimatorTrigger_t1270260293, ___triggerName_4)); }
	inline String_t* get_triggerName_4() const { return ___triggerName_4; }
	inline String_t** get_address_of_triggerName_4() { return &___triggerName_4; }
	inline void set_triggerName_4(String_t* value)
	{
		___triggerName_4 = value;
		Il2CppCodeGenWriteBarrier(&___triggerName_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
