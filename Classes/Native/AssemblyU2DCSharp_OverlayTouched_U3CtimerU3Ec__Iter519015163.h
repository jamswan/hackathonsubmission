﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// OverlayTouched
struct OverlayTouched_t2030538974;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OverlayTouched/<timer>c__Iterator0
struct  U3CtimerU3Ec__Iterator0_t519015163  : public Il2CppObject
{
public:
	// System.Single OverlayTouched/<timer>c__Iterator0::IGIHJBALJEK
	float ___IGIHJBALJEK_0;
	// OverlayTouched OverlayTouched/<timer>c__Iterator0::AOOLEAHHMIH
	OverlayTouched_t2030538974 * ___AOOLEAHHMIH_1;
	// System.Object OverlayTouched/<timer>c__Iterator0::LGBFNMECDHC
	Il2CppObject * ___LGBFNMECDHC_2;
	// System.Boolean OverlayTouched/<timer>c__Iterator0::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_3;
	// System.Int32 OverlayTouched/<timer>c__Iterator0::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_4;

public:
	inline static int32_t get_offset_of_IGIHJBALJEK_0() { return static_cast<int32_t>(offsetof(U3CtimerU3Ec__Iterator0_t519015163, ___IGIHJBALJEK_0)); }
	inline float get_IGIHJBALJEK_0() const { return ___IGIHJBALJEK_0; }
	inline float* get_address_of_IGIHJBALJEK_0() { return &___IGIHJBALJEK_0; }
	inline void set_IGIHJBALJEK_0(float value)
	{
		___IGIHJBALJEK_0 = value;
	}

	inline static int32_t get_offset_of_AOOLEAHHMIH_1() { return static_cast<int32_t>(offsetof(U3CtimerU3Ec__Iterator0_t519015163, ___AOOLEAHHMIH_1)); }
	inline OverlayTouched_t2030538974 * get_AOOLEAHHMIH_1() const { return ___AOOLEAHHMIH_1; }
	inline OverlayTouched_t2030538974 ** get_address_of_AOOLEAHHMIH_1() { return &___AOOLEAHHMIH_1; }
	inline void set_AOOLEAHHMIH_1(OverlayTouched_t2030538974 * value)
	{
		___AOOLEAHHMIH_1 = value;
		Il2CppCodeGenWriteBarrier(&___AOOLEAHHMIH_1, value);
	}

	inline static int32_t get_offset_of_LGBFNMECDHC_2() { return static_cast<int32_t>(offsetof(U3CtimerU3Ec__Iterator0_t519015163, ___LGBFNMECDHC_2)); }
	inline Il2CppObject * get_LGBFNMECDHC_2() const { return ___LGBFNMECDHC_2; }
	inline Il2CppObject ** get_address_of_LGBFNMECDHC_2() { return &___LGBFNMECDHC_2; }
	inline void set_LGBFNMECDHC_2(Il2CppObject * value)
	{
		___LGBFNMECDHC_2 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_2, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_3() { return static_cast<int32_t>(offsetof(U3CtimerU3Ec__Iterator0_t519015163, ___IMAGLIMLPFK_3)); }
	inline bool get_IMAGLIMLPFK_3() const { return ___IMAGLIMLPFK_3; }
	inline bool* get_address_of_IMAGLIMLPFK_3() { return &___IMAGLIMLPFK_3; }
	inline void set_IMAGLIMLPFK_3(bool value)
	{
		___IMAGLIMLPFK_3 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_4() { return static_cast<int32_t>(offsetof(U3CtimerU3Ec__Iterator0_t519015163, ___EPCFNNGDBGC_4)); }
	inline int32_t get_EPCFNNGDBGC_4() const { return ___EPCFNNGDBGC_4; }
	inline int32_t* get_address_of_EPCFNNGDBGC_4() { return &___EPCFNNGDBGC_4; }
	inline void set_EPCFNNGDBGC_4(int32_t value)
	{
		___EPCFNNGDBGC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
