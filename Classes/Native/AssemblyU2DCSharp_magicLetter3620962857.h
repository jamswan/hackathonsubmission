﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// magicLetter
struct  magicLetter_t3620962857  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 magicLetter::index
	int32_t ___index_2;
	// UnityEngine.TextMesh magicLetter::textMesh
	TextMesh_t1641806576 * ___textMesh_3;
	// UnityEngine.Renderer magicLetter::renderer
	Renderer_t257310565 * ___renderer_4;
	// UnityEngine.BoxCollider magicLetter::boxCollider
	BoxCollider_t22920061 * ___boxCollider_5;
	// System.Single magicLetter::NEENKLOMGFJ
	float ___NEENKLOMGFJ_6;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(magicLetter_t3620962857, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_textMesh_3() { return static_cast<int32_t>(offsetof(magicLetter_t3620962857, ___textMesh_3)); }
	inline TextMesh_t1641806576 * get_textMesh_3() const { return ___textMesh_3; }
	inline TextMesh_t1641806576 ** get_address_of_textMesh_3() { return &___textMesh_3; }
	inline void set_textMesh_3(TextMesh_t1641806576 * value)
	{
		___textMesh_3 = value;
		Il2CppCodeGenWriteBarrier(&___textMesh_3, value);
	}

	inline static int32_t get_offset_of_renderer_4() { return static_cast<int32_t>(offsetof(magicLetter_t3620962857, ___renderer_4)); }
	inline Renderer_t257310565 * get_renderer_4() const { return ___renderer_4; }
	inline Renderer_t257310565 ** get_address_of_renderer_4() { return &___renderer_4; }
	inline void set_renderer_4(Renderer_t257310565 * value)
	{
		___renderer_4 = value;
		Il2CppCodeGenWriteBarrier(&___renderer_4, value);
	}

	inline static int32_t get_offset_of_boxCollider_5() { return static_cast<int32_t>(offsetof(magicLetter_t3620962857, ___boxCollider_5)); }
	inline BoxCollider_t22920061 * get_boxCollider_5() const { return ___boxCollider_5; }
	inline BoxCollider_t22920061 ** get_address_of_boxCollider_5() { return &___boxCollider_5; }
	inline void set_boxCollider_5(BoxCollider_t22920061 * value)
	{
		___boxCollider_5 = value;
		Il2CppCodeGenWriteBarrier(&___boxCollider_5, value);
	}

	inline static int32_t get_offset_of_NEENKLOMGFJ_6() { return static_cast<int32_t>(offsetof(magicLetter_t3620962857, ___NEENKLOMGFJ_6)); }
	inline float get_NEENKLOMGFJ_6() const { return ___NEENKLOMGFJ_6; }
	inline float* get_address_of_NEENKLOMGFJ_6() { return &___NEENKLOMGFJ_6; }
	inline void set_NEENKLOMGFJ_6(float value)
	{
		___NEENKLOMGFJ_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
