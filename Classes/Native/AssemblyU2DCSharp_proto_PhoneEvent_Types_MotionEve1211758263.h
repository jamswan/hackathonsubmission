﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Genera15047533.h"

// proto.PhoneEvent/Types/MotionEvent/Types/Pointer
struct Pointer_t1211758263;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/MotionEvent/Types/Pointer
struct  Pointer_t1211758263  : public GeneratedMessageLite_2_t15047533
{
public:
	// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::AEEJBGMNBPG
	bool ___AEEJBGMNBPG_4;
	// System.Int32 proto.PhoneEvent/Types/MotionEvent/Types/Pointer::IBKNCIPJFEK
	int32_t ___IBKNCIPJFEK_5;
	// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::DCGBGFAMFIG
	bool ___DCGBGFAMFIG_7;
	// System.Single proto.PhoneEvent/Types/MotionEvent/Types/Pointer::BGMOJHPMOJE
	float ___BGMOJHPMOJE_8;
	// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::GGMHCMHPELH
	bool ___GGMHCMHPELH_10;
	// System.Single proto.PhoneEvent/Types/MotionEvent/Types/Pointer::LOHGDOHOBOL
	float ___LOHGDOHOBOL_11;
	// System.Int32 proto.PhoneEvent/Types/MotionEvent/Types/Pointer::BABCKJCECDJ
	int32_t ___BABCKJCECDJ_12;

public:
	inline static int32_t get_offset_of_AEEJBGMNBPG_4() { return static_cast<int32_t>(offsetof(Pointer_t1211758263, ___AEEJBGMNBPG_4)); }
	inline bool get_AEEJBGMNBPG_4() const { return ___AEEJBGMNBPG_4; }
	inline bool* get_address_of_AEEJBGMNBPG_4() { return &___AEEJBGMNBPG_4; }
	inline void set_AEEJBGMNBPG_4(bool value)
	{
		___AEEJBGMNBPG_4 = value;
	}

	inline static int32_t get_offset_of_IBKNCIPJFEK_5() { return static_cast<int32_t>(offsetof(Pointer_t1211758263, ___IBKNCIPJFEK_5)); }
	inline int32_t get_IBKNCIPJFEK_5() const { return ___IBKNCIPJFEK_5; }
	inline int32_t* get_address_of_IBKNCIPJFEK_5() { return &___IBKNCIPJFEK_5; }
	inline void set_IBKNCIPJFEK_5(int32_t value)
	{
		___IBKNCIPJFEK_5 = value;
	}

	inline static int32_t get_offset_of_DCGBGFAMFIG_7() { return static_cast<int32_t>(offsetof(Pointer_t1211758263, ___DCGBGFAMFIG_7)); }
	inline bool get_DCGBGFAMFIG_7() const { return ___DCGBGFAMFIG_7; }
	inline bool* get_address_of_DCGBGFAMFIG_7() { return &___DCGBGFAMFIG_7; }
	inline void set_DCGBGFAMFIG_7(bool value)
	{
		___DCGBGFAMFIG_7 = value;
	}

	inline static int32_t get_offset_of_BGMOJHPMOJE_8() { return static_cast<int32_t>(offsetof(Pointer_t1211758263, ___BGMOJHPMOJE_8)); }
	inline float get_BGMOJHPMOJE_8() const { return ___BGMOJHPMOJE_8; }
	inline float* get_address_of_BGMOJHPMOJE_8() { return &___BGMOJHPMOJE_8; }
	inline void set_BGMOJHPMOJE_8(float value)
	{
		___BGMOJHPMOJE_8 = value;
	}

	inline static int32_t get_offset_of_GGMHCMHPELH_10() { return static_cast<int32_t>(offsetof(Pointer_t1211758263, ___GGMHCMHPELH_10)); }
	inline bool get_GGMHCMHPELH_10() const { return ___GGMHCMHPELH_10; }
	inline bool* get_address_of_GGMHCMHPELH_10() { return &___GGMHCMHPELH_10; }
	inline void set_GGMHCMHPELH_10(bool value)
	{
		___GGMHCMHPELH_10 = value;
	}

	inline static int32_t get_offset_of_LOHGDOHOBOL_11() { return static_cast<int32_t>(offsetof(Pointer_t1211758263, ___LOHGDOHOBOL_11)); }
	inline float get_LOHGDOHOBOL_11() const { return ___LOHGDOHOBOL_11; }
	inline float* get_address_of_LOHGDOHOBOL_11() { return &___LOHGDOHOBOL_11; }
	inline void set_LOHGDOHOBOL_11(float value)
	{
		___LOHGDOHOBOL_11 = value;
	}

	inline static int32_t get_offset_of_BABCKJCECDJ_12() { return static_cast<int32_t>(offsetof(Pointer_t1211758263, ___BABCKJCECDJ_12)); }
	inline int32_t get_BABCKJCECDJ_12() const { return ___BABCKJCECDJ_12; }
	inline int32_t* get_address_of_BABCKJCECDJ_12() { return &___BABCKJCECDJ_12; }
	inline void set_BABCKJCECDJ_12(int32_t value)
	{
		___BABCKJCECDJ_12 = value;
	}
};

struct Pointer_t1211758263_StaticFields
{
public:
	// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::CACHHOCBHAE
	Pointer_t1211758263 * ___CACHHOCBHAE_0;
	// System.String[] proto.PhoneEvent/Types/MotionEvent/Types/Pointer::BLJKKGIJBPB
	StringU5BU5D_t1642385972* ___BLJKKGIJBPB_1;
	// System.UInt32[] proto.PhoneEvent/Types/MotionEvent/Types/Pointer::KJBMGPMKNOF
	UInt32U5BU5D_t59386216* ___KJBMGPMKNOF_2;

public:
	inline static int32_t get_offset_of_CACHHOCBHAE_0() { return static_cast<int32_t>(offsetof(Pointer_t1211758263_StaticFields, ___CACHHOCBHAE_0)); }
	inline Pointer_t1211758263 * get_CACHHOCBHAE_0() const { return ___CACHHOCBHAE_0; }
	inline Pointer_t1211758263 ** get_address_of_CACHHOCBHAE_0() { return &___CACHHOCBHAE_0; }
	inline void set_CACHHOCBHAE_0(Pointer_t1211758263 * value)
	{
		___CACHHOCBHAE_0 = value;
		Il2CppCodeGenWriteBarrier(&___CACHHOCBHAE_0, value);
	}

	inline static int32_t get_offset_of_BLJKKGIJBPB_1() { return static_cast<int32_t>(offsetof(Pointer_t1211758263_StaticFields, ___BLJKKGIJBPB_1)); }
	inline StringU5BU5D_t1642385972* get_BLJKKGIJBPB_1() const { return ___BLJKKGIJBPB_1; }
	inline StringU5BU5D_t1642385972** get_address_of_BLJKKGIJBPB_1() { return &___BLJKKGIJBPB_1; }
	inline void set_BLJKKGIJBPB_1(StringU5BU5D_t1642385972* value)
	{
		___BLJKKGIJBPB_1 = value;
		Il2CppCodeGenWriteBarrier(&___BLJKKGIJBPB_1, value);
	}

	inline static int32_t get_offset_of_KJBMGPMKNOF_2() { return static_cast<int32_t>(offsetof(Pointer_t1211758263_StaticFields, ___KJBMGPMKNOF_2)); }
	inline UInt32U5BU5D_t59386216* get_KJBMGPMKNOF_2() const { return ___KJBMGPMKNOF_2; }
	inline UInt32U5BU5D_t59386216** get_address_of_KJBMGPMKNOF_2() { return &___KJBMGPMKNOF_2; }
	inline void set_KJBMGPMKNOF_2(UInt32U5BU5D_t59386216* value)
	{
		___KJBMGPMKNOF_2 = value;
		Il2CppCodeGenWriteBarrier(&___KJBMGPMKNOF_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
