﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearAbst3719235061.h"

// Vuforia.DigitalEyewearBehaviour
struct DigitalEyewearBehaviour_t495394589;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearBehaviour
struct  DigitalEyewearBehaviour_t495394589  : public DigitalEyewearAbstractBehaviour_t3719235061
{
public:

public:
};

struct DigitalEyewearBehaviour_t495394589_StaticFields
{
public:
	// Vuforia.DigitalEyewearBehaviour Vuforia.DigitalEyewearBehaviour::LAFNKGCHHID
	DigitalEyewearBehaviour_t495394589 * ___LAFNKGCHHID_26;

public:
	inline static int32_t get_offset_of_LAFNKGCHHID_26() { return static_cast<int32_t>(offsetof(DigitalEyewearBehaviour_t495394589_StaticFields, ___LAFNKGCHHID_26)); }
	inline DigitalEyewearBehaviour_t495394589 * get_LAFNKGCHHID_26() const { return ___LAFNKGCHHID_26; }
	inline DigitalEyewearBehaviour_t495394589 ** get_address_of_LAFNKGCHHID_26() { return &___LAFNKGCHHID_26; }
	inline void set_LAFNKGCHHID_26(DigitalEyewearBehaviour_t495394589 * value)
	{
		___LAFNKGCHHID_26 = value;
		Il2CppCodeGenWriteBarrier(&___LAFNKGCHHID_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
