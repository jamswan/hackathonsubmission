﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// GimbalBehavior/MOJBLHLFFBF
struct MOJBLHLFFBF_t3646302815;
// GimbalBehavior/HENLMMPHIIK
struct HENLMMPHIIK_t734504213;
// GimbalBehavior/ADEPLOIANJA
struct ADEPLOIANJA_t795553003;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GimbalBehavior
struct  GimbalBehavior_t2141330552  : public MonoBehaviour_t1158329972
{
public:
	// System.String GimbalBehavior::iosApiKey
	String_t* ___iosApiKey_2;
	// System.String GimbalBehavior::androidApiKey
	String_t* ___androidApiKey_3;
	// System.Boolean GimbalBehavior::autoStartBeaconManager
	bool ___autoStartBeaconManager_4;
	// System.Boolean GimbalBehavior::autoStartPlaceManager
	bool ___autoStartPlaceManager_5;
	// GimbalBehavior/MOJBLHLFFBF GimbalBehavior::BeaconSighted
	MOJBLHLFFBF_t3646302815 * ___BeaconSighted_6;
	// GimbalBehavior/HENLMMPHIIK GimbalBehavior::BeginVisit
	HENLMMPHIIK_t734504213 * ___BeginVisit_7;
	// GimbalBehavior/ADEPLOIANJA GimbalBehavior::EndVisit
	ADEPLOIANJA_t795553003 * ___EndVisit_8;
	// System.Boolean GimbalBehavior::LLEEELJBCBO
	bool ___LLEEELJBCBO_9;
	// System.Boolean GimbalBehavior::MMJBLPHCNBG
	bool ___MMJBLPHCNBG_10;

public:
	inline static int32_t get_offset_of_iosApiKey_2() { return static_cast<int32_t>(offsetof(GimbalBehavior_t2141330552, ___iosApiKey_2)); }
	inline String_t* get_iosApiKey_2() const { return ___iosApiKey_2; }
	inline String_t** get_address_of_iosApiKey_2() { return &___iosApiKey_2; }
	inline void set_iosApiKey_2(String_t* value)
	{
		___iosApiKey_2 = value;
		Il2CppCodeGenWriteBarrier(&___iosApiKey_2, value);
	}

	inline static int32_t get_offset_of_androidApiKey_3() { return static_cast<int32_t>(offsetof(GimbalBehavior_t2141330552, ___androidApiKey_3)); }
	inline String_t* get_androidApiKey_3() const { return ___androidApiKey_3; }
	inline String_t** get_address_of_androidApiKey_3() { return &___androidApiKey_3; }
	inline void set_androidApiKey_3(String_t* value)
	{
		___androidApiKey_3 = value;
		Il2CppCodeGenWriteBarrier(&___androidApiKey_3, value);
	}

	inline static int32_t get_offset_of_autoStartBeaconManager_4() { return static_cast<int32_t>(offsetof(GimbalBehavior_t2141330552, ___autoStartBeaconManager_4)); }
	inline bool get_autoStartBeaconManager_4() const { return ___autoStartBeaconManager_4; }
	inline bool* get_address_of_autoStartBeaconManager_4() { return &___autoStartBeaconManager_4; }
	inline void set_autoStartBeaconManager_4(bool value)
	{
		___autoStartBeaconManager_4 = value;
	}

	inline static int32_t get_offset_of_autoStartPlaceManager_5() { return static_cast<int32_t>(offsetof(GimbalBehavior_t2141330552, ___autoStartPlaceManager_5)); }
	inline bool get_autoStartPlaceManager_5() const { return ___autoStartPlaceManager_5; }
	inline bool* get_address_of_autoStartPlaceManager_5() { return &___autoStartPlaceManager_5; }
	inline void set_autoStartPlaceManager_5(bool value)
	{
		___autoStartPlaceManager_5 = value;
	}

	inline static int32_t get_offset_of_BeaconSighted_6() { return static_cast<int32_t>(offsetof(GimbalBehavior_t2141330552, ___BeaconSighted_6)); }
	inline MOJBLHLFFBF_t3646302815 * get_BeaconSighted_6() const { return ___BeaconSighted_6; }
	inline MOJBLHLFFBF_t3646302815 ** get_address_of_BeaconSighted_6() { return &___BeaconSighted_6; }
	inline void set_BeaconSighted_6(MOJBLHLFFBF_t3646302815 * value)
	{
		___BeaconSighted_6 = value;
		Il2CppCodeGenWriteBarrier(&___BeaconSighted_6, value);
	}

	inline static int32_t get_offset_of_BeginVisit_7() { return static_cast<int32_t>(offsetof(GimbalBehavior_t2141330552, ___BeginVisit_7)); }
	inline HENLMMPHIIK_t734504213 * get_BeginVisit_7() const { return ___BeginVisit_7; }
	inline HENLMMPHIIK_t734504213 ** get_address_of_BeginVisit_7() { return &___BeginVisit_7; }
	inline void set_BeginVisit_7(HENLMMPHIIK_t734504213 * value)
	{
		___BeginVisit_7 = value;
		Il2CppCodeGenWriteBarrier(&___BeginVisit_7, value);
	}

	inline static int32_t get_offset_of_EndVisit_8() { return static_cast<int32_t>(offsetof(GimbalBehavior_t2141330552, ___EndVisit_8)); }
	inline ADEPLOIANJA_t795553003 * get_EndVisit_8() const { return ___EndVisit_8; }
	inline ADEPLOIANJA_t795553003 ** get_address_of_EndVisit_8() { return &___EndVisit_8; }
	inline void set_EndVisit_8(ADEPLOIANJA_t795553003 * value)
	{
		___EndVisit_8 = value;
		Il2CppCodeGenWriteBarrier(&___EndVisit_8, value);
	}

	inline static int32_t get_offset_of_LLEEELJBCBO_9() { return static_cast<int32_t>(offsetof(GimbalBehavior_t2141330552, ___LLEEELJBCBO_9)); }
	inline bool get_LLEEELJBCBO_9() const { return ___LLEEELJBCBO_9; }
	inline bool* get_address_of_LLEEELJBCBO_9() { return &___LLEEELJBCBO_9; }
	inline void set_LLEEELJBCBO_9(bool value)
	{
		___LLEEELJBCBO_9 = value;
	}

	inline static int32_t get_offset_of_MMJBLPHCNBG_10() { return static_cast<int32_t>(offsetof(GimbalBehavior_t2141330552, ___MMJBLPHCNBG_10)); }
	inline bool get_MMJBLPHCNBG_10() const { return ___MMJBLPHCNBG_10; }
	inline bool* get_address_of_MMJBLPHCNBG_10() { return &___MMJBLPHCNBG_10; }
	inline void set_MMJBLPHCNBG_10(bool value)
	{
		___MMJBLPHCNBG_10 = value;
	}
};

struct GimbalBehavior_t2141330552_StaticFields
{
public:
	// GimbalBehavior/MOJBLHLFFBF GimbalBehavior::<>f__am$cache0
	MOJBLHLFFBF_t3646302815 * ___U3CU3Ef__amU24cache0_11;
	// GimbalBehavior/HENLMMPHIIK GimbalBehavior::<>f__am$cache1
	HENLMMPHIIK_t734504213 * ___U3CU3Ef__amU24cache1_12;
	// GimbalBehavior/ADEPLOIANJA GimbalBehavior::<>f__am$cache2
	ADEPLOIANJA_t795553003 * ___U3CU3Ef__amU24cache2_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(GimbalBehavior_t2141330552_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline MOJBLHLFFBF_t3646302815 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline MOJBLHLFFBF_t3646302815 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(MOJBLHLFFBF_t3646302815 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_12() { return static_cast<int32_t>(offsetof(GimbalBehavior_t2141330552_StaticFields, ___U3CU3Ef__amU24cache1_12)); }
	inline HENLMMPHIIK_t734504213 * get_U3CU3Ef__amU24cache1_12() const { return ___U3CU3Ef__amU24cache1_12; }
	inline HENLMMPHIIK_t734504213 ** get_address_of_U3CU3Ef__amU24cache1_12() { return &___U3CU3Ef__amU24cache1_12; }
	inline void set_U3CU3Ef__amU24cache1_12(HENLMMPHIIK_t734504213 * value)
	{
		___U3CU3Ef__amU24cache1_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_13() { return static_cast<int32_t>(offsetof(GimbalBehavior_t2141330552_StaticFields, ___U3CU3Ef__amU24cache2_13)); }
	inline ADEPLOIANJA_t795553003 * get_U3CU3Ef__amU24cache2_13() const { return ___U3CU3Ef__amU24cache2_13; }
	inline ADEPLOIANJA_t795553003 ** get_address_of_U3CU3Ef__amU24cache2_13() { return &___U3CU3Ef__amU24cache2_13; }
	inline void set_U3CU3Ef__amU24cache2_13(ADEPLOIANJA_t795553003 * value)
	{
		___U3CU3Ef__amU24cache2_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
