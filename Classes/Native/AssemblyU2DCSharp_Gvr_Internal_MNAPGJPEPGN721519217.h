﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// Gvr.Internal.MNAPGJPEPGN
struct MNAPGJPEPGN_t721519217;
// GvrProfile
struct GvrProfile_t2070273202;
// ENOAJGBOEPC
struct ENOAJGBOEPC_t2312888837;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.MNAPGJPEPGN
struct  MNAPGJPEPGN_t721519217  : public Il2CppObject
{
public:
	// GvrProfile Gvr.Internal.MNAPGJPEPGN::<KFMPCJJLEKH>k__BackingField
	GvrProfile_t2070273202 * ___U3CKFMPCJJLEKHU3Ek__BackingField_1;
	// ENOAJGBOEPC Gvr.Internal.MNAPGJPEPGN::HNKDFALAEAM
	ENOAJGBOEPC_t2312888837 * ___HNKDFALAEAM_2;
	// ENOAJGBOEPC Gvr.Internal.MNAPGJPEPGN::MDCAIGAFFMH
	ENOAJGBOEPC_t2312888837 * ___MDCAIGAFFMH_3;
	// ENOAJGBOEPC Gvr.Internal.MNAPGJPEPGN::HKAIEACNHEM
	ENOAJGBOEPC_t2312888837 * ___HKAIEACNHEM_4;
	// UnityEngine.Matrix4x4 Gvr.Internal.MNAPGJPEPGN::GENCMNOKAHN
	Matrix4x4_t2933234003  ___GENCMNOKAHN_5;
	// UnityEngine.Matrix4x4 Gvr.Internal.MNAPGJPEPGN::PDECLGJCOFL
	Matrix4x4_t2933234003  ___PDECLGJCOFL_6;
	// UnityEngine.Matrix4x4 Gvr.Internal.MNAPGJPEPGN::IJJBAMPMNHA
	Matrix4x4_t2933234003  ___IJJBAMPMNHA_7;
	// UnityEngine.Matrix4x4 Gvr.Internal.MNAPGJPEPGN::AEPECOPFMNO
	Matrix4x4_t2933234003  ___AEPECOPFMNO_8;
	// UnityEngine.Rect Gvr.Internal.MNAPGJPEPGN::BEONPPLJDGF
	Rect_t3681755626  ___BEONPPLJDGF_9;
	// UnityEngine.Rect Gvr.Internal.MNAPGJPEPGN::EJBOAKLLIJH
	Rect_t3681755626  ___EJBOAKLLIJH_10;
	// UnityEngine.Rect Gvr.Internal.MNAPGJPEPGN::FDLJGEIMBOK
	Rect_t3681755626  ___FDLJGEIMBOK_11;
	// UnityEngine.Rect Gvr.Internal.MNAPGJPEPGN::KJHLIAGKOBJ
	Rect_t3681755626  ___KJHLIAGKOBJ_12;
	// UnityEngine.Vector2 Gvr.Internal.MNAPGJPEPGN::BFNLEKGOEFE
	Vector2_t2243707579  ___BFNLEKGOEFE_13;
	// System.Int32 Gvr.Internal.MNAPGJPEPGN::POBPEGACDHK
	int32_t ___POBPEGACDHK_14;
	// System.Int32 Gvr.Internal.MNAPGJPEPGN::ELDGOGFMNOP
	int32_t ___ELDGOGFMNOP_15;
	// System.Boolean Gvr.Internal.MNAPGJPEPGN::FAPKBLPOPHP
	bool ___FAPKBLPOPHP_16;
	// System.Boolean Gvr.Internal.MNAPGJPEPGN::DFFBPELBEFE
	bool ___DFFBPELBEFE_17;
	// System.Boolean Gvr.Internal.MNAPGJPEPGN::ALGNDFCEIOD
	bool ___ALGNDFCEIOD_18;

public:
	inline static int32_t get_offset_of_U3CKFMPCJJLEKHU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___U3CKFMPCJJLEKHU3Ek__BackingField_1)); }
	inline GvrProfile_t2070273202 * get_U3CKFMPCJJLEKHU3Ek__BackingField_1() const { return ___U3CKFMPCJJLEKHU3Ek__BackingField_1; }
	inline GvrProfile_t2070273202 ** get_address_of_U3CKFMPCJJLEKHU3Ek__BackingField_1() { return &___U3CKFMPCJJLEKHU3Ek__BackingField_1; }
	inline void set_U3CKFMPCJJLEKHU3Ek__BackingField_1(GvrProfile_t2070273202 * value)
	{
		___U3CKFMPCJJLEKHU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CKFMPCJJLEKHU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_HNKDFALAEAM_2() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___HNKDFALAEAM_2)); }
	inline ENOAJGBOEPC_t2312888837 * get_HNKDFALAEAM_2() const { return ___HNKDFALAEAM_2; }
	inline ENOAJGBOEPC_t2312888837 ** get_address_of_HNKDFALAEAM_2() { return &___HNKDFALAEAM_2; }
	inline void set_HNKDFALAEAM_2(ENOAJGBOEPC_t2312888837 * value)
	{
		___HNKDFALAEAM_2 = value;
		Il2CppCodeGenWriteBarrier(&___HNKDFALAEAM_2, value);
	}

	inline static int32_t get_offset_of_MDCAIGAFFMH_3() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___MDCAIGAFFMH_3)); }
	inline ENOAJGBOEPC_t2312888837 * get_MDCAIGAFFMH_3() const { return ___MDCAIGAFFMH_3; }
	inline ENOAJGBOEPC_t2312888837 ** get_address_of_MDCAIGAFFMH_3() { return &___MDCAIGAFFMH_3; }
	inline void set_MDCAIGAFFMH_3(ENOAJGBOEPC_t2312888837 * value)
	{
		___MDCAIGAFFMH_3 = value;
		Il2CppCodeGenWriteBarrier(&___MDCAIGAFFMH_3, value);
	}

	inline static int32_t get_offset_of_HKAIEACNHEM_4() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___HKAIEACNHEM_4)); }
	inline ENOAJGBOEPC_t2312888837 * get_HKAIEACNHEM_4() const { return ___HKAIEACNHEM_4; }
	inline ENOAJGBOEPC_t2312888837 ** get_address_of_HKAIEACNHEM_4() { return &___HKAIEACNHEM_4; }
	inline void set_HKAIEACNHEM_4(ENOAJGBOEPC_t2312888837 * value)
	{
		___HKAIEACNHEM_4 = value;
		Il2CppCodeGenWriteBarrier(&___HKAIEACNHEM_4, value);
	}

	inline static int32_t get_offset_of_GENCMNOKAHN_5() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___GENCMNOKAHN_5)); }
	inline Matrix4x4_t2933234003  get_GENCMNOKAHN_5() const { return ___GENCMNOKAHN_5; }
	inline Matrix4x4_t2933234003 * get_address_of_GENCMNOKAHN_5() { return &___GENCMNOKAHN_5; }
	inline void set_GENCMNOKAHN_5(Matrix4x4_t2933234003  value)
	{
		___GENCMNOKAHN_5 = value;
	}

	inline static int32_t get_offset_of_PDECLGJCOFL_6() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___PDECLGJCOFL_6)); }
	inline Matrix4x4_t2933234003  get_PDECLGJCOFL_6() const { return ___PDECLGJCOFL_6; }
	inline Matrix4x4_t2933234003 * get_address_of_PDECLGJCOFL_6() { return &___PDECLGJCOFL_6; }
	inline void set_PDECLGJCOFL_6(Matrix4x4_t2933234003  value)
	{
		___PDECLGJCOFL_6 = value;
	}

	inline static int32_t get_offset_of_IJJBAMPMNHA_7() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___IJJBAMPMNHA_7)); }
	inline Matrix4x4_t2933234003  get_IJJBAMPMNHA_7() const { return ___IJJBAMPMNHA_7; }
	inline Matrix4x4_t2933234003 * get_address_of_IJJBAMPMNHA_7() { return &___IJJBAMPMNHA_7; }
	inline void set_IJJBAMPMNHA_7(Matrix4x4_t2933234003  value)
	{
		___IJJBAMPMNHA_7 = value;
	}

	inline static int32_t get_offset_of_AEPECOPFMNO_8() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___AEPECOPFMNO_8)); }
	inline Matrix4x4_t2933234003  get_AEPECOPFMNO_8() const { return ___AEPECOPFMNO_8; }
	inline Matrix4x4_t2933234003 * get_address_of_AEPECOPFMNO_8() { return &___AEPECOPFMNO_8; }
	inline void set_AEPECOPFMNO_8(Matrix4x4_t2933234003  value)
	{
		___AEPECOPFMNO_8 = value;
	}

	inline static int32_t get_offset_of_BEONPPLJDGF_9() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___BEONPPLJDGF_9)); }
	inline Rect_t3681755626  get_BEONPPLJDGF_9() const { return ___BEONPPLJDGF_9; }
	inline Rect_t3681755626 * get_address_of_BEONPPLJDGF_9() { return &___BEONPPLJDGF_9; }
	inline void set_BEONPPLJDGF_9(Rect_t3681755626  value)
	{
		___BEONPPLJDGF_9 = value;
	}

	inline static int32_t get_offset_of_EJBOAKLLIJH_10() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___EJBOAKLLIJH_10)); }
	inline Rect_t3681755626  get_EJBOAKLLIJH_10() const { return ___EJBOAKLLIJH_10; }
	inline Rect_t3681755626 * get_address_of_EJBOAKLLIJH_10() { return &___EJBOAKLLIJH_10; }
	inline void set_EJBOAKLLIJH_10(Rect_t3681755626  value)
	{
		___EJBOAKLLIJH_10 = value;
	}

	inline static int32_t get_offset_of_FDLJGEIMBOK_11() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___FDLJGEIMBOK_11)); }
	inline Rect_t3681755626  get_FDLJGEIMBOK_11() const { return ___FDLJGEIMBOK_11; }
	inline Rect_t3681755626 * get_address_of_FDLJGEIMBOK_11() { return &___FDLJGEIMBOK_11; }
	inline void set_FDLJGEIMBOK_11(Rect_t3681755626  value)
	{
		___FDLJGEIMBOK_11 = value;
	}

	inline static int32_t get_offset_of_KJHLIAGKOBJ_12() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___KJHLIAGKOBJ_12)); }
	inline Rect_t3681755626  get_KJHLIAGKOBJ_12() const { return ___KJHLIAGKOBJ_12; }
	inline Rect_t3681755626 * get_address_of_KJHLIAGKOBJ_12() { return &___KJHLIAGKOBJ_12; }
	inline void set_KJHLIAGKOBJ_12(Rect_t3681755626  value)
	{
		___KJHLIAGKOBJ_12 = value;
	}

	inline static int32_t get_offset_of_BFNLEKGOEFE_13() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___BFNLEKGOEFE_13)); }
	inline Vector2_t2243707579  get_BFNLEKGOEFE_13() const { return ___BFNLEKGOEFE_13; }
	inline Vector2_t2243707579 * get_address_of_BFNLEKGOEFE_13() { return &___BFNLEKGOEFE_13; }
	inline void set_BFNLEKGOEFE_13(Vector2_t2243707579  value)
	{
		___BFNLEKGOEFE_13 = value;
	}

	inline static int32_t get_offset_of_POBPEGACDHK_14() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___POBPEGACDHK_14)); }
	inline int32_t get_POBPEGACDHK_14() const { return ___POBPEGACDHK_14; }
	inline int32_t* get_address_of_POBPEGACDHK_14() { return &___POBPEGACDHK_14; }
	inline void set_POBPEGACDHK_14(int32_t value)
	{
		___POBPEGACDHK_14 = value;
	}

	inline static int32_t get_offset_of_ELDGOGFMNOP_15() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___ELDGOGFMNOP_15)); }
	inline int32_t get_ELDGOGFMNOP_15() const { return ___ELDGOGFMNOP_15; }
	inline int32_t* get_address_of_ELDGOGFMNOP_15() { return &___ELDGOGFMNOP_15; }
	inline void set_ELDGOGFMNOP_15(int32_t value)
	{
		___ELDGOGFMNOP_15 = value;
	}

	inline static int32_t get_offset_of_FAPKBLPOPHP_16() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___FAPKBLPOPHP_16)); }
	inline bool get_FAPKBLPOPHP_16() const { return ___FAPKBLPOPHP_16; }
	inline bool* get_address_of_FAPKBLPOPHP_16() { return &___FAPKBLPOPHP_16; }
	inline void set_FAPKBLPOPHP_16(bool value)
	{
		___FAPKBLPOPHP_16 = value;
	}

	inline static int32_t get_offset_of_DFFBPELBEFE_17() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___DFFBPELBEFE_17)); }
	inline bool get_DFFBPELBEFE_17() const { return ___DFFBPELBEFE_17; }
	inline bool* get_address_of_DFFBPELBEFE_17() { return &___DFFBPELBEFE_17; }
	inline void set_DFFBPELBEFE_17(bool value)
	{
		___DFFBPELBEFE_17 = value;
	}

	inline static int32_t get_offset_of_ALGNDFCEIOD_18() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217, ___ALGNDFCEIOD_18)); }
	inline bool get_ALGNDFCEIOD_18() const { return ___ALGNDFCEIOD_18; }
	inline bool* get_address_of_ALGNDFCEIOD_18() { return &___ALGNDFCEIOD_18; }
	inline void set_ALGNDFCEIOD_18(bool value)
	{
		___ALGNDFCEIOD_18 = value;
	}
};

struct MNAPGJPEPGN_t721519217_StaticFields
{
public:
	// Gvr.Internal.MNAPGJPEPGN Gvr.Internal.MNAPGJPEPGN::FCEOCGPNFEC
	MNAPGJPEPGN_t721519217 * ___FCEOCGPNFEC_0;

public:
	inline static int32_t get_offset_of_FCEOCGPNFEC_0() { return static_cast<int32_t>(offsetof(MNAPGJPEPGN_t721519217_StaticFields, ___FCEOCGPNFEC_0)); }
	inline MNAPGJPEPGN_t721519217 * get_FCEOCGPNFEC_0() const { return ___FCEOCGPNFEC_0; }
	inline MNAPGJPEPGN_t721519217 ** get_address_of_FCEOCGPNFEC_0() { return &___FCEOCGPNFEC_0; }
	inline void set_FCEOCGPNFEC_0(MNAPGJPEPGN_t721519217 * value)
	{
		___FCEOCGPNFEC_0 = value;
		Il2CppCodeGenWriteBarrier(&___FCEOCGPNFEC_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
