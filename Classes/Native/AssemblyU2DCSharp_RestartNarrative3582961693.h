﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestartNarrative
struct  RestartNarrative_t3582961693  : public NarrativeEffect_t3336735121
{
public:
	// System.String RestartNarrative::CheckPointName
	String_t* ___CheckPointName_3;

public:
	inline static int32_t get_offset_of_CheckPointName_3() { return static_cast<int32_t>(offsetof(RestartNarrative_t3582961693, ___CheckPointName_3)); }
	inline String_t* get_CheckPointName_3() const { return ___CheckPointName_3; }
	inline String_t** get_address_of_CheckPointName_3() { return &___CheckPointName_3; }
	inline void set_CheckPointName_3(String_t* value)
	{
		___CheckPointName_3 = value;
		Il2CppCodeGenWriteBarrier(&___CheckPointName_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
