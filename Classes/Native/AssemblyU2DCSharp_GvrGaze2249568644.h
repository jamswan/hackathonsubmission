﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// FPFPADIHBIM
struct FPFPADIHBIM_t767626572;
// UnityEngine.Camera
struct Camera_t189460977;
// JKJEPKGBKFI
struct JKJEPKGBKFI_t138753614;
// System.Func`2<FPFPADIHBIM,FPFPADIHBIM>
struct Func_2_t4180843809;
// System.Func`2<FPFPADIHBIM,System.Boolean>
struct Func_2_t2943824659;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrGaze
struct  GvrGaze_t2249568644  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject GvrGaze::pointerObject
	GameObject_t1756533147 * ___pointerObject_2;
	// FPFPADIHBIM GvrGaze::PGDIBLKFBDK
	FPFPADIHBIM_t767626572 * ___PGDIBLKFBDK_3;
	// UnityEngine.Camera GvrGaze::<HIICJNOPLPF>k__BackingField
	Camera_t189460977 * ___U3CHIICJNOPLPFU3Ek__BackingField_4;
	// UnityEngine.LayerMask GvrGaze::mask
	LayerMask_t3188175821  ___mask_5;
	// JKJEPKGBKFI GvrGaze::NDFPLFJJFMD
	Il2CppObject * ___NDFPLFJJFMD_6;
	// UnityEngine.GameObject GvrGaze::IECMNHDFCEG
	GameObject_t1756533147 * ___IECMNHDFCEG_7;
	// UnityEngine.Vector3 GvrGaze::FFPEMKIHNNN
	Vector3_t2243707580  ___FFPEMKIHNNN_8;
	// UnityEngine.Ray GvrGaze::MPJPMMJANBI
	Ray_t2469606224  ___MPJPMMJANBI_9;
	// System.Boolean GvrGaze::FCCNLMFLDJE
	bool ___FCCNLMFLDJE_10;

public:
	inline static int32_t get_offset_of_pointerObject_2() { return static_cast<int32_t>(offsetof(GvrGaze_t2249568644, ___pointerObject_2)); }
	inline GameObject_t1756533147 * get_pointerObject_2() const { return ___pointerObject_2; }
	inline GameObject_t1756533147 ** get_address_of_pointerObject_2() { return &___pointerObject_2; }
	inline void set_pointerObject_2(GameObject_t1756533147 * value)
	{
		___pointerObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___pointerObject_2, value);
	}

	inline static int32_t get_offset_of_PGDIBLKFBDK_3() { return static_cast<int32_t>(offsetof(GvrGaze_t2249568644, ___PGDIBLKFBDK_3)); }
	inline FPFPADIHBIM_t767626572 * get_PGDIBLKFBDK_3() const { return ___PGDIBLKFBDK_3; }
	inline FPFPADIHBIM_t767626572 ** get_address_of_PGDIBLKFBDK_3() { return &___PGDIBLKFBDK_3; }
	inline void set_PGDIBLKFBDK_3(FPFPADIHBIM_t767626572 * value)
	{
		___PGDIBLKFBDK_3 = value;
		Il2CppCodeGenWriteBarrier(&___PGDIBLKFBDK_3, value);
	}

	inline static int32_t get_offset_of_U3CHIICJNOPLPFU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GvrGaze_t2249568644, ___U3CHIICJNOPLPFU3Ek__BackingField_4)); }
	inline Camera_t189460977 * get_U3CHIICJNOPLPFU3Ek__BackingField_4() const { return ___U3CHIICJNOPLPFU3Ek__BackingField_4; }
	inline Camera_t189460977 ** get_address_of_U3CHIICJNOPLPFU3Ek__BackingField_4() { return &___U3CHIICJNOPLPFU3Ek__BackingField_4; }
	inline void set_U3CHIICJNOPLPFU3Ek__BackingField_4(Camera_t189460977 * value)
	{
		___U3CHIICJNOPLPFU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHIICJNOPLPFU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_mask_5() { return static_cast<int32_t>(offsetof(GvrGaze_t2249568644, ___mask_5)); }
	inline LayerMask_t3188175821  get_mask_5() const { return ___mask_5; }
	inline LayerMask_t3188175821 * get_address_of_mask_5() { return &___mask_5; }
	inline void set_mask_5(LayerMask_t3188175821  value)
	{
		___mask_5 = value;
	}

	inline static int32_t get_offset_of_NDFPLFJJFMD_6() { return static_cast<int32_t>(offsetof(GvrGaze_t2249568644, ___NDFPLFJJFMD_6)); }
	inline Il2CppObject * get_NDFPLFJJFMD_6() const { return ___NDFPLFJJFMD_6; }
	inline Il2CppObject ** get_address_of_NDFPLFJJFMD_6() { return &___NDFPLFJJFMD_6; }
	inline void set_NDFPLFJJFMD_6(Il2CppObject * value)
	{
		___NDFPLFJJFMD_6 = value;
		Il2CppCodeGenWriteBarrier(&___NDFPLFJJFMD_6, value);
	}

	inline static int32_t get_offset_of_IECMNHDFCEG_7() { return static_cast<int32_t>(offsetof(GvrGaze_t2249568644, ___IECMNHDFCEG_7)); }
	inline GameObject_t1756533147 * get_IECMNHDFCEG_7() const { return ___IECMNHDFCEG_7; }
	inline GameObject_t1756533147 ** get_address_of_IECMNHDFCEG_7() { return &___IECMNHDFCEG_7; }
	inline void set_IECMNHDFCEG_7(GameObject_t1756533147 * value)
	{
		___IECMNHDFCEG_7 = value;
		Il2CppCodeGenWriteBarrier(&___IECMNHDFCEG_7, value);
	}

	inline static int32_t get_offset_of_FFPEMKIHNNN_8() { return static_cast<int32_t>(offsetof(GvrGaze_t2249568644, ___FFPEMKIHNNN_8)); }
	inline Vector3_t2243707580  get_FFPEMKIHNNN_8() const { return ___FFPEMKIHNNN_8; }
	inline Vector3_t2243707580 * get_address_of_FFPEMKIHNNN_8() { return &___FFPEMKIHNNN_8; }
	inline void set_FFPEMKIHNNN_8(Vector3_t2243707580  value)
	{
		___FFPEMKIHNNN_8 = value;
	}

	inline static int32_t get_offset_of_MPJPMMJANBI_9() { return static_cast<int32_t>(offsetof(GvrGaze_t2249568644, ___MPJPMMJANBI_9)); }
	inline Ray_t2469606224  get_MPJPMMJANBI_9() const { return ___MPJPMMJANBI_9; }
	inline Ray_t2469606224 * get_address_of_MPJPMMJANBI_9() { return &___MPJPMMJANBI_9; }
	inline void set_MPJPMMJANBI_9(Ray_t2469606224  value)
	{
		___MPJPMMJANBI_9 = value;
	}

	inline static int32_t get_offset_of_FCCNLMFLDJE_10() { return static_cast<int32_t>(offsetof(GvrGaze_t2249568644, ___FCCNLMFLDJE_10)); }
	inline bool get_FCCNLMFLDJE_10() const { return ___FCCNLMFLDJE_10; }
	inline bool* get_address_of_FCCNLMFLDJE_10() { return &___FCCNLMFLDJE_10; }
	inline void set_FCCNLMFLDJE_10(bool value)
	{
		___FCCNLMFLDJE_10 = value;
	}
};

struct GvrGaze_t2249568644_StaticFields
{
public:
	// System.Func`2<FPFPADIHBIM,FPFPADIHBIM> GvrGaze::<>f__am$cache0
	Func_2_t4180843809 * ___U3CU3Ef__amU24cache0_11;
	// System.Func`2<FPFPADIHBIM,System.Boolean> GvrGaze::<>f__am$cache1
	Func_2_t2943824659 * ___U3CU3Ef__amU24cache1_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(GvrGaze_t2249568644_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Func_2_t4180843809 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Func_2_t4180843809 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Func_2_t4180843809 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_12() { return static_cast<int32_t>(offsetof(GvrGaze_t2249568644_StaticFields, ___U3CU3Ef__amU24cache1_12)); }
	inline Func_2_t2943824659 * get_U3CU3Ef__amU24cache1_12() const { return ___U3CU3Ef__amU24cache1_12; }
	inline Func_2_t2943824659 ** get_address_of_U3CU3Ef__amU24cache1_12() { return &___U3CU3Ef__amU24cache1_12; }
	inline void set_U3CU3Ef__amU24cache1_12(Func_2_t2943824659 * value)
	{
		___U3CU3Ef__amU24cache1_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
