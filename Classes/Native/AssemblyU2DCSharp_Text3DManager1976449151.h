﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Singleton_1_gen3152333538.h"

// LetterSizes[]
struct LetterSizesU5BU5D_t3365683723;
// System.Collections.Generic.List`1<Text3D>
struct List_1_t3726780064;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Text3DManager
struct  Text3DManager_t1976449151  : public Singleton_1_t3152333538
{
public:
	// LetterSizes[] Text3DManager::Fonts
	LetterSizesU5BU5D_t3365683723* ___Fonts_5;
	// System.Collections.Generic.List`1<Text3D> Text3DManager::pooledLetters
	List_1_t3726780064 * ___pooledLetters_6;
	// UnityEngine.Transform Text3DManager::letterObject
	Transform_t3275118058 * ___letterObject_7;
	// System.Int32 Text3DManager::poolCount
	int32_t ___poolCount_8;

public:
	inline static int32_t get_offset_of_Fonts_5() { return static_cast<int32_t>(offsetof(Text3DManager_t1976449151, ___Fonts_5)); }
	inline LetterSizesU5BU5D_t3365683723* get_Fonts_5() const { return ___Fonts_5; }
	inline LetterSizesU5BU5D_t3365683723** get_address_of_Fonts_5() { return &___Fonts_5; }
	inline void set_Fonts_5(LetterSizesU5BU5D_t3365683723* value)
	{
		___Fonts_5 = value;
		Il2CppCodeGenWriteBarrier(&___Fonts_5, value);
	}

	inline static int32_t get_offset_of_pooledLetters_6() { return static_cast<int32_t>(offsetof(Text3DManager_t1976449151, ___pooledLetters_6)); }
	inline List_1_t3726780064 * get_pooledLetters_6() const { return ___pooledLetters_6; }
	inline List_1_t3726780064 ** get_address_of_pooledLetters_6() { return &___pooledLetters_6; }
	inline void set_pooledLetters_6(List_1_t3726780064 * value)
	{
		___pooledLetters_6 = value;
		Il2CppCodeGenWriteBarrier(&___pooledLetters_6, value);
	}

	inline static int32_t get_offset_of_letterObject_7() { return static_cast<int32_t>(offsetof(Text3DManager_t1976449151, ___letterObject_7)); }
	inline Transform_t3275118058 * get_letterObject_7() const { return ___letterObject_7; }
	inline Transform_t3275118058 ** get_address_of_letterObject_7() { return &___letterObject_7; }
	inline void set_letterObject_7(Transform_t3275118058 * value)
	{
		___letterObject_7 = value;
		Il2CppCodeGenWriteBarrier(&___letterObject_7, value);
	}

	inline static int32_t get_offset_of_poolCount_8() { return static_cast<int32_t>(offsetof(Text3DManager_t1976449151, ___poolCount_8)); }
	inline int32_t get_poolCount_8() const { return ___poolCount_8; }
	inline int32_t* get_address_of_poolCount_8() { return &___poolCount_8; }
	inline void set_poolCount_8(int32_t value)
	{
		___poolCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
