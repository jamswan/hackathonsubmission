﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SimpleJSON_GFGBGCMOLKN3233773149.h"

// System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.GFGBGCMOLKN>
struct Dictionary_2_t853585115;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.HHOPCHIIGDD
struct  HHOPCHIIGDD_t3187511541  : public GFGBGCMOLKN_t3233773149
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.GFGBGCMOLKN> SimpleJSON.HHOPCHIIGDD::GMGKONILBJM
	Dictionary_2_t853585115 * ___GMGKONILBJM_0;

public:
	inline static int32_t get_offset_of_GMGKONILBJM_0() { return static_cast<int32_t>(offsetof(HHOPCHIIGDD_t3187511541, ___GMGKONILBJM_0)); }
	inline Dictionary_2_t853585115 * get_GMGKONILBJM_0() const { return ___GMGKONILBJM_0; }
	inline Dictionary_2_t853585115 ** get_address_of_GMGKONILBJM_0() { return &___GMGKONILBJM_0; }
	inline void set_GMGKONILBJM_0(Dictionary_2_t853585115 * value)
	{
		___GMGKONILBJM_0 = value;
		Il2CppCodeGenWriteBarrier(&___GMGKONILBJM_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
