﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t965510117  : public MonoBehaviour_t1158329972
{
public:
	// System.String Vuforia.DefaultInitializationErrorHandler::DLMMNEGCAKE
	String_t* ___DLMMNEGCAKE_2;
	// System.Boolean Vuforia.DefaultInitializationErrorHandler::MEHPMDDJBKE
	bool ___MEHPMDDJBKE_3;

public:
	inline static int32_t get_offset_of_DLMMNEGCAKE_2() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t965510117, ___DLMMNEGCAKE_2)); }
	inline String_t* get_DLMMNEGCAKE_2() const { return ___DLMMNEGCAKE_2; }
	inline String_t** get_address_of_DLMMNEGCAKE_2() { return &___DLMMNEGCAKE_2; }
	inline void set_DLMMNEGCAKE_2(String_t* value)
	{
		___DLMMNEGCAKE_2 = value;
		Il2CppCodeGenWriteBarrier(&___DLMMNEGCAKE_2, value);
	}

	inline static int32_t get_offset_of_MEHPMDDJBKE_3() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t965510117, ___MEHPMDDJBKE_3)); }
	inline bool get_MEHPMDDJBKE_3() const { return ___MEHPMDDJBKE_3; }
	inline bool* get_address_of_MEHPMDDJBKE_3() { return &___MEHPMDDJBKE_3; }
	inline void set_MEHPMDDJBKE_3(bool value)
	{
		___MEHPMDDJBKE_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
