﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KHBDFKCPFBH
struct  KHBDFKCPFBH_t2524227425  : public Il2CppObject
{
public:
	// System.Int32 KHBDFKCPFBH::CADGGMOGOAD
	int32_t ___CADGGMOGOAD_0;
	// System.String KHBDFKCPFBH::DIHIPDEOAAB
	String_t* ___DIHIPDEOAAB_1;
	// System.String KHBDFKCPFBH::GANGPNMIIIF
	String_t* ___GANGPNMIIIF_2;
	// System.String KHBDFKCPFBH::GAAMIIMINFA
	String_t* ___GAAMIIMINFA_3;
	// System.Int32 KHBDFKCPFBH::OEBIDPPDOAJ
	int32_t ___OEBIDPPDOAJ_4;

public:
	inline static int32_t get_offset_of_CADGGMOGOAD_0() { return static_cast<int32_t>(offsetof(KHBDFKCPFBH_t2524227425, ___CADGGMOGOAD_0)); }
	inline int32_t get_CADGGMOGOAD_0() const { return ___CADGGMOGOAD_0; }
	inline int32_t* get_address_of_CADGGMOGOAD_0() { return &___CADGGMOGOAD_0; }
	inline void set_CADGGMOGOAD_0(int32_t value)
	{
		___CADGGMOGOAD_0 = value;
	}

	inline static int32_t get_offset_of_DIHIPDEOAAB_1() { return static_cast<int32_t>(offsetof(KHBDFKCPFBH_t2524227425, ___DIHIPDEOAAB_1)); }
	inline String_t* get_DIHIPDEOAAB_1() const { return ___DIHIPDEOAAB_1; }
	inline String_t** get_address_of_DIHIPDEOAAB_1() { return &___DIHIPDEOAAB_1; }
	inline void set_DIHIPDEOAAB_1(String_t* value)
	{
		___DIHIPDEOAAB_1 = value;
		Il2CppCodeGenWriteBarrier(&___DIHIPDEOAAB_1, value);
	}

	inline static int32_t get_offset_of_GANGPNMIIIF_2() { return static_cast<int32_t>(offsetof(KHBDFKCPFBH_t2524227425, ___GANGPNMIIIF_2)); }
	inline String_t* get_GANGPNMIIIF_2() const { return ___GANGPNMIIIF_2; }
	inline String_t** get_address_of_GANGPNMIIIF_2() { return &___GANGPNMIIIF_2; }
	inline void set_GANGPNMIIIF_2(String_t* value)
	{
		___GANGPNMIIIF_2 = value;
		Il2CppCodeGenWriteBarrier(&___GANGPNMIIIF_2, value);
	}

	inline static int32_t get_offset_of_GAAMIIMINFA_3() { return static_cast<int32_t>(offsetof(KHBDFKCPFBH_t2524227425, ___GAAMIIMINFA_3)); }
	inline String_t* get_GAAMIIMINFA_3() const { return ___GAAMIIMINFA_3; }
	inline String_t** get_address_of_GAAMIIMINFA_3() { return &___GAAMIIMINFA_3; }
	inline void set_GAAMIIMINFA_3(String_t* value)
	{
		___GAAMIIMINFA_3 = value;
		Il2CppCodeGenWriteBarrier(&___GAAMIIMINFA_3, value);
	}

	inline static int32_t get_offset_of_OEBIDPPDOAJ_4() { return static_cast<int32_t>(offsetof(KHBDFKCPFBH_t2524227425, ___OEBIDPPDOAJ_4)); }
	inline int32_t get_OEBIDPPDOAJ_4() const { return ___OEBIDPPDOAJ_4; }
	inline int32_t* get_address_of_OEBIDPPDOAJ_4() { return &___OEBIDPPDOAJ_4; }
	inline void set_OEBIDPPDOAJ_4(int32_t value)
	{
		___OEBIDPPDOAJ_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
