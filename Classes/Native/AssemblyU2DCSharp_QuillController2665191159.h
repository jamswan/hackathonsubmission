﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Singleton_1_gen3841075546.h"

// UnityEngine.TextMesh[]
struct TextMeshU5BU5D_t979077969;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// UnityEngine.Animator
struct Animator_t69676727;
// System.String
struct String_t;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuillController
struct  QuillController_t2665191159  : public Singleton_1_t3841075546
{
public:
	// UnityEngine.TextMesh[] QuillController::textObjects
	TextMeshU5BU5D_t979077969* ___textObjects_5;
	// UnityEngine.TextMesh QuillController::IIOBJLAHCPP
	TextMesh_t1641806576 * ___IIOBJLAHCPP_6;
	// UnityEngine.Animator QuillController::animator
	Animator_t69676727 * ___animator_7;
	// System.String QuillController::writeAnimationClipName
	String_t* ___writeAnimationClipName_8;
	// System.String QuillController::idleAnimationClipName
	String_t* ___idleAnimationClipName_9;
	// System.String QuillController::idleNoPaperAnimationClipName
	String_t* ___idleNoPaperAnimationClipName_10;
	// System.String QuillController::pointAnimationClipName
	String_t* ___pointAnimationClipName_11;
	// System.String QuillController::introAnimationClipName
	String_t* ___introAnimationClipName_12;
	// System.String QuillController::exitAnimationClipName
	String_t* ___exitAnimationClipName_13;
	// UnityEngine.Coroutine QuillController::LDJMKOICPCO
	Coroutine_t2299508840 * ___LDJMKOICPCO_14;

public:
	inline static int32_t get_offset_of_textObjects_5() { return static_cast<int32_t>(offsetof(QuillController_t2665191159, ___textObjects_5)); }
	inline TextMeshU5BU5D_t979077969* get_textObjects_5() const { return ___textObjects_5; }
	inline TextMeshU5BU5D_t979077969** get_address_of_textObjects_5() { return &___textObjects_5; }
	inline void set_textObjects_5(TextMeshU5BU5D_t979077969* value)
	{
		___textObjects_5 = value;
		Il2CppCodeGenWriteBarrier(&___textObjects_5, value);
	}

	inline static int32_t get_offset_of_IIOBJLAHCPP_6() { return static_cast<int32_t>(offsetof(QuillController_t2665191159, ___IIOBJLAHCPP_6)); }
	inline TextMesh_t1641806576 * get_IIOBJLAHCPP_6() const { return ___IIOBJLAHCPP_6; }
	inline TextMesh_t1641806576 ** get_address_of_IIOBJLAHCPP_6() { return &___IIOBJLAHCPP_6; }
	inline void set_IIOBJLAHCPP_6(TextMesh_t1641806576 * value)
	{
		___IIOBJLAHCPP_6 = value;
		Il2CppCodeGenWriteBarrier(&___IIOBJLAHCPP_6, value);
	}

	inline static int32_t get_offset_of_animator_7() { return static_cast<int32_t>(offsetof(QuillController_t2665191159, ___animator_7)); }
	inline Animator_t69676727 * get_animator_7() const { return ___animator_7; }
	inline Animator_t69676727 ** get_address_of_animator_7() { return &___animator_7; }
	inline void set_animator_7(Animator_t69676727 * value)
	{
		___animator_7 = value;
		Il2CppCodeGenWriteBarrier(&___animator_7, value);
	}

	inline static int32_t get_offset_of_writeAnimationClipName_8() { return static_cast<int32_t>(offsetof(QuillController_t2665191159, ___writeAnimationClipName_8)); }
	inline String_t* get_writeAnimationClipName_8() const { return ___writeAnimationClipName_8; }
	inline String_t** get_address_of_writeAnimationClipName_8() { return &___writeAnimationClipName_8; }
	inline void set_writeAnimationClipName_8(String_t* value)
	{
		___writeAnimationClipName_8 = value;
		Il2CppCodeGenWriteBarrier(&___writeAnimationClipName_8, value);
	}

	inline static int32_t get_offset_of_idleAnimationClipName_9() { return static_cast<int32_t>(offsetof(QuillController_t2665191159, ___idleAnimationClipName_9)); }
	inline String_t* get_idleAnimationClipName_9() const { return ___idleAnimationClipName_9; }
	inline String_t** get_address_of_idleAnimationClipName_9() { return &___idleAnimationClipName_9; }
	inline void set_idleAnimationClipName_9(String_t* value)
	{
		___idleAnimationClipName_9 = value;
		Il2CppCodeGenWriteBarrier(&___idleAnimationClipName_9, value);
	}

	inline static int32_t get_offset_of_idleNoPaperAnimationClipName_10() { return static_cast<int32_t>(offsetof(QuillController_t2665191159, ___idleNoPaperAnimationClipName_10)); }
	inline String_t* get_idleNoPaperAnimationClipName_10() const { return ___idleNoPaperAnimationClipName_10; }
	inline String_t** get_address_of_idleNoPaperAnimationClipName_10() { return &___idleNoPaperAnimationClipName_10; }
	inline void set_idleNoPaperAnimationClipName_10(String_t* value)
	{
		___idleNoPaperAnimationClipName_10 = value;
		Il2CppCodeGenWriteBarrier(&___idleNoPaperAnimationClipName_10, value);
	}

	inline static int32_t get_offset_of_pointAnimationClipName_11() { return static_cast<int32_t>(offsetof(QuillController_t2665191159, ___pointAnimationClipName_11)); }
	inline String_t* get_pointAnimationClipName_11() const { return ___pointAnimationClipName_11; }
	inline String_t** get_address_of_pointAnimationClipName_11() { return &___pointAnimationClipName_11; }
	inline void set_pointAnimationClipName_11(String_t* value)
	{
		___pointAnimationClipName_11 = value;
		Il2CppCodeGenWriteBarrier(&___pointAnimationClipName_11, value);
	}

	inline static int32_t get_offset_of_introAnimationClipName_12() { return static_cast<int32_t>(offsetof(QuillController_t2665191159, ___introAnimationClipName_12)); }
	inline String_t* get_introAnimationClipName_12() const { return ___introAnimationClipName_12; }
	inline String_t** get_address_of_introAnimationClipName_12() { return &___introAnimationClipName_12; }
	inline void set_introAnimationClipName_12(String_t* value)
	{
		___introAnimationClipName_12 = value;
		Il2CppCodeGenWriteBarrier(&___introAnimationClipName_12, value);
	}

	inline static int32_t get_offset_of_exitAnimationClipName_13() { return static_cast<int32_t>(offsetof(QuillController_t2665191159, ___exitAnimationClipName_13)); }
	inline String_t* get_exitAnimationClipName_13() const { return ___exitAnimationClipName_13; }
	inline String_t** get_address_of_exitAnimationClipName_13() { return &___exitAnimationClipName_13; }
	inline void set_exitAnimationClipName_13(String_t* value)
	{
		___exitAnimationClipName_13 = value;
		Il2CppCodeGenWriteBarrier(&___exitAnimationClipName_13, value);
	}

	inline static int32_t get_offset_of_LDJMKOICPCO_14() { return static_cast<int32_t>(offsetof(QuillController_t2665191159, ___LDJMKOICPCO_14)); }
	inline Coroutine_t2299508840 * get_LDJMKOICPCO_14() const { return ___LDJMKOICPCO_14; }
	inline Coroutine_t2299508840 ** get_address_of_LDJMKOICPCO_14() { return &___LDJMKOICPCO_14; }
	inline void set_LDJMKOICPCO_14(Coroutine_t2299508840 * value)
	{
		___LDJMKOICPCO_14 = value;
		Il2CppCodeGenWriteBarrier(&___LDJMKOICPCO_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
