﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.Font
struct Font_t4239498691;
// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LetterSizes
struct  LetterSizes_t1582613310  : public Il2CppObject
{
public:
	// System.String LetterSizes::name
	String_t* ___name_0;
	// UnityEngine.Font LetterSizes::font
	Font_t4239498691 * ___font_1;
	// System.Single[] LetterSizes::widths
	SingleU5BU5D_t577127397* ___widths_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(LetterSizes_t1582613310, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_font_1() { return static_cast<int32_t>(offsetof(LetterSizes_t1582613310, ___font_1)); }
	inline Font_t4239498691 * get_font_1() const { return ___font_1; }
	inline Font_t4239498691 ** get_address_of_font_1() { return &___font_1; }
	inline void set_font_1(Font_t4239498691 * value)
	{
		___font_1 = value;
		Il2CppCodeGenWriteBarrier(&___font_1, value);
	}

	inline static int32_t get_offset_of_widths_2() { return static_cast<int32_t>(offsetof(LetterSizes_t1582613310, ___widths_2)); }
	inline SingleU5BU5D_t577127397* get_widths_2() const { return ___widths_2; }
	inline SingleU5BU5D_t577127397** get_address_of_widths_2() { return &___widths_2; }
	inline void set_widths_2(SingleU5BU5D_t577127397* value)
	{
		___widths_2 = value;
		Il2CppCodeGenWriteBarrier(&___widths_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
