﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuillMessageFinished
struct  QuillMessageFinished_t3889532506  : public NarrativeCondition_t4123859457
{
public:
	// System.Int32 QuillMessageFinished::messageIndex
	int32_t ___messageIndex_13;
	// System.Boolean QuillMessageFinished::MAOFEGMJEJD
	bool ___MAOFEGMJEJD_14;

public:
	inline static int32_t get_offset_of_messageIndex_13() { return static_cast<int32_t>(offsetof(QuillMessageFinished_t3889532506, ___messageIndex_13)); }
	inline int32_t get_messageIndex_13() const { return ___messageIndex_13; }
	inline int32_t* get_address_of_messageIndex_13() { return &___messageIndex_13; }
	inline void set_messageIndex_13(int32_t value)
	{
		___messageIndex_13 = value;
	}

	inline static int32_t get_offset_of_MAOFEGMJEJD_14() { return static_cast<int32_t>(offsetof(QuillMessageFinished_t3889532506, ___MAOFEGMJEJD_14)); }
	inline bool get_MAOFEGMJEJD_14() const { return ___MAOFEGMJEJD_14; }
	inline bool* get_address_of_MAOFEGMJEJD_14() { return &___MAOFEGMJEJD_14; }
	inline void set_MAOFEGMJEJD_14(bool value)
	{
		___MAOFEGMJEJD_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
