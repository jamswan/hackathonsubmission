﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// HKKKOKILPBA[]
struct HKKKOKILPBAU5BU5D_t998425978;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t3035069757;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
struct Dictionary_2_t2833400353;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HKKKOKILPBA
struct  HKKKOKILPBA_t3869624123  : public Il2CppObject
{
public:
	// HKKKOKILPBA[] HKKKOKILPBA::IDKKBIIELLO
	HKKKOKILPBAU5BU5D_t998425978* ___IDKKBIIELLO_0;
	// UnityEngine.MonoBehaviour[] HKKKOKILPBA::DEOJLEIHKLJ
	MonoBehaviourU5BU5D_t3035069757* ___DEOJLEIHKLJ_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> HKKKOKILPBA::DMLIHJIFAAL
	Dictionary_2_t2833400353 * ___DMLIHJIFAAL_2;
	// System.Boolean HKKKOKILPBA::AJDAFFOJBND
	bool ___AJDAFFOJBND_3;
	// UnityEngine.GameObject HKKKOKILPBA::JFDOMEIADAE
	GameObject_t1756533147 * ___JFDOMEIADAE_4;

public:
	inline static int32_t get_offset_of_IDKKBIIELLO_0() { return static_cast<int32_t>(offsetof(HKKKOKILPBA_t3869624123, ___IDKKBIIELLO_0)); }
	inline HKKKOKILPBAU5BU5D_t998425978* get_IDKKBIIELLO_0() const { return ___IDKKBIIELLO_0; }
	inline HKKKOKILPBAU5BU5D_t998425978** get_address_of_IDKKBIIELLO_0() { return &___IDKKBIIELLO_0; }
	inline void set_IDKKBIIELLO_0(HKKKOKILPBAU5BU5D_t998425978* value)
	{
		___IDKKBIIELLO_0 = value;
		Il2CppCodeGenWriteBarrier(&___IDKKBIIELLO_0, value);
	}

	inline static int32_t get_offset_of_DEOJLEIHKLJ_1() { return static_cast<int32_t>(offsetof(HKKKOKILPBA_t3869624123, ___DEOJLEIHKLJ_1)); }
	inline MonoBehaviourU5BU5D_t3035069757* get_DEOJLEIHKLJ_1() const { return ___DEOJLEIHKLJ_1; }
	inline MonoBehaviourU5BU5D_t3035069757** get_address_of_DEOJLEIHKLJ_1() { return &___DEOJLEIHKLJ_1; }
	inline void set_DEOJLEIHKLJ_1(MonoBehaviourU5BU5D_t3035069757* value)
	{
		___DEOJLEIHKLJ_1 = value;
		Il2CppCodeGenWriteBarrier(&___DEOJLEIHKLJ_1, value);
	}

	inline static int32_t get_offset_of_DMLIHJIFAAL_2() { return static_cast<int32_t>(offsetof(HKKKOKILPBA_t3869624123, ___DMLIHJIFAAL_2)); }
	inline Dictionary_2_t2833400353 * get_DMLIHJIFAAL_2() const { return ___DMLIHJIFAAL_2; }
	inline Dictionary_2_t2833400353 ** get_address_of_DMLIHJIFAAL_2() { return &___DMLIHJIFAAL_2; }
	inline void set_DMLIHJIFAAL_2(Dictionary_2_t2833400353 * value)
	{
		___DMLIHJIFAAL_2 = value;
		Il2CppCodeGenWriteBarrier(&___DMLIHJIFAAL_2, value);
	}

	inline static int32_t get_offset_of_AJDAFFOJBND_3() { return static_cast<int32_t>(offsetof(HKKKOKILPBA_t3869624123, ___AJDAFFOJBND_3)); }
	inline bool get_AJDAFFOJBND_3() const { return ___AJDAFFOJBND_3; }
	inline bool* get_address_of_AJDAFFOJBND_3() { return &___AJDAFFOJBND_3; }
	inline void set_AJDAFFOJBND_3(bool value)
	{
		___AJDAFFOJBND_3 = value;
	}

	inline static int32_t get_offset_of_JFDOMEIADAE_4() { return static_cast<int32_t>(offsetof(HKKKOKILPBA_t3869624123, ___JFDOMEIADAE_4)); }
	inline GameObject_t1756533147 * get_JFDOMEIADAE_4() const { return ___JFDOMEIADAE_4; }
	inline GameObject_t1756533147 ** get_address_of_JFDOMEIADAE_4() { return &___JFDOMEIADAE_4; }
	inline void set_JFDOMEIADAE_4(GameObject_t1756533147 * value)
	{
		___JFDOMEIADAE_4 = value;
		Il2CppCodeGenWriteBarrier(&___JFDOMEIADAE_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
