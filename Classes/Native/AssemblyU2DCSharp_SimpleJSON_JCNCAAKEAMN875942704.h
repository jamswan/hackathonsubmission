﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SimpleJSON_GFGBGCMOLKN3233773149.h"

// System.Collections.Generic.List`1<SimpleJSON.GFGBGCMOLKN>
struct List_1_t2602894281;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JCNCAAKEAMN
struct  JCNCAAKEAMN_t875942704  : public GFGBGCMOLKN_t3233773149
{
public:
	// System.Collections.Generic.List`1<SimpleJSON.GFGBGCMOLKN> SimpleJSON.JCNCAAKEAMN::PHPJILLBJEE
	List_1_t2602894281 * ___PHPJILLBJEE_0;

public:
	inline static int32_t get_offset_of_PHPJILLBJEE_0() { return static_cast<int32_t>(offsetof(JCNCAAKEAMN_t875942704, ___PHPJILLBJEE_0)); }
	inline List_1_t2602894281 * get_PHPJILLBJEE_0() const { return ___PHPJILLBJEE_0; }
	inline List_1_t2602894281 ** get_address_of_PHPJILLBJEE_0() { return &___PHPJILLBJEE_0; }
	inline void set_PHPJILLBJEE_0(List_1_t2602894281 * value)
	{
		___PHPJILLBJEE_0 = value;
		Il2CppCodeGenWriteBarrier(&___PHPJILLBJEE_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
