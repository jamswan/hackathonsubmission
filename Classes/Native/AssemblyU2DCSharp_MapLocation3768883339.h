﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapLocation
struct  MapLocation_t3768883339  : public Il2CppObject
{
public:
	// System.String MapLocation::name
	String_t* ___name_0;
	// UnityEngine.GameObject MapLocation::location
	GameObject_t1756533147 * ___location_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(MapLocation_t3768883339, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_location_1() { return static_cast<int32_t>(offsetof(MapLocation_t3768883339, ___location_1)); }
	inline GameObject_t1756533147 * get_location_1() const { return ___location_1; }
	inline GameObject_t1756533147 ** get_address_of_location_1() { return &___location_1; }
	inline void set_location_1(GameObject_t1756533147 * value)
	{
		___location_1 = value;
		Il2CppCodeGenWriteBarrier(&___location_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
