﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Gener663764295.h"

// proto.PhoneEvent/Types/DepthMapEvent
struct DepthMapEvent_t1516604558;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>
struct PopsicleList_1_t271158803;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/DepthMapEvent
struct  DepthMapEvent_t1516604558  : public GeneratedMessageLite_2_t663764295
{
public:
	// System.Boolean proto.PhoneEvent/Types/DepthMapEvent::MIGGKHHBAID
	bool ___MIGGKHHBAID_4;
	// System.Int64 proto.PhoneEvent/Types/DepthMapEvent::HMBIGJIDGNB
	int64_t ___HMBIGJIDGNB_5;
	// System.Boolean proto.PhoneEvent/Types/DepthMapEvent::BACCJGEKFJB
	bool ___BACCJGEKFJB_7;
	// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::AKLIGHJBHJD
	int32_t ___AKLIGHJBHJD_8;
	// System.Boolean proto.PhoneEvent/Types/DepthMapEvent::FACLGENOEIJ
	bool ___FACLGENOEIJ_10;
	// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::KAOMCBNGBBG
	int32_t ___KAOMCBNGBBG_11;
	// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::AJGIEBFLLCM
	int32_t ___AJGIEBFLLCM_13;
	// Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single> proto.PhoneEvent/Types/DepthMapEvent::GBADDIAPLKK
	PopsicleList_1_t271158803 * ___GBADDIAPLKK_14;
	// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::BABCKJCECDJ
	int32_t ___BABCKJCECDJ_15;

public:
	inline static int32_t get_offset_of_MIGGKHHBAID_4() { return static_cast<int32_t>(offsetof(DepthMapEvent_t1516604558, ___MIGGKHHBAID_4)); }
	inline bool get_MIGGKHHBAID_4() const { return ___MIGGKHHBAID_4; }
	inline bool* get_address_of_MIGGKHHBAID_4() { return &___MIGGKHHBAID_4; }
	inline void set_MIGGKHHBAID_4(bool value)
	{
		___MIGGKHHBAID_4 = value;
	}

	inline static int32_t get_offset_of_HMBIGJIDGNB_5() { return static_cast<int32_t>(offsetof(DepthMapEvent_t1516604558, ___HMBIGJIDGNB_5)); }
	inline int64_t get_HMBIGJIDGNB_5() const { return ___HMBIGJIDGNB_5; }
	inline int64_t* get_address_of_HMBIGJIDGNB_5() { return &___HMBIGJIDGNB_5; }
	inline void set_HMBIGJIDGNB_5(int64_t value)
	{
		___HMBIGJIDGNB_5 = value;
	}

	inline static int32_t get_offset_of_BACCJGEKFJB_7() { return static_cast<int32_t>(offsetof(DepthMapEvent_t1516604558, ___BACCJGEKFJB_7)); }
	inline bool get_BACCJGEKFJB_7() const { return ___BACCJGEKFJB_7; }
	inline bool* get_address_of_BACCJGEKFJB_7() { return &___BACCJGEKFJB_7; }
	inline void set_BACCJGEKFJB_7(bool value)
	{
		___BACCJGEKFJB_7 = value;
	}

	inline static int32_t get_offset_of_AKLIGHJBHJD_8() { return static_cast<int32_t>(offsetof(DepthMapEvent_t1516604558, ___AKLIGHJBHJD_8)); }
	inline int32_t get_AKLIGHJBHJD_8() const { return ___AKLIGHJBHJD_8; }
	inline int32_t* get_address_of_AKLIGHJBHJD_8() { return &___AKLIGHJBHJD_8; }
	inline void set_AKLIGHJBHJD_8(int32_t value)
	{
		___AKLIGHJBHJD_8 = value;
	}

	inline static int32_t get_offset_of_FACLGENOEIJ_10() { return static_cast<int32_t>(offsetof(DepthMapEvent_t1516604558, ___FACLGENOEIJ_10)); }
	inline bool get_FACLGENOEIJ_10() const { return ___FACLGENOEIJ_10; }
	inline bool* get_address_of_FACLGENOEIJ_10() { return &___FACLGENOEIJ_10; }
	inline void set_FACLGENOEIJ_10(bool value)
	{
		___FACLGENOEIJ_10 = value;
	}

	inline static int32_t get_offset_of_KAOMCBNGBBG_11() { return static_cast<int32_t>(offsetof(DepthMapEvent_t1516604558, ___KAOMCBNGBBG_11)); }
	inline int32_t get_KAOMCBNGBBG_11() const { return ___KAOMCBNGBBG_11; }
	inline int32_t* get_address_of_KAOMCBNGBBG_11() { return &___KAOMCBNGBBG_11; }
	inline void set_KAOMCBNGBBG_11(int32_t value)
	{
		___KAOMCBNGBBG_11 = value;
	}

	inline static int32_t get_offset_of_AJGIEBFLLCM_13() { return static_cast<int32_t>(offsetof(DepthMapEvent_t1516604558, ___AJGIEBFLLCM_13)); }
	inline int32_t get_AJGIEBFLLCM_13() const { return ___AJGIEBFLLCM_13; }
	inline int32_t* get_address_of_AJGIEBFLLCM_13() { return &___AJGIEBFLLCM_13; }
	inline void set_AJGIEBFLLCM_13(int32_t value)
	{
		___AJGIEBFLLCM_13 = value;
	}

	inline static int32_t get_offset_of_GBADDIAPLKK_14() { return static_cast<int32_t>(offsetof(DepthMapEvent_t1516604558, ___GBADDIAPLKK_14)); }
	inline PopsicleList_1_t271158803 * get_GBADDIAPLKK_14() const { return ___GBADDIAPLKK_14; }
	inline PopsicleList_1_t271158803 ** get_address_of_GBADDIAPLKK_14() { return &___GBADDIAPLKK_14; }
	inline void set_GBADDIAPLKK_14(PopsicleList_1_t271158803 * value)
	{
		___GBADDIAPLKK_14 = value;
		Il2CppCodeGenWriteBarrier(&___GBADDIAPLKK_14, value);
	}

	inline static int32_t get_offset_of_BABCKJCECDJ_15() { return static_cast<int32_t>(offsetof(DepthMapEvent_t1516604558, ___BABCKJCECDJ_15)); }
	inline int32_t get_BABCKJCECDJ_15() const { return ___BABCKJCECDJ_15; }
	inline int32_t* get_address_of_BABCKJCECDJ_15() { return &___BABCKJCECDJ_15; }
	inline void set_BABCKJCECDJ_15(int32_t value)
	{
		___BABCKJCECDJ_15 = value;
	}
};

struct DepthMapEvent_t1516604558_StaticFields
{
public:
	// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent/Types/DepthMapEvent::CACHHOCBHAE
	DepthMapEvent_t1516604558 * ___CACHHOCBHAE_0;
	// System.String[] proto.PhoneEvent/Types/DepthMapEvent::JALKHEGJNPP
	StringU5BU5D_t1642385972* ___JALKHEGJNPP_1;
	// System.UInt32[] proto.PhoneEvent/Types/DepthMapEvent::AKGOEFJJEMA
	UInt32U5BU5D_t59386216* ___AKGOEFJJEMA_2;

public:
	inline static int32_t get_offset_of_CACHHOCBHAE_0() { return static_cast<int32_t>(offsetof(DepthMapEvent_t1516604558_StaticFields, ___CACHHOCBHAE_0)); }
	inline DepthMapEvent_t1516604558 * get_CACHHOCBHAE_0() const { return ___CACHHOCBHAE_0; }
	inline DepthMapEvent_t1516604558 ** get_address_of_CACHHOCBHAE_0() { return &___CACHHOCBHAE_0; }
	inline void set_CACHHOCBHAE_0(DepthMapEvent_t1516604558 * value)
	{
		___CACHHOCBHAE_0 = value;
		Il2CppCodeGenWriteBarrier(&___CACHHOCBHAE_0, value);
	}

	inline static int32_t get_offset_of_JALKHEGJNPP_1() { return static_cast<int32_t>(offsetof(DepthMapEvent_t1516604558_StaticFields, ___JALKHEGJNPP_1)); }
	inline StringU5BU5D_t1642385972* get_JALKHEGJNPP_1() const { return ___JALKHEGJNPP_1; }
	inline StringU5BU5D_t1642385972** get_address_of_JALKHEGJNPP_1() { return &___JALKHEGJNPP_1; }
	inline void set_JALKHEGJNPP_1(StringU5BU5D_t1642385972* value)
	{
		___JALKHEGJNPP_1 = value;
		Il2CppCodeGenWriteBarrier(&___JALKHEGJNPP_1, value);
	}

	inline static int32_t get_offset_of_AKGOEFJJEMA_2() { return static_cast<int32_t>(offsetof(DepthMapEvent_t1516604558_StaticFields, ___AKGOEFJJEMA_2)); }
	inline UInt32U5BU5D_t59386216* get_AKGOEFJJEMA_2() const { return ___AKGOEFJJEMA_2; }
	inline UInt32U5BU5D_t59386216** get_address_of_AKGOEFJJEMA_2() { return &___AKGOEFJJEMA_2; }
	inline void set_AKGOEFJJEMA_2(UInt32U5BU5D_t59386216* value)
	{
		___AKGOEFJJEMA_2 = value;
		Il2CppCodeGenWriteBarrier(&___AKGOEFJJEMA_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
