﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// CameraSettings
struct CameraSettings_t3536359094;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSettings/<RestoreOriginalFocusMode>c__Iterator0
struct  U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758  : public Il2CppObject
{
public:
	// CameraSettings CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::AOOLEAHHMIH
	CameraSettings_t3536359094 * ___AOOLEAHHMIH_0;
	// System.Object CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::LGBFNMECDHC
	Il2CppObject * ___LGBFNMECDHC_1;
	// System.Boolean CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_2;
	// System.Int32 CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_3;

public:
	inline static int32_t get_offset_of_AOOLEAHHMIH_0() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758, ___AOOLEAHHMIH_0)); }
	inline CameraSettings_t3536359094 * get_AOOLEAHHMIH_0() const { return ___AOOLEAHHMIH_0; }
	inline CameraSettings_t3536359094 ** get_address_of_AOOLEAHHMIH_0() { return &___AOOLEAHHMIH_0; }
	inline void set_AOOLEAHHMIH_0(CameraSettings_t3536359094 * value)
	{
		___AOOLEAHHMIH_0 = value;
		Il2CppCodeGenWriteBarrier(&___AOOLEAHHMIH_0, value);
	}

	inline static int32_t get_offset_of_LGBFNMECDHC_1() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758, ___LGBFNMECDHC_1)); }
	inline Il2CppObject * get_LGBFNMECDHC_1() const { return ___LGBFNMECDHC_1; }
	inline Il2CppObject ** get_address_of_LGBFNMECDHC_1() { return &___LGBFNMECDHC_1; }
	inline void set_LGBFNMECDHC_1(Il2CppObject * value)
	{
		___LGBFNMECDHC_1 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_1, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_2() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758, ___IMAGLIMLPFK_2)); }
	inline bool get_IMAGLIMLPFK_2() const { return ___IMAGLIMLPFK_2; }
	inline bool* get_address_of_IMAGLIMLPFK_2() { return &___IMAGLIMLPFK_2; }
	inline void set_IMAGLIMLPFK_2(bool value)
	{
		___IMAGLIMLPFK_2 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_3() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758, ___EPCFNNGDBGC_3)); }
	inline int32_t get_EPCFNNGDBGC_3() const { return ___EPCFNNGDBGC_3; }
	inline int32_t* get_address_of_EPCFNNGDBGC_3() { return &___EPCFNNGDBGC_3; }
	inline void set_EPCFNNGDBGC_3(int32_t value)
	{
		___EPCFNNGDBGC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
