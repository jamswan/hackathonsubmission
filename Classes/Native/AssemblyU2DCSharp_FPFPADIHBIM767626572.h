﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPFPADIHBIM
struct  FPFPADIHBIM_t767626572  : public Il2CppObject
{
public:
	// System.Boolean FPFPADIHBIM::<CDLPGEPJPID>k__BackingField
	bool ___U3CCDLPGEPJPIDU3Ek__BackingField_0;
	// UnityEngine.Transform FPFPADIHBIM::<INNBOBHBEOL>k__BackingField
	Transform_t3275118058 * ___U3CINNBOBHBEOLU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCDLPGEPJPIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FPFPADIHBIM_t767626572, ___U3CCDLPGEPJPIDU3Ek__BackingField_0)); }
	inline bool get_U3CCDLPGEPJPIDU3Ek__BackingField_0() const { return ___U3CCDLPGEPJPIDU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CCDLPGEPJPIDU3Ek__BackingField_0() { return &___U3CCDLPGEPJPIDU3Ek__BackingField_0; }
	inline void set_U3CCDLPGEPJPIDU3Ek__BackingField_0(bool value)
	{
		___U3CCDLPGEPJPIDU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CINNBOBHBEOLU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FPFPADIHBIM_t767626572, ___U3CINNBOBHBEOLU3Ek__BackingField_1)); }
	inline Transform_t3275118058 * get_U3CINNBOBHBEOLU3Ek__BackingField_1() const { return ___U3CINNBOBHBEOLU3Ek__BackingField_1; }
	inline Transform_t3275118058 ** get_address_of_U3CINNBOBHBEOLU3Ek__BackingField_1() { return &___U3CINNBOBHBEOLU3Ek__BackingField_1; }
	inline void set_U3CINNBOBHBEOLU3Ek__BackingField_1(Transform_t3275118058 * value)
	{
		___U3CINNBOBHBEOLU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CINNBOBHBEOLU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
