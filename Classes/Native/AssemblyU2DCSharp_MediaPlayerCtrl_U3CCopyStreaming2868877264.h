﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// MediaPlayerCtrl
struct MediaPlayerCtrl_t1284484152;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1
struct  U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264  : public Il2CppObject
{
public:
	// System.String MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1::MGLLOMDMBGD
	String_t* ___MGLLOMDMBGD_0;
	// System.String MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1::FOLKNEJDGFP
	String_t* ___FOLKNEJDGFP_1;
	// UnityEngine.WWW MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1::ALLKOOONAMJ
	WWW_t2919945039 * ___ALLKOOONAMJ_2;
	// MediaPlayerCtrl MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1::AOOLEAHHMIH
	MediaPlayerCtrl_t1284484152 * ___AOOLEAHHMIH_3;
	// System.Object MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1::LGBFNMECDHC
	Il2CppObject * ___LGBFNMECDHC_4;
	// System.Boolean MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_5;
	// System.Int32 MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_6;

public:
	inline static int32_t get_offset_of_MGLLOMDMBGD_0() { return static_cast<int32_t>(offsetof(U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264, ___MGLLOMDMBGD_0)); }
	inline String_t* get_MGLLOMDMBGD_0() const { return ___MGLLOMDMBGD_0; }
	inline String_t** get_address_of_MGLLOMDMBGD_0() { return &___MGLLOMDMBGD_0; }
	inline void set_MGLLOMDMBGD_0(String_t* value)
	{
		___MGLLOMDMBGD_0 = value;
		Il2CppCodeGenWriteBarrier(&___MGLLOMDMBGD_0, value);
	}

	inline static int32_t get_offset_of_FOLKNEJDGFP_1() { return static_cast<int32_t>(offsetof(U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264, ___FOLKNEJDGFP_1)); }
	inline String_t* get_FOLKNEJDGFP_1() const { return ___FOLKNEJDGFP_1; }
	inline String_t** get_address_of_FOLKNEJDGFP_1() { return &___FOLKNEJDGFP_1; }
	inline void set_FOLKNEJDGFP_1(String_t* value)
	{
		___FOLKNEJDGFP_1 = value;
		Il2CppCodeGenWriteBarrier(&___FOLKNEJDGFP_1, value);
	}

	inline static int32_t get_offset_of_ALLKOOONAMJ_2() { return static_cast<int32_t>(offsetof(U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264, ___ALLKOOONAMJ_2)); }
	inline WWW_t2919945039 * get_ALLKOOONAMJ_2() const { return ___ALLKOOONAMJ_2; }
	inline WWW_t2919945039 ** get_address_of_ALLKOOONAMJ_2() { return &___ALLKOOONAMJ_2; }
	inline void set_ALLKOOONAMJ_2(WWW_t2919945039 * value)
	{
		___ALLKOOONAMJ_2 = value;
		Il2CppCodeGenWriteBarrier(&___ALLKOOONAMJ_2, value);
	}

	inline static int32_t get_offset_of_AOOLEAHHMIH_3() { return static_cast<int32_t>(offsetof(U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264, ___AOOLEAHHMIH_3)); }
	inline MediaPlayerCtrl_t1284484152 * get_AOOLEAHHMIH_3() const { return ___AOOLEAHHMIH_3; }
	inline MediaPlayerCtrl_t1284484152 ** get_address_of_AOOLEAHHMIH_3() { return &___AOOLEAHHMIH_3; }
	inline void set_AOOLEAHHMIH_3(MediaPlayerCtrl_t1284484152 * value)
	{
		___AOOLEAHHMIH_3 = value;
		Il2CppCodeGenWriteBarrier(&___AOOLEAHHMIH_3, value);
	}

	inline static int32_t get_offset_of_LGBFNMECDHC_4() { return static_cast<int32_t>(offsetof(U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264, ___LGBFNMECDHC_4)); }
	inline Il2CppObject * get_LGBFNMECDHC_4() const { return ___LGBFNMECDHC_4; }
	inline Il2CppObject ** get_address_of_LGBFNMECDHC_4() { return &___LGBFNMECDHC_4; }
	inline void set_LGBFNMECDHC_4(Il2CppObject * value)
	{
		___LGBFNMECDHC_4 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_4, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_5() { return static_cast<int32_t>(offsetof(U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264, ___IMAGLIMLPFK_5)); }
	inline bool get_IMAGLIMLPFK_5() const { return ___IMAGLIMLPFK_5; }
	inline bool* get_address_of_IMAGLIMLPFK_5() { return &___IMAGLIMLPFK_5; }
	inline void set_IMAGLIMLPFK_5(bool value)
	{
		___IMAGLIMLPFK_5 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_6() { return static_cast<int32_t>(offsetof(U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264, ___EPCFNNGDBGC_6)); }
	inline int32_t get_EPCFNNGDBGC_6() const { return ___EPCFNNGDBGC_6; }
	inline int32_t* get_address_of_EPCFNNGDBGC_6() { return &___EPCFNNGDBGC_6; }
	inline void set_EPCFNNGDBGC_6(int32_t value)
	{
		___EPCFNNGDBGC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
