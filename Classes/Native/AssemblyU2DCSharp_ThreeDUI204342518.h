﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Singleton_1_gen1380226905.h"

// LetterSizes[]
struct LetterSizesU5BU5D_t3365683723;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThreeDUI
struct  ThreeDUI_t204342518  : public Singleton_1_t1380226905
{
public:
	// LetterSizes[] ThreeDUI::Fonts
	LetterSizesU5BU5D_t3365683723* ___Fonts_5;

public:
	inline static int32_t get_offset_of_Fonts_5() { return static_cast<int32_t>(offsetof(ThreeDUI_t204342518, ___Fonts_5)); }
	inline LetterSizesU5BU5D_t3365683723* get_Fonts_5() const { return ___Fonts_5; }
	inline LetterSizesU5BU5D_t3365683723** get_address_of_Fonts_5() { return &___Fonts_5; }
	inline void set_Fonts_5(LetterSizesU5BU5D_t3365683723* value)
	{
		___Fonts_5 = value;
		Il2CppCodeGenWriteBarrier(&___Fonts_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
