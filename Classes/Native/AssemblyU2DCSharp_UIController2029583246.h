﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Singleton_1_gen3205467633.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.GUISkin
struct GUISkin_t1436893342;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,APEPFIBKILA>
struct Dictionary_2_t1756423532;
// APEPFIBKILA
struct APEPFIBKILA_t4136611566;
// System.Collections.Generic.List`1<GBMIMKMMFPO>
struct List_1_t4265599188;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<EMKDNKNNALF>
struct List_1_t53850203;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController
struct  UIController_t2029583246  : public Singleton_1_t3205467633
{
public:
	// UnityEngine.GUISkin UIController::mainSkin
	GUISkin_t1436893342 * ___mainSkin_5;
	// UnityEngine.GUIStyle UIController::HKOJCMJPDNN
	GUIStyle_t1799908754 * ___HKOJCMJPDNN_6;
	// System.String UIController::MapButtonStyleName
	String_t* ___MapButtonStyleName_7;
	// UnityEngine.GUIStyle UIController::PJJNMAKANJM
	GUIStyle_t1799908754 * ___PJJNMAKANJM_8;
	// System.String UIController::LogButtonStyleName
	String_t* ___LogButtonStyleName_9;
	// UnityEngine.GUIStyle UIController::FMENMEOCJNN
	GUIStyle_t1799908754 * ___FMENMEOCJNN_10;
	// System.String UIController::ReturnStyleName
	String_t* ___ReturnStyleName_11;
	// UnityEngine.GUIStyle UIController::AJPMLMBBNCJ
	GUIStyle_t1799908754 * ___AJPMLMBBNCJ_12;
	// System.String UIController::DownStyleName
	String_t* ___DownStyleName_13;
	// System.Collections.Generic.Dictionary`2<System.String,APEPFIBKILA> UIController::CCIMIICKAKN
	Dictionary_2_t1756423532 * ___CCIMIICKAKN_14;
	// APEPFIBKILA UIController::scrollLog
	APEPFIBKILA_t4136611566 * ___scrollLog_15;
	// System.String UIController::currentScrollLog
	String_t* ___currentScrollLog_16;
	// System.Collections.Generic.List`1<GBMIMKMMFPO> UIController::BBCJADHMNPG
	List_1_t4265599188 * ___BBCJADHMNPG_17;
	// System.String UIController::globalLogItemStyleName
	String_t* ___globalLogItemStyleName_18;
	// UnityEngine.GameObject UIController::map
	GameObject_t1756533147 * ___map_19;
	// System.Single UIController::MaxButtonInchSize
	float ___MaxButtonInchSize_20;
	// System.Single UIController::ButtonScreenPercentSize
	float ___ButtonScreenPercentSize_21;
	// System.Single UIController::bufferInchSize
	float ___bufferInchSize_22;
	// System.Int32 UIController::ALGOAFLCJLH
	int32_t ___ALGOAFLCJLH_23;
	// System.Int32 UIController::ADOLICJBCNN
	int32_t ___ADOLICJBCNN_24;
	// System.Collections.Generic.List`1<EMKDNKNNALF> UIController::CFLJPJJAIOA
	List_1_t53850203 * ___CFLJPJJAIOA_25;
	// System.Single UIController::popupWidth
	float ___popupWidth_26;
	// System.Single UIController::popupHeight
	float ___popupHeight_27;
	// System.Boolean UIController::HFIHIMBLAFN
	bool ___HFIHIMBLAFN_28;
	// System.Single UIController::LogHeight
	float ___LogHeight_29;
	// UnityEngine.Vector2 UIController::LEFAFPBDGIB
	Vector2_t2243707579  ___LEFAFPBDGIB_30;
	// System.Boolean UIController::MapFound
	bool ___MapFound_31;
	// System.Boolean UIController::NEMPOOIIENI
	bool ___NEMPOOIIENI_32;
	// System.Boolean UIController::showLog
	bool ___showLog_33;
	// System.Boolean UIController::debugLogVisible
	bool ___debugLogVisible_34;
	// System.String UIController::HNMMOMKIPLL
	String_t* ___HNMMOMKIPLL_35;
	// System.Boolean UIController::LLMMHPGBGJJ
	bool ___LLMMHPGBGJJ_36;
	// System.Boolean UIController::hasMap
	bool ___hasMap_37;
	// System.Boolean UIController::mapview
	bool ___mapview_38;

public:
	inline static int32_t get_offset_of_mainSkin_5() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___mainSkin_5)); }
	inline GUISkin_t1436893342 * get_mainSkin_5() const { return ___mainSkin_5; }
	inline GUISkin_t1436893342 ** get_address_of_mainSkin_5() { return &___mainSkin_5; }
	inline void set_mainSkin_5(GUISkin_t1436893342 * value)
	{
		___mainSkin_5 = value;
		Il2CppCodeGenWriteBarrier(&___mainSkin_5, value);
	}

	inline static int32_t get_offset_of_HKOJCMJPDNN_6() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___HKOJCMJPDNN_6)); }
	inline GUIStyle_t1799908754 * get_HKOJCMJPDNN_6() const { return ___HKOJCMJPDNN_6; }
	inline GUIStyle_t1799908754 ** get_address_of_HKOJCMJPDNN_6() { return &___HKOJCMJPDNN_6; }
	inline void set_HKOJCMJPDNN_6(GUIStyle_t1799908754 * value)
	{
		___HKOJCMJPDNN_6 = value;
		Il2CppCodeGenWriteBarrier(&___HKOJCMJPDNN_6, value);
	}

	inline static int32_t get_offset_of_MapButtonStyleName_7() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___MapButtonStyleName_7)); }
	inline String_t* get_MapButtonStyleName_7() const { return ___MapButtonStyleName_7; }
	inline String_t** get_address_of_MapButtonStyleName_7() { return &___MapButtonStyleName_7; }
	inline void set_MapButtonStyleName_7(String_t* value)
	{
		___MapButtonStyleName_7 = value;
		Il2CppCodeGenWriteBarrier(&___MapButtonStyleName_7, value);
	}

	inline static int32_t get_offset_of_PJJNMAKANJM_8() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___PJJNMAKANJM_8)); }
	inline GUIStyle_t1799908754 * get_PJJNMAKANJM_8() const { return ___PJJNMAKANJM_8; }
	inline GUIStyle_t1799908754 ** get_address_of_PJJNMAKANJM_8() { return &___PJJNMAKANJM_8; }
	inline void set_PJJNMAKANJM_8(GUIStyle_t1799908754 * value)
	{
		___PJJNMAKANJM_8 = value;
		Il2CppCodeGenWriteBarrier(&___PJJNMAKANJM_8, value);
	}

	inline static int32_t get_offset_of_LogButtonStyleName_9() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___LogButtonStyleName_9)); }
	inline String_t* get_LogButtonStyleName_9() const { return ___LogButtonStyleName_9; }
	inline String_t** get_address_of_LogButtonStyleName_9() { return &___LogButtonStyleName_9; }
	inline void set_LogButtonStyleName_9(String_t* value)
	{
		___LogButtonStyleName_9 = value;
		Il2CppCodeGenWriteBarrier(&___LogButtonStyleName_9, value);
	}

	inline static int32_t get_offset_of_FMENMEOCJNN_10() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___FMENMEOCJNN_10)); }
	inline GUIStyle_t1799908754 * get_FMENMEOCJNN_10() const { return ___FMENMEOCJNN_10; }
	inline GUIStyle_t1799908754 ** get_address_of_FMENMEOCJNN_10() { return &___FMENMEOCJNN_10; }
	inline void set_FMENMEOCJNN_10(GUIStyle_t1799908754 * value)
	{
		___FMENMEOCJNN_10 = value;
		Il2CppCodeGenWriteBarrier(&___FMENMEOCJNN_10, value);
	}

	inline static int32_t get_offset_of_ReturnStyleName_11() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___ReturnStyleName_11)); }
	inline String_t* get_ReturnStyleName_11() const { return ___ReturnStyleName_11; }
	inline String_t** get_address_of_ReturnStyleName_11() { return &___ReturnStyleName_11; }
	inline void set_ReturnStyleName_11(String_t* value)
	{
		___ReturnStyleName_11 = value;
		Il2CppCodeGenWriteBarrier(&___ReturnStyleName_11, value);
	}

	inline static int32_t get_offset_of_AJPMLMBBNCJ_12() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___AJPMLMBBNCJ_12)); }
	inline GUIStyle_t1799908754 * get_AJPMLMBBNCJ_12() const { return ___AJPMLMBBNCJ_12; }
	inline GUIStyle_t1799908754 ** get_address_of_AJPMLMBBNCJ_12() { return &___AJPMLMBBNCJ_12; }
	inline void set_AJPMLMBBNCJ_12(GUIStyle_t1799908754 * value)
	{
		___AJPMLMBBNCJ_12 = value;
		Il2CppCodeGenWriteBarrier(&___AJPMLMBBNCJ_12, value);
	}

	inline static int32_t get_offset_of_DownStyleName_13() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___DownStyleName_13)); }
	inline String_t* get_DownStyleName_13() const { return ___DownStyleName_13; }
	inline String_t** get_address_of_DownStyleName_13() { return &___DownStyleName_13; }
	inline void set_DownStyleName_13(String_t* value)
	{
		___DownStyleName_13 = value;
		Il2CppCodeGenWriteBarrier(&___DownStyleName_13, value);
	}

	inline static int32_t get_offset_of_CCIMIICKAKN_14() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___CCIMIICKAKN_14)); }
	inline Dictionary_2_t1756423532 * get_CCIMIICKAKN_14() const { return ___CCIMIICKAKN_14; }
	inline Dictionary_2_t1756423532 ** get_address_of_CCIMIICKAKN_14() { return &___CCIMIICKAKN_14; }
	inline void set_CCIMIICKAKN_14(Dictionary_2_t1756423532 * value)
	{
		___CCIMIICKAKN_14 = value;
		Il2CppCodeGenWriteBarrier(&___CCIMIICKAKN_14, value);
	}

	inline static int32_t get_offset_of_scrollLog_15() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___scrollLog_15)); }
	inline APEPFIBKILA_t4136611566 * get_scrollLog_15() const { return ___scrollLog_15; }
	inline APEPFIBKILA_t4136611566 ** get_address_of_scrollLog_15() { return &___scrollLog_15; }
	inline void set_scrollLog_15(APEPFIBKILA_t4136611566 * value)
	{
		___scrollLog_15 = value;
		Il2CppCodeGenWriteBarrier(&___scrollLog_15, value);
	}

	inline static int32_t get_offset_of_currentScrollLog_16() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___currentScrollLog_16)); }
	inline String_t* get_currentScrollLog_16() const { return ___currentScrollLog_16; }
	inline String_t** get_address_of_currentScrollLog_16() { return &___currentScrollLog_16; }
	inline void set_currentScrollLog_16(String_t* value)
	{
		___currentScrollLog_16 = value;
		Il2CppCodeGenWriteBarrier(&___currentScrollLog_16, value);
	}

	inline static int32_t get_offset_of_BBCJADHMNPG_17() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___BBCJADHMNPG_17)); }
	inline List_1_t4265599188 * get_BBCJADHMNPG_17() const { return ___BBCJADHMNPG_17; }
	inline List_1_t4265599188 ** get_address_of_BBCJADHMNPG_17() { return &___BBCJADHMNPG_17; }
	inline void set_BBCJADHMNPG_17(List_1_t4265599188 * value)
	{
		___BBCJADHMNPG_17 = value;
		Il2CppCodeGenWriteBarrier(&___BBCJADHMNPG_17, value);
	}

	inline static int32_t get_offset_of_globalLogItemStyleName_18() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___globalLogItemStyleName_18)); }
	inline String_t* get_globalLogItemStyleName_18() const { return ___globalLogItemStyleName_18; }
	inline String_t** get_address_of_globalLogItemStyleName_18() { return &___globalLogItemStyleName_18; }
	inline void set_globalLogItemStyleName_18(String_t* value)
	{
		___globalLogItemStyleName_18 = value;
		Il2CppCodeGenWriteBarrier(&___globalLogItemStyleName_18, value);
	}

	inline static int32_t get_offset_of_map_19() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___map_19)); }
	inline GameObject_t1756533147 * get_map_19() const { return ___map_19; }
	inline GameObject_t1756533147 ** get_address_of_map_19() { return &___map_19; }
	inline void set_map_19(GameObject_t1756533147 * value)
	{
		___map_19 = value;
		Il2CppCodeGenWriteBarrier(&___map_19, value);
	}

	inline static int32_t get_offset_of_MaxButtonInchSize_20() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___MaxButtonInchSize_20)); }
	inline float get_MaxButtonInchSize_20() const { return ___MaxButtonInchSize_20; }
	inline float* get_address_of_MaxButtonInchSize_20() { return &___MaxButtonInchSize_20; }
	inline void set_MaxButtonInchSize_20(float value)
	{
		___MaxButtonInchSize_20 = value;
	}

	inline static int32_t get_offset_of_ButtonScreenPercentSize_21() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___ButtonScreenPercentSize_21)); }
	inline float get_ButtonScreenPercentSize_21() const { return ___ButtonScreenPercentSize_21; }
	inline float* get_address_of_ButtonScreenPercentSize_21() { return &___ButtonScreenPercentSize_21; }
	inline void set_ButtonScreenPercentSize_21(float value)
	{
		___ButtonScreenPercentSize_21 = value;
	}

	inline static int32_t get_offset_of_bufferInchSize_22() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___bufferInchSize_22)); }
	inline float get_bufferInchSize_22() const { return ___bufferInchSize_22; }
	inline float* get_address_of_bufferInchSize_22() { return &___bufferInchSize_22; }
	inline void set_bufferInchSize_22(float value)
	{
		___bufferInchSize_22 = value;
	}

	inline static int32_t get_offset_of_ALGOAFLCJLH_23() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___ALGOAFLCJLH_23)); }
	inline int32_t get_ALGOAFLCJLH_23() const { return ___ALGOAFLCJLH_23; }
	inline int32_t* get_address_of_ALGOAFLCJLH_23() { return &___ALGOAFLCJLH_23; }
	inline void set_ALGOAFLCJLH_23(int32_t value)
	{
		___ALGOAFLCJLH_23 = value;
	}

	inline static int32_t get_offset_of_ADOLICJBCNN_24() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___ADOLICJBCNN_24)); }
	inline int32_t get_ADOLICJBCNN_24() const { return ___ADOLICJBCNN_24; }
	inline int32_t* get_address_of_ADOLICJBCNN_24() { return &___ADOLICJBCNN_24; }
	inline void set_ADOLICJBCNN_24(int32_t value)
	{
		___ADOLICJBCNN_24 = value;
	}

	inline static int32_t get_offset_of_CFLJPJJAIOA_25() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___CFLJPJJAIOA_25)); }
	inline List_1_t53850203 * get_CFLJPJJAIOA_25() const { return ___CFLJPJJAIOA_25; }
	inline List_1_t53850203 ** get_address_of_CFLJPJJAIOA_25() { return &___CFLJPJJAIOA_25; }
	inline void set_CFLJPJJAIOA_25(List_1_t53850203 * value)
	{
		___CFLJPJJAIOA_25 = value;
		Il2CppCodeGenWriteBarrier(&___CFLJPJJAIOA_25, value);
	}

	inline static int32_t get_offset_of_popupWidth_26() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___popupWidth_26)); }
	inline float get_popupWidth_26() const { return ___popupWidth_26; }
	inline float* get_address_of_popupWidth_26() { return &___popupWidth_26; }
	inline void set_popupWidth_26(float value)
	{
		___popupWidth_26 = value;
	}

	inline static int32_t get_offset_of_popupHeight_27() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___popupHeight_27)); }
	inline float get_popupHeight_27() const { return ___popupHeight_27; }
	inline float* get_address_of_popupHeight_27() { return &___popupHeight_27; }
	inline void set_popupHeight_27(float value)
	{
		___popupHeight_27 = value;
	}

	inline static int32_t get_offset_of_HFIHIMBLAFN_28() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___HFIHIMBLAFN_28)); }
	inline bool get_HFIHIMBLAFN_28() const { return ___HFIHIMBLAFN_28; }
	inline bool* get_address_of_HFIHIMBLAFN_28() { return &___HFIHIMBLAFN_28; }
	inline void set_HFIHIMBLAFN_28(bool value)
	{
		___HFIHIMBLAFN_28 = value;
	}

	inline static int32_t get_offset_of_LogHeight_29() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___LogHeight_29)); }
	inline float get_LogHeight_29() const { return ___LogHeight_29; }
	inline float* get_address_of_LogHeight_29() { return &___LogHeight_29; }
	inline void set_LogHeight_29(float value)
	{
		___LogHeight_29 = value;
	}

	inline static int32_t get_offset_of_LEFAFPBDGIB_30() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___LEFAFPBDGIB_30)); }
	inline Vector2_t2243707579  get_LEFAFPBDGIB_30() const { return ___LEFAFPBDGIB_30; }
	inline Vector2_t2243707579 * get_address_of_LEFAFPBDGIB_30() { return &___LEFAFPBDGIB_30; }
	inline void set_LEFAFPBDGIB_30(Vector2_t2243707579  value)
	{
		___LEFAFPBDGIB_30 = value;
	}

	inline static int32_t get_offset_of_MapFound_31() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___MapFound_31)); }
	inline bool get_MapFound_31() const { return ___MapFound_31; }
	inline bool* get_address_of_MapFound_31() { return &___MapFound_31; }
	inline void set_MapFound_31(bool value)
	{
		___MapFound_31 = value;
	}

	inline static int32_t get_offset_of_NEMPOOIIENI_32() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___NEMPOOIIENI_32)); }
	inline bool get_NEMPOOIIENI_32() const { return ___NEMPOOIIENI_32; }
	inline bool* get_address_of_NEMPOOIIENI_32() { return &___NEMPOOIIENI_32; }
	inline void set_NEMPOOIIENI_32(bool value)
	{
		___NEMPOOIIENI_32 = value;
	}

	inline static int32_t get_offset_of_showLog_33() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___showLog_33)); }
	inline bool get_showLog_33() const { return ___showLog_33; }
	inline bool* get_address_of_showLog_33() { return &___showLog_33; }
	inline void set_showLog_33(bool value)
	{
		___showLog_33 = value;
	}

	inline static int32_t get_offset_of_debugLogVisible_34() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___debugLogVisible_34)); }
	inline bool get_debugLogVisible_34() const { return ___debugLogVisible_34; }
	inline bool* get_address_of_debugLogVisible_34() { return &___debugLogVisible_34; }
	inline void set_debugLogVisible_34(bool value)
	{
		___debugLogVisible_34 = value;
	}

	inline static int32_t get_offset_of_HNMMOMKIPLL_35() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___HNMMOMKIPLL_35)); }
	inline String_t* get_HNMMOMKIPLL_35() const { return ___HNMMOMKIPLL_35; }
	inline String_t** get_address_of_HNMMOMKIPLL_35() { return &___HNMMOMKIPLL_35; }
	inline void set_HNMMOMKIPLL_35(String_t* value)
	{
		___HNMMOMKIPLL_35 = value;
		Il2CppCodeGenWriteBarrier(&___HNMMOMKIPLL_35, value);
	}

	inline static int32_t get_offset_of_LLMMHPGBGJJ_36() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___LLMMHPGBGJJ_36)); }
	inline bool get_LLMMHPGBGJJ_36() const { return ___LLMMHPGBGJJ_36; }
	inline bool* get_address_of_LLMMHPGBGJJ_36() { return &___LLMMHPGBGJJ_36; }
	inline void set_LLMMHPGBGJJ_36(bool value)
	{
		___LLMMHPGBGJJ_36 = value;
	}

	inline static int32_t get_offset_of_hasMap_37() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___hasMap_37)); }
	inline bool get_hasMap_37() const { return ___hasMap_37; }
	inline bool* get_address_of_hasMap_37() { return &___hasMap_37; }
	inline void set_hasMap_37(bool value)
	{
		___hasMap_37 = value;
	}

	inline static int32_t get_offset_of_mapview_38() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___mapview_38)); }
	inline bool get_mapview_38() const { return ___mapview_38; }
	inline bool* get_address_of_mapview_38() { return &___mapview_38; }
	inline void set_mapview_38(bool value)
	{
		___mapview_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
