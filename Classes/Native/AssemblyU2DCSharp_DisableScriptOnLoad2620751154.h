﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisableScriptOnLoad
struct  DisableScriptOnLoad_t2620751154  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.MonoBehaviour DisableScriptOnLoad::mono
	MonoBehaviour_t1158329972 * ___mono_2;
	// System.String DisableScriptOnLoad::SceneName
	String_t* ___SceneName_3;

public:
	inline static int32_t get_offset_of_mono_2() { return static_cast<int32_t>(offsetof(DisableScriptOnLoad_t2620751154, ___mono_2)); }
	inline MonoBehaviour_t1158329972 * get_mono_2() const { return ___mono_2; }
	inline MonoBehaviour_t1158329972 ** get_address_of_mono_2() { return &___mono_2; }
	inline void set_mono_2(MonoBehaviour_t1158329972 * value)
	{
		___mono_2 = value;
		Il2CppCodeGenWriteBarrier(&___mono_2, value);
	}

	inline static int32_t get_offset_of_SceneName_3() { return static_cast<int32_t>(offsetof(DisableScriptOnLoad_t2620751154, ___SceneName_3)); }
	inline String_t* get_SceneName_3() const { return ___SceneName_3; }
	inline String_t** get_address_of_SceneName_3() { return &___SceneName_3; }
	inline void set_SceneName_3(String_t* value)
	{
		___SceneName_3 = value;
		Il2CppCodeGenWriteBarrier(&___SceneName_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
