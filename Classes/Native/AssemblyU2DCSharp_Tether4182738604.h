﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Augmentation
struct Augmentation_t530534012;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tether
struct  Tether_t4182738604  : public MonoBehaviour_t1158329972
{
public:
	// Augmentation Tether::augmentation
	Augmentation_t530534012 * ___augmentation_2;
	// System.Int32 Tether::setPriority
	int32_t ___setPriority_3;
	// System.Boolean Tether::active
	bool ___active_4;
	// System.String Tether::targetName
	String_t* ___targetName_5;

public:
	inline static int32_t get_offset_of_augmentation_2() { return static_cast<int32_t>(offsetof(Tether_t4182738604, ___augmentation_2)); }
	inline Augmentation_t530534012 * get_augmentation_2() const { return ___augmentation_2; }
	inline Augmentation_t530534012 ** get_address_of_augmentation_2() { return &___augmentation_2; }
	inline void set_augmentation_2(Augmentation_t530534012 * value)
	{
		___augmentation_2 = value;
		Il2CppCodeGenWriteBarrier(&___augmentation_2, value);
	}

	inline static int32_t get_offset_of_setPriority_3() { return static_cast<int32_t>(offsetof(Tether_t4182738604, ___setPriority_3)); }
	inline int32_t get_setPriority_3() const { return ___setPriority_3; }
	inline int32_t* get_address_of_setPriority_3() { return &___setPriority_3; }
	inline void set_setPriority_3(int32_t value)
	{
		___setPriority_3 = value;
	}

	inline static int32_t get_offset_of_active_4() { return static_cast<int32_t>(offsetof(Tether_t4182738604, ___active_4)); }
	inline bool get_active_4() const { return ___active_4; }
	inline bool* get_address_of_active_4() { return &___active_4; }
	inline void set_active_4(bool value)
	{
		___active_4 = value;
	}

	inline static int32_t get_offset_of_targetName_5() { return static_cast<int32_t>(offsetof(Tether_t4182738604, ___targetName_5)); }
	inline String_t* get_targetName_5() const { return ___targetName_5; }
	inline String_t** get_address_of_targetName_5() { return &___targetName_5; }
	inline void set_targetName_5(String_t* value)
	{
		___targetName_5 = value;
		Il2CppCodeGenWriteBarrier(&___targetName_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
