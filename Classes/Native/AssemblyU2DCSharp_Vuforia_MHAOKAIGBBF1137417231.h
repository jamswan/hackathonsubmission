﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_ScreenOrientation4019489636.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MHAOKAIGBBF
struct  MHAOKAIGBBF_t1137417231  : public Il2CppObject
{
public:
	// UnityEngine.ScreenOrientation Vuforia.MHAOKAIGBBF::KJNPPDBFOMG
	int32_t ___KJNPPDBFOMG_0;

public:
	inline static int32_t get_offset_of_KJNPPDBFOMG_0() { return static_cast<int32_t>(offsetof(MHAOKAIGBBF_t1137417231, ___KJNPPDBFOMG_0)); }
	inline int32_t get_KJNPPDBFOMG_0() const { return ___KJNPPDBFOMG_0; }
	inline int32_t* get_address_of_KJNPPDBFOMG_0() { return &___KJNPPDBFOMG_0; }
	inline void set_KJNPPDBFOMG_0(int32_t value)
	{
		___KJNPPDBFOMG_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
