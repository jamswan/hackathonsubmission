﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"

// Tether
struct Tether_t4182738604;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AugmentationFocusedOnTarget
struct  AugmentationFocusedOnTarget_t1552339247  : public NarrativeCondition_t4123859457
{
public:
	// Tether AugmentationFocusedOnTarget::tether
	Tether_t4182738604 * ___tether_13;

public:
	inline static int32_t get_offset_of_tether_13() { return static_cast<int32_t>(offsetof(AugmentationFocusedOnTarget_t1552339247, ___tether_13)); }
	inline Tether_t4182738604 * get_tether_13() const { return ___tether_13; }
	inline Tether_t4182738604 ** get_address_of_tether_13() { return &___tether_13; }
	inline void set_tether_13(Tether_t4182738604 * value)
	{
		___tether_13 = value;
		Il2CppCodeGenWriteBarrier(&___tether_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
