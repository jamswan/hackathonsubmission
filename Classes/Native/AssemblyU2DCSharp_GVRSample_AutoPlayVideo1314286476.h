﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GvrVideoPlayerTexture
struct GvrVideoPlayerTexture_t673526704;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GVRSample.AutoPlayVideo
struct  AutoPlayVideo_t1314286476  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean GVRSample.AutoPlayVideo::CPEFGHEFDMK
	bool ___CPEFGHEFDMK_2;
	// System.Single GVRSample.AutoPlayVideo::BHFNEOEPKGC
	float ___BHFNEOEPKGC_3;
	// GvrVideoPlayerTexture GVRSample.AutoPlayVideo::OECALIBMPJK
	GvrVideoPlayerTexture_t673526704 * ___OECALIBMPJK_4;
	// System.Single GVRSample.AutoPlayVideo::delay
	float ___delay_5;
	// System.Boolean GVRSample.AutoPlayVideo::loop
	bool ___loop_6;

public:
	inline static int32_t get_offset_of_CPEFGHEFDMK_2() { return static_cast<int32_t>(offsetof(AutoPlayVideo_t1314286476, ___CPEFGHEFDMK_2)); }
	inline bool get_CPEFGHEFDMK_2() const { return ___CPEFGHEFDMK_2; }
	inline bool* get_address_of_CPEFGHEFDMK_2() { return &___CPEFGHEFDMK_2; }
	inline void set_CPEFGHEFDMK_2(bool value)
	{
		___CPEFGHEFDMK_2 = value;
	}

	inline static int32_t get_offset_of_BHFNEOEPKGC_3() { return static_cast<int32_t>(offsetof(AutoPlayVideo_t1314286476, ___BHFNEOEPKGC_3)); }
	inline float get_BHFNEOEPKGC_3() const { return ___BHFNEOEPKGC_3; }
	inline float* get_address_of_BHFNEOEPKGC_3() { return &___BHFNEOEPKGC_3; }
	inline void set_BHFNEOEPKGC_3(float value)
	{
		___BHFNEOEPKGC_3 = value;
	}

	inline static int32_t get_offset_of_OECALIBMPJK_4() { return static_cast<int32_t>(offsetof(AutoPlayVideo_t1314286476, ___OECALIBMPJK_4)); }
	inline GvrVideoPlayerTexture_t673526704 * get_OECALIBMPJK_4() const { return ___OECALIBMPJK_4; }
	inline GvrVideoPlayerTexture_t673526704 ** get_address_of_OECALIBMPJK_4() { return &___OECALIBMPJK_4; }
	inline void set_OECALIBMPJK_4(GvrVideoPlayerTexture_t673526704 * value)
	{
		___OECALIBMPJK_4 = value;
		Il2CppCodeGenWriteBarrier(&___OECALIBMPJK_4, value);
	}

	inline static int32_t get_offset_of_delay_5() { return static_cast<int32_t>(offsetof(AutoPlayVideo_t1314286476, ___delay_5)); }
	inline float get_delay_5() const { return ___delay_5; }
	inline float* get_address_of_delay_5() { return &___delay_5; }
	inline void set_delay_5(float value)
	{
		___delay_5 = value;
	}

	inline static int32_t get_offset_of_loop_6() { return static_cast<int32_t>(offsetof(AutoPlayVideo_t1314286476, ___loop_6)); }
	inline bool get_loop_6() const { return ___loop_6; }
	inline bool* get_address_of_loop_6() { return &___loop_6; }
	inline void set_loop_6(bool value)
	{
		___loop_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
