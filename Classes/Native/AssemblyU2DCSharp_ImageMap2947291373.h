﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageMap
struct  ImageMap_t2947291373  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ImageMap::rayDistance
	float ___rayDistance_2;
	// UnityEngine.LayerMask ImageMap::layer
	LayerMask_t3188175821  ___layer_3;
	// UnityEngine.Texture2D ImageMap::clickTexture
	Texture2D_t3542995729 * ___clickTexture_4;
	// UnityEngine.Camera ImageMap::cam
	Camera_t189460977 * ___cam_5;
	// UnityEngine.Color[] ImageMap::colors
	ColorU5BU5D_t672350442* ___colors_6;
	// System.String[] ImageMap::texts
	StringU5BU5D_t1642385972* ___texts_7;
	// UnityEngine.Vector3 ImageMap::OFLFFDAIAEI
	Vector3_t2243707580  ___OFLFFDAIAEI_8;

public:
	inline static int32_t get_offset_of_rayDistance_2() { return static_cast<int32_t>(offsetof(ImageMap_t2947291373, ___rayDistance_2)); }
	inline float get_rayDistance_2() const { return ___rayDistance_2; }
	inline float* get_address_of_rayDistance_2() { return &___rayDistance_2; }
	inline void set_rayDistance_2(float value)
	{
		___rayDistance_2 = value;
	}

	inline static int32_t get_offset_of_layer_3() { return static_cast<int32_t>(offsetof(ImageMap_t2947291373, ___layer_3)); }
	inline LayerMask_t3188175821  get_layer_3() const { return ___layer_3; }
	inline LayerMask_t3188175821 * get_address_of_layer_3() { return &___layer_3; }
	inline void set_layer_3(LayerMask_t3188175821  value)
	{
		___layer_3 = value;
	}

	inline static int32_t get_offset_of_clickTexture_4() { return static_cast<int32_t>(offsetof(ImageMap_t2947291373, ___clickTexture_4)); }
	inline Texture2D_t3542995729 * get_clickTexture_4() const { return ___clickTexture_4; }
	inline Texture2D_t3542995729 ** get_address_of_clickTexture_4() { return &___clickTexture_4; }
	inline void set_clickTexture_4(Texture2D_t3542995729 * value)
	{
		___clickTexture_4 = value;
		Il2CppCodeGenWriteBarrier(&___clickTexture_4, value);
	}

	inline static int32_t get_offset_of_cam_5() { return static_cast<int32_t>(offsetof(ImageMap_t2947291373, ___cam_5)); }
	inline Camera_t189460977 * get_cam_5() const { return ___cam_5; }
	inline Camera_t189460977 ** get_address_of_cam_5() { return &___cam_5; }
	inline void set_cam_5(Camera_t189460977 * value)
	{
		___cam_5 = value;
		Il2CppCodeGenWriteBarrier(&___cam_5, value);
	}

	inline static int32_t get_offset_of_colors_6() { return static_cast<int32_t>(offsetof(ImageMap_t2947291373, ___colors_6)); }
	inline ColorU5BU5D_t672350442* get_colors_6() const { return ___colors_6; }
	inline ColorU5BU5D_t672350442** get_address_of_colors_6() { return &___colors_6; }
	inline void set_colors_6(ColorU5BU5D_t672350442* value)
	{
		___colors_6 = value;
		Il2CppCodeGenWriteBarrier(&___colors_6, value);
	}

	inline static int32_t get_offset_of_texts_7() { return static_cast<int32_t>(offsetof(ImageMap_t2947291373, ___texts_7)); }
	inline StringU5BU5D_t1642385972* get_texts_7() const { return ___texts_7; }
	inline StringU5BU5D_t1642385972** get_address_of_texts_7() { return &___texts_7; }
	inline void set_texts_7(StringU5BU5D_t1642385972* value)
	{
		___texts_7 = value;
		Il2CppCodeGenWriteBarrier(&___texts_7, value);
	}

	inline static int32_t get_offset_of_OFLFFDAIAEI_8() { return static_cast<int32_t>(offsetof(ImageMap_t2947291373, ___OFLFFDAIAEI_8)); }
	inline Vector3_t2243707580  get_OFLFFDAIAEI_8() const { return ___OFLFFDAIAEI_8; }
	inline Vector3_t2243707580 * get_address_of_OFLFFDAIAEI_8() { return &___OFLFFDAIAEI_8; }
	inline void set_OFLFFDAIAEI_8(Vector3_t2243707580  value)
	{
		___OFLFFDAIAEI_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
