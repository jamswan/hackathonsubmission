﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnableGameObject
struct  EnableGameObject_t1635748018  : public NarrativeEffect_t3336735121
{
public:
	// UnityEngine.GameObject EnableGameObject::targetObject
	GameObject_t1756533147 * ___targetObject_3;
	// System.Boolean EnableGameObject::enableParents
	bool ___enableParents_4;

public:
	inline static int32_t get_offset_of_targetObject_3() { return static_cast<int32_t>(offsetof(EnableGameObject_t1635748018, ___targetObject_3)); }
	inline GameObject_t1756533147 * get_targetObject_3() const { return ___targetObject_3; }
	inline GameObject_t1756533147 ** get_address_of_targetObject_3() { return &___targetObject_3; }
	inline void set_targetObject_3(GameObject_t1756533147 * value)
	{
		___targetObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___targetObject_3, value);
	}

	inline static int32_t get_offset_of_enableParents_4() { return static_cast<int32_t>(offsetof(EnableGameObject_t1635748018, ___enableParents_4)); }
	inline bool get_enableParents_4() const { return ___enableParents_4; }
	inline bool* get_address_of_enableParents_4() { return &___enableParents_4; }
	inline void set_enableParents_4(bool value)
	{
		___enableParents_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
