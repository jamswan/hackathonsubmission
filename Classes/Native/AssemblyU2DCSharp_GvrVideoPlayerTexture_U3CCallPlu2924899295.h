﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1785723201;
// GvrVideoPlayerTexture
struct GvrVideoPlayerTexture_t673526704;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1
struct  U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295  : public Il2CppObject
{
public:
	// System.Boolean GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::BPHNLOBKGGN
	bool ___BPHNLOBKGGN_0;
	// UnityEngine.WaitForEndOfFrame GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::AJDBPNNEJMP
	WaitForEndOfFrame_t1785723201 * ___AJDBPNNEJMP_1;
	// System.IntPtr GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::IHFNOMJELBH
	IntPtr_t ___IHFNOMJELBH_2;
	// System.Int32 GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::DGMMPFCFBDO
	int32_t ___DGMMPFCFBDO_3;
	// System.Int32 GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::CGCGAGEDEIA
	int32_t ___CGCGAGEDEIA_4;
	// System.Int64 GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::LDKJKNJKCJF
	int64_t ___LDKJKNJKCJF_5;
	// GvrVideoPlayerTexture GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::AOOLEAHHMIH
	GvrVideoPlayerTexture_t673526704 * ___AOOLEAHHMIH_6;
	// System.Object GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::LGBFNMECDHC
	Il2CppObject * ___LGBFNMECDHC_7;
	// System.Boolean GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_8;
	// System.Int32 GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_9;

public:
	inline static int32_t get_offset_of_BPHNLOBKGGN_0() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___BPHNLOBKGGN_0)); }
	inline bool get_BPHNLOBKGGN_0() const { return ___BPHNLOBKGGN_0; }
	inline bool* get_address_of_BPHNLOBKGGN_0() { return &___BPHNLOBKGGN_0; }
	inline void set_BPHNLOBKGGN_0(bool value)
	{
		___BPHNLOBKGGN_0 = value;
	}

	inline static int32_t get_offset_of_AJDBPNNEJMP_1() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___AJDBPNNEJMP_1)); }
	inline WaitForEndOfFrame_t1785723201 * get_AJDBPNNEJMP_1() const { return ___AJDBPNNEJMP_1; }
	inline WaitForEndOfFrame_t1785723201 ** get_address_of_AJDBPNNEJMP_1() { return &___AJDBPNNEJMP_1; }
	inline void set_AJDBPNNEJMP_1(WaitForEndOfFrame_t1785723201 * value)
	{
		___AJDBPNNEJMP_1 = value;
		Il2CppCodeGenWriteBarrier(&___AJDBPNNEJMP_1, value);
	}

	inline static int32_t get_offset_of_IHFNOMJELBH_2() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___IHFNOMJELBH_2)); }
	inline IntPtr_t get_IHFNOMJELBH_2() const { return ___IHFNOMJELBH_2; }
	inline IntPtr_t* get_address_of_IHFNOMJELBH_2() { return &___IHFNOMJELBH_2; }
	inline void set_IHFNOMJELBH_2(IntPtr_t value)
	{
		___IHFNOMJELBH_2 = value;
	}

	inline static int32_t get_offset_of_DGMMPFCFBDO_3() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___DGMMPFCFBDO_3)); }
	inline int32_t get_DGMMPFCFBDO_3() const { return ___DGMMPFCFBDO_3; }
	inline int32_t* get_address_of_DGMMPFCFBDO_3() { return &___DGMMPFCFBDO_3; }
	inline void set_DGMMPFCFBDO_3(int32_t value)
	{
		___DGMMPFCFBDO_3 = value;
	}

	inline static int32_t get_offset_of_CGCGAGEDEIA_4() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___CGCGAGEDEIA_4)); }
	inline int32_t get_CGCGAGEDEIA_4() const { return ___CGCGAGEDEIA_4; }
	inline int32_t* get_address_of_CGCGAGEDEIA_4() { return &___CGCGAGEDEIA_4; }
	inline void set_CGCGAGEDEIA_4(int32_t value)
	{
		___CGCGAGEDEIA_4 = value;
	}

	inline static int32_t get_offset_of_LDKJKNJKCJF_5() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___LDKJKNJKCJF_5)); }
	inline int64_t get_LDKJKNJKCJF_5() const { return ___LDKJKNJKCJF_5; }
	inline int64_t* get_address_of_LDKJKNJKCJF_5() { return &___LDKJKNJKCJF_5; }
	inline void set_LDKJKNJKCJF_5(int64_t value)
	{
		___LDKJKNJKCJF_5 = value;
	}

	inline static int32_t get_offset_of_AOOLEAHHMIH_6() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___AOOLEAHHMIH_6)); }
	inline GvrVideoPlayerTexture_t673526704 * get_AOOLEAHHMIH_6() const { return ___AOOLEAHHMIH_6; }
	inline GvrVideoPlayerTexture_t673526704 ** get_address_of_AOOLEAHHMIH_6() { return &___AOOLEAHHMIH_6; }
	inline void set_AOOLEAHHMIH_6(GvrVideoPlayerTexture_t673526704 * value)
	{
		___AOOLEAHHMIH_6 = value;
		Il2CppCodeGenWriteBarrier(&___AOOLEAHHMIH_6, value);
	}

	inline static int32_t get_offset_of_LGBFNMECDHC_7() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___LGBFNMECDHC_7)); }
	inline Il2CppObject * get_LGBFNMECDHC_7() const { return ___LGBFNMECDHC_7; }
	inline Il2CppObject ** get_address_of_LGBFNMECDHC_7() { return &___LGBFNMECDHC_7; }
	inline void set_LGBFNMECDHC_7(Il2CppObject * value)
	{
		___LGBFNMECDHC_7 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_7, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_8() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___IMAGLIMLPFK_8)); }
	inline bool get_IMAGLIMLPFK_8() const { return ___IMAGLIMLPFK_8; }
	inline bool* get_address_of_IMAGLIMLPFK_8() { return &___IMAGLIMLPFK_8; }
	inline void set_IMAGLIMLPFK_8(bool value)
	{
		___IMAGLIMLPFK_8 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_9() { return static_cast<int32_t>(offsetof(U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295, ___EPCFNNGDBGC_9)); }
	inline int32_t get_EPCFNNGDBGC_9() const { return ___EPCFNNGDBGC_9; }
	inline int32_t* get_address_of_EPCFNNGDBGC_9() { return &___EPCFNNGDBGC_9; }
	inline void set_EPCFNNGDBGC_9(int32_t value)
	{
		___EPCFNNGDBGC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
