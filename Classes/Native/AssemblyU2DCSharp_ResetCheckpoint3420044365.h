﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResetCheckpoint
struct  ResetCheckpoint_t3420044365  : public NarrativeEffect_t3336735121
{
public:
	// System.String ResetCheckpoint::checkpointName
	String_t* ___checkpointName_3;

public:
	inline static int32_t get_offset_of_checkpointName_3() { return static_cast<int32_t>(offsetof(ResetCheckpoint_t3420044365, ___checkpointName_3)); }
	inline String_t* get_checkpointName_3() const { return ___checkpointName_3; }
	inline String_t** get_address_of_checkpointName_3() { return &___checkpointName_3; }
	inline void set_checkpointName_3(String_t* value)
	{
		___checkpointName_3 = value;
		Il2CppCodeGenWriteBarrier(&___checkpointName_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
