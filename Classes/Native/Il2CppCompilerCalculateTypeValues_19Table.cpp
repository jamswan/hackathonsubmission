﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_BaseStereoViewerCa1102239676.h"
#include "Vuforia_UnityExtensions_Vuforia_StereoViewerCamera3365023487.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkManager3604726399.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkManagerImpl1660847547.h"
#include "Vuforia_UnityExtensions_Vuforia_InstanceIdImpl3955455590.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkTargetImpl2700679413.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableImpl3421455115.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTargetImpl1796148526.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkTemplateImpl199901830.h"
#include "Vuforia_UnityExtensions_Vuforia_MixedRealityContro1276557833.h"
#include "Vuforia_UnityExtensions_Vuforia_MixedRealityControll38013191.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTr3644694819.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTra111727860.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerParameters1247673784.h"
#include "Vuforia_UnityExtensions_Vuforia_CustomViewerParamet779886969.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTrackingMana2097550852.h"
#include "Vuforia_UnityExtensions_Vuforia_FactorySetter648583075.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearCalibration2025108506.h"
#include "Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbs3732945727.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearUserCalibra1518014586.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalPlayMode3894463544.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTra832065887.h"
#include "Vuforia_UnityExtensions_Vuforia_IOSCamRecoveringHel979448318.h"
#include "Vuforia_UnityExtensions_Vuforia_MeshUtils2110180948.h"
#include "Vuforia_UnityExtensions_Vuforia_ExternalStereoCame4187656756.h"
#include "Vuforia_UnityExtensions_Vuforia_NullHideExcessArea2290611987.h"
#include "Vuforia_UnityExtensions_Vuforia_StencilHideExcessA3377289090.h"
#include "Vuforia_UnityExtensions_Vuforia_LegacyHideExcessAr3475621185.h"
#include "Vuforia_UnityExtensions_Vuforia_DedicatedEyewearCam816511398.h"
#include "Vuforia_UnityExtensions_Vuforia_NullCameraConfigura133234522.h"
#include "Vuforia_UnityExtensions_Vuforia_MonoCameraConfigur3796201132.h"
#include "Vuforia_UnityExtensions_Vuforia_UnityCameraExtensi2392150382.h"
#include "Vuforia_UnityExtensions_Vuforia_View3542740111.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerParametersLi3152440868.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1779888572.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour4057911311.h"
#include "Vuforia_UnityExtensions_Vuforia_DataSetTrackableBe3452781876.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstra2805337095.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice3827827595.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer2705300828.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Focus4087668361.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer1654543970.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Video3451594282.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer3847513849.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer3959695208.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Int64Ra56370998.h"
#include "Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractB2070832277.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionImpl2149050756.h"
#include "Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbst2687577327.h"
#include "Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbst2100449024.h"
#include "Vuforia_UnityExtensions_Vuforia_UnityPlayer3164516142.h"
#include "Vuforia_UnityExtensions_Vuforia_NullUnityPlayer754446093.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer918240325.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionFrom1192715795.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerButtonType413015468.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerTrayAlignment791464321.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkAbstractBeha1830666997.h"
#include "Vuforia_UnityExtensions_Vuforia_InstanceIdType467315012.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionFrom4122236588.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilde1628525337.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainInitia1102352056.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracka2391102074.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke4026264541.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeh2669615494.h"
#include "Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr665872082.h"
#include "Vuforia_UnityExtensions_Vuforia_DataSet626511550.h"
#include "Vuforia_UnityExtensions_Vuforia_DataSet_StorageTyp4268322930.h"
#include "Vuforia_UnityExtensions_Vuforia_DatabaseLoadAbstra1458632096.h"
#include "Vuforia_UnityExtensions_Vuforia_RectangleData934532407.h"
#include "Vuforia_UnityExtensions_Vuforia_RectangleIntData2869769236.h"
#include "Vuforia_UnityExtensions_Vuforia_OrientedBoundingBo3172429123.h"
#include "Vuforia_UnityExtensions_Vuforia_OrientedBoundingBo1484796416.h"
#include "Vuforia_UnityExtensions_Vuforia_BehaviourComponent3267823770.h"
#include "Vuforia_UnityExtensions_Vuforia_BehaviourComponent3849641556.h"
#include "Vuforia_UnityExtensions_Vuforia_CloudRecoImageTarg2427143318.h"
#include "Vuforia_UnityExtensions_Vuforia_CylinderTargetImpl259286421.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetType3906864138.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetData1326050618.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder518883741.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder2061101710.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDeviceImpl610967511.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (BaseStereoViewerCameraConfiguration_t1102239676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[5] = 
{
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mPrimaryCamera_11(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mSecondaryCamera_12(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mSkewFrustum_13(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mScreenWidth_14(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mScreenHeight_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (StereoViewerCameraConfiguration_t3365023487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[7] = 
{
	0,
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedLeftNearClipPlane_17(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedLeftFarClipPlane_18(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedRightNearClipPlane_19(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedRightFarClipPlane_20(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mCameraOffset_21(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mIsDistorted_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (VuMarkManager_t3604726399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (VuMarkManagerImpl_t1660847547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[6] = 
{
	VuMarkManagerImpl_t1660847547::get_offset_of_mBehaviours_0(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mActiveVuMarkTargets_1(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mDestroyedBehaviours_2(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mOnVuMarkDetected_3(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mOnVuMarkLost_4(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mOnVuMarkBehaviourDetected_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (InstanceIdImpl_t3955455590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[5] = 
{
	InstanceIdImpl_t3955455590::get_offset_of_mDataType_0(),
	InstanceIdImpl_t3955455590::get_offset_of_mBuffer_1(),
	InstanceIdImpl_t3955455590::get_offset_of_mNumericValue_2(),
	InstanceIdImpl_t3955455590::get_offset_of_mDataLength_3(),
	InstanceIdImpl_t3955455590::get_offset_of_mCachedStringValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (VuMarkTargetImpl_t2700679413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[5] = 
{
	VuMarkTargetImpl_t2700679413::get_offset_of_mVuMarkTemplate_0(),
	VuMarkTargetImpl_t2700679413::get_offset_of_mInstanceId_1(),
	VuMarkTargetImpl_t2700679413::get_offset_of_mTargetId_2(),
	VuMarkTargetImpl_t2700679413::get_offset_of_mInstanceImage_3(),
	VuMarkTargetImpl_t2700679413::get_offset_of_mInstanceImageHeader_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (TrackableImpl_t3421455115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[2] = 
{
	TrackableImpl_t3421455115::get_offset_of_U3CNameU3Ek__BackingField_0(),
	TrackableImpl_t3421455115::get_offset_of_U3CIDU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (ObjectTargetImpl_t1796148526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[2] = 
{
	ObjectTargetImpl_t1796148526::get_offset_of_mSize_2(),
	ObjectTargetImpl_t1796148526::get_offset_of_mDataSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (VuMarkTemplateImpl_t199901830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1916[3] = 
{
	VuMarkTemplateImpl_t199901830::get_offset_of_mUserData_4(),
	VuMarkTemplateImpl_t199901830::get_offset_of_mOrigin_5(),
	VuMarkTemplateImpl_t199901830::get_offset_of_mTrackingFromRuntimeAppearance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (MixedRealityController_t1276557833), -1, sizeof(MixedRealityController_t1276557833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1917[14] = 
{
	MixedRealityController_t1276557833_StaticFields::get_offset_of_mInstance_0(),
	MixedRealityController_t1276557833::get_offset_of_mVuforiaBehaviour_1(),
	MixedRealityController_t1276557833::get_offset_of_mDigitalEyewearBehaviour_2(),
	MixedRealityController_t1276557833::get_offset_of_mVideoBackgroundManager_3(),
	MixedRealityController_t1276557833::get_offset_of_mViewerHasBeenSetExternally_4(),
	MixedRealityController_t1276557833::get_offset_of_mViewerParameters_5(),
	MixedRealityController_t1276557833::get_offset_of_mFrameWorkHasBeenSetExternally_6(),
	MixedRealityController_t1276557833::get_offset_of_mStereoFramework_7(),
	MixedRealityController_t1276557833::get_offset_of_mCentralAnchorPoint_8(),
	MixedRealityController_t1276557833::get_offset_of_mLeftCameraOfExternalSDK_9(),
	MixedRealityController_t1276557833::get_offset_of_mRightCameraOfExternalSDK_10(),
	MixedRealityController_t1276557833::get_offset_of_mObjectTrackerStopped_11(),
	MixedRealityController_t1276557833::get_offset_of_mMarkerTrackerStopped_12(),
	MixedRealityController_t1276557833::get_offset_of_mAutoStopCameraIfNotRequired_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (Mode_t38013191)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1918[7] = 
{
	Mode_t38013191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (RotationalDeviceTracker_t3644694819), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (MODEL_CORRECTION_MODE_t111727860)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1920[4] = 
{
	MODEL_CORRECTION_MODE_t111727860::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (ViewerParameters_t1247673784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[1] = 
{
	ViewerParameters_t1247673784::get_offset_of_mNativeVP_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (CustomViewerParameters_t779886969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1922[7] = 
{
	CustomViewerParameters_t779886969::get_offset_of_mVersion_1(),
	CustomViewerParameters_t779886969::get_offset_of_mName_2(),
	CustomViewerParameters_t779886969::get_offset_of_mManufacturer_3(),
	CustomViewerParameters_t779886969::get_offset_of_mButtonType_4(),
	CustomViewerParameters_t779886969::get_offset_of_mScreenToLensDistance_5(),
	CustomViewerParameters_t779886969::get_offset_of_mTrayAlignment_6(),
	CustomViewerParameters_t779886969::get_offset_of_mMagnet_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (DeviceTrackingManager_t2097550852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[4] = 
{
	DeviceTrackingManager_t2097550852::get_offset_of_mDeviceTrackerPositonOffset_0(),
	DeviceTrackingManager_t2097550852::get_offset_of_mDeviceTrackerRotationOffset_1(),
	DeviceTrackingManager_t2097550852::get_offset_of_mBeforeDevicePoseUpdated_2(),
	DeviceTrackingManager_t2097550852::get_offset_of_mAfterDevicePoseUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (FactorySetter_t648583075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (EyewearCalibrationProfileManagerImpl_t2025108506), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (BackgroundPlaneAbstractBehaviour_t3732945727), -1, sizeof(BackgroundPlaneAbstractBehaviour_t3732945727_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1926[13] = 
{
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mTextureInfo_2(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mViewWidth_3(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mViewHeight_4(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mVideoBgConfigChanged_5(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mCamera_6(),
	BackgroundPlaneAbstractBehaviour_t3732945727_StaticFields::get_offset_of_maxDisplacement_7(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_defaultNumDivisions_8(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mMesh_9(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mStereoDepth_10(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mBackgroundOffset_11(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mVuforiaBehaviour_12(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mBackgroundPlacedCallback_13(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mNumDivisions_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (EyewearUserCalibratorImpl_t1518014586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (RotationalPlayModeDeviceTrackerImpl_t3894463544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[3] = 
{
	RotationalPlayModeDeviceTrackerImpl_t3894463544::get_offset_of_mRotation_1(),
	RotationalPlayModeDeviceTrackerImpl_t3894463544::get_offset_of_mModelCorrectionTransform_2(),
	RotationalPlayModeDeviceTrackerImpl_t3894463544::get_offset_of_mModelCorrection_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (RotationalDeviceTrackerImpl_t832065887), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (IOSCamRecoveringHelper_t979448318), -1, sizeof(IOSCamRecoveringHelper_t979448318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1931[9] = 
{
	0,
	0,
	0,
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sHasJustResumed_3(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sCheckFailedFrameAfterResume_4(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sCheckedFailedFrameCounter_5(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sWaitToRecoverCameraRestart_6(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sWaitedFrameRecoverCounter_7(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sRecoveryAttemptCounter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (MeshUtils_t2110180948), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (ExternalStereoCameraConfiguration_t4187656756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[21] = 
{
	0,
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftNearClipPlane_17(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftFarClipPlane_18(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightNearClipPlane_19(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightFarClipPlane_20(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftVerticalVirtualFoV_21(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftHorizontalVirtualFoV_22(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightVerticalVirtualFoV_23(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightHorizontalVirtualFoV_24(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftProjection_25(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightProjection_26(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftNearClipPlane_27(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftFarClipPlane_28(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightNearClipPlane_29(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightFarClipPlane_30(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftVerticalVirtualFoV_31(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftHorizontalVirtualFoV_32(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightVerticalVirtualFoV_33(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightHorizontalVirtualFoV_34(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mExternallySetLeftMatrix_35(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mExternallySetRightMatrix_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (NullHideExcessAreaClipping_t2290611987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (StencilHideExcessAreaClipping_t3377289090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[13] = 
{
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mGameObject_0(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mMatteShader_1(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mClippingPlane_2(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCamera_3(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraNearPlane_4(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraFarPlane_5(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraPixelRect_6(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraFieldOfView_7(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mVuforiaBehaviour_8(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mPlanesActivated_9(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mBgPlane_10(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mBgPlaneLocalPos_11(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mBgPlaneLocalScale_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (LegacyHideExcessAreaClipping_t3475621185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[21] = 
{
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mGameObject_0(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mMatteShader_1(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBgPlane_2(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mLeftPlane_3(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mRightPlane_4(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mTopPlane_5(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBottomPlane_6(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mCamera_7(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBgPlaneLocalPos_8(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBgPlaneLocalScale_9(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mCameraNearPlane_10(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mCameraPixelRect_11(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mCameraFieldOFView_12(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mVuforiaBehaviour_13(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mHideBehaviours_14(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mDeactivatedHideBehaviours_15(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mPlanesActivated_16(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mLeftPlaneCachedScale_17(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mRightPlaneCachedScale_18(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBottomPlaneCachedScale_19(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mTopPlaneCachedScale_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (DedicatedEyewearCameraConfiguration_t816511398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1937[13] = 
{
	0,
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mPrimaryCamera_12(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mSecondaryCamera_13(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mScreenWidth_14(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mScreenHeight_15(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mNeedToCheckStereo_16(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mLastAppliedNearClipPlane_17(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mLastAppliedFarClipPlane_18(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mLastAppliedVirtualFoV_19(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mNewNearClipPlane_20(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mNewFarClipPlane_21(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mNewVirtualFoV_22(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mEyewearDevice_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (NullCameraConfiguration_t133234522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1938[1] = 
{
	NullCameraConfiguration_t133234522::get_offset_of_mProjectionOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (MonoCameraConfiguration_t3796201132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[7] = 
{
	0,
	MonoCameraConfiguration_t3796201132::get_offset_of_mPrimaryCamera_12(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mCameraViewPortWidth_13(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mCameraViewPortHeight_14(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mLastAppliedNearClipPlane_15(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mLastAppliedFarClipPlane_16(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mLastAppliedFoV_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (UnityCameraExtensions_t2392150382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (View_t3542740111)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1941[6] = 
{
	View_t3542740111::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (ViewerParametersList_t3152440868), -1, sizeof(ViewerParametersList_t3152440868_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1943[2] = 
{
	ViewerParametersList_t3152440868::get_offset_of_mNativeVPL_0(),
	ViewerParametersList_t3152440868_StaticFields::get_offset_of_mListForAuthoringTools_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (TrackableBehaviour_t1779888572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[8] = 
{
	TrackableBehaviour_t1779888572::get_offset_of_mTrackableName_2(),
	TrackableBehaviour_t1779888572::get_offset_of_mPreserveChildSize_3(),
	TrackableBehaviour_t1779888572::get_offset_of_mInitializedInEditor_4(),
	TrackableBehaviour_t1779888572::get_offset_of_mPreviousScale_5(),
	TrackableBehaviour_t1779888572::get_offset_of_mStatus_6(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackable_7(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackableEventHandlers_8(),
	TrackableBehaviour_t1779888572::get_offset_of_U3CTimeStampU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (Status_t4057911311)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1945[7] = 
{
	Status_t4057911311::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (DataSetTrackableBehaviour_t3452781876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1947[10] = 
{
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mDataSetPath_10(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mExtendedTracking_11(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mInitializeSmartTerrain_12(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mReconstructionToInitialize_13(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mSmartTerrainOccluderBoundsMin_14(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mSmartTerrainOccluderBoundsMax_15(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mIsSmartTerrainOccluderOffset_16(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mSmartTerrainOccluderOffset_17(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mSmartTerrainOccluderRotation_18(),
	DataSetTrackableBehaviour_t3452781876::get_offset_of_mAutoSetOccluderFromTargetSize_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (ObjectTargetAbstractBehaviour_t2805337095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1948[12] = 
{
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mObjectTarget_20(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mAspectRatioXY_21(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mAspectRatioXZ_22(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mShowBoundingBox_23(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mBBoxMin_24(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mBBoxMax_25(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mPreviewImage_26(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mLength_27(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mWidth_28(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mHeight_29(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mLastTransformScale_30(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mLastSize_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (CameraDevice_t3827827595), -1, sizeof(CameraDevice_t3827827595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1949[1] = 
{
	CameraDevice_t3827827595_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (CameraDeviceMode_t2705300828)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1950[4] = 
{
	CameraDeviceMode_t2705300828::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (FocusMode_t4087668361)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1951[6] = 
{
	FocusMode_t4087668361::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (CameraDirection_t1654543970)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1952[4] = 
{
	CameraDirection_t1654543970::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (VideoModeData_t3451594282)+ sizeof (Il2CppObject), sizeof(VideoModeData_t3451594282 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1953[4] = 
{
	VideoModeData_t3451594282::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t3451594282::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t3451594282::get_offset_of_frameRate_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoModeData_t3451594282::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (CameraField_t3847513849)+ sizeof (Il2CppObject), sizeof(CameraField_t3847513849_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1954[2] = 
{
	CameraField_t3847513849::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CameraField_t3847513849::get_offset_of_Key_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (DataType_t3959695208)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1955[7] = 
{
	DataType_t3959695208::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (Int64Range_t56370998)+ sizeof (Il2CppObject), sizeof(Int64Range_t56370998 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1956[2] = 
{
	Int64Range_t56370998::get_offset_of_fromInt_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Int64Range_t56370998::get_offset_of_toInt_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (CloudRecoAbstractBehaviour_t2070832277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[9] = 
{
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mObjectTracker_2(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mCurrentlyInitializing_3(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mInitSuccess_4(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mCloudRecoStarted_5(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mOnInitializedCalled_6(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mHandlers_7(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_mTargetFinderStartedBeforeDisable_8(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_AccessKey_9(),
	CloudRecoAbstractBehaviour_t2070832277::get_offset_of_SecretKey_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (ReconstructionImpl_t2149050756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[5] = 
{
	ReconstructionImpl_t2149050756::get_offset_of_mNativeReconstructionPtr_0(),
	ReconstructionImpl_t2149050756::get_offset_of_mMaximumAreaIsSet_1(),
	ReconstructionImpl_t2149050756::get_offset_of_mMaximumArea_2(),
	ReconstructionImpl_t2149050756::get_offset_of_mNavMeshPadding_3(),
	ReconstructionImpl_t2149050756::get_offset_of_mNavMeshUpdatesEnabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (HideExcessAreaAbstractBehaviour_t2687577327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[7] = 
{
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mClippingImpl_2(),
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mClippingMode_3(),
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mVuforiaBehaviour_4(),
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mVideoBgMgr_5(),
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mPlaneOffset_6(),
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mSceneScaledDown_7(),
	HideExcessAreaAbstractBehaviour_t2687577327::get_offset_of_mStarted_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (CLIPPING_MODE_t2100449024)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1960[4] = 
{
	CLIPPING_MODE_t2100449024::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (UnityPlayer_t3164516142), -1, sizeof(UnityPlayer_t3164516142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1961[1] = 
{
	UnityPlayer_t3164516142_StaticFields::get_offset_of_sPlayer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (NullUnityPlayer_t754446093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (PlayModeUnityPlayer_t918240325), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (ReconstructionFromTargetImpl_t1192715795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1967[6] = 
{
	ReconstructionFromTargetImpl_t1192715795::get_offset_of_mOccluderMin_5(),
	ReconstructionFromTargetImpl_t1192715795::get_offset_of_mOccluderMax_6(),
	ReconstructionFromTargetImpl_t1192715795::get_offset_of_mOccluderOffset_7(),
	ReconstructionFromTargetImpl_t1192715795::get_offset_of_mOccluderRotation_8(),
	ReconstructionFromTargetImpl_t1192715795::get_offset_of_mInitializationTarget_9(),
	ReconstructionFromTargetImpl_t1192715795::get_offset_of_mCanAutoSetInitializationTarget_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (ViewerButtonType_t413015468)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1968[5] = 
{
	ViewerButtonType_t413015468::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (ViewerTrayAlignment_t791464321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1969[4] = 
{
	ViewerTrayAlignment_t791464321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (VuMarkAbstractBehaviour_t1830666997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1970[16] = 
{
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mAspectRatio_20(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mWidth_21(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mHeight_22(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mPreviewImage_23(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mIdType_24(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mIdLength_25(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mBoundingBox_26(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mOrigin_27(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mTrackingFromRuntimeAppearance_28(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mVuMarkTemplate_29(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mVuMarkTarget_30(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mVuMarkResultId_31(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mOnTargetAssigned_32(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mOnTargetLost_33(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mLastTransformScale_34(),
	VuMarkAbstractBehaviour_t1830666997::get_offset_of_mLastSize_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (InstanceIdType_t467315012)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1971[4] = 
{
	InstanceIdType_t467315012::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (ReconstructionFromTargetAbstractBehaviour_t4122236588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1972[2] = 
{
	ReconstructionFromTargetAbstractBehaviour_t4122236588::get_offset_of_mReconstructionFromTarget_2(),
	ReconstructionFromTargetAbstractBehaviour_t4122236588::get_offset_of_mReconstructionBehaviour_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (SmartTerrainBuilder_t1628525337), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (SmartTerrainInitializationInfo_t1102352056)+ sizeof (Il2CppObject), sizeof(SmartTerrainInitializationInfo_t1102352056 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (SmartTerrainTrackableBehaviour_t2391102074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1975[4] = 
{
	SmartTerrainTrackableBehaviour_t2391102074::get_offset_of_mSmartTerrainTrackable_10(),
	SmartTerrainTrackableBehaviour_t2391102074::get_offset_of_mDisableAutomaticUpdates_11(),
	SmartTerrainTrackableBehaviour_t2391102074::get_offset_of_mMeshFilterToUpdate_12(),
	SmartTerrainTrackableBehaviour_t2391102074::get_offset_of_mMeshColliderToUpdate_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (SmartTerrainTrackerAbstractBehaviour_t4026264541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1976[7] = 
{
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mAutoInitTracker_2(),
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mAutoStartTracker_3(),
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mAutoInitBuilder_4(),
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mSceneUnitsToMillimeter_5(),
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mTrackerStarted_6(),
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mTrackerWasActiveBeforePause_7(),
	SmartTerrainTrackerAbstractBehaviour_t4026264541::get_offset_of_mTrackerWasActiveBeforeDisabling_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (SurfaceAbstractBehaviour_t2669615494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[1] = 
{
	SurfaceAbstractBehaviour_t2669615494::get_offset_of_mSurface_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (CylinderTargetAbstractBehaviour_t665872082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[10] = 
{
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mCylinderTarget_20(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mTopDiameterRatio_21(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mBottomDiameterRatio_22(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mSideLength_23(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mTopDiameter_24(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mBottomDiameter_25(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mFrameIndex_26(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mUpdateFrameIndex_27(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mFutureScale_28(),
	CylinderTargetAbstractBehaviour_t665872082::get_offset_of_mLastTransformScale_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (DataSet_t626511550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (StorageType_t4268322930)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1981[4] = 
{
	StorageType_t4268322930::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (DatabaseLoadAbstractBehaviour_t1458632096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1982[4] = 
{
	DatabaseLoadAbstractBehaviour_t1458632096::get_offset_of_mDatasetsLoaded_2(),
	DatabaseLoadAbstractBehaviour_t1458632096::get_offset_of_mDataSetsToLoad_3(),
	DatabaseLoadAbstractBehaviour_t1458632096::get_offset_of_mDataSetsToActivate_4(),
	DatabaseLoadAbstractBehaviour_t1458632096::get_offset_of_mExternalDatasetRoots_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (RectangleData_t934532407)+ sizeof (Il2CppObject), sizeof(RectangleData_t934532407 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1983[4] = 
{
	RectangleData_t934532407::get_offset_of_leftTopX_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleData_t934532407::get_offset_of_leftTopY_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleData_t934532407::get_offset_of_rightBottomX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleData_t934532407::get_offset_of_rightBottomY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (RectangleIntData_t2869769236)+ sizeof (Il2CppObject), sizeof(RectangleIntData_t2869769236 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1984[4] = 
{
	RectangleIntData_t2869769236::get_offset_of_leftTopX_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleIntData_t2869769236::get_offset_of_leftTopY_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleIntData_t2869769236::get_offset_of_rightBottomX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleIntData_t2869769236::get_offset_of_rightBottomY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (OrientedBoundingBox_t3172429123)+ sizeof (Il2CppObject), sizeof(OrientedBoundingBox_t3172429123 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1985[3] = 
{
	OrientedBoundingBox_t3172429123::get_offset_of_U3CCenterU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox_t3172429123::get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox_t3172429123::get_offset_of_U3CRotationU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (OrientedBoundingBox3D_t1484796416)+ sizeof (Il2CppObject), sizeof(OrientedBoundingBox3D_t1484796416 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1986[3] = 
{
	OrientedBoundingBox3D_t1484796416::get_offset_of_U3CCenterU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox3D_t1484796416::get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox3D_t1484796416::get_offset_of_U3CRotationYU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (BehaviourComponentFactory_t3267823770), -1, sizeof(BehaviourComponentFactory_t3267823770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1989[1] = 
{
	BehaviourComponentFactory_t3267823770_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (NullBehaviourComponentFactory_t3849641556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (CloudRecoImageTargetImpl_t2427143318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[1] = 
{
	CloudRecoImageTargetImpl_t2427143318::get_offset_of_mSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (CylinderTargetImpl_t259286421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1994[3] = 
{
	CylinderTargetImpl_t259286421::get_offset_of_mSideLength_4(),
	CylinderTargetImpl_t259286421::get_offset_of_mTopDiameter_5(),
	CylinderTargetImpl_t259286421::get_offset_of_mBottomDiameter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (ImageTargetType_t3906864138)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1995[4] = 
{
	ImageTargetType_t3906864138::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (ImageTargetData_t1326050618)+ sizeof (Il2CppObject), sizeof(ImageTargetData_t1326050618 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1996[2] = 
{
	ImageTargetData_t1326050618::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageTargetData_t1326050618::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (ImageTargetBuilder_t518883741), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (FrameQuality_t2061101710)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1998[5] = 
{
	FrameQuality_t2061101710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (CameraDeviceImpl_t610967511), -1, sizeof(CameraDeviceImpl_t610967511_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1999[12] = 
{
	CameraDeviceImpl_t610967511::get_offset_of_mCameraImages_1(),
	CameraDeviceImpl_t610967511::get_offset_of_mForcedCameraFormats_2(),
	CameraDeviceImpl_t610967511_StaticFields::get_offset_of_mWebCam_3(),
	CameraDeviceImpl_t610967511::get_offset_of_mCameraReady_4(),
	CameraDeviceImpl_t610967511::get_offset_of_mIsDirty_5(),
	CameraDeviceImpl_t610967511::get_offset_of_mActualCameraDirection_6(),
	CameraDeviceImpl_t610967511::get_offset_of_mSelectedCameraDirection_7(),
	CameraDeviceImpl_t610967511::get_offset_of_mCameraDeviceMode_8(),
	CameraDeviceImpl_t610967511::get_offset_of_mVideoModeData_9(),
	CameraDeviceImpl_t610967511::get_offset_of_mVideoModeDataNeedsUpdate_10(),
	CameraDeviceImpl_t610967511::get_offset_of_mHasCameraDeviceModeBeenSet_11(),
	CameraDeviceImpl_t610967511::get_offset_of_mCameraActive_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
