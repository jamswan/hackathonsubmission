﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

// UnityEngine.Camera
struct Camera_t189460977;
// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t2687577327;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRIntegrationHelper
struct  VRIntegrationHelper_t556656694  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean VRIntegrationHelper::IsLeft
	bool ___IsLeft_12;
	// UnityEngine.Transform VRIntegrationHelper::TrackableParent
	Transform_t3275118058 * ___TrackableParent_13;

public:
	inline static int32_t get_offset_of_IsLeft_12() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t556656694, ___IsLeft_12)); }
	inline bool get_IsLeft_12() const { return ___IsLeft_12; }
	inline bool* get_address_of_IsLeft_12() { return &___IsLeft_12; }
	inline void set_IsLeft_12(bool value)
	{
		___IsLeft_12 = value;
	}

	inline static int32_t get_offset_of_TrackableParent_13() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t556656694, ___TrackableParent_13)); }
	inline Transform_t3275118058 * get_TrackableParent_13() const { return ___TrackableParent_13; }
	inline Transform_t3275118058 ** get_address_of_TrackableParent_13() { return &___TrackableParent_13; }
	inline void set_TrackableParent_13(Transform_t3275118058 * value)
	{
		___TrackableParent_13 = value;
		Il2CppCodeGenWriteBarrier(&___TrackableParent_13, value);
	}
};

struct VRIntegrationHelper_t556656694_StaticFields
{
public:
	// UnityEngine.Matrix4x4 VRIntegrationHelper::GCLNDKHKABD
	Matrix4x4_t2933234003  ___GCLNDKHKABD_2;
	// UnityEngine.Matrix4x4 VRIntegrationHelper::CAECEMMGAEG
	Matrix4x4_t2933234003  ___CAECEMMGAEG_3;
	// UnityEngine.Camera VRIntegrationHelper::PPMOBGOPIML
	Camera_t189460977 * ___PPMOBGOPIML_4;
	// UnityEngine.Camera VRIntegrationHelper::PGKPEGHLGFI
	Camera_t189460977 * ___PGKPEGHLGFI_5;
	// Vuforia.HideExcessAreaAbstractBehaviour VRIntegrationHelper::BBAAELKOOAF
	HideExcessAreaAbstractBehaviour_t2687577327 * ___BBAAELKOOAF_6;
	// Vuforia.HideExcessAreaAbstractBehaviour VRIntegrationHelper::KAAJFCNDPMM
	HideExcessAreaAbstractBehaviour_t2687577327 * ___KAAJFCNDPMM_7;
	// UnityEngine.Rect VRIntegrationHelper::LCFGMPMDHKK
	Rect_t3681755626  ___LCFGMPMDHKK_8;
	// UnityEngine.Rect VRIntegrationHelper::INDLCFFADED
	Rect_t3681755626  ___INDLCFFADED_9;
	// System.Boolean VRIntegrationHelper::GGHGBJJNCCC
	bool ___GGHGBJJNCCC_10;
	// System.Boolean VRIntegrationHelper::LANCOANLELG
	bool ___LANCOANLELG_11;

public:
	inline static int32_t get_offset_of_GCLNDKHKABD_2() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t556656694_StaticFields, ___GCLNDKHKABD_2)); }
	inline Matrix4x4_t2933234003  get_GCLNDKHKABD_2() const { return ___GCLNDKHKABD_2; }
	inline Matrix4x4_t2933234003 * get_address_of_GCLNDKHKABD_2() { return &___GCLNDKHKABD_2; }
	inline void set_GCLNDKHKABD_2(Matrix4x4_t2933234003  value)
	{
		___GCLNDKHKABD_2 = value;
	}

	inline static int32_t get_offset_of_CAECEMMGAEG_3() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t556656694_StaticFields, ___CAECEMMGAEG_3)); }
	inline Matrix4x4_t2933234003  get_CAECEMMGAEG_3() const { return ___CAECEMMGAEG_3; }
	inline Matrix4x4_t2933234003 * get_address_of_CAECEMMGAEG_3() { return &___CAECEMMGAEG_3; }
	inline void set_CAECEMMGAEG_3(Matrix4x4_t2933234003  value)
	{
		___CAECEMMGAEG_3 = value;
	}

	inline static int32_t get_offset_of_PPMOBGOPIML_4() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t556656694_StaticFields, ___PPMOBGOPIML_4)); }
	inline Camera_t189460977 * get_PPMOBGOPIML_4() const { return ___PPMOBGOPIML_4; }
	inline Camera_t189460977 ** get_address_of_PPMOBGOPIML_4() { return &___PPMOBGOPIML_4; }
	inline void set_PPMOBGOPIML_4(Camera_t189460977 * value)
	{
		___PPMOBGOPIML_4 = value;
		Il2CppCodeGenWriteBarrier(&___PPMOBGOPIML_4, value);
	}

	inline static int32_t get_offset_of_PGKPEGHLGFI_5() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t556656694_StaticFields, ___PGKPEGHLGFI_5)); }
	inline Camera_t189460977 * get_PGKPEGHLGFI_5() const { return ___PGKPEGHLGFI_5; }
	inline Camera_t189460977 ** get_address_of_PGKPEGHLGFI_5() { return &___PGKPEGHLGFI_5; }
	inline void set_PGKPEGHLGFI_5(Camera_t189460977 * value)
	{
		___PGKPEGHLGFI_5 = value;
		Il2CppCodeGenWriteBarrier(&___PGKPEGHLGFI_5, value);
	}

	inline static int32_t get_offset_of_BBAAELKOOAF_6() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t556656694_StaticFields, ___BBAAELKOOAF_6)); }
	inline HideExcessAreaAbstractBehaviour_t2687577327 * get_BBAAELKOOAF_6() const { return ___BBAAELKOOAF_6; }
	inline HideExcessAreaAbstractBehaviour_t2687577327 ** get_address_of_BBAAELKOOAF_6() { return &___BBAAELKOOAF_6; }
	inline void set_BBAAELKOOAF_6(HideExcessAreaAbstractBehaviour_t2687577327 * value)
	{
		___BBAAELKOOAF_6 = value;
		Il2CppCodeGenWriteBarrier(&___BBAAELKOOAF_6, value);
	}

	inline static int32_t get_offset_of_KAAJFCNDPMM_7() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t556656694_StaticFields, ___KAAJFCNDPMM_7)); }
	inline HideExcessAreaAbstractBehaviour_t2687577327 * get_KAAJFCNDPMM_7() const { return ___KAAJFCNDPMM_7; }
	inline HideExcessAreaAbstractBehaviour_t2687577327 ** get_address_of_KAAJFCNDPMM_7() { return &___KAAJFCNDPMM_7; }
	inline void set_KAAJFCNDPMM_7(HideExcessAreaAbstractBehaviour_t2687577327 * value)
	{
		___KAAJFCNDPMM_7 = value;
		Il2CppCodeGenWriteBarrier(&___KAAJFCNDPMM_7, value);
	}

	inline static int32_t get_offset_of_LCFGMPMDHKK_8() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t556656694_StaticFields, ___LCFGMPMDHKK_8)); }
	inline Rect_t3681755626  get_LCFGMPMDHKK_8() const { return ___LCFGMPMDHKK_8; }
	inline Rect_t3681755626 * get_address_of_LCFGMPMDHKK_8() { return &___LCFGMPMDHKK_8; }
	inline void set_LCFGMPMDHKK_8(Rect_t3681755626  value)
	{
		___LCFGMPMDHKK_8 = value;
	}

	inline static int32_t get_offset_of_INDLCFFADED_9() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t556656694_StaticFields, ___INDLCFFADED_9)); }
	inline Rect_t3681755626  get_INDLCFFADED_9() const { return ___INDLCFFADED_9; }
	inline Rect_t3681755626 * get_address_of_INDLCFFADED_9() { return &___INDLCFFADED_9; }
	inline void set_INDLCFFADED_9(Rect_t3681755626  value)
	{
		___INDLCFFADED_9 = value;
	}

	inline static int32_t get_offset_of_GGHGBJJNCCC_10() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t556656694_StaticFields, ___GGHGBJJNCCC_10)); }
	inline bool get_GGHGBJJNCCC_10() const { return ___GGHGBJJNCCC_10; }
	inline bool* get_address_of_GGHGBJJNCCC_10() { return &___GGHGBJJNCCC_10; }
	inline void set_GGHGBJJNCCC_10(bool value)
	{
		___GGHGBJJNCCC_10 = value;
	}

	inline static int32_t get_offset_of_LANCOANLELG_11() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t556656694_StaticFields, ___LANCOANLELG_11)); }
	inline bool get_LANCOANLELG_11() const { return ___LANCOANLELG_11; }
	inline bool* get_address_of_LANCOANLELG_11() { return &___LANCOANLELG_11; }
	inline void set_LANCOANLELG_11(bool value)
	{
		___LANCOANLELG_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
