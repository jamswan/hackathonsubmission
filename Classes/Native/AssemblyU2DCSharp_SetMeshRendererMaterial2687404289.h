﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetMeshRendererMaterial
struct  SetMeshRendererMaterial_t2687404289  : public NarrativeEffect_t3336735121
{
public:
	// UnityEngine.Material SetMeshRendererMaterial::material
	Material_t193706927 * ___material_3;
	// UnityEngine.GameObject SetMeshRendererMaterial::target
	GameObject_t1756533147 * ___target_4;

public:
	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(SetMeshRendererMaterial_t2687404289, ___material_3)); }
	inline Material_t193706927 * get_material_3() const { return ___material_3; }
	inline Material_t193706927 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t193706927 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier(&___material_3, value);
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(SetMeshRendererMaterial_t2687404289, ___target_4)); }
	inline GameObject_t1756533147 * get_target_4() const { return ___target_4; }
	inline GameObject_t1756533147 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(GameObject_t1756533147 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier(&___target_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
