﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// Tether
struct Tether_t4182738604;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TetherActivate
struct  TetherActivate_t3288536165  : public NarrativeEffect_t3336735121
{
public:
	// Tether TetherActivate::tether
	Tether_t4182738604 * ___tether_3;

public:
	inline static int32_t get_offset_of_tether_3() { return static_cast<int32_t>(offsetof(TetherActivate_t3288536165, ___tether_3)); }
	inline Tether_t4182738604 * get_tether_3() const { return ___tether_3; }
	inline Tether_t4182738604 ** get_address_of_tether_3() { return &___tether_3; }
	inline void set_tether_3(Tether_t4182738604 * value)
	{
		___tether_3 = value;
		Il2CppCodeGenWriteBarrier(&___tether_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
