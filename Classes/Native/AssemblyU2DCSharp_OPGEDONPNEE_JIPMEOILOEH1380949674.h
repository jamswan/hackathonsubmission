﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_GvrAudioRoom_LOLCNKGCDKE1199949914.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OPGEDONPNEE/JIPMEOILOEH
struct  JIPMEOILOEH_t1380949674 
{
public:
	// System.Single OPGEDONPNEE/JIPMEOILOEH::OFFLCJMANOF
	float ___OFFLCJMANOF_0;
	// System.Single OPGEDONPNEE/JIPMEOILOEH::KAODMLIEJAI
	float ___KAODMLIEJAI_1;
	// System.Single OPGEDONPNEE/JIPMEOILOEH::BIAPMBHCAGP
	float ___BIAPMBHCAGP_2;
	// System.Single OPGEDONPNEE/JIPMEOILOEH::KDFDEMGLDAK
	float ___KDFDEMGLDAK_3;
	// System.Single OPGEDONPNEE/JIPMEOILOEH::LBIELBFAJFB
	float ___LBIELBFAJFB_4;
	// System.Single OPGEDONPNEE/JIPMEOILOEH::LPGKIDBHBHA
	float ___LPGKIDBHBHA_5;
	// System.Single OPGEDONPNEE/JIPMEOILOEH::JPHAMIOKOGL
	float ___JPHAMIOKOGL_6;
	// System.Single OPGEDONPNEE/JIPMEOILOEH::HCFDHMLHGNB
	float ___HCFDHMLHGNB_7;
	// System.Single OPGEDONPNEE/JIPMEOILOEH::GAHOOADHIFL
	float ___GAHOOADHIFL_8;
	// System.Single OPGEDONPNEE/JIPMEOILOEH::EKMKCCEBADC
	float ___EKMKCCEBADC_9;
	// GvrAudioRoom/LOLCNKGCDKE OPGEDONPNEE/JIPMEOILOEH::KNBPEDDOJCK
	int32_t ___KNBPEDDOJCK_10;
	// GvrAudioRoom/LOLCNKGCDKE OPGEDONPNEE/JIPMEOILOEH::IEEALBNNMKL
	int32_t ___IEEALBNNMKL_11;
	// GvrAudioRoom/LOLCNKGCDKE OPGEDONPNEE/JIPMEOILOEH::HKKEPLFEJKA
	int32_t ___HKKEPLFEJKA_12;
	// GvrAudioRoom/LOLCNKGCDKE OPGEDONPNEE/JIPMEOILOEH::IGMDOGBKGKP
	int32_t ___IGMDOGBKGKP_13;
	// GvrAudioRoom/LOLCNKGCDKE OPGEDONPNEE/JIPMEOILOEH::BCMOEOMGAGH
	int32_t ___BCMOEOMGAGH_14;
	// GvrAudioRoom/LOLCNKGCDKE OPGEDONPNEE/JIPMEOILOEH::DPKBDFEHGJA
	int32_t ___DPKBDFEHGJA_15;
	// System.Single OPGEDONPNEE/JIPMEOILOEH::PBIFAKIBOGA
	float ___PBIFAKIBOGA_16;
	// System.Single OPGEDONPNEE/JIPMEOILOEH::IBKIILPFNME
	float ___IBKIILPFNME_17;
	// System.Single OPGEDONPNEE/JIPMEOILOEH::APCENFFNMBB
	float ___APCENFFNMBB_18;
	// System.Single OPGEDONPNEE/JIPMEOILOEH::AKMHGGLAKCH
	float ___AKMHGGLAKCH_19;

public:
	inline static int32_t get_offset_of_OFFLCJMANOF_0() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___OFFLCJMANOF_0)); }
	inline float get_OFFLCJMANOF_0() const { return ___OFFLCJMANOF_0; }
	inline float* get_address_of_OFFLCJMANOF_0() { return &___OFFLCJMANOF_0; }
	inline void set_OFFLCJMANOF_0(float value)
	{
		___OFFLCJMANOF_0 = value;
	}

	inline static int32_t get_offset_of_KAODMLIEJAI_1() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___KAODMLIEJAI_1)); }
	inline float get_KAODMLIEJAI_1() const { return ___KAODMLIEJAI_1; }
	inline float* get_address_of_KAODMLIEJAI_1() { return &___KAODMLIEJAI_1; }
	inline void set_KAODMLIEJAI_1(float value)
	{
		___KAODMLIEJAI_1 = value;
	}

	inline static int32_t get_offset_of_BIAPMBHCAGP_2() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___BIAPMBHCAGP_2)); }
	inline float get_BIAPMBHCAGP_2() const { return ___BIAPMBHCAGP_2; }
	inline float* get_address_of_BIAPMBHCAGP_2() { return &___BIAPMBHCAGP_2; }
	inline void set_BIAPMBHCAGP_2(float value)
	{
		___BIAPMBHCAGP_2 = value;
	}

	inline static int32_t get_offset_of_KDFDEMGLDAK_3() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___KDFDEMGLDAK_3)); }
	inline float get_KDFDEMGLDAK_3() const { return ___KDFDEMGLDAK_3; }
	inline float* get_address_of_KDFDEMGLDAK_3() { return &___KDFDEMGLDAK_3; }
	inline void set_KDFDEMGLDAK_3(float value)
	{
		___KDFDEMGLDAK_3 = value;
	}

	inline static int32_t get_offset_of_LBIELBFAJFB_4() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___LBIELBFAJFB_4)); }
	inline float get_LBIELBFAJFB_4() const { return ___LBIELBFAJFB_4; }
	inline float* get_address_of_LBIELBFAJFB_4() { return &___LBIELBFAJFB_4; }
	inline void set_LBIELBFAJFB_4(float value)
	{
		___LBIELBFAJFB_4 = value;
	}

	inline static int32_t get_offset_of_LPGKIDBHBHA_5() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___LPGKIDBHBHA_5)); }
	inline float get_LPGKIDBHBHA_5() const { return ___LPGKIDBHBHA_5; }
	inline float* get_address_of_LPGKIDBHBHA_5() { return &___LPGKIDBHBHA_5; }
	inline void set_LPGKIDBHBHA_5(float value)
	{
		___LPGKIDBHBHA_5 = value;
	}

	inline static int32_t get_offset_of_JPHAMIOKOGL_6() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___JPHAMIOKOGL_6)); }
	inline float get_JPHAMIOKOGL_6() const { return ___JPHAMIOKOGL_6; }
	inline float* get_address_of_JPHAMIOKOGL_6() { return &___JPHAMIOKOGL_6; }
	inline void set_JPHAMIOKOGL_6(float value)
	{
		___JPHAMIOKOGL_6 = value;
	}

	inline static int32_t get_offset_of_HCFDHMLHGNB_7() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___HCFDHMLHGNB_7)); }
	inline float get_HCFDHMLHGNB_7() const { return ___HCFDHMLHGNB_7; }
	inline float* get_address_of_HCFDHMLHGNB_7() { return &___HCFDHMLHGNB_7; }
	inline void set_HCFDHMLHGNB_7(float value)
	{
		___HCFDHMLHGNB_7 = value;
	}

	inline static int32_t get_offset_of_GAHOOADHIFL_8() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___GAHOOADHIFL_8)); }
	inline float get_GAHOOADHIFL_8() const { return ___GAHOOADHIFL_8; }
	inline float* get_address_of_GAHOOADHIFL_8() { return &___GAHOOADHIFL_8; }
	inline void set_GAHOOADHIFL_8(float value)
	{
		___GAHOOADHIFL_8 = value;
	}

	inline static int32_t get_offset_of_EKMKCCEBADC_9() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___EKMKCCEBADC_9)); }
	inline float get_EKMKCCEBADC_9() const { return ___EKMKCCEBADC_9; }
	inline float* get_address_of_EKMKCCEBADC_9() { return &___EKMKCCEBADC_9; }
	inline void set_EKMKCCEBADC_9(float value)
	{
		___EKMKCCEBADC_9 = value;
	}

	inline static int32_t get_offset_of_KNBPEDDOJCK_10() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___KNBPEDDOJCK_10)); }
	inline int32_t get_KNBPEDDOJCK_10() const { return ___KNBPEDDOJCK_10; }
	inline int32_t* get_address_of_KNBPEDDOJCK_10() { return &___KNBPEDDOJCK_10; }
	inline void set_KNBPEDDOJCK_10(int32_t value)
	{
		___KNBPEDDOJCK_10 = value;
	}

	inline static int32_t get_offset_of_IEEALBNNMKL_11() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___IEEALBNNMKL_11)); }
	inline int32_t get_IEEALBNNMKL_11() const { return ___IEEALBNNMKL_11; }
	inline int32_t* get_address_of_IEEALBNNMKL_11() { return &___IEEALBNNMKL_11; }
	inline void set_IEEALBNNMKL_11(int32_t value)
	{
		___IEEALBNNMKL_11 = value;
	}

	inline static int32_t get_offset_of_HKKEPLFEJKA_12() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___HKKEPLFEJKA_12)); }
	inline int32_t get_HKKEPLFEJKA_12() const { return ___HKKEPLFEJKA_12; }
	inline int32_t* get_address_of_HKKEPLFEJKA_12() { return &___HKKEPLFEJKA_12; }
	inline void set_HKKEPLFEJKA_12(int32_t value)
	{
		___HKKEPLFEJKA_12 = value;
	}

	inline static int32_t get_offset_of_IGMDOGBKGKP_13() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___IGMDOGBKGKP_13)); }
	inline int32_t get_IGMDOGBKGKP_13() const { return ___IGMDOGBKGKP_13; }
	inline int32_t* get_address_of_IGMDOGBKGKP_13() { return &___IGMDOGBKGKP_13; }
	inline void set_IGMDOGBKGKP_13(int32_t value)
	{
		___IGMDOGBKGKP_13 = value;
	}

	inline static int32_t get_offset_of_BCMOEOMGAGH_14() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___BCMOEOMGAGH_14)); }
	inline int32_t get_BCMOEOMGAGH_14() const { return ___BCMOEOMGAGH_14; }
	inline int32_t* get_address_of_BCMOEOMGAGH_14() { return &___BCMOEOMGAGH_14; }
	inline void set_BCMOEOMGAGH_14(int32_t value)
	{
		___BCMOEOMGAGH_14 = value;
	}

	inline static int32_t get_offset_of_DPKBDFEHGJA_15() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___DPKBDFEHGJA_15)); }
	inline int32_t get_DPKBDFEHGJA_15() const { return ___DPKBDFEHGJA_15; }
	inline int32_t* get_address_of_DPKBDFEHGJA_15() { return &___DPKBDFEHGJA_15; }
	inline void set_DPKBDFEHGJA_15(int32_t value)
	{
		___DPKBDFEHGJA_15 = value;
	}

	inline static int32_t get_offset_of_PBIFAKIBOGA_16() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___PBIFAKIBOGA_16)); }
	inline float get_PBIFAKIBOGA_16() const { return ___PBIFAKIBOGA_16; }
	inline float* get_address_of_PBIFAKIBOGA_16() { return &___PBIFAKIBOGA_16; }
	inline void set_PBIFAKIBOGA_16(float value)
	{
		___PBIFAKIBOGA_16 = value;
	}

	inline static int32_t get_offset_of_IBKIILPFNME_17() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___IBKIILPFNME_17)); }
	inline float get_IBKIILPFNME_17() const { return ___IBKIILPFNME_17; }
	inline float* get_address_of_IBKIILPFNME_17() { return &___IBKIILPFNME_17; }
	inline void set_IBKIILPFNME_17(float value)
	{
		___IBKIILPFNME_17 = value;
	}

	inline static int32_t get_offset_of_APCENFFNMBB_18() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___APCENFFNMBB_18)); }
	inline float get_APCENFFNMBB_18() const { return ___APCENFFNMBB_18; }
	inline float* get_address_of_APCENFFNMBB_18() { return &___APCENFFNMBB_18; }
	inline void set_APCENFFNMBB_18(float value)
	{
		___APCENFFNMBB_18 = value;
	}

	inline static int32_t get_offset_of_AKMHGGLAKCH_19() { return static_cast<int32_t>(offsetof(JIPMEOILOEH_t1380949674, ___AKMHGGLAKCH_19)); }
	inline float get_AKMHGGLAKCH_19() const { return ___AKMHGGLAKCH_19; }
	inline float* get_address_of_AKMHGGLAKCH_19() { return &___AKMHGGLAKCH_19; }
	inline void set_AKMHGGLAKCH_19(float value)
	{
		___AKMHGGLAKCH_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
