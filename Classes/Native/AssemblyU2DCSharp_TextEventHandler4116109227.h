﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Material
struct Material_t193706927;
// System.Collections.Generic.List`1<Vuforia.WordResult>
struct List_1_t1284628329;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;
// UnityEngine.Canvas
struct Canvas_t209405766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextEventHandler
struct  TextEventHandler_t4116109227  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TextEventHandler::DGHIIGFLIBN
	float ___DGHIIGFLIBN_2;
	// System.Single TextEventHandler::CBMMAHGIJDK
	float ___CBMMAHGIJDK_3;
	// System.Single TextEventHandler::GICCCFHNIJP
	float ___GICCCFHNIJP_4;
	// System.Single TextEventHandler::IPPNHBNCMDG
	float ___IPPNHBNCMDG_5;
	// UnityEngine.Color TextEventHandler::EEGCDGMGMCA
	Color_t2020392075  ___EEGCDGMGMCA_6;
	// UnityEngine.Rect TextEventHandler::LIBNLFLPENI
	Rect_t3681755626  ___LIBNLFLPENI_7;
	// UnityEngine.Texture2D TextEventHandler::PGCBBCAOJHI
	Texture2D_t3542995729 * ___PGCBBCAOJHI_8;
	// UnityEngine.Material TextEventHandler::NCEMCAAKJLM
	Material_t193706927 * ___NCEMCAAKJLM_9;
	// System.Boolean TextEventHandler::OIAOLNMENJC
	bool ___OIAOLNMENJC_10;
	// System.Boolean TextEventHandler::PGLHJGGJHEI
	bool ___PGLHJGGJHEI_11;
	// System.Collections.Generic.List`1<Vuforia.WordResult> TextEventHandler::NCNNHEHENKA
	List_1_t1284628329 * ___NCNNHEHENKA_12;
	// UnityEngine.UI.Text[] TextEventHandler::PODOCCPNBFB
	TextU5BU5D_t4216439300* ___PODOCCPNBFB_13;
	// UnityEngine.Material TextEventHandler::boundingBoxMaterial
	Material_t193706927 * ___boundingBoxMaterial_14;
	// UnityEngine.Canvas TextEventHandler::textRecoCanvas
	Canvas_t209405766 * ___textRecoCanvas_15;

public:
	inline static int32_t get_offset_of_DGHIIGFLIBN_2() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___DGHIIGFLIBN_2)); }
	inline float get_DGHIIGFLIBN_2() const { return ___DGHIIGFLIBN_2; }
	inline float* get_address_of_DGHIIGFLIBN_2() { return &___DGHIIGFLIBN_2; }
	inline void set_DGHIIGFLIBN_2(float value)
	{
		___DGHIIGFLIBN_2 = value;
	}

	inline static int32_t get_offset_of_CBMMAHGIJDK_3() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___CBMMAHGIJDK_3)); }
	inline float get_CBMMAHGIJDK_3() const { return ___CBMMAHGIJDK_3; }
	inline float* get_address_of_CBMMAHGIJDK_3() { return &___CBMMAHGIJDK_3; }
	inline void set_CBMMAHGIJDK_3(float value)
	{
		___CBMMAHGIJDK_3 = value;
	}

	inline static int32_t get_offset_of_GICCCFHNIJP_4() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___GICCCFHNIJP_4)); }
	inline float get_GICCCFHNIJP_4() const { return ___GICCCFHNIJP_4; }
	inline float* get_address_of_GICCCFHNIJP_4() { return &___GICCCFHNIJP_4; }
	inline void set_GICCCFHNIJP_4(float value)
	{
		___GICCCFHNIJP_4 = value;
	}

	inline static int32_t get_offset_of_IPPNHBNCMDG_5() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___IPPNHBNCMDG_5)); }
	inline float get_IPPNHBNCMDG_5() const { return ___IPPNHBNCMDG_5; }
	inline float* get_address_of_IPPNHBNCMDG_5() { return &___IPPNHBNCMDG_5; }
	inline void set_IPPNHBNCMDG_5(float value)
	{
		___IPPNHBNCMDG_5 = value;
	}

	inline static int32_t get_offset_of_EEGCDGMGMCA_6() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___EEGCDGMGMCA_6)); }
	inline Color_t2020392075  get_EEGCDGMGMCA_6() const { return ___EEGCDGMGMCA_6; }
	inline Color_t2020392075 * get_address_of_EEGCDGMGMCA_6() { return &___EEGCDGMGMCA_6; }
	inline void set_EEGCDGMGMCA_6(Color_t2020392075  value)
	{
		___EEGCDGMGMCA_6 = value;
	}

	inline static int32_t get_offset_of_LIBNLFLPENI_7() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___LIBNLFLPENI_7)); }
	inline Rect_t3681755626  get_LIBNLFLPENI_7() const { return ___LIBNLFLPENI_7; }
	inline Rect_t3681755626 * get_address_of_LIBNLFLPENI_7() { return &___LIBNLFLPENI_7; }
	inline void set_LIBNLFLPENI_7(Rect_t3681755626  value)
	{
		___LIBNLFLPENI_7 = value;
	}

	inline static int32_t get_offset_of_PGCBBCAOJHI_8() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___PGCBBCAOJHI_8)); }
	inline Texture2D_t3542995729 * get_PGCBBCAOJHI_8() const { return ___PGCBBCAOJHI_8; }
	inline Texture2D_t3542995729 ** get_address_of_PGCBBCAOJHI_8() { return &___PGCBBCAOJHI_8; }
	inline void set_PGCBBCAOJHI_8(Texture2D_t3542995729 * value)
	{
		___PGCBBCAOJHI_8 = value;
		Il2CppCodeGenWriteBarrier(&___PGCBBCAOJHI_8, value);
	}

	inline static int32_t get_offset_of_NCEMCAAKJLM_9() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___NCEMCAAKJLM_9)); }
	inline Material_t193706927 * get_NCEMCAAKJLM_9() const { return ___NCEMCAAKJLM_9; }
	inline Material_t193706927 ** get_address_of_NCEMCAAKJLM_9() { return &___NCEMCAAKJLM_9; }
	inline void set_NCEMCAAKJLM_9(Material_t193706927 * value)
	{
		___NCEMCAAKJLM_9 = value;
		Il2CppCodeGenWriteBarrier(&___NCEMCAAKJLM_9, value);
	}

	inline static int32_t get_offset_of_OIAOLNMENJC_10() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___OIAOLNMENJC_10)); }
	inline bool get_OIAOLNMENJC_10() const { return ___OIAOLNMENJC_10; }
	inline bool* get_address_of_OIAOLNMENJC_10() { return &___OIAOLNMENJC_10; }
	inline void set_OIAOLNMENJC_10(bool value)
	{
		___OIAOLNMENJC_10 = value;
	}

	inline static int32_t get_offset_of_PGLHJGGJHEI_11() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___PGLHJGGJHEI_11)); }
	inline bool get_PGLHJGGJHEI_11() const { return ___PGLHJGGJHEI_11; }
	inline bool* get_address_of_PGLHJGGJHEI_11() { return &___PGLHJGGJHEI_11; }
	inline void set_PGLHJGGJHEI_11(bool value)
	{
		___PGLHJGGJHEI_11 = value;
	}

	inline static int32_t get_offset_of_NCNNHEHENKA_12() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___NCNNHEHENKA_12)); }
	inline List_1_t1284628329 * get_NCNNHEHENKA_12() const { return ___NCNNHEHENKA_12; }
	inline List_1_t1284628329 ** get_address_of_NCNNHEHENKA_12() { return &___NCNNHEHENKA_12; }
	inline void set_NCNNHEHENKA_12(List_1_t1284628329 * value)
	{
		___NCNNHEHENKA_12 = value;
		Il2CppCodeGenWriteBarrier(&___NCNNHEHENKA_12, value);
	}

	inline static int32_t get_offset_of_PODOCCPNBFB_13() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___PODOCCPNBFB_13)); }
	inline TextU5BU5D_t4216439300* get_PODOCCPNBFB_13() const { return ___PODOCCPNBFB_13; }
	inline TextU5BU5D_t4216439300** get_address_of_PODOCCPNBFB_13() { return &___PODOCCPNBFB_13; }
	inline void set_PODOCCPNBFB_13(TextU5BU5D_t4216439300* value)
	{
		___PODOCCPNBFB_13 = value;
		Il2CppCodeGenWriteBarrier(&___PODOCCPNBFB_13, value);
	}

	inline static int32_t get_offset_of_boundingBoxMaterial_14() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___boundingBoxMaterial_14)); }
	inline Material_t193706927 * get_boundingBoxMaterial_14() const { return ___boundingBoxMaterial_14; }
	inline Material_t193706927 ** get_address_of_boundingBoxMaterial_14() { return &___boundingBoxMaterial_14; }
	inline void set_boundingBoxMaterial_14(Material_t193706927 * value)
	{
		___boundingBoxMaterial_14 = value;
		Il2CppCodeGenWriteBarrier(&___boundingBoxMaterial_14, value);
	}

	inline static int32_t get_offset_of_textRecoCanvas_15() { return static_cast<int32_t>(offsetof(TextEventHandler_t4116109227, ___textRecoCanvas_15)); }
	inline Canvas_t209405766 * get_textRecoCanvas_15() const { return ___textRecoCanvas_15; }
	inline Canvas_t209405766 ** get_address_of_textRecoCanvas_15() { return &___textRecoCanvas_15; }
	inline void set_textRecoCanvas_15(Canvas_t209405766 * value)
	{
		___textRecoCanvas_15 = value;
		Il2CppCodeGenWriteBarrier(&___textRecoCanvas_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
