﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopupDuration
struct  PopupDuration_t1273651850  : public NarrativeEffect_t3336735121
{
public:
	// System.String PopupDuration::popupMessage
	String_t* ___popupMessage_3;
	// System.Single PopupDuration::popupDuration
	float ___popupDuration_4;

public:
	inline static int32_t get_offset_of_popupMessage_3() { return static_cast<int32_t>(offsetof(PopupDuration_t1273651850, ___popupMessage_3)); }
	inline String_t* get_popupMessage_3() const { return ___popupMessage_3; }
	inline String_t** get_address_of_popupMessage_3() { return &___popupMessage_3; }
	inline void set_popupMessage_3(String_t* value)
	{
		___popupMessage_3 = value;
		Il2CppCodeGenWriteBarrier(&___popupMessage_3, value);
	}

	inline static int32_t get_offset_of_popupDuration_4() { return static_cast<int32_t>(offsetof(PopupDuration_t1273651850, ___popupDuration_4)); }
	inline float get_popupDuration_4() const { return ___popupDuration_4; }
	inline float* get_address_of_popupDuration_4() { return &___popupDuration_4; }
	inline void set_popupDuration_4(float value)
	{
		___popupDuration_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
