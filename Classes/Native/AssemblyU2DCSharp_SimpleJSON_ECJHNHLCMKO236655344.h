﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SimpleJSON_GFGBGCMOLKN3233773149.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.ECJHNHLCMKO
struct  ECJHNHLCMKO_t236655344  : public GFGBGCMOLKN_t3233773149
{
public:
	// System.String SimpleJSON.ECJHNHLCMKO::IIMABKDGIKG
	String_t* ___IIMABKDGIKG_0;

public:
	inline static int32_t get_offset_of_IIMABKDGIKG_0() { return static_cast<int32_t>(offsetof(ECJHNHLCMKO_t236655344, ___IIMABKDGIKG_0)); }
	inline String_t* get_IIMABKDGIKG_0() const { return ___IIMABKDGIKG_0; }
	inline String_t** get_address_of_IIMABKDGIKG_0() { return &___IIMABKDGIKG_0; }
	inline void set_IIMABKDGIKG_0(String_t* value)
	{
		___IIMABKDGIKG_0 = value;
		Il2CppCodeGenWriteBarrier(&___IIMABKDGIKG_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
