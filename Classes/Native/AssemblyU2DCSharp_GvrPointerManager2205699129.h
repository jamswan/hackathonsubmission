﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GvrPointerManager
struct GvrPointerManager_t2205699129;
// FPFPADIHBIM
struct FPFPADIHBIM_t767626572;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerManager
struct  GvrPointerManager_t2205699129  : public MonoBehaviour_t1158329972
{
public:
	// FPFPADIHBIM GvrPointerManager::PGDIBLKFBDK
	FPFPADIHBIM_t767626572 * ___PGDIBLKFBDK_3;

public:
	inline static int32_t get_offset_of_PGDIBLKFBDK_3() { return static_cast<int32_t>(offsetof(GvrPointerManager_t2205699129, ___PGDIBLKFBDK_3)); }
	inline FPFPADIHBIM_t767626572 * get_PGDIBLKFBDK_3() const { return ___PGDIBLKFBDK_3; }
	inline FPFPADIHBIM_t767626572 ** get_address_of_PGDIBLKFBDK_3() { return &___PGDIBLKFBDK_3; }
	inline void set_PGDIBLKFBDK_3(FPFPADIHBIM_t767626572 * value)
	{
		___PGDIBLKFBDK_3 = value;
		Il2CppCodeGenWriteBarrier(&___PGDIBLKFBDK_3, value);
	}
};

struct GvrPointerManager_t2205699129_StaticFields
{
public:
	// GvrPointerManager GvrPointerManager::OLMOHEEKDMC
	GvrPointerManager_t2205699129 * ___OLMOHEEKDMC_2;

public:
	inline static int32_t get_offset_of_OLMOHEEKDMC_2() { return static_cast<int32_t>(offsetof(GvrPointerManager_t2205699129_StaticFields, ___OLMOHEEKDMC_2)); }
	inline GvrPointerManager_t2205699129 * get_OLMOHEEKDMC_2() const { return ___OLMOHEEKDMC_2; }
	inline GvrPointerManager_t2205699129 ** get_address_of_OLMOHEEKDMC_2() { return &___OLMOHEEKDMC_2; }
	inline void set_OLMOHEEKDMC_2(GvrPointerManager_t2205699129 * value)
	{
		___OLMOHEEKDMC_2 = value;
		Il2CppCodeGenWriteBarrier(&___OLMOHEEKDMC_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
