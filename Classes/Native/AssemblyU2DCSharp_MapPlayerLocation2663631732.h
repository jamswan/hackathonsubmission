﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapPlayerLocation
struct  MapPlayerLocation_t2663631732  : public NarrativeEffect_t3336735121
{
public:
	// System.String MapPlayerLocation::locationName
	String_t* ___locationName_3;

public:
	inline static int32_t get_offset_of_locationName_3() { return static_cast<int32_t>(offsetof(MapPlayerLocation_t2663631732, ___locationName_3)); }
	inline String_t* get_locationName_3() const { return ___locationName_3; }
	inline String_t** get_address_of_locationName_3() { return &___locationName_3; }
	inline void set_locationName_3(String_t* value)
	{
		___locationName_3 = value;
		Il2CppCodeGenWriteBarrier(&___locationName_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
