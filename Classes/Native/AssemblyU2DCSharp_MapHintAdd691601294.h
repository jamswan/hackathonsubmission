﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapHintAdd
struct  MapHintAdd_t691601294  : public NarrativeEffect_t3336735121
{
public:
	// System.String MapHintAdd::locationName
	String_t* ___locationName_3;
	// System.String MapHintAdd::hintText
	String_t* ___hintText_4;

public:
	inline static int32_t get_offset_of_locationName_3() { return static_cast<int32_t>(offsetof(MapHintAdd_t691601294, ___locationName_3)); }
	inline String_t* get_locationName_3() const { return ___locationName_3; }
	inline String_t** get_address_of_locationName_3() { return &___locationName_3; }
	inline void set_locationName_3(String_t* value)
	{
		___locationName_3 = value;
		Il2CppCodeGenWriteBarrier(&___locationName_3, value);
	}

	inline static int32_t get_offset_of_hintText_4() { return static_cast<int32_t>(offsetof(MapHintAdd_t691601294, ___hintText_4)); }
	inline String_t* get_hintText_4() const { return ___hintText_4; }
	inline String_t** get_address_of_hintText_4() { return &___hintText_4; }
	inline void set_hintText_4(String_t* value)
	{
		___hintText_4 = value;
		Il2CppCodeGenWriteBarrier(&___hintText_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
