﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapDrag
struct  MapDrag_t1912005084  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform MapDrag::KJLBEHOKPME
	Transform_t3275118058 * ___KJLBEHOKPME_2;
	// UnityEngine.Vector3 MapDrag::BBMILCGFAEN
	Vector3_t2243707580  ___BBMILCGFAEN_3;
	// UnityEngine.Vector3 MapDrag::KHFIGCOODNA
	Vector3_t2243707580  ___KHFIGCOODNA_4;

public:
	inline static int32_t get_offset_of_KJLBEHOKPME_2() { return static_cast<int32_t>(offsetof(MapDrag_t1912005084, ___KJLBEHOKPME_2)); }
	inline Transform_t3275118058 * get_KJLBEHOKPME_2() const { return ___KJLBEHOKPME_2; }
	inline Transform_t3275118058 ** get_address_of_KJLBEHOKPME_2() { return &___KJLBEHOKPME_2; }
	inline void set_KJLBEHOKPME_2(Transform_t3275118058 * value)
	{
		___KJLBEHOKPME_2 = value;
		Il2CppCodeGenWriteBarrier(&___KJLBEHOKPME_2, value);
	}

	inline static int32_t get_offset_of_BBMILCGFAEN_3() { return static_cast<int32_t>(offsetof(MapDrag_t1912005084, ___BBMILCGFAEN_3)); }
	inline Vector3_t2243707580  get_BBMILCGFAEN_3() const { return ___BBMILCGFAEN_3; }
	inline Vector3_t2243707580 * get_address_of_BBMILCGFAEN_3() { return &___BBMILCGFAEN_3; }
	inline void set_BBMILCGFAEN_3(Vector3_t2243707580  value)
	{
		___BBMILCGFAEN_3 = value;
	}

	inline static int32_t get_offset_of_KHFIGCOODNA_4() { return static_cast<int32_t>(offsetof(MapDrag_t1912005084, ___KHFIGCOODNA_4)); }
	inline Vector3_t2243707580  get_KHFIGCOODNA_4() const { return ___KHFIGCOODNA_4; }
	inline Vector3_t2243707580 * get_address_of_KHFIGCOODNA_4() { return &___KHFIGCOODNA_4; }
	inline void set_KHFIGCOODNA_4(Vector3_t2243707580  value)
	{
		___KHFIGCOODNA_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
