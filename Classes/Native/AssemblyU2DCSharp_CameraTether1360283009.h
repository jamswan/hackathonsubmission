﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Tether4182738604.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraTether
struct  CameraTether_t1360283009  : public Tether_t4182738604
{
public:
	// System.Boolean CameraTether::addOnStart
	bool ___addOnStart_6;

public:
	inline static int32_t get_offset_of_addOnStart_6() { return static_cast<int32_t>(offsetof(CameraTether_t1360283009, ___addOnStart_6)); }
	inline bool get_addOnStart_6() const { return ___addOnStart_6; }
	inline bool* get_address_of_addOnStart_6() { return &___addOnStart_6; }
	inline void set_addOnStart_6(bool value)
	{
		___addOnStart_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
