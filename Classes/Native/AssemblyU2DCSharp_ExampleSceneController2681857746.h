﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GimbalBehavior
struct GimbalBehavior_t2141330552;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.Collections.Generic.Dictionary`2<System.String,KMMIOPFCKMA>
struct Dictionary_2_t1578535683;
// System.Collections.Generic.Dictionary`2<System.String,BKNEONNPPOE>
struct Dictionary_2_t1655568071;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleSceneController
struct  ExampleSceneController_t2681857746  : public MonoBehaviour_t1158329972
{
public:
	// GimbalBehavior ExampleSceneController::gimbalBehavior
	GimbalBehavior_t2141330552 * ___gimbalBehavior_2;
	// UnityEngine.UI.Text ExampleSceneController::beaconStatusText
	Text_t356221433 * ___beaconStatusText_3;
	// UnityEngine.UI.Text ExampleSceneController::placeStatusText
	Text_t356221433 * ___placeStatusText_4;
	// UnityEngine.UI.Text ExampleSceneController::beaconsFoundText
	Text_t356221433 * ___beaconsFoundText_5;
	// UnityEngine.UI.Text ExampleSceneController::placesVisitingText
	Text_t356221433 * ___placesVisitingText_6;
	// UnityEngine.UI.Text ExampleSceneController::placesVisitedText
	Text_t356221433 * ___placesVisitedText_7;
	// UnityEngine.UI.Button ExampleSceneController::togglePlacesManagerButton
	Button_t2872111280 * ___togglePlacesManagerButton_8;
	// UnityEngine.UI.Button ExampleSceneController::toggleBeaconManagerButton
	Button_t2872111280 * ___toggleBeaconManagerButton_9;
	// System.Collections.Generic.Dictionary`2<System.String,KMMIOPFCKMA> ExampleSceneController::BDGBMPDNEAF
	Dictionary_2_t1578535683 * ___BDGBMPDNEAF_10;
	// System.Collections.Generic.Dictionary`2<System.String,BKNEONNPPOE> ExampleSceneController::PABHOAJMGFB
	Dictionary_2_t1655568071 * ___PABHOAJMGFB_11;
	// System.Collections.Generic.Dictionary`2<System.String,BKNEONNPPOE> ExampleSceneController::NNHPNBALPEE
	Dictionary_2_t1655568071 * ___NNHPNBALPEE_12;
	// System.Boolean ExampleSceneController::PPLIEHFLJPD
	bool ___PPLIEHFLJPD_13;

public:
	inline static int32_t get_offset_of_gimbalBehavior_2() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2681857746, ___gimbalBehavior_2)); }
	inline GimbalBehavior_t2141330552 * get_gimbalBehavior_2() const { return ___gimbalBehavior_2; }
	inline GimbalBehavior_t2141330552 ** get_address_of_gimbalBehavior_2() { return &___gimbalBehavior_2; }
	inline void set_gimbalBehavior_2(GimbalBehavior_t2141330552 * value)
	{
		___gimbalBehavior_2 = value;
		Il2CppCodeGenWriteBarrier(&___gimbalBehavior_2, value);
	}

	inline static int32_t get_offset_of_beaconStatusText_3() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2681857746, ___beaconStatusText_3)); }
	inline Text_t356221433 * get_beaconStatusText_3() const { return ___beaconStatusText_3; }
	inline Text_t356221433 ** get_address_of_beaconStatusText_3() { return &___beaconStatusText_3; }
	inline void set_beaconStatusText_3(Text_t356221433 * value)
	{
		___beaconStatusText_3 = value;
		Il2CppCodeGenWriteBarrier(&___beaconStatusText_3, value);
	}

	inline static int32_t get_offset_of_placeStatusText_4() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2681857746, ___placeStatusText_4)); }
	inline Text_t356221433 * get_placeStatusText_4() const { return ___placeStatusText_4; }
	inline Text_t356221433 ** get_address_of_placeStatusText_4() { return &___placeStatusText_4; }
	inline void set_placeStatusText_4(Text_t356221433 * value)
	{
		___placeStatusText_4 = value;
		Il2CppCodeGenWriteBarrier(&___placeStatusText_4, value);
	}

	inline static int32_t get_offset_of_beaconsFoundText_5() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2681857746, ___beaconsFoundText_5)); }
	inline Text_t356221433 * get_beaconsFoundText_5() const { return ___beaconsFoundText_5; }
	inline Text_t356221433 ** get_address_of_beaconsFoundText_5() { return &___beaconsFoundText_5; }
	inline void set_beaconsFoundText_5(Text_t356221433 * value)
	{
		___beaconsFoundText_5 = value;
		Il2CppCodeGenWriteBarrier(&___beaconsFoundText_5, value);
	}

	inline static int32_t get_offset_of_placesVisitingText_6() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2681857746, ___placesVisitingText_6)); }
	inline Text_t356221433 * get_placesVisitingText_6() const { return ___placesVisitingText_6; }
	inline Text_t356221433 ** get_address_of_placesVisitingText_6() { return &___placesVisitingText_6; }
	inline void set_placesVisitingText_6(Text_t356221433 * value)
	{
		___placesVisitingText_6 = value;
		Il2CppCodeGenWriteBarrier(&___placesVisitingText_6, value);
	}

	inline static int32_t get_offset_of_placesVisitedText_7() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2681857746, ___placesVisitedText_7)); }
	inline Text_t356221433 * get_placesVisitedText_7() const { return ___placesVisitedText_7; }
	inline Text_t356221433 ** get_address_of_placesVisitedText_7() { return &___placesVisitedText_7; }
	inline void set_placesVisitedText_7(Text_t356221433 * value)
	{
		___placesVisitedText_7 = value;
		Il2CppCodeGenWriteBarrier(&___placesVisitedText_7, value);
	}

	inline static int32_t get_offset_of_togglePlacesManagerButton_8() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2681857746, ___togglePlacesManagerButton_8)); }
	inline Button_t2872111280 * get_togglePlacesManagerButton_8() const { return ___togglePlacesManagerButton_8; }
	inline Button_t2872111280 ** get_address_of_togglePlacesManagerButton_8() { return &___togglePlacesManagerButton_8; }
	inline void set_togglePlacesManagerButton_8(Button_t2872111280 * value)
	{
		___togglePlacesManagerButton_8 = value;
		Il2CppCodeGenWriteBarrier(&___togglePlacesManagerButton_8, value);
	}

	inline static int32_t get_offset_of_toggleBeaconManagerButton_9() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2681857746, ___toggleBeaconManagerButton_9)); }
	inline Button_t2872111280 * get_toggleBeaconManagerButton_9() const { return ___toggleBeaconManagerButton_9; }
	inline Button_t2872111280 ** get_address_of_toggleBeaconManagerButton_9() { return &___toggleBeaconManagerButton_9; }
	inline void set_toggleBeaconManagerButton_9(Button_t2872111280 * value)
	{
		___toggleBeaconManagerButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___toggleBeaconManagerButton_9, value);
	}

	inline static int32_t get_offset_of_BDGBMPDNEAF_10() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2681857746, ___BDGBMPDNEAF_10)); }
	inline Dictionary_2_t1578535683 * get_BDGBMPDNEAF_10() const { return ___BDGBMPDNEAF_10; }
	inline Dictionary_2_t1578535683 ** get_address_of_BDGBMPDNEAF_10() { return &___BDGBMPDNEAF_10; }
	inline void set_BDGBMPDNEAF_10(Dictionary_2_t1578535683 * value)
	{
		___BDGBMPDNEAF_10 = value;
		Il2CppCodeGenWriteBarrier(&___BDGBMPDNEAF_10, value);
	}

	inline static int32_t get_offset_of_PABHOAJMGFB_11() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2681857746, ___PABHOAJMGFB_11)); }
	inline Dictionary_2_t1655568071 * get_PABHOAJMGFB_11() const { return ___PABHOAJMGFB_11; }
	inline Dictionary_2_t1655568071 ** get_address_of_PABHOAJMGFB_11() { return &___PABHOAJMGFB_11; }
	inline void set_PABHOAJMGFB_11(Dictionary_2_t1655568071 * value)
	{
		___PABHOAJMGFB_11 = value;
		Il2CppCodeGenWriteBarrier(&___PABHOAJMGFB_11, value);
	}

	inline static int32_t get_offset_of_NNHPNBALPEE_12() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2681857746, ___NNHPNBALPEE_12)); }
	inline Dictionary_2_t1655568071 * get_NNHPNBALPEE_12() const { return ___NNHPNBALPEE_12; }
	inline Dictionary_2_t1655568071 ** get_address_of_NNHPNBALPEE_12() { return &___NNHPNBALPEE_12; }
	inline void set_NNHPNBALPEE_12(Dictionary_2_t1655568071 * value)
	{
		___NNHPNBALPEE_12 = value;
		Il2CppCodeGenWriteBarrier(&___NNHPNBALPEE_12, value);
	}

	inline static int32_t get_offset_of_PPLIEHFLJPD_13() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2681857746, ___PPLIEHFLJPD_13)); }
	inline bool get_PPLIEHFLJPD_13() const { return ___PPLIEHFLJPD_13; }
	inline bool* get_address_of_PPLIEHFLJPD_13() { return &___PPLIEHFLJPD_13; }
	inline void set_PPLIEHFLJPD_13(bool value)
	{
		___PPLIEHFLJPD_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
