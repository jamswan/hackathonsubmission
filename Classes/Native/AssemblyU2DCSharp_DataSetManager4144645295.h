﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Singleton_1_gen1025562386.h"

// LoadedDataSet[]
struct LoadedDataSetU5BU5D_t459111350;
// Vuforia.DataSet
struct DataSet_t626511550;
// Vuforia.DatabaseLoadAbstractBehaviour
struct DatabaseLoadAbstractBehaviour_t1458632096;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataSetManager
struct  DataSetManager_t4144645295  : public Singleton_1_t1025562386
{
public:
	// LoadedDataSet[] DataSetManager::loadedDataSets
	LoadedDataSetU5BU5D_t459111350* ___loadedDataSets_5;
	// Vuforia.DataSet DataSetManager::aux
	DataSet_t626511550 * ___aux_6;
	// Vuforia.DatabaseLoadAbstractBehaviour DataSetManager::databaseLoad
	DatabaseLoadAbstractBehaviour_t1458632096 * ___databaseLoad_7;

public:
	inline static int32_t get_offset_of_loadedDataSets_5() { return static_cast<int32_t>(offsetof(DataSetManager_t4144645295, ___loadedDataSets_5)); }
	inline LoadedDataSetU5BU5D_t459111350* get_loadedDataSets_5() const { return ___loadedDataSets_5; }
	inline LoadedDataSetU5BU5D_t459111350** get_address_of_loadedDataSets_5() { return &___loadedDataSets_5; }
	inline void set_loadedDataSets_5(LoadedDataSetU5BU5D_t459111350* value)
	{
		___loadedDataSets_5 = value;
		Il2CppCodeGenWriteBarrier(&___loadedDataSets_5, value);
	}

	inline static int32_t get_offset_of_aux_6() { return static_cast<int32_t>(offsetof(DataSetManager_t4144645295, ___aux_6)); }
	inline DataSet_t626511550 * get_aux_6() const { return ___aux_6; }
	inline DataSet_t626511550 ** get_address_of_aux_6() { return &___aux_6; }
	inline void set_aux_6(DataSet_t626511550 * value)
	{
		___aux_6 = value;
		Il2CppCodeGenWriteBarrier(&___aux_6, value);
	}

	inline static int32_t get_offset_of_databaseLoad_7() { return static_cast<int32_t>(offsetof(DataSetManager_t4144645295, ___databaseLoad_7)); }
	inline DatabaseLoadAbstractBehaviour_t1458632096 * get_databaseLoad_7() const { return ___databaseLoad_7; }
	inline DatabaseLoadAbstractBehaviour_t1458632096 ** get_address_of_databaseLoad_7() { return &___databaseLoad_7; }
	inline void set_databaseLoad_7(DatabaseLoadAbstractBehaviour_t1458632096 * value)
	{
		___databaseLoad_7 = value;
		Il2CppCodeGenWriteBarrier(&___databaseLoad_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
