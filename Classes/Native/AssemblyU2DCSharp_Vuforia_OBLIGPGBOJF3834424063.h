﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_ScreenOrientation4019489636.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.OBLIGPGBOJF
struct  OBLIGPGBOJF_t3834424063  : public Il2CppObject
{
public:
	// UnityEngine.ScreenOrientation Vuforia.OBLIGPGBOJF::KJNPPDBFOMG
	int32_t ___KJNPPDBFOMG_2;
	// UnityEngine.ScreenOrientation Vuforia.OBLIGPGBOJF::POMHIFIBILE
	int32_t ___POMHIFIBILE_3;
	// System.Int32 Vuforia.OBLIGPGBOJF::IDCLCHMDDKB
	int32_t ___IDCLCHMDDKB_4;
	// System.Int32 Vuforia.OBLIGPGBOJF::IAFOLDJGNBL
	int32_t ___IAFOLDJGNBL_5;

public:
	inline static int32_t get_offset_of_KJNPPDBFOMG_2() { return static_cast<int32_t>(offsetof(OBLIGPGBOJF_t3834424063, ___KJNPPDBFOMG_2)); }
	inline int32_t get_KJNPPDBFOMG_2() const { return ___KJNPPDBFOMG_2; }
	inline int32_t* get_address_of_KJNPPDBFOMG_2() { return &___KJNPPDBFOMG_2; }
	inline void set_KJNPPDBFOMG_2(int32_t value)
	{
		___KJNPPDBFOMG_2 = value;
	}

	inline static int32_t get_offset_of_POMHIFIBILE_3() { return static_cast<int32_t>(offsetof(OBLIGPGBOJF_t3834424063, ___POMHIFIBILE_3)); }
	inline int32_t get_POMHIFIBILE_3() const { return ___POMHIFIBILE_3; }
	inline int32_t* get_address_of_POMHIFIBILE_3() { return &___POMHIFIBILE_3; }
	inline void set_POMHIFIBILE_3(int32_t value)
	{
		___POMHIFIBILE_3 = value;
	}

	inline static int32_t get_offset_of_IDCLCHMDDKB_4() { return static_cast<int32_t>(offsetof(OBLIGPGBOJF_t3834424063, ___IDCLCHMDDKB_4)); }
	inline int32_t get_IDCLCHMDDKB_4() const { return ___IDCLCHMDDKB_4; }
	inline int32_t* get_address_of_IDCLCHMDDKB_4() { return &___IDCLCHMDDKB_4; }
	inline void set_IDCLCHMDDKB_4(int32_t value)
	{
		___IDCLCHMDDKB_4 = value;
	}

	inline static int32_t get_offset_of_IAFOLDJGNBL_5() { return static_cast<int32_t>(offsetof(OBLIGPGBOJF_t3834424063, ___IAFOLDJGNBL_5)); }
	inline int32_t get_IAFOLDJGNBL_5() const { return ___IAFOLDJGNBL_5; }
	inline int32_t* get_address_of_IAFOLDJGNBL_5() { return &___IAFOLDJGNBL_5; }
	inline void set_IAFOLDJGNBL_5(int32_t value)
	{
		___IAFOLDJGNBL_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
