﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Gene2979169773.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Type1530480861.h"

// proto.PhoneEvent
struct PhoneEvent_t2572128318;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// proto.PhoneEvent/Types/MotionEvent
struct MotionEvent_t4072706903;
// proto.PhoneEvent/Types/GyroscopeEvent
struct GyroscopeEvent_t182225200;
// proto.PhoneEvent/Types/AccelerometerEvent
struct AccelerometerEvent_t1893725728;
// proto.PhoneEvent/Types/DepthMapEvent
struct DepthMapEvent_t1516604558;
// proto.PhoneEvent/Types/OrientationEvent
struct OrientationEvent_t2038376807;
// proto.PhoneEvent/Types/KeyEvent
struct KeyEvent_t639576718;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent
struct  PhoneEvent_t2572128318  : public GeneratedMessageLite_2_t2979169773
{
public:
	// System.Boolean proto.PhoneEvent::NIHNHFPILMA
	bool ___NIHNHFPILMA_4;
	// proto.PhoneEvent/Types/Type proto.PhoneEvent::DOINLOMPNMA
	int32_t ___DOINLOMPNMA_5;
	// System.Boolean proto.PhoneEvent::FGGOIEMNPLG
	bool ___FGGOIEMNPLG_7;
	// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent::PNANHMPMHBN
	MotionEvent_t4072706903 * ___PNANHMPMHBN_8;
	// System.Boolean proto.PhoneEvent::JHDDFCJDKAG
	bool ___JHDDFCJDKAG_10;
	// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent::GPFHMBPFHCD
	GyroscopeEvent_t182225200 * ___GPFHMBPFHCD_11;
	// System.Boolean proto.PhoneEvent::JEIKFAMKNBC
	bool ___JEIKFAMKNBC_13;
	// proto.PhoneEvent/Types/AccelerometerEvent proto.PhoneEvent::JKIMNBPOOFH
	AccelerometerEvent_t1893725728 * ___JKIMNBPOOFH_14;
	// System.Boolean proto.PhoneEvent::PMAMJBPPGBG
	bool ___PMAMJBPPGBG_16;
	// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent::CHGLJAKDIOC
	DepthMapEvent_t1516604558 * ___CHGLJAKDIOC_17;
	// System.Boolean proto.PhoneEvent::MCCAEHJIOKD
	bool ___MCCAEHJIOKD_19;
	// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent::KOIDJIKCLAJ
	OrientationEvent_t2038376807 * ___KOIDJIKCLAJ_20;
	// System.Boolean proto.PhoneEvent::DNDECDHEFBH
	bool ___DNDECDHEFBH_22;
	// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent::EFIOHEECJKI
	KeyEvent_t639576718 * ___EFIOHEECJKI_23;
	// System.Int32 proto.PhoneEvent::BABCKJCECDJ
	int32_t ___BABCKJCECDJ_24;

public:
	inline static int32_t get_offset_of_NIHNHFPILMA_4() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___NIHNHFPILMA_4)); }
	inline bool get_NIHNHFPILMA_4() const { return ___NIHNHFPILMA_4; }
	inline bool* get_address_of_NIHNHFPILMA_4() { return &___NIHNHFPILMA_4; }
	inline void set_NIHNHFPILMA_4(bool value)
	{
		___NIHNHFPILMA_4 = value;
	}

	inline static int32_t get_offset_of_DOINLOMPNMA_5() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___DOINLOMPNMA_5)); }
	inline int32_t get_DOINLOMPNMA_5() const { return ___DOINLOMPNMA_5; }
	inline int32_t* get_address_of_DOINLOMPNMA_5() { return &___DOINLOMPNMA_5; }
	inline void set_DOINLOMPNMA_5(int32_t value)
	{
		___DOINLOMPNMA_5 = value;
	}

	inline static int32_t get_offset_of_FGGOIEMNPLG_7() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___FGGOIEMNPLG_7)); }
	inline bool get_FGGOIEMNPLG_7() const { return ___FGGOIEMNPLG_7; }
	inline bool* get_address_of_FGGOIEMNPLG_7() { return &___FGGOIEMNPLG_7; }
	inline void set_FGGOIEMNPLG_7(bool value)
	{
		___FGGOIEMNPLG_7 = value;
	}

	inline static int32_t get_offset_of_PNANHMPMHBN_8() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___PNANHMPMHBN_8)); }
	inline MotionEvent_t4072706903 * get_PNANHMPMHBN_8() const { return ___PNANHMPMHBN_8; }
	inline MotionEvent_t4072706903 ** get_address_of_PNANHMPMHBN_8() { return &___PNANHMPMHBN_8; }
	inline void set_PNANHMPMHBN_8(MotionEvent_t4072706903 * value)
	{
		___PNANHMPMHBN_8 = value;
		Il2CppCodeGenWriteBarrier(&___PNANHMPMHBN_8, value);
	}

	inline static int32_t get_offset_of_JHDDFCJDKAG_10() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___JHDDFCJDKAG_10)); }
	inline bool get_JHDDFCJDKAG_10() const { return ___JHDDFCJDKAG_10; }
	inline bool* get_address_of_JHDDFCJDKAG_10() { return &___JHDDFCJDKAG_10; }
	inline void set_JHDDFCJDKAG_10(bool value)
	{
		___JHDDFCJDKAG_10 = value;
	}

	inline static int32_t get_offset_of_GPFHMBPFHCD_11() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___GPFHMBPFHCD_11)); }
	inline GyroscopeEvent_t182225200 * get_GPFHMBPFHCD_11() const { return ___GPFHMBPFHCD_11; }
	inline GyroscopeEvent_t182225200 ** get_address_of_GPFHMBPFHCD_11() { return &___GPFHMBPFHCD_11; }
	inline void set_GPFHMBPFHCD_11(GyroscopeEvent_t182225200 * value)
	{
		___GPFHMBPFHCD_11 = value;
		Il2CppCodeGenWriteBarrier(&___GPFHMBPFHCD_11, value);
	}

	inline static int32_t get_offset_of_JEIKFAMKNBC_13() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___JEIKFAMKNBC_13)); }
	inline bool get_JEIKFAMKNBC_13() const { return ___JEIKFAMKNBC_13; }
	inline bool* get_address_of_JEIKFAMKNBC_13() { return &___JEIKFAMKNBC_13; }
	inline void set_JEIKFAMKNBC_13(bool value)
	{
		___JEIKFAMKNBC_13 = value;
	}

	inline static int32_t get_offset_of_JKIMNBPOOFH_14() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___JKIMNBPOOFH_14)); }
	inline AccelerometerEvent_t1893725728 * get_JKIMNBPOOFH_14() const { return ___JKIMNBPOOFH_14; }
	inline AccelerometerEvent_t1893725728 ** get_address_of_JKIMNBPOOFH_14() { return &___JKIMNBPOOFH_14; }
	inline void set_JKIMNBPOOFH_14(AccelerometerEvent_t1893725728 * value)
	{
		___JKIMNBPOOFH_14 = value;
		Il2CppCodeGenWriteBarrier(&___JKIMNBPOOFH_14, value);
	}

	inline static int32_t get_offset_of_PMAMJBPPGBG_16() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___PMAMJBPPGBG_16)); }
	inline bool get_PMAMJBPPGBG_16() const { return ___PMAMJBPPGBG_16; }
	inline bool* get_address_of_PMAMJBPPGBG_16() { return &___PMAMJBPPGBG_16; }
	inline void set_PMAMJBPPGBG_16(bool value)
	{
		___PMAMJBPPGBG_16 = value;
	}

	inline static int32_t get_offset_of_CHGLJAKDIOC_17() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___CHGLJAKDIOC_17)); }
	inline DepthMapEvent_t1516604558 * get_CHGLJAKDIOC_17() const { return ___CHGLJAKDIOC_17; }
	inline DepthMapEvent_t1516604558 ** get_address_of_CHGLJAKDIOC_17() { return &___CHGLJAKDIOC_17; }
	inline void set_CHGLJAKDIOC_17(DepthMapEvent_t1516604558 * value)
	{
		___CHGLJAKDIOC_17 = value;
		Il2CppCodeGenWriteBarrier(&___CHGLJAKDIOC_17, value);
	}

	inline static int32_t get_offset_of_MCCAEHJIOKD_19() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___MCCAEHJIOKD_19)); }
	inline bool get_MCCAEHJIOKD_19() const { return ___MCCAEHJIOKD_19; }
	inline bool* get_address_of_MCCAEHJIOKD_19() { return &___MCCAEHJIOKD_19; }
	inline void set_MCCAEHJIOKD_19(bool value)
	{
		___MCCAEHJIOKD_19 = value;
	}

	inline static int32_t get_offset_of_KOIDJIKCLAJ_20() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___KOIDJIKCLAJ_20)); }
	inline OrientationEvent_t2038376807 * get_KOIDJIKCLAJ_20() const { return ___KOIDJIKCLAJ_20; }
	inline OrientationEvent_t2038376807 ** get_address_of_KOIDJIKCLAJ_20() { return &___KOIDJIKCLAJ_20; }
	inline void set_KOIDJIKCLAJ_20(OrientationEvent_t2038376807 * value)
	{
		___KOIDJIKCLAJ_20 = value;
		Il2CppCodeGenWriteBarrier(&___KOIDJIKCLAJ_20, value);
	}

	inline static int32_t get_offset_of_DNDECDHEFBH_22() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___DNDECDHEFBH_22)); }
	inline bool get_DNDECDHEFBH_22() const { return ___DNDECDHEFBH_22; }
	inline bool* get_address_of_DNDECDHEFBH_22() { return &___DNDECDHEFBH_22; }
	inline void set_DNDECDHEFBH_22(bool value)
	{
		___DNDECDHEFBH_22 = value;
	}

	inline static int32_t get_offset_of_EFIOHEECJKI_23() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___EFIOHEECJKI_23)); }
	inline KeyEvent_t639576718 * get_EFIOHEECJKI_23() const { return ___EFIOHEECJKI_23; }
	inline KeyEvent_t639576718 ** get_address_of_EFIOHEECJKI_23() { return &___EFIOHEECJKI_23; }
	inline void set_EFIOHEECJKI_23(KeyEvent_t639576718 * value)
	{
		___EFIOHEECJKI_23 = value;
		Il2CppCodeGenWriteBarrier(&___EFIOHEECJKI_23, value);
	}

	inline static int32_t get_offset_of_BABCKJCECDJ_24() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318, ___BABCKJCECDJ_24)); }
	inline int32_t get_BABCKJCECDJ_24() const { return ___BABCKJCECDJ_24; }
	inline int32_t* get_address_of_BABCKJCECDJ_24() { return &___BABCKJCECDJ_24; }
	inline void set_BABCKJCECDJ_24(int32_t value)
	{
		___BABCKJCECDJ_24 = value;
	}
};

struct PhoneEvent_t2572128318_StaticFields
{
public:
	// proto.PhoneEvent proto.PhoneEvent::CACHHOCBHAE
	PhoneEvent_t2572128318 * ___CACHHOCBHAE_0;
	// System.String[] proto.PhoneEvent::EOOGGAADBHL
	StringU5BU5D_t1642385972* ___EOOGGAADBHL_1;
	// System.UInt32[] proto.PhoneEvent::HDBEOCNLPFD
	UInt32U5BU5D_t59386216* ___HDBEOCNLPFD_2;

public:
	inline static int32_t get_offset_of_CACHHOCBHAE_0() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318_StaticFields, ___CACHHOCBHAE_0)); }
	inline PhoneEvent_t2572128318 * get_CACHHOCBHAE_0() const { return ___CACHHOCBHAE_0; }
	inline PhoneEvent_t2572128318 ** get_address_of_CACHHOCBHAE_0() { return &___CACHHOCBHAE_0; }
	inline void set_CACHHOCBHAE_0(PhoneEvent_t2572128318 * value)
	{
		___CACHHOCBHAE_0 = value;
		Il2CppCodeGenWriteBarrier(&___CACHHOCBHAE_0, value);
	}

	inline static int32_t get_offset_of_EOOGGAADBHL_1() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318_StaticFields, ___EOOGGAADBHL_1)); }
	inline StringU5BU5D_t1642385972* get_EOOGGAADBHL_1() const { return ___EOOGGAADBHL_1; }
	inline StringU5BU5D_t1642385972** get_address_of_EOOGGAADBHL_1() { return &___EOOGGAADBHL_1; }
	inline void set_EOOGGAADBHL_1(StringU5BU5D_t1642385972* value)
	{
		___EOOGGAADBHL_1 = value;
		Il2CppCodeGenWriteBarrier(&___EOOGGAADBHL_1, value);
	}

	inline static int32_t get_offset_of_HDBEOCNLPFD_2() { return static_cast<int32_t>(offsetof(PhoneEvent_t2572128318_StaticFields, ___HDBEOCNLPFD_2)); }
	inline UInt32U5BU5D_t59386216* get_HDBEOCNLPFD_2() const { return ___HDBEOCNLPFD_2; }
	inline UInt32U5BU5D_t59386216** get_address_of_HDBEOCNLPFD_2() { return &___HDBEOCNLPFD_2; }
	inline void set_HDBEOCNLPFD_2(UInt32U5BU5D_t59386216* value)
	{
		___HDBEOCNLPFD_2 = value;
		Il2CppCodeGenWriteBarrier(&___HDBEOCNLPFD_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
