﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SimpleJSON.GFGBGCMOLKN
struct GFGBGCMOLKN_t3233773149;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.GFGBGCMOLKN/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t3077522505  : public Il2CppObject
{
public:
	// SimpleJSON.GFGBGCMOLKN SimpleJSON.GFGBGCMOLKN/<>c__Iterator0::LGBFNMECDHC
	GFGBGCMOLKN_t3233773149 * ___LGBFNMECDHC_0;
	// System.Boolean SimpleJSON.GFGBGCMOLKN/<>c__Iterator0::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_1;
	// System.Int32 SimpleJSON.GFGBGCMOLKN/<>c__Iterator0::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_2;

public:
	inline static int32_t get_offset_of_LGBFNMECDHC_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3077522505, ___LGBFNMECDHC_0)); }
	inline GFGBGCMOLKN_t3233773149 * get_LGBFNMECDHC_0() const { return ___LGBFNMECDHC_0; }
	inline GFGBGCMOLKN_t3233773149 ** get_address_of_LGBFNMECDHC_0() { return &___LGBFNMECDHC_0; }
	inline void set_LGBFNMECDHC_0(GFGBGCMOLKN_t3233773149 * value)
	{
		___LGBFNMECDHC_0 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_0, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3077522505, ___IMAGLIMLPFK_1)); }
	inline bool get_IMAGLIMLPFK_1() const { return ___IMAGLIMLPFK_1; }
	inline bool* get_address_of_IMAGLIMLPFK_1() { return &___IMAGLIMLPFK_1; }
	inline void set_IMAGLIMLPFK_1(bool value)
	{
		___IMAGLIMLPFK_1 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3077522505, ___EPCFNNGDBGC_2)); }
	inline int32_t get_EPCFNNGDBGC_2() const { return ___EPCFNNGDBGC_2; }
	inline int32_t* get_address_of_EPCFNNGDBGC_2() { return &___EPCFNNGDBGC_2; }
	inline void set_EPCFNNGDBGC_2(int32_t value)
	{
		___EPCFNNGDBGC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
