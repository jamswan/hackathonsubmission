﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Gvr_Internal_LNNAMOEPEAP2980568462.h"
#include "AssemblyU2DCSharp_Gvr_Internal_AMPNKIOMOGM1147901973.h"
#include "AssemblyU2DCSharp_Gvr_Internal_BADMPKCPJFF1360680984.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EHFLFEDGDEF1158703932.h"
#include "AssemblyU2DCSharp_Gvr_Internal_LCEDPLMOCBI3496077470.h"

// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1785723201;
// Gvr.Internal.EmulatorManager
struct EmulatorManager_t3364249716;
// Gvr.Internal.EmulatorManager/EBNNCJIJNIF
struct EBNNCJIJNIF_t3366934859;
// Gvr.Internal.EmulatorManager/HFEGPIEMKOK
struct HFEGPIEMKOK_t1480002485;
// Gvr.Internal.EmulatorManager/PBBMGAFIDCA
struct PBBMGAFIDCA_t3727933525;
// Gvr.Internal.EmulatorManager/BGIHENCBNHH
struct BGIHENCBNHH_t1108385459;
// Gvr.Internal.EmulatorManager/AMGONBCFIKO
struct AMGONBCFIKO_t2280436239;
// System.Collections.Queue
struct Queue_t1288490777;
// Gvr.Internal.EmulatorClientSocket
struct EmulatorClientSocket_t2001911543;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorManager
struct  EmulatorManager_t3364249716  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.IEnumerator Gvr.Internal.EmulatorManager::OKPGJBDLLGN
	Il2CppObject * ___OKPGJBDLLGN_2;
	// UnityEngine.WaitForEndOfFrame Gvr.Internal.EmulatorManager::KGLPCKKPJHB
	WaitForEndOfFrame_t1785723201 * ___KGLPCKKPJHB_3;
	// Gvr.Internal.LNNAMOEPEAP Gvr.Internal.EmulatorManager::IAKEONFNHAO
	LNNAMOEPEAP_t2980568462  ___IAKEONFNHAO_5;
	// Gvr.Internal.AMPNKIOMOGM Gvr.Internal.EmulatorManager::JFNCPDBJPEI
	AMPNKIOMOGM_t1147901973  ___JFNCPDBJPEI_6;
	// Gvr.Internal.BADMPKCPJFF Gvr.Internal.EmulatorManager::BJFGFMDKCNA
	BADMPKCPJFF_t1360680984  ___BJFGFMDKCNA_7;
	// Gvr.Internal.EHFLFEDGDEF Gvr.Internal.EmulatorManager::POPAFDBBHOC
	EHFLFEDGDEF_t1158703932  ___POPAFDBBHOC_8;
	// Gvr.Internal.LCEDPLMOCBI Gvr.Internal.EmulatorManager::NFHAFJMHDGO
	LCEDPLMOCBI_t3496077470  ___NFHAFJMHDGO_9;
	// Gvr.Internal.EmulatorManager/EBNNCJIJNIF Gvr.Internal.EmulatorManager::gyroEventListenersInternal
	EBNNCJIJNIF_t3366934859 * ___gyroEventListenersInternal_10;
	// Gvr.Internal.EmulatorManager/HFEGPIEMKOK Gvr.Internal.EmulatorManager::accelEventListenersInternal
	HFEGPIEMKOK_t1480002485 * ___accelEventListenersInternal_11;
	// Gvr.Internal.EmulatorManager/PBBMGAFIDCA Gvr.Internal.EmulatorManager::touchEventListenersInternal
	PBBMGAFIDCA_t3727933525 * ___touchEventListenersInternal_12;
	// Gvr.Internal.EmulatorManager/BGIHENCBNHH Gvr.Internal.EmulatorManager::orientationEventListenersInternal
	BGIHENCBNHH_t1108385459 * ___orientationEventListenersInternal_13;
	// Gvr.Internal.EmulatorManager/AMGONBCFIKO Gvr.Internal.EmulatorManager::buttonEventListenersInternal
	AMGONBCFIKO_t2280436239 * ___buttonEventListenersInternal_14;
	// System.Collections.Queue Gvr.Internal.EmulatorManager::KAKIBKIINOI
	Queue_t1288490777 * ___KAKIBKIINOI_15;
	// Gvr.Internal.EmulatorClientSocket Gvr.Internal.EmulatorManager::ICJHENHPDEI
	EmulatorClientSocket_t2001911543 * ___ICJHENHPDEI_16;
	// System.Int64 Gvr.Internal.EmulatorManager::PNJJEALMAFB
	int64_t ___PNJJEALMAFB_17;

public:
	inline static int32_t get_offset_of_OKPGJBDLLGN_2() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___OKPGJBDLLGN_2)); }
	inline Il2CppObject * get_OKPGJBDLLGN_2() const { return ___OKPGJBDLLGN_2; }
	inline Il2CppObject ** get_address_of_OKPGJBDLLGN_2() { return &___OKPGJBDLLGN_2; }
	inline void set_OKPGJBDLLGN_2(Il2CppObject * value)
	{
		___OKPGJBDLLGN_2 = value;
		Il2CppCodeGenWriteBarrier(&___OKPGJBDLLGN_2, value);
	}

	inline static int32_t get_offset_of_KGLPCKKPJHB_3() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___KGLPCKKPJHB_3)); }
	inline WaitForEndOfFrame_t1785723201 * get_KGLPCKKPJHB_3() const { return ___KGLPCKKPJHB_3; }
	inline WaitForEndOfFrame_t1785723201 ** get_address_of_KGLPCKKPJHB_3() { return &___KGLPCKKPJHB_3; }
	inline void set_KGLPCKKPJHB_3(WaitForEndOfFrame_t1785723201 * value)
	{
		___KGLPCKKPJHB_3 = value;
		Il2CppCodeGenWriteBarrier(&___KGLPCKKPJHB_3, value);
	}

	inline static int32_t get_offset_of_IAKEONFNHAO_5() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___IAKEONFNHAO_5)); }
	inline LNNAMOEPEAP_t2980568462  get_IAKEONFNHAO_5() const { return ___IAKEONFNHAO_5; }
	inline LNNAMOEPEAP_t2980568462 * get_address_of_IAKEONFNHAO_5() { return &___IAKEONFNHAO_5; }
	inline void set_IAKEONFNHAO_5(LNNAMOEPEAP_t2980568462  value)
	{
		___IAKEONFNHAO_5 = value;
	}

	inline static int32_t get_offset_of_JFNCPDBJPEI_6() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___JFNCPDBJPEI_6)); }
	inline AMPNKIOMOGM_t1147901973  get_JFNCPDBJPEI_6() const { return ___JFNCPDBJPEI_6; }
	inline AMPNKIOMOGM_t1147901973 * get_address_of_JFNCPDBJPEI_6() { return &___JFNCPDBJPEI_6; }
	inline void set_JFNCPDBJPEI_6(AMPNKIOMOGM_t1147901973  value)
	{
		___JFNCPDBJPEI_6 = value;
	}

	inline static int32_t get_offset_of_BJFGFMDKCNA_7() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___BJFGFMDKCNA_7)); }
	inline BADMPKCPJFF_t1360680984  get_BJFGFMDKCNA_7() const { return ___BJFGFMDKCNA_7; }
	inline BADMPKCPJFF_t1360680984 * get_address_of_BJFGFMDKCNA_7() { return &___BJFGFMDKCNA_7; }
	inline void set_BJFGFMDKCNA_7(BADMPKCPJFF_t1360680984  value)
	{
		___BJFGFMDKCNA_7 = value;
	}

	inline static int32_t get_offset_of_POPAFDBBHOC_8() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___POPAFDBBHOC_8)); }
	inline EHFLFEDGDEF_t1158703932  get_POPAFDBBHOC_8() const { return ___POPAFDBBHOC_8; }
	inline EHFLFEDGDEF_t1158703932 * get_address_of_POPAFDBBHOC_8() { return &___POPAFDBBHOC_8; }
	inline void set_POPAFDBBHOC_8(EHFLFEDGDEF_t1158703932  value)
	{
		___POPAFDBBHOC_8 = value;
	}

	inline static int32_t get_offset_of_NFHAFJMHDGO_9() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___NFHAFJMHDGO_9)); }
	inline LCEDPLMOCBI_t3496077470  get_NFHAFJMHDGO_9() const { return ___NFHAFJMHDGO_9; }
	inline LCEDPLMOCBI_t3496077470 * get_address_of_NFHAFJMHDGO_9() { return &___NFHAFJMHDGO_9; }
	inline void set_NFHAFJMHDGO_9(LCEDPLMOCBI_t3496077470  value)
	{
		___NFHAFJMHDGO_9 = value;
	}

	inline static int32_t get_offset_of_gyroEventListenersInternal_10() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___gyroEventListenersInternal_10)); }
	inline EBNNCJIJNIF_t3366934859 * get_gyroEventListenersInternal_10() const { return ___gyroEventListenersInternal_10; }
	inline EBNNCJIJNIF_t3366934859 ** get_address_of_gyroEventListenersInternal_10() { return &___gyroEventListenersInternal_10; }
	inline void set_gyroEventListenersInternal_10(EBNNCJIJNIF_t3366934859 * value)
	{
		___gyroEventListenersInternal_10 = value;
		Il2CppCodeGenWriteBarrier(&___gyroEventListenersInternal_10, value);
	}

	inline static int32_t get_offset_of_accelEventListenersInternal_11() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___accelEventListenersInternal_11)); }
	inline HFEGPIEMKOK_t1480002485 * get_accelEventListenersInternal_11() const { return ___accelEventListenersInternal_11; }
	inline HFEGPIEMKOK_t1480002485 ** get_address_of_accelEventListenersInternal_11() { return &___accelEventListenersInternal_11; }
	inline void set_accelEventListenersInternal_11(HFEGPIEMKOK_t1480002485 * value)
	{
		___accelEventListenersInternal_11 = value;
		Il2CppCodeGenWriteBarrier(&___accelEventListenersInternal_11, value);
	}

	inline static int32_t get_offset_of_touchEventListenersInternal_12() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___touchEventListenersInternal_12)); }
	inline PBBMGAFIDCA_t3727933525 * get_touchEventListenersInternal_12() const { return ___touchEventListenersInternal_12; }
	inline PBBMGAFIDCA_t3727933525 ** get_address_of_touchEventListenersInternal_12() { return &___touchEventListenersInternal_12; }
	inline void set_touchEventListenersInternal_12(PBBMGAFIDCA_t3727933525 * value)
	{
		___touchEventListenersInternal_12 = value;
		Il2CppCodeGenWriteBarrier(&___touchEventListenersInternal_12, value);
	}

	inline static int32_t get_offset_of_orientationEventListenersInternal_13() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___orientationEventListenersInternal_13)); }
	inline BGIHENCBNHH_t1108385459 * get_orientationEventListenersInternal_13() const { return ___orientationEventListenersInternal_13; }
	inline BGIHENCBNHH_t1108385459 ** get_address_of_orientationEventListenersInternal_13() { return &___orientationEventListenersInternal_13; }
	inline void set_orientationEventListenersInternal_13(BGIHENCBNHH_t1108385459 * value)
	{
		___orientationEventListenersInternal_13 = value;
		Il2CppCodeGenWriteBarrier(&___orientationEventListenersInternal_13, value);
	}

	inline static int32_t get_offset_of_buttonEventListenersInternal_14() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___buttonEventListenersInternal_14)); }
	inline AMGONBCFIKO_t2280436239 * get_buttonEventListenersInternal_14() const { return ___buttonEventListenersInternal_14; }
	inline AMGONBCFIKO_t2280436239 ** get_address_of_buttonEventListenersInternal_14() { return &___buttonEventListenersInternal_14; }
	inline void set_buttonEventListenersInternal_14(AMGONBCFIKO_t2280436239 * value)
	{
		___buttonEventListenersInternal_14 = value;
		Il2CppCodeGenWriteBarrier(&___buttonEventListenersInternal_14, value);
	}

	inline static int32_t get_offset_of_KAKIBKIINOI_15() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___KAKIBKIINOI_15)); }
	inline Queue_t1288490777 * get_KAKIBKIINOI_15() const { return ___KAKIBKIINOI_15; }
	inline Queue_t1288490777 ** get_address_of_KAKIBKIINOI_15() { return &___KAKIBKIINOI_15; }
	inline void set_KAKIBKIINOI_15(Queue_t1288490777 * value)
	{
		___KAKIBKIINOI_15 = value;
		Il2CppCodeGenWriteBarrier(&___KAKIBKIINOI_15, value);
	}

	inline static int32_t get_offset_of_ICJHENHPDEI_16() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___ICJHENHPDEI_16)); }
	inline EmulatorClientSocket_t2001911543 * get_ICJHENHPDEI_16() const { return ___ICJHENHPDEI_16; }
	inline EmulatorClientSocket_t2001911543 ** get_address_of_ICJHENHPDEI_16() { return &___ICJHENHPDEI_16; }
	inline void set_ICJHENHPDEI_16(EmulatorClientSocket_t2001911543 * value)
	{
		___ICJHENHPDEI_16 = value;
		Il2CppCodeGenWriteBarrier(&___ICJHENHPDEI_16, value);
	}

	inline static int32_t get_offset_of_PNJJEALMAFB_17() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716, ___PNJJEALMAFB_17)); }
	inline int64_t get_PNJJEALMAFB_17() const { return ___PNJJEALMAFB_17; }
	inline int64_t* get_address_of_PNJJEALMAFB_17() { return &___PNJJEALMAFB_17; }
	inline void set_PNJJEALMAFB_17(int64_t value)
	{
		___PNJJEALMAFB_17 = value;
	}
};

struct EmulatorManager_t3364249716_StaticFields
{
public:
	// Gvr.Internal.EmulatorManager Gvr.Internal.EmulatorManager::OLMOHEEKDMC
	EmulatorManager_t3364249716 * ___OLMOHEEKDMC_4;

public:
	inline static int32_t get_offset_of_OLMOHEEKDMC_4() { return static_cast<int32_t>(offsetof(EmulatorManager_t3364249716_StaticFields, ___OLMOHEEKDMC_4)); }
	inline EmulatorManager_t3364249716 * get_OLMOHEEKDMC_4() const { return ___OLMOHEEKDMC_4; }
	inline EmulatorManager_t3364249716 ** get_address_of_OLMOHEEKDMC_4() { return &___OLMOHEEKDMC_4; }
	inline void set_OLMOHEEKDMC_4(EmulatorManager_t3364249716 * value)
	{
		___OLMOHEEKDMC_4 = value;
		Il2CppCodeGenWriteBarrier(&___OLMOHEEKDMC_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
