﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LogMessage
struct  LogMessage_t3320573177  : public NarrativeEffect_t3336735121
{
public:
	// System.String LogMessage::logName
	String_t* ___logName_3;
	// System.String LogMessage::message
	String_t* ___message_4;
	// System.String LogMessage::overrideStyleName
	String_t* ___overrideStyleName_5;
	// UnityEngine.GUIStyle LogMessage::style
	GUIStyle_t1799908754 * ___style_6;

public:
	inline static int32_t get_offset_of_logName_3() { return static_cast<int32_t>(offsetof(LogMessage_t3320573177, ___logName_3)); }
	inline String_t* get_logName_3() const { return ___logName_3; }
	inline String_t** get_address_of_logName_3() { return &___logName_3; }
	inline void set_logName_3(String_t* value)
	{
		___logName_3 = value;
		Il2CppCodeGenWriteBarrier(&___logName_3, value);
	}

	inline static int32_t get_offset_of_message_4() { return static_cast<int32_t>(offsetof(LogMessage_t3320573177, ___message_4)); }
	inline String_t* get_message_4() const { return ___message_4; }
	inline String_t** get_address_of_message_4() { return &___message_4; }
	inline void set_message_4(String_t* value)
	{
		___message_4 = value;
		Il2CppCodeGenWriteBarrier(&___message_4, value);
	}

	inline static int32_t get_offset_of_overrideStyleName_5() { return static_cast<int32_t>(offsetof(LogMessage_t3320573177, ___overrideStyleName_5)); }
	inline String_t* get_overrideStyleName_5() const { return ___overrideStyleName_5; }
	inline String_t** get_address_of_overrideStyleName_5() { return &___overrideStyleName_5; }
	inline void set_overrideStyleName_5(String_t* value)
	{
		___overrideStyleName_5 = value;
		Il2CppCodeGenWriteBarrier(&___overrideStyleName_5, value);
	}

	inline static int32_t get_offset_of_style_6() { return static_cast<int32_t>(offsetof(LogMessage_t3320573177, ___style_6)); }
	inline GUIStyle_t1799908754 * get_style_6() const { return ___style_6; }
	inline GUIStyle_t1799908754 ** get_address_of_style_6() { return &___style_6; }
	inline void set_style_6(GUIStyle_t1799908754 * value)
	{
		___style_6 = value;
		Il2CppCodeGenWriteBarrier(&___style_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
