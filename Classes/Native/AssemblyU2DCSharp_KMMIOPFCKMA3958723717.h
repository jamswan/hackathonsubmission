﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

// KHBDFKCPFBH
struct KHBDFKCPFBH_t2524227425;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KMMIOPFCKMA
struct  KMMIOPFCKMA_t3958723717  : public Il2CppObject
{
public:
	// KHBDFKCPFBH KMMIOPFCKMA::NFDAOEEMCDI
	KHBDFKCPFBH_t2524227425 * ___NFDAOEEMCDI_0;
	// System.Int32 KMMIOPFCKMA::BPNAHLIGNDA
	int32_t ___BPNAHLIGNDA_1;
	// System.DateTime KMMIOPFCKMA::CBNELEHGAJI
	DateTime_t693205669  ___CBNELEHGAJI_2;

public:
	inline static int32_t get_offset_of_NFDAOEEMCDI_0() { return static_cast<int32_t>(offsetof(KMMIOPFCKMA_t3958723717, ___NFDAOEEMCDI_0)); }
	inline KHBDFKCPFBH_t2524227425 * get_NFDAOEEMCDI_0() const { return ___NFDAOEEMCDI_0; }
	inline KHBDFKCPFBH_t2524227425 ** get_address_of_NFDAOEEMCDI_0() { return &___NFDAOEEMCDI_0; }
	inline void set_NFDAOEEMCDI_0(KHBDFKCPFBH_t2524227425 * value)
	{
		___NFDAOEEMCDI_0 = value;
		Il2CppCodeGenWriteBarrier(&___NFDAOEEMCDI_0, value);
	}

	inline static int32_t get_offset_of_BPNAHLIGNDA_1() { return static_cast<int32_t>(offsetof(KMMIOPFCKMA_t3958723717, ___BPNAHLIGNDA_1)); }
	inline int32_t get_BPNAHLIGNDA_1() const { return ___BPNAHLIGNDA_1; }
	inline int32_t* get_address_of_BPNAHLIGNDA_1() { return &___BPNAHLIGNDA_1; }
	inline void set_BPNAHLIGNDA_1(int32_t value)
	{
		___BPNAHLIGNDA_1 = value;
	}

	inline static int32_t get_offset_of_CBNELEHGAJI_2() { return static_cast<int32_t>(offsetof(KMMIOPFCKMA_t3958723717, ___CBNELEHGAJI_2)); }
	inline DateTime_t693205669  get_CBNELEHGAJI_2() const { return ___CBNELEHGAJI_2; }
	inline DateTime_t693205669 * get_address_of_CBNELEHGAJI_2() { return &___CBNELEHGAJI_2; }
	inline void set_CBNELEHGAJI_2(DateTime_t693205669  value)
	{
		___CBNELEHGAJI_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
