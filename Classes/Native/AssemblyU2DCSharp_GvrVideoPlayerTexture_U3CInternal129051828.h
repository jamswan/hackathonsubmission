﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// GvrVideoPlayerTexture
struct GvrVideoPlayerTexture_t673526704;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey2
struct  U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828  : public Il2CppObject
{
public:
	// GvrVideoPlayerTexture GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey2::OECALIBMPJK
	GvrVideoPlayerTexture_t673526704 * ___OECALIBMPJK_0;
	// System.Int32 GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey2::DFDMADNLFGI
	int32_t ___DFDMADNLFGI_1;

public:
	inline static int32_t get_offset_of_OECALIBMPJK_0() { return static_cast<int32_t>(offsetof(U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828, ___OECALIBMPJK_0)); }
	inline GvrVideoPlayerTexture_t673526704 * get_OECALIBMPJK_0() const { return ___OECALIBMPJK_0; }
	inline GvrVideoPlayerTexture_t673526704 ** get_address_of_OECALIBMPJK_0() { return &___OECALIBMPJK_0; }
	inline void set_OECALIBMPJK_0(GvrVideoPlayerTexture_t673526704 * value)
	{
		___OECALIBMPJK_0 = value;
		Il2CppCodeGenWriteBarrier(&___OECALIBMPJK_0, value);
	}

	inline static int32_t get_offset_of_DFDMADNLFGI_1() { return static_cast<int32_t>(offsetof(U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828, ___DFDMADNLFGI_1)); }
	inline int32_t get_DFDMADNLFGI_1() const { return ___DFDMADNLFGI_1; }
	inline int32_t* get_address_of_DFDMADNLFGI_1() { return &___DFDMADNLFGI_1; }
	inline void set_DFDMADNLFGI_1(int32_t value)
	{
		___DFDMADNLFGI_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
