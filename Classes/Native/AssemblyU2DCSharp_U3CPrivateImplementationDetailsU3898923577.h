﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3969012783.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{959a34e8-bda6-48f7-b3e7-b3f75a3a3202}.ArrayCopy148
struct  ArrayCopy148_t898923577  : public Il2CppObject
{
public:

public:
};

struct ArrayCopy148_t898923577_StaticFields
{
public:
	// <PrivateImplementationDetails>{959a34e8-bda6-48f7-b3e7-b3f75a3a3202}.ArrayCopy148/$ArrayType$151 <PrivateImplementationDetails>{959a34e8-bda6-48f7-b3e7-b3f75a3a3202}.ArrayCopy148::$$field-0
	U24ArrayTypeU24151_t3969012783  ___U24U24fieldU2D0_0;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(ArrayCopy148_t898923577_StaticFields, ___U24U24fieldU2D0_0)); }
	inline U24ArrayTypeU24151_t3969012783  get_U24U24fieldU2D0_0() const { return ___U24U24fieldU2D0_0; }
	inline U24ArrayTypeU24151_t3969012783 * get_address_of_U24U24fieldU2D0_0() { return &___U24U24fieldU2D0_0; }
	inline void set_U24U24fieldU2D0_0(U24ArrayTypeU24151_t3969012783  value)
	{
		___U24U24fieldU2D0_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
