﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Singleton_1_gen2459686238.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Material
struct Material_t193706927;
// System.Collections.Generic.List`1<Vuforia.WordResult>
struct List_1_t1284628329;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;
// UnityEngine.Canvas
struct Canvas_t209405766;
// TextRecognizor/DOMBFHLCEEF
struct DOMBFHLCEEF_t1522506999;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextRecognizor
struct  TextRecognizor_t1283801851  : public Singleton_1_t2459686238
{
public:
	// System.Single TextRecognizor::DGHIIGFLIBN
	float ___DGHIIGFLIBN_5;
	// System.Single TextRecognizor::CBMMAHGIJDK
	float ___CBMMAHGIJDK_6;
	// System.Single TextRecognizor::GICCCFHNIJP
	float ___GICCCFHNIJP_7;
	// System.Single TextRecognizor::IPPNHBNCMDG
	float ___IPPNHBNCMDG_8;
	// UnityEngine.Color TextRecognizor::EEGCDGMGMCA
	Color_t2020392075  ___EEGCDGMGMCA_9;
	// UnityEngine.Rect TextRecognizor::LIBNLFLPENI
	Rect_t3681755626  ___LIBNLFLPENI_10;
	// UnityEngine.Texture2D TextRecognizor::PGCBBCAOJHI
	Texture2D_t3542995729 * ___PGCBBCAOJHI_11;
	// UnityEngine.Material TextRecognizor::NCEMCAAKJLM
	Material_t193706927 * ___NCEMCAAKJLM_12;
	// System.Boolean TextRecognizor::OIAOLNMENJC
	bool ___OIAOLNMENJC_13;
	// System.Boolean TextRecognizor::PGLHJGGJHEI
	bool ___PGLHJGGJHEI_14;
	// System.Collections.Generic.List`1<Vuforia.WordResult> TextRecognizor::NCNNHEHENKA
	List_1_t1284628329 * ___NCNNHEHENKA_15;
	// UnityEngine.UI.Text[] TextRecognizor::PODOCCPNBFB
	TextU5BU5D_t4216439300* ___PODOCCPNBFB_16;
	// UnityEngine.Material TextRecognizor::boundingBoxMaterial
	Material_t193706927 * ___boundingBoxMaterial_17;
	// System.Boolean TextRecognizor::detecting
	bool ___detecting_18;
	// UnityEngine.Canvas TextRecognizor::textRecoCanvas
	Canvas_t209405766 * ___textRecoCanvas_19;
	// TextRecognizor/DOMBFHLCEEF TextRecognizor::WordListChange
	DOMBFHLCEEF_t1522506999 * ___WordListChange_20;

public:
	inline static int32_t get_offset_of_DGHIIGFLIBN_5() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___DGHIIGFLIBN_5)); }
	inline float get_DGHIIGFLIBN_5() const { return ___DGHIIGFLIBN_5; }
	inline float* get_address_of_DGHIIGFLIBN_5() { return &___DGHIIGFLIBN_5; }
	inline void set_DGHIIGFLIBN_5(float value)
	{
		___DGHIIGFLIBN_5 = value;
	}

	inline static int32_t get_offset_of_CBMMAHGIJDK_6() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___CBMMAHGIJDK_6)); }
	inline float get_CBMMAHGIJDK_6() const { return ___CBMMAHGIJDK_6; }
	inline float* get_address_of_CBMMAHGIJDK_6() { return &___CBMMAHGIJDK_6; }
	inline void set_CBMMAHGIJDK_6(float value)
	{
		___CBMMAHGIJDK_6 = value;
	}

	inline static int32_t get_offset_of_GICCCFHNIJP_7() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___GICCCFHNIJP_7)); }
	inline float get_GICCCFHNIJP_7() const { return ___GICCCFHNIJP_7; }
	inline float* get_address_of_GICCCFHNIJP_7() { return &___GICCCFHNIJP_7; }
	inline void set_GICCCFHNIJP_7(float value)
	{
		___GICCCFHNIJP_7 = value;
	}

	inline static int32_t get_offset_of_IPPNHBNCMDG_8() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___IPPNHBNCMDG_8)); }
	inline float get_IPPNHBNCMDG_8() const { return ___IPPNHBNCMDG_8; }
	inline float* get_address_of_IPPNHBNCMDG_8() { return &___IPPNHBNCMDG_8; }
	inline void set_IPPNHBNCMDG_8(float value)
	{
		___IPPNHBNCMDG_8 = value;
	}

	inline static int32_t get_offset_of_EEGCDGMGMCA_9() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___EEGCDGMGMCA_9)); }
	inline Color_t2020392075  get_EEGCDGMGMCA_9() const { return ___EEGCDGMGMCA_9; }
	inline Color_t2020392075 * get_address_of_EEGCDGMGMCA_9() { return &___EEGCDGMGMCA_9; }
	inline void set_EEGCDGMGMCA_9(Color_t2020392075  value)
	{
		___EEGCDGMGMCA_9 = value;
	}

	inline static int32_t get_offset_of_LIBNLFLPENI_10() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___LIBNLFLPENI_10)); }
	inline Rect_t3681755626  get_LIBNLFLPENI_10() const { return ___LIBNLFLPENI_10; }
	inline Rect_t3681755626 * get_address_of_LIBNLFLPENI_10() { return &___LIBNLFLPENI_10; }
	inline void set_LIBNLFLPENI_10(Rect_t3681755626  value)
	{
		___LIBNLFLPENI_10 = value;
	}

	inline static int32_t get_offset_of_PGCBBCAOJHI_11() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___PGCBBCAOJHI_11)); }
	inline Texture2D_t3542995729 * get_PGCBBCAOJHI_11() const { return ___PGCBBCAOJHI_11; }
	inline Texture2D_t3542995729 ** get_address_of_PGCBBCAOJHI_11() { return &___PGCBBCAOJHI_11; }
	inline void set_PGCBBCAOJHI_11(Texture2D_t3542995729 * value)
	{
		___PGCBBCAOJHI_11 = value;
		Il2CppCodeGenWriteBarrier(&___PGCBBCAOJHI_11, value);
	}

	inline static int32_t get_offset_of_NCEMCAAKJLM_12() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___NCEMCAAKJLM_12)); }
	inline Material_t193706927 * get_NCEMCAAKJLM_12() const { return ___NCEMCAAKJLM_12; }
	inline Material_t193706927 ** get_address_of_NCEMCAAKJLM_12() { return &___NCEMCAAKJLM_12; }
	inline void set_NCEMCAAKJLM_12(Material_t193706927 * value)
	{
		___NCEMCAAKJLM_12 = value;
		Il2CppCodeGenWriteBarrier(&___NCEMCAAKJLM_12, value);
	}

	inline static int32_t get_offset_of_OIAOLNMENJC_13() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___OIAOLNMENJC_13)); }
	inline bool get_OIAOLNMENJC_13() const { return ___OIAOLNMENJC_13; }
	inline bool* get_address_of_OIAOLNMENJC_13() { return &___OIAOLNMENJC_13; }
	inline void set_OIAOLNMENJC_13(bool value)
	{
		___OIAOLNMENJC_13 = value;
	}

	inline static int32_t get_offset_of_PGLHJGGJHEI_14() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___PGLHJGGJHEI_14)); }
	inline bool get_PGLHJGGJHEI_14() const { return ___PGLHJGGJHEI_14; }
	inline bool* get_address_of_PGLHJGGJHEI_14() { return &___PGLHJGGJHEI_14; }
	inline void set_PGLHJGGJHEI_14(bool value)
	{
		___PGLHJGGJHEI_14 = value;
	}

	inline static int32_t get_offset_of_NCNNHEHENKA_15() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___NCNNHEHENKA_15)); }
	inline List_1_t1284628329 * get_NCNNHEHENKA_15() const { return ___NCNNHEHENKA_15; }
	inline List_1_t1284628329 ** get_address_of_NCNNHEHENKA_15() { return &___NCNNHEHENKA_15; }
	inline void set_NCNNHEHENKA_15(List_1_t1284628329 * value)
	{
		___NCNNHEHENKA_15 = value;
		Il2CppCodeGenWriteBarrier(&___NCNNHEHENKA_15, value);
	}

	inline static int32_t get_offset_of_PODOCCPNBFB_16() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___PODOCCPNBFB_16)); }
	inline TextU5BU5D_t4216439300* get_PODOCCPNBFB_16() const { return ___PODOCCPNBFB_16; }
	inline TextU5BU5D_t4216439300** get_address_of_PODOCCPNBFB_16() { return &___PODOCCPNBFB_16; }
	inline void set_PODOCCPNBFB_16(TextU5BU5D_t4216439300* value)
	{
		___PODOCCPNBFB_16 = value;
		Il2CppCodeGenWriteBarrier(&___PODOCCPNBFB_16, value);
	}

	inline static int32_t get_offset_of_boundingBoxMaterial_17() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___boundingBoxMaterial_17)); }
	inline Material_t193706927 * get_boundingBoxMaterial_17() const { return ___boundingBoxMaterial_17; }
	inline Material_t193706927 ** get_address_of_boundingBoxMaterial_17() { return &___boundingBoxMaterial_17; }
	inline void set_boundingBoxMaterial_17(Material_t193706927 * value)
	{
		___boundingBoxMaterial_17 = value;
		Il2CppCodeGenWriteBarrier(&___boundingBoxMaterial_17, value);
	}

	inline static int32_t get_offset_of_detecting_18() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___detecting_18)); }
	inline bool get_detecting_18() const { return ___detecting_18; }
	inline bool* get_address_of_detecting_18() { return &___detecting_18; }
	inline void set_detecting_18(bool value)
	{
		___detecting_18 = value;
	}

	inline static int32_t get_offset_of_textRecoCanvas_19() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___textRecoCanvas_19)); }
	inline Canvas_t209405766 * get_textRecoCanvas_19() const { return ___textRecoCanvas_19; }
	inline Canvas_t209405766 ** get_address_of_textRecoCanvas_19() { return &___textRecoCanvas_19; }
	inline void set_textRecoCanvas_19(Canvas_t209405766 * value)
	{
		___textRecoCanvas_19 = value;
		Il2CppCodeGenWriteBarrier(&___textRecoCanvas_19, value);
	}

	inline static int32_t get_offset_of_WordListChange_20() { return static_cast<int32_t>(offsetof(TextRecognizor_t1283801851, ___WordListChange_20)); }
	inline DOMBFHLCEEF_t1522506999 * get_WordListChange_20() const { return ___WordListChange_20; }
	inline DOMBFHLCEEF_t1522506999 ** get_address_of_WordListChange_20() { return &___WordListChange_20; }
	inline void set_WordListChange_20(DOMBFHLCEEF_t1522506999 * value)
	{
		___WordListChange_20 = value;
		Il2CppCodeGenWriteBarrier(&___WordListChange_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
