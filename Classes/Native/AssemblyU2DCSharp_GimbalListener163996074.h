﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Singleton_1_gen1339880461.h"

// GimbalBehavior
struct GimbalBehavior_t2141330552;
// System.Collections.Generic.Dictionary`2<System.String,KMMIOPFCKMA>
struct Dictionary_2_t1578535683;
// System.Collections.Generic.Dictionary`2<System.String,BKNEONNPPOE>
struct Dictionary_2_t1655568071;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GimbalListener
struct  GimbalListener_t163996074  : public Singleton_1_t1339880461
{
public:
	// GimbalBehavior GimbalListener::gimbal
	GimbalBehavior_t2141330552 * ___gimbal_5;
	// System.Collections.Generic.Dictionary`2<System.String,KMMIOPFCKMA> GimbalListener::BDGBMPDNEAF
	Dictionary_2_t1578535683 * ___BDGBMPDNEAF_6;
	// System.Collections.Generic.Dictionary`2<System.String,BKNEONNPPOE> GimbalListener::PABHOAJMGFB
	Dictionary_2_t1655568071 * ___PABHOAJMGFB_7;
	// System.Collections.Generic.Dictionary`2<System.String,BKNEONNPPOE> GimbalListener::NNHPNBALPEE
	Dictionary_2_t1655568071 * ___NNHPNBALPEE_8;
	// System.String GimbalListener::currentPlace
	String_t* ___currentPlace_9;

public:
	inline static int32_t get_offset_of_gimbal_5() { return static_cast<int32_t>(offsetof(GimbalListener_t163996074, ___gimbal_5)); }
	inline GimbalBehavior_t2141330552 * get_gimbal_5() const { return ___gimbal_5; }
	inline GimbalBehavior_t2141330552 ** get_address_of_gimbal_5() { return &___gimbal_5; }
	inline void set_gimbal_5(GimbalBehavior_t2141330552 * value)
	{
		___gimbal_5 = value;
		Il2CppCodeGenWriteBarrier(&___gimbal_5, value);
	}

	inline static int32_t get_offset_of_BDGBMPDNEAF_6() { return static_cast<int32_t>(offsetof(GimbalListener_t163996074, ___BDGBMPDNEAF_6)); }
	inline Dictionary_2_t1578535683 * get_BDGBMPDNEAF_6() const { return ___BDGBMPDNEAF_6; }
	inline Dictionary_2_t1578535683 ** get_address_of_BDGBMPDNEAF_6() { return &___BDGBMPDNEAF_6; }
	inline void set_BDGBMPDNEAF_6(Dictionary_2_t1578535683 * value)
	{
		___BDGBMPDNEAF_6 = value;
		Il2CppCodeGenWriteBarrier(&___BDGBMPDNEAF_6, value);
	}

	inline static int32_t get_offset_of_PABHOAJMGFB_7() { return static_cast<int32_t>(offsetof(GimbalListener_t163996074, ___PABHOAJMGFB_7)); }
	inline Dictionary_2_t1655568071 * get_PABHOAJMGFB_7() const { return ___PABHOAJMGFB_7; }
	inline Dictionary_2_t1655568071 ** get_address_of_PABHOAJMGFB_7() { return &___PABHOAJMGFB_7; }
	inline void set_PABHOAJMGFB_7(Dictionary_2_t1655568071 * value)
	{
		___PABHOAJMGFB_7 = value;
		Il2CppCodeGenWriteBarrier(&___PABHOAJMGFB_7, value);
	}

	inline static int32_t get_offset_of_NNHPNBALPEE_8() { return static_cast<int32_t>(offsetof(GimbalListener_t163996074, ___NNHPNBALPEE_8)); }
	inline Dictionary_2_t1655568071 * get_NNHPNBALPEE_8() const { return ___NNHPNBALPEE_8; }
	inline Dictionary_2_t1655568071 ** get_address_of_NNHPNBALPEE_8() { return &___NNHPNBALPEE_8; }
	inline void set_NNHPNBALPEE_8(Dictionary_2_t1655568071 * value)
	{
		___NNHPNBALPEE_8 = value;
		Il2CppCodeGenWriteBarrier(&___NNHPNBALPEE_8, value);
	}

	inline static int32_t get_offset_of_currentPlace_9() { return static_cast<int32_t>(offsetof(GimbalListener_t163996074, ___currentPlace_9)); }
	inline String_t* get_currentPlace_9() const { return ___currentPlace_9; }
	inline String_t** get_address_of_currentPlace_9() { return &___currentPlace_9; }
	inline void set_currentPlace_9(String_t* value)
	{
		___currentPlace_9 = value;
		Il2CppCodeGenWriteBarrier(&___currentPlace_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
