﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_NOMEFGGFNGH2967056203.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_BCENGFHBBOI531979988.h"

// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// MediaPlayerCtrl/DPHJBDJDHMD
struct DPHJBDJDHMD_t4102044392;
// MediaPlayerCtrl/KBIFEBKFHJP
struct KBIFEBKFHJP_t9122301;
// MediaPlayerCtrl/NJENMFCKEPC
struct NJENMFCKEPC_t2069907229;
// MediaPlayerCtrl/HDIIHNOEAOH
struct HDIIHNOEAOH_t1508400025;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MediaPlayerCtrl
struct  MediaPlayerCtrl_t1284484152  : public MonoBehaviour_t1158329972
{
public:
	// System.String MediaPlayerCtrl::m_strFileName
	String_t* ___m_strFileName_2;
	// UnityEngine.GameObject[] MediaPlayerCtrl::m_TargetMaterial
	GameObjectU5BU5D_t3057952154* ___m_TargetMaterial_3;
	// UnityEngine.Texture2D MediaPlayerCtrl::OJFFFGDDMIL
	Texture2D_t3542995729 * ___OJFFFGDDMIL_4;
	// UnityEngine.Texture2D MediaPlayerCtrl::FGAMFEHIGBA
	Texture2D_t3542995729 * ___FGAMFEHIGBA_5;
	// MediaPlayerCtrl/NOMEFGGFNGH MediaPlayerCtrl::MLMOCCFIOMO
	int32_t ___MLMOCCFIOMO_6;
	// System.Int32 MediaPlayerCtrl::EMBDGBJEJPH
	int32_t ___EMBDGBJEJPH_7;
	// System.Single MediaPlayerCtrl::IENGHMEHDND
	float ___IENGHMEHDND_8;
	// System.Boolean MediaPlayerCtrl::m_bFullScreen
	bool ___m_bFullScreen_9;
	// System.Boolean MediaPlayerCtrl::m_bSupportRockchip
	bool ___m_bSupportRockchip_10;
	// MediaPlayerCtrl/DPHJBDJDHMD MediaPlayerCtrl::OnReady
	DPHJBDJDHMD_t4102044392 * ___OnReady_11;
	// MediaPlayerCtrl/KBIFEBKFHJP MediaPlayerCtrl::OnEnd
	KBIFEBKFHJP_t9122301 * ___OnEnd_12;
	// MediaPlayerCtrl/NJENMFCKEPC MediaPlayerCtrl::OnVideoError
	NJENMFCKEPC_t2069907229 * ___OnVideoError_13;
	// MediaPlayerCtrl/HDIIHNOEAOH MediaPlayerCtrl::OnVideoFirstFrameReady
	HDIIHNOEAOH_t1508400025 * ___OnVideoFirstFrameReady_14;
	// System.Int32 MediaPlayerCtrl::NMIEDBOEIJP
	int32_t ___NMIEDBOEIJP_15;
	// System.Int32 MediaPlayerCtrl::EEKCFBFHCKN
	int32_t ___EEKCFBFHCKN_16;
	// System.Boolean MediaPlayerCtrl::KPFIKJCMFHL
	bool ___KPFIKJCMFHL_17;
	// System.Boolean MediaPlayerCtrl::JHEPNDDEMKF
	bool ___JHEPNDDEMKF_18;
	// MediaPlayerCtrl/BCENGFHBBOI MediaPlayerCtrl::m_ScaleValue
	int32_t ___m_ScaleValue_19;
	// UnityEngine.GameObject[] MediaPlayerCtrl::m_objResize
	GameObjectU5BU5D_t3057952154* ___m_objResize_20;
	// System.Boolean MediaPlayerCtrl::m_bLoop
	bool ___m_bLoop_21;
	// System.Boolean MediaPlayerCtrl::m_bAutoPlay
	bool ___m_bAutoPlay_22;
	// System.Boolean MediaPlayerCtrl::AHBCOEDBHPB
	bool ___AHBCOEDBHPB_23;
	// System.Boolean MediaPlayerCtrl::m_bInit
	bool ___m_bInit_24;
	// System.Boolean MediaPlayerCtrl::LBGKOPJKMLN
	bool ___LBGKOPJKMLN_25;
	// System.Boolean MediaPlayerCtrl::FDHFBCKLMIG
	bool ___FDHFBCKLMIG_26;
	// System.Int32 MediaPlayerCtrl::IKNNGFLLGPB
	int32_t ___IKNNGFLLGPB_27;
	// UnityEngine.Texture2D MediaPlayerCtrl::LPOPFENPPMN
	Texture2D_t3542995729 * ___LPOPFENPPMN_28;

public:
	inline static int32_t get_offset_of_m_strFileName_2() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_strFileName_2)); }
	inline String_t* get_m_strFileName_2() const { return ___m_strFileName_2; }
	inline String_t** get_address_of_m_strFileName_2() { return &___m_strFileName_2; }
	inline void set_m_strFileName_2(String_t* value)
	{
		___m_strFileName_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_strFileName_2, value);
	}

	inline static int32_t get_offset_of_m_TargetMaterial_3() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_TargetMaterial_3)); }
	inline GameObjectU5BU5D_t3057952154* get_m_TargetMaterial_3() const { return ___m_TargetMaterial_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_TargetMaterial_3() { return &___m_TargetMaterial_3; }
	inline void set_m_TargetMaterial_3(GameObjectU5BU5D_t3057952154* value)
	{
		___m_TargetMaterial_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_TargetMaterial_3, value);
	}

	inline static int32_t get_offset_of_OJFFFGDDMIL_4() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___OJFFFGDDMIL_4)); }
	inline Texture2D_t3542995729 * get_OJFFFGDDMIL_4() const { return ___OJFFFGDDMIL_4; }
	inline Texture2D_t3542995729 ** get_address_of_OJFFFGDDMIL_4() { return &___OJFFFGDDMIL_4; }
	inline void set_OJFFFGDDMIL_4(Texture2D_t3542995729 * value)
	{
		___OJFFFGDDMIL_4 = value;
		Il2CppCodeGenWriteBarrier(&___OJFFFGDDMIL_4, value);
	}

	inline static int32_t get_offset_of_FGAMFEHIGBA_5() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___FGAMFEHIGBA_5)); }
	inline Texture2D_t3542995729 * get_FGAMFEHIGBA_5() const { return ___FGAMFEHIGBA_5; }
	inline Texture2D_t3542995729 ** get_address_of_FGAMFEHIGBA_5() { return &___FGAMFEHIGBA_5; }
	inline void set_FGAMFEHIGBA_5(Texture2D_t3542995729 * value)
	{
		___FGAMFEHIGBA_5 = value;
		Il2CppCodeGenWriteBarrier(&___FGAMFEHIGBA_5, value);
	}

	inline static int32_t get_offset_of_MLMOCCFIOMO_6() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___MLMOCCFIOMO_6)); }
	inline int32_t get_MLMOCCFIOMO_6() const { return ___MLMOCCFIOMO_6; }
	inline int32_t* get_address_of_MLMOCCFIOMO_6() { return &___MLMOCCFIOMO_6; }
	inline void set_MLMOCCFIOMO_6(int32_t value)
	{
		___MLMOCCFIOMO_6 = value;
	}

	inline static int32_t get_offset_of_EMBDGBJEJPH_7() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___EMBDGBJEJPH_7)); }
	inline int32_t get_EMBDGBJEJPH_7() const { return ___EMBDGBJEJPH_7; }
	inline int32_t* get_address_of_EMBDGBJEJPH_7() { return &___EMBDGBJEJPH_7; }
	inline void set_EMBDGBJEJPH_7(int32_t value)
	{
		___EMBDGBJEJPH_7 = value;
	}

	inline static int32_t get_offset_of_IENGHMEHDND_8() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___IENGHMEHDND_8)); }
	inline float get_IENGHMEHDND_8() const { return ___IENGHMEHDND_8; }
	inline float* get_address_of_IENGHMEHDND_8() { return &___IENGHMEHDND_8; }
	inline void set_IENGHMEHDND_8(float value)
	{
		___IENGHMEHDND_8 = value;
	}

	inline static int32_t get_offset_of_m_bFullScreen_9() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bFullScreen_9)); }
	inline bool get_m_bFullScreen_9() const { return ___m_bFullScreen_9; }
	inline bool* get_address_of_m_bFullScreen_9() { return &___m_bFullScreen_9; }
	inline void set_m_bFullScreen_9(bool value)
	{
		___m_bFullScreen_9 = value;
	}

	inline static int32_t get_offset_of_m_bSupportRockchip_10() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bSupportRockchip_10)); }
	inline bool get_m_bSupportRockchip_10() const { return ___m_bSupportRockchip_10; }
	inline bool* get_address_of_m_bSupportRockchip_10() { return &___m_bSupportRockchip_10; }
	inline void set_m_bSupportRockchip_10(bool value)
	{
		___m_bSupportRockchip_10 = value;
	}

	inline static int32_t get_offset_of_OnReady_11() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___OnReady_11)); }
	inline DPHJBDJDHMD_t4102044392 * get_OnReady_11() const { return ___OnReady_11; }
	inline DPHJBDJDHMD_t4102044392 ** get_address_of_OnReady_11() { return &___OnReady_11; }
	inline void set_OnReady_11(DPHJBDJDHMD_t4102044392 * value)
	{
		___OnReady_11 = value;
		Il2CppCodeGenWriteBarrier(&___OnReady_11, value);
	}

	inline static int32_t get_offset_of_OnEnd_12() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___OnEnd_12)); }
	inline KBIFEBKFHJP_t9122301 * get_OnEnd_12() const { return ___OnEnd_12; }
	inline KBIFEBKFHJP_t9122301 ** get_address_of_OnEnd_12() { return &___OnEnd_12; }
	inline void set_OnEnd_12(KBIFEBKFHJP_t9122301 * value)
	{
		___OnEnd_12 = value;
		Il2CppCodeGenWriteBarrier(&___OnEnd_12, value);
	}

	inline static int32_t get_offset_of_OnVideoError_13() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___OnVideoError_13)); }
	inline NJENMFCKEPC_t2069907229 * get_OnVideoError_13() const { return ___OnVideoError_13; }
	inline NJENMFCKEPC_t2069907229 ** get_address_of_OnVideoError_13() { return &___OnVideoError_13; }
	inline void set_OnVideoError_13(NJENMFCKEPC_t2069907229 * value)
	{
		___OnVideoError_13 = value;
		Il2CppCodeGenWriteBarrier(&___OnVideoError_13, value);
	}

	inline static int32_t get_offset_of_OnVideoFirstFrameReady_14() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___OnVideoFirstFrameReady_14)); }
	inline HDIIHNOEAOH_t1508400025 * get_OnVideoFirstFrameReady_14() const { return ___OnVideoFirstFrameReady_14; }
	inline HDIIHNOEAOH_t1508400025 ** get_address_of_OnVideoFirstFrameReady_14() { return &___OnVideoFirstFrameReady_14; }
	inline void set_OnVideoFirstFrameReady_14(HDIIHNOEAOH_t1508400025 * value)
	{
		___OnVideoFirstFrameReady_14 = value;
		Il2CppCodeGenWriteBarrier(&___OnVideoFirstFrameReady_14, value);
	}

	inline static int32_t get_offset_of_NMIEDBOEIJP_15() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___NMIEDBOEIJP_15)); }
	inline int32_t get_NMIEDBOEIJP_15() const { return ___NMIEDBOEIJP_15; }
	inline int32_t* get_address_of_NMIEDBOEIJP_15() { return &___NMIEDBOEIJP_15; }
	inline void set_NMIEDBOEIJP_15(int32_t value)
	{
		___NMIEDBOEIJP_15 = value;
	}

	inline static int32_t get_offset_of_EEKCFBFHCKN_16() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___EEKCFBFHCKN_16)); }
	inline int32_t get_EEKCFBFHCKN_16() const { return ___EEKCFBFHCKN_16; }
	inline int32_t* get_address_of_EEKCFBFHCKN_16() { return &___EEKCFBFHCKN_16; }
	inline void set_EEKCFBFHCKN_16(int32_t value)
	{
		___EEKCFBFHCKN_16 = value;
	}

	inline static int32_t get_offset_of_KPFIKJCMFHL_17() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___KPFIKJCMFHL_17)); }
	inline bool get_KPFIKJCMFHL_17() const { return ___KPFIKJCMFHL_17; }
	inline bool* get_address_of_KPFIKJCMFHL_17() { return &___KPFIKJCMFHL_17; }
	inline void set_KPFIKJCMFHL_17(bool value)
	{
		___KPFIKJCMFHL_17 = value;
	}

	inline static int32_t get_offset_of_JHEPNDDEMKF_18() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___JHEPNDDEMKF_18)); }
	inline bool get_JHEPNDDEMKF_18() const { return ___JHEPNDDEMKF_18; }
	inline bool* get_address_of_JHEPNDDEMKF_18() { return &___JHEPNDDEMKF_18; }
	inline void set_JHEPNDDEMKF_18(bool value)
	{
		___JHEPNDDEMKF_18 = value;
	}

	inline static int32_t get_offset_of_m_ScaleValue_19() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_ScaleValue_19)); }
	inline int32_t get_m_ScaleValue_19() const { return ___m_ScaleValue_19; }
	inline int32_t* get_address_of_m_ScaleValue_19() { return &___m_ScaleValue_19; }
	inline void set_m_ScaleValue_19(int32_t value)
	{
		___m_ScaleValue_19 = value;
	}

	inline static int32_t get_offset_of_m_objResize_20() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_objResize_20)); }
	inline GameObjectU5BU5D_t3057952154* get_m_objResize_20() const { return ___m_objResize_20; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_objResize_20() { return &___m_objResize_20; }
	inline void set_m_objResize_20(GameObjectU5BU5D_t3057952154* value)
	{
		___m_objResize_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_objResize_20, value);
	}

	inline static int32_t get_offset_of_m_bLoop_21() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bLoop_21)); }
	inline bool get_m_bLoop_21() const { return ___m_bLoop_21; }
	inline bool* get_address_of_m_bLoop_21() { return &___m_bLoop_21; }
	inline void set_m_bLoop_21(bool value)
	{
		___m_bLoop_21 = value;
	}

	inline static int32_t get_offset_of_m_bAutoPlay_22() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bAutoPlay_22)); }
	inline bool get_m_bAutoPlay_22() const { return ___m_bAutoPlay_22; }
	inline bool* get_address_of_m_bAutoPlay_22() { return &___m_bAutoPlay_22; }
	inline void set_m_bAutoPlay_22(bool value)
	{
		___m_bAutoPlay_22 = value;
	}

	inline static int32_t get_offset_of_AHBCOEDBHPB_23() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___AHBCOEDBHPB_23)); }
	inline bool get_AHBCOEDBHPB_23() const { return ___AHBCOEDBHPB_23; }
	inline bool* get_address_of_AHBCOEDBHPB_23() { return &___AHBCOEDBHPB_23; }
	inline void set_AHBCOEDBHPB_23(bool value)
	{
		___AHBCOEDBHPB_23 = value;
	}

	inline static int32_t get_offset_of_m_bInit_24() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bInit_24)); }
	inline bool get_m_bInit_24() const { return ___m_bInit_24; }
	inline bool* get_address_of_m_bInit_24() { return &___m_bInit_24; }
	inline void set_m_bInit_24(bool value)
	{
		___m_bInit_24 = value;
	}

	inline static int32_t get_offset_of_LBGKOPJKMLN_25() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___LBGKOPJKMLN_25)); }
	inline bool get_LBGKOPJKMLN_25() const { return ___LBGKOPJKMLN_25; }
	inline bool* get_address_of_LBGKOPJKMLN_25() { return &___LBGKOPJKMLN_25; }
	inline void set_LBGKOPJKMLN_25(bool value)
	{
		___LBGKOPJKMLN_25 = value;
	}

	inline static int32_t get_offset_of_FDHFBCKLMIG_26() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___FDHFBCKLMIG_26)); }
	inline bool get_FDHFBCKLMIG_26() const { return ___FDHFBCKLMIG_26; }
	inline bool* get_address_of_FDHFBCKLMIG_26() { return &___FDHFBCKLMIG_26; }
	inline void set_FDHFBCKLMIG_26(bool value)
	{
		___FDHFBCKLMIG_26 = value;
	}

	inline static int32_t get_offset_of_IKNNGFLLGPB_27() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___IKNNGFLLGPB_27)); }
	inline int32_t get_IKNNGFLLGPB_27() const { return ___IKNNGFLLGPB_27; }
	inline int32_t* get_address_of_IKNNGFLLGPB_27() { return &___IKNNGFLLGPB_27; }
	inline void set_IKNNGFLLGPB_27(int32_t value)
	{
		___IKNNGFLLGPB_27 = value;
	}

	inline static int32_t get_offset_of_LPOPFENPPMN_28() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___LPOPFENPPMN_28)); }
	inline Texture2D_t3542995729 * get_LPOPFENPPMN_28() const { return ___LPOPFENPPMN_28; }
	inline Texture2D_t3542995729 ** get_address_of_LPOPFENPPMN_28() { return &___LPOPFENPPMN_28; }
	inline void set_LPOPFENPPMN_28(Texture2D_t3542995729 * value)
	{
		___LPOPFENPPMN_28 = value;
		Il2CppCodeGenWriteBarrier(&___LPOPFENPPMN_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
