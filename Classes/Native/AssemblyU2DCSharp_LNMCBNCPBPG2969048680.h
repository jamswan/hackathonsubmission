﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LNMCBNCPBPG
struct  LNMCBNCPBPG_t2969048680  : public Il2CppObject
{
public:
	// System.String LNMCBNCPBPG::GHJFMBOIPDE
	String_t* ___GHJFMBOIPDE_0;
	// System.String LNMCBNCPBPG::ICKMGMCBBGA
	String_t* ___ICKMGMCBBGA_1;
	// System.IntPtr LNMCBNCPBPG::POLCFLGGFAL
	IntPtr_t ___POLCFLGGFAL_2;

public:
	inline static int32_t get_offset_of_GHJFMBOIPDE_0() { return static_cast<int32_t>(offsetof(LNMCBNCPBPG_t2969048680, ___GHJFMBOIPDE_0)); }
	inline String_t* get_GHJFMBOIPDE_0() const { return ___GHJFMBOIPDE_0; }
	inline String_t** get_address_of_GHJFMBOIPDE_0() { return &___GHJFMBOIPDE_0; }
	inline void set_GHJFMBOIPDE_0(String_t* value)
	{
		___GHJFMBOIPDE_0 = value;
		Il2CppCodeGenWriteBarrier(&___GHJFMBOIPDE_0, value);
	}

	inline static int32_t get_offset_of_ICKMGMCBBGA_1() { return static_cast<int32_t>(offsetof(LNMCBNCPBPG_t2969048680, ___ICKMGMCBBGA_1)); }
	inline String_t* get_ICKMGMCBBGA_1() const { return ___ICKMGMCBBGA_1; }
	inline String_t** get_address_of_ICKMGMCBBGA_1() { return &___ICKMGMCBBGA_1; }
	inline void set_ICKMGMCBBGA_1(String_t* value)
	{
		___ICKMGMCBBGA_1 = value;
		Il2CppCodeGenWriteBarrier(&___ICKMGMCBBGA_1, value);
	}

	inline static int32_t get_offset_of_POLCFLGGFAL_2() { return static_cast<int32_t>(offsetof(LNMCBNCPBPG_t2969048680, ___POLCFLGGFAL_2)); }
	inline IntPtr_t get_POLCFLGGFAL_2() const { return ___POLCFLGGFAL_2; }
	inline IntPtr_t* get_address_of_POLCFLGGFAL_2() { return &___POLCFLGGFAL_2; }
	inline void set_POLCFLGGFAL_2(IntPtr_t value)
	{
		___POLCFLGGFAL_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
