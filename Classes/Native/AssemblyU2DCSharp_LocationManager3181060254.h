﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Singleton_1_gen61977345.h"

// System.String
struct String_t;
// LocationManager/NEPBPOIONDG
struct NEPBPOIONDG_t194714732;
// LocationManager/HKHFLGLBEKJ
struct HKHFLGLBEKJ_t3398908651;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocationManager
struct  LocationManager_t3181060254  : public Singleton_1_t61977345
{
public:
	// System.String LocationManager::KALMLPEGDFC
	String_t* ___KALMLPEGDFC_5;
	// System.String LocationManager::MCABJJDMKKD
	String_t* ___MCABJJDMKKD_6;
	// LocationManager/NEPBPOIONDG LocationManager::NewPlace
	NEPBPOIONDG_t194714732 * ___NewPlace_7;
	// LocationManager/NEPBPOIONDG LocationManager::NewLocation
	NEPBPOIONDG_t194714732 * ___NewLocation_8;
	// LocationManager/HKHFLGLBEKJ LocationManager::NewTarget
	HKHFLGLBEKJ_t3398908651 * ___NewTarget_9;
	// System.String LocationManager::previousTarget
	String_t* ___previousTarget_10;
	// System.String LocationManager::MJMKEOPFCAM
	String_t* ___MJMKEOPFCAM_11;

public:
	inline static int32_t get_offset_of_KALMLPEGDFC_5() { return static_cast<int32_t>(offsetof(LocationManager_t3181060254, ___KALMLPEGDFC_5)); }
	inline String_t* get_KALMLPEGDFC_5() const { return ___KALMLPEGDFC_5; }
	inline String_t** get_address_of_KALMLPEGDFC_5() { return &___KALMLPEGDFC_5; }
	inline void set_KALMLPEGDFC_5(String_t* value)
	{
		___KALMLPEGDFC_5 = value;
		Il2CppCodeGenWriteBarrier(&___KALMLPEGDFC_5, value);
	}

	inline static int32_t get_offset_of_MCABJJDMKKD_6() { return static_cast<int32_t>(offsetof(LocationManager_t3181060254, ___MCABJJDMKKD_6)); }
	inline String_t* get_MCABJJDMKKD_6() const { return ___MCABJJDMKKD_6; }
	inline String_t** get_address_of_MCABJJDMKKD_6() { return &___MCABJJDMKKD_6; }
	inline void set_MCABJJDMKKD_6(String_t* value)
	{
		___MCABJJDMKKD_6 = value;
		Il2CppCodeGenWriteBarrier(&___MCABJJDMKKD_6, value);
	}

	inline static int32_t get_offset_of_NewPlace_7() { return static_cast<int32_t>(offsetof(LocationManager_t3181060254, ___NewPlace_7)); }
	inline NEPBPOIONDG_t194714732 * get_NewPlace_7() const { return ___NewPlace_7; }
	inline NEPBPOIONDG_t194714732 ** get_address_of_NewPlace_7() { return &___NewPlace_7; }
	inline void set_NewPlace_7(NEPBPOIONDG_t194714732 * value)
	{
		___NewPlace_7 = value;
		Il2CppCodeGenWriteBarrier(&___NewPlace_7, value);
	}

	inline static int32_t get_offset_of_NewLocation_8() { return static_cast<int32_t>(offsetof(LocationManager_t3181060254, ___NewLocation_8)); }
	inline NEPBPOIONDG_t194714732 * get_NewLocation_8() const { return ___NewLocation_8; }
	inline NEPBPOIONDG_t194714732 ** get_address_of_NewLocation_8() { return &___NewLocation_8; }
	inline void set_NewLocation_8(NEPBPOIONDG_t194714732 * value)
	{
		___NewLocation_8 = value;
		Il2CppCodeGenWriteBarrier(&___NewLocation_8, value);
	}

	inline static int32_t get_offset_of_NewTarget_9() { return static_cast<int32_t>(offsetof(LocationManager_t3181060254, ___NewTarget_9)); }
	inline HKHFLGLBEKJ_t3398908651 * get_NewTarget_9() const { return ___NewTarget_9; }
	inline HKHFLGLBEKJ_t3398908651 ** get_address_of_NewTarget_9() { return &___NewTarget_9; }
	inline void set_NewTarget_9(HKHFLGLBEKJ_t3398908651 * value)
	{
		___NewTarget_9 = value;
		Il2CppCodeGenWriteBarrier(&___NewTarget_9, value);
	}

	inline static int32_t get_offset_of_previousTarget_10() { return static_cast<int32_t>(offsetof(LocationManager_t3181060254, ___previousTarget_10)); }
	inline String_t* get_previousTarget_10() const { return ___previousTarget_10; }
	inline String_t** get_address_of_previousTarget_10() { return &___previousTarget_10; }
	inline void set_previousTarget_10(String_t* value)
	{
		___previousTarget_10 = value;
		Il2CppCodeGenWriteBarrier(&___previousTarget_10, value);
	}

	inline static int32_t get_offset_of_MJMKEOPFCAM_11() { return static_cast<int32_t>(offsetof(LocationManager_t3181060254, ___MJMKEOPFCAM_11)); }
	inline String_t* get_MJMKEOPFCAM_11() const { return ___MJMKEOPFCAM_11; }
	inline String_t** get_address_of_MJMKEOPFCAM_11() { return &___MJMKEOPFCAM_11; }
	inline void set_MJMKEOPFCAM_11(String_t* value)
	{
		___MJMKEOPFCAM_11 = value;
		Il2CppCodeGenWriteBarrier(&___MJMKEOPFCAM_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
