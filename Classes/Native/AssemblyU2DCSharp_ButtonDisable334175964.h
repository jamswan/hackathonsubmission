﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonDisable
struct  ButtonDisable_t334175964  : public NarrativeCondition_t4123859457
{
public:
	// System.Int32 ButtonDisable::location
	int32_t ___location_13;
	// System.String ButtonDisable::text
	String_t* ___text_14;
	// System.Boolean ButtonDisable::Force
	bool ___Force_15;

public:
	inline static int32_t get_offset_of_location_13() { return static_cast<int32_t>(offsetof(ButtonDisable_t334175964, ___location_13)); }
	inline int32_t get_location_13() const { return ___location_13; }
	inline int32_t* get_address_of_location_13() { return &___location_13; }
	inline void set_location_13(int32_t value)
	{
		___location_13 = value;
	}

	inline static int32_t get_offset_of_text_14() { return static_cast<int32_t>(offsetof(ButtonDisable_t334175964, ___text_14)); }
	inline String_t* get_text_14() const { return ___text_14; }
	inline String_t** get_address_of_text_14() { return &___text_14; }
	inline void set_text_14(String_t* value)
	{
		___text_14 = value;
		Il2CppCodeGenWriteBarrier(&___text_14, value);
	}

	inline static int32_t get_offset_of_Force_15() { return static_cast<int32_t>(offsetof(ButtonDisable_t334175964, ___Force_15)); }
	inline bool get_Force_15() const { return ___Force_15; }
	inline bool* get_address_of_Force_15() { return &___Force_15; }
	inline void set_Force_15(bool value)
	{
		___Force_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
