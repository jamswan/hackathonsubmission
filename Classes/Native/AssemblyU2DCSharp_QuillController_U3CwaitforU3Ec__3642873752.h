﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Action
struct Action_t3226471752;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuillController/<waitfor>c__Iterator1
struct  U3CwaitforU3Ec__Iterator1_t3642873752  : public Il2CppObject
{
public:
	// System.Single QuillController/<waitfor>c__Iterator1::KJGAGDEJFFK
	float ___KJGAGDEJFFK_0;
	// System.Action QuillController/<waitfor>c__Iterator1::PLLPPEEBFAP
	Action_t3226471752 * ___PLLPPEEBFAP_1;
	// System.Object QuillController/<waitfor>c__Iterator1::LGBFNMECDHC
	Il2CppObject * ___LGBFNMECDHC_2;
	// System.Boolean QuillController/<waitfor>c__Iterator1::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_3;
	// System.Int32 QuillController/<waitfor>c__Iterator1::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_4;

public:
	inline static int32_t get_offset_of_KJGAGDEJFFK_0() { return static_cast<int32_t>(offsetof(U3CwaitforU3Ec__Iterator1_t3642873752, ___KJGAGDEJFFK_0)); }
	inline float get_KJGAGDEJFFK_0() const { return ___KJGAGDEJFFK_0; }
	inline float* get_address_of_KJGAGDEJFFK_0() { return &___KJGAGDEJFFK_0; }
	inline void set_KJGAGDEJFFK_0(float value)
	{
		___KJGAGDEJFFK_0 = value;
	}

	inline static int32_t get_offset_of_PLLPPEEBFAP_1() { return static_cast<int32_t>(offsetof(U3CwaitforU3Ec__Iterator1_t3642873752, ___PLLPPEEBFAP_1)); }
	inline Action_t3226471752 * get_PLLPPEEBFAP_1() const { return ___PLLPPEEBFAP_1; }
	inline Action_t3226471752 ** get_address_of_PLLPPEEBFAP_1() { return &___PLLPPEEBFAP_1; }
	inline void set_PLLPPEEBFAP_1(Action_t3226471752 * value)
	{
		___PLLPPEEBFAP_1 = value;
		Il2CppCodeGenWriteBarrier(&___PLLPPEEBFAP_1, value);
	}

	inline static int32_t get_offset_of_LGBFNMECDHC_2() { return static_cast<int32_t>(offsetof(U3CwaitforU3Ec__Iterator1_t3642873752, ___LGBFNMECDHC_2)); }
	inline Il2CppObject * get_LGBFNMECDHC_2() const { return ___LGBFNMECDHC_2; }
	inline Il2CppObject ** get_address_of_LGBFNMECDHC_2() { return &___LGBFNMECDHC_2; }
	inline void set_LGBFNMECDHC_2(Il2CppObject * value)
	{
		___LGBFNMECDHC_2 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_2, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_3() { return static_cast<int32_t>(offsetof(U3CwaitforU3Ec__Iterator1_t3642873752, ___IMAGLIMLPFK_3)); }
	inline bool get_IMAGLIMLPFK_3() const { return ___IMAGLIMLPFK_3; }
	inline bool* get_address_of_IMAGLIMLPFK_3() { return &___IMAGLIMLPFK_3; }
	inline void set_IMAGLIMLPFK_3(bool value)
	{
		___IMAGLIMLPFK_3 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_4() { return static_cast<int32_t>(offsetof(U3CwaitforU3Ec__Iterator1_t3642873752, ___EPCFNNGDBGC_4)); }
	inline int32_t get_EPCFNNGDBGC_4() const { return ___EPCFNNGDBGC_4; }
	inline int32_t* get_address_of_EPCFNNGDBGC_4() { return &___EPCFNNGDBGC_4; }
	inline void set_EPCFNNGDBGC_4(int32_t value)
	{
		___EPCFNNGDBGC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
