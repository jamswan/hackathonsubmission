﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MNHAJJFNAFB1310365889.h"
#include "AssemblyU2DCSharp_GvrPointerGraphicRaycaster1649506702.h"
#include "AssemblyU2DCSharp_GvrPointerGraphicRaycaster_ANDCO1527353484.h"
#include "AssemblyU2DCSharp_GvrPointerInputModule1603976810.h"
#include "AssemblyU2DCSharp_GvrPointerPhysicsRaycaster2558158517.h"
#include "AssemblyU2DCSharp_CCKAJFFEBPK3986018998.h"
#include "AssemblyU2DCSharp_GvrViewer2583885279.h"
#include "AssemblyU2DCSharp_GvrViewer_PIKNPGGIJMF2016830092.h"
#include "AssemblyU2DCSharp_GvrViewer_BKCOFAHPJHN660377940.h"
#include "AssemblyU2DCSharp_GvrViewer_MIEIKCNOKMB1094753305.h"
#include "AssemblyU2DCSharp_GvrViewer_FKEGMFBCHEH1921487960.h"
#include "AssemblyU2DCSharp_DEDPNODFPKJ3965868249.h"
#include "AssemblyU2DCSharp_ENOAJGBOEPC2312888837.h"
#include "AssemblyU2DCSharp_GvrDropdown2234606196.h"
#include "AssemblyU2DCSharp_GvrLaserPointer2879974839.h"
#include "AssemblyU2DCSharp_LHNONKLGLJM3154435330.h"
#include "AssemblyU2DCSharp_GvrReticlePointer2836438988.h"
#include "AssemblyU2DCSharp_IGHKEEOAKDP3590090476.h"
#include "AssemblyU2DCSharp_CLMFPLGBKNK864559409.h"
#include "AssemblyU2DCSharp_GvrFPS750935016.h"
#include "AssemblyU2DCSharp_AEMKKCGEHBD322909534.h"
#include "AssemblyU2DCSharp_PJJNKINPAEK3708484331.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture673526704.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_NDBLEKFJACN774508329.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_MHDNMHAPLO1088781421.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_OLFIBMLEAH1971693957.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_FONBIOMKJEP138175225.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_EMHHHFBNMJ4234973779.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_NNKMLFAKIH2285959827.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_PBCECLKMLFI589383309.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CStartU32919987970.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CCallPlu2924899295.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CInternal129051828.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CInterna2021693473.h"
#include "AssemblyU2DCSharp_ThreeDUI204342518.h"
#include "AssemblyU2DCSharp_magicLetter3620962857.h"
#include "AssemblyU2DCSharp_magicLetter_U3CsetFontSizeU3Ec__2254649741.h"
#include "AssemblyU2DCSharp_magicText4012646348.h"
#include "AssemblyU2DCSharp_magicText_CNKPOABMIJM1800042644.h"
#include "AssemblyU2DCSharp_VideoPlaybackBehaviour222960481.h"
#include "AssemblyU2DCSharp_VideoPlaybackBehaviour_U3CResetT3955307691.h"
#include "AssemblyU2DCSharp_LNMCBNCPBPG2969048680.h"
#include "AssemblyU2DCSharp_LNMCBNCPBPG_DGIJFIBFFPD968251196.h"
#include "AssemblyU2DCSharp_LNMCBNCPBPG_EPPHLMJAIBL3913485307.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour2431285219.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3077176941.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour2091399712.h"
#include "AssemblyU2DCSharp_Vuforia_DatabaseLoadBehaviour450246482.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErro965510117.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH870608571.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan1082256726.h"
#include "AssemblyU2DCSharp_Vuforia_DeviceTrackerBehaviour1884988499.h"
#include "AssemblyU2DCSharp_Vuforia_DigitalEyewearBehaviour495394589.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler3809113141.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour3495034315.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour2654589389.h"
#include "AssemblyU2DCSharp_Vuforia_OBLIGPGBOJF3834424063.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB3249343815.h"
#include "AssemblyU2DCSharp_Vuforia_MHAOKAIGBBF1137417231.h"
#include "AssemblyU2DCSharp_Vuforia_JEAELKHNPJM4251970019.h"
#include "AssemblyU2DCSharp_Vuforia_KDDDFNIEPOB809780732.h"
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour1532923751.h"
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour1265232977.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour2994129365.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour3504654311.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour3836044259.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour966064926.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour4009935945.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTarget2111803406.h"
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehav3844158157.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour2405314212.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour3400239837.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour3058161409.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour584991835.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin4184040062.h"
#include "AssemblyU2DCSharp_VRIntegrationHelper556656694.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour3161817952.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundManager3515346924.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2515041812.h"
#include "AssemblyU2DCSharp_Vuforia_VuMarkBehaviour2060629989.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour359035403.h"
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour407765638.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour2494532455.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH1535150527.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour3366478421.h"
#include "AssemblyU2DCSharp_Text3D62691636.h"
#include "AssemblyU2DCSharp_Text3D_EGCLHEJBIKM4071414764.h"
#include "AssemblyU2DCSharp_Text3DExample192478882.h"
#include "AssemblyU2DCSharp_Text3DManager1976449151.h"
#include "AssemblyU2DCSharp_LetterSizes1582613310.h"
#include "AssemblyU2DCSharp_HDNFKHHFDGG1814939731.h"
#include "AssemblyU2DCSharp_FNIOBOKOKNO3395776259.h"
#include "AssemblyU2DCSharp_AEHNGNBNKNH1552223508.h"
#include "AssemblyU2DCSharp_DataSetManager4144645295.h"
#include "AssemblyU2DCSharp_DataSetManager_U3CWaitForLoadU3E2529810677.h"
#include "AssemblyU2DCSharp_LoadedDataSet1410136239.h"
#include "AssemblyU2DCSharp_GimbalListener163996074.h"
#include "AssemblyU2DCSharp_LocationManager3181060254.h"
#include "AssemblyU2DCSharp_LocationManager_GLDMKCBJKLK3811661921.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (MNHAJJFNAFB_t1310365889), -1, sizeof(MNHAJJFNAFB_t1310365889_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2301[2] = 
{
	MNHAJJFNAFB_t1310365889_StaticFields::get_offset_of_NBOIJKLFLJL_0(),
	MNHAJJFNAFB_t1310365889_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (GvrPointerGraphicRaycaster_t1649506702), -1, sizeof(GvrPointerGraphicRaycaster_t1649506702_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2302[9] = 
{
	0,
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_ignoreReversedGraphics_5(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_blockingObjects_6(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_blockingMask_7(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_BDAPKBKPNFL_8(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_LANKDNECCCD_9(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_HFJJEBIAPFJ_10(),
	GvrPointerGraphicRaycaster_t1649506702_StaticFields::get_offset_of_DIHNIKEHCFP_11(),
	GvrPointerGraphicRaycaster_t1649506702_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (ANDCOEEBLJH_t1527353484)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2303[5] = 
{
	ANDCOEEBLJH_t1527353484::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (GvrPointerInputModule_t1603976810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[9] = 
{
	GvrPointerInputModule_t1603976810::get_offset_of_vrModeOnly_8(),
	GvrPointerInputModule_t1603976810::get_offset_of_LHLLOCFLGCJ_9(),
	GvrPointerInputModule_t1603976810::get_offset_of_OIJLKMOAKHM_10(),
	GvrPointerInputModule_t1603976810::get_offset_of_LLNECJFHFCK_11(),
	GvrPointerInputModule_t1603976810::get_offset_of_DEJGPGIIABI_12(),
	GvrPointerInputModule_t1603976810::get_offset_of_OOHKMKEDDKP_13(),
	GvrPointerInputModule_t1603976810::get_offset_of_AKNJNLJHIGB_14(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (GvrPointerPhysicsRaycaster_t2558158517), -1, sizeof(GvrPointerPhysicsRaycaster_t2558158517_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2305[4] = 
{
	0,
	GvrPointerPhysicsRaycaster_t2558158517::get_offset_of_raycasterEventMask_5(),
	GvrPointerPhysicsRaycaster_t2558158517::get_offset_of_BPFPMIFDCFA_6(),
	GvrPointerPhysicsRaycaster_t2558158517_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (CCKAJFFEBPK_t3986018998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (GvrViewer_t2583885279), -1, sizeof(GvrViewer_t2583885279_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2307[20] = 
{
	0,
	GvrViewer_t2583885279_StaticFields::get_offset_of_OLMOHEEKDMC_3(),
	GvrViewer_t2583885279_StaticFields::get_offset_of_NNHNEIGFDPN_4(),
	GvrViewer_t2583885279_StaticFields::get_offset_of_EDEIECMOKKM_5(),
	GvrViewer_t2583885279::get_offset_of_vrModeEnabled_6(),
	GvrViewer_t2583885279::get_offset_of_distortionCorrection_7(),
	GvrViewer_t2583885279::get_offset_of_neckModelScale_8(),
	GvrViewer_t2583885279_StaticFields::get_offset_of_FCEOCGPNFEC_9(),
	GvrViewer_t2583885279::get_offset_of_U3CGGHAFPCOIEOU3Ek__BackingField_10(),
	GvrViewer_t2583885279::get_offset_of_U3CIIMPFPMMONPU3Ek__BackingField_11(),
	GvrViewer_t2583885279::get_offset_of_stereoScreenScale_12(),
	GvrViewer_t2583885279_StaticFields::get_offset_of_NKHOKKEKDJA_13(),
	GvrViewer_t2583885279::get_offset_of_OnStereoScreenChanged_14(),
	GvrViewer_t2583885279::get_offset_of_CENNILMLHFB_15(),
	GvrViewer_t2583885279::get_offset_of_DefaultDeviceProfile_16(),
	GvrViewer_t2583885279::get_offset_of_U3COBEGINALOIOU3Ek__BackingField_17(),
	GvrViewer_t2583885279::get_offset_of_U3CPDMMEIFEFMPU3Ek__BackingField_18(),
	GvrViewer_t2583885279::get_offset_of_U3CJGIPJKKMJKEU3Ek__BackingField_19(),
	GvrViewer_t2583885279::get_offset_of_U3CGKLHCBNBOKLU3Ek__BackingField_20(),
	GvrViewer_t2583885279::get_offset_of_CJBBJHPNPOO_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (PIKNPGGIJMF_t2016830092)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2308[4] = 
{
	PIKNPGGIJMF_t2016830092::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (BKCOFAHPJHN_t660377940), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (MIEIKCNOKMB_t1094753305)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2310[4] = 
{
	MIEIKCNOKMB_t1094753305::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (FKEGMFBCHEH_t1921487960)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2311[3] = 
{
	FKEGMFBCHEH_t1921487960::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (DEDPNODFPKJ_t3965868249), -1, sizeof(DEDPNODFPKJ_t3965868249_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2312[4] = 
{
	DEDPNODFPKJ_t3965868249_StaticFields::get_offset_of_BDFOMCKLCAE_0(),
	DEDPNODFPKJ_t3965868249::get_offset_of_U3CAJFEJFELOENU3Ek__BackingField_1(),
	DEDPNODFPKJ_t3965868249::get_offset_of_U3CHMFBDDPFDMEU3Ek__BackingField_2(),
	DEDPNODFPKJ_t3965868249::get_offset_of_U3CCMOBGCHPKFAU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (ENOAJGBOEPC_t2312888837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (GvrDropdown_t2234606196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[1] = 
{
	GvrDropdown_t2234606196::get_offset_of_KENCLOHPDHP_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (GvrLaserPointer_t2879974839), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (LHNONKLGLJM_t3154435330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[11] = 
{
	0,
	0,
	LHNONKLGLJM_t3154435330::get_offset_of_U3CNGECFJFDOBDU3Ek__BackingField_4(),
	LHNONKLGLJM_t3154435330::get_offset_of_U3CFJOJKADPKMIU3Ek__BackingField_5(),
	LHNONKLGLJM_t3154435330::get_offset_of_U3CLBHHNFHCHIFU3Ek__BackingField_6(),
	LHNONKLGLJM_t3154435330::get_offset_of_U3CDOOIOHCKFPLU3Ek__BackingField_7(),
	LHNONKLGLJM_t3154435330::get_offset_of_U3COFPPOKOEELDU3Ek__BackingField_8(),
	LHNONKLGLJM_t3154435330::get_offset_of_U3CJAMKMLFAOCGU3Ek__BackingField_9(),
	LHNONKLGLJM_t3154435330::get_offset_of_U3CCLKBAKAIDBFU3Ek__BackingField_10(),
	LHNONKLGLJM_t3154435330::get_offset_of_U3CIMLNFEICBCNU3Ek__BackingField_11(),
	LHNONKLGLJM_t3154435330::get_offset_of_U3CPGFIHODJAJHU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (GvrReticlePointer_t2836438988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[3] = 
{
	GvrReticlePointer_t2836438988::get_offset_of_JAMHFIIINGC_2(),
	GvrReticlePointer_t2836438988::get_offset_of_reticleSegments_3(),
	GvrReticlePointer_t2836438988::get_offset_of_reticleGrowthSpeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (IGHKEEOAKDP_t3590090476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[12] = 
{
	0,
	0,
	0,
	0,
	0,
	IGHKEEOAKDP_t3590090476::get_offset_of_U3CCIFFNPAGDLBU3Ek__BackingField_7(),
	IGHKEEOAKDP_t3590090476::get_offset_of_U3CDCDPLHDGAHFU3Ek__BackingField_8(),
	IGHKEEOAKDP_t3590090476::get_offset_of_U3CHAMJJGLDFNJU3Ek__BackingField_9(),
	IGHKEEOAKDP_t3590090476::get_offset_of_U3CLHFKJHOJJLIU3Ek__BackingField_10(),
	IGHKEEOAKDP_t3590090476::get_offset_of_U3CMBKNCNKEDEDU3Ek__BackingField_11(),
	IGHKEEOAKDP_t3590090476::get_offset_of_U3CNIKGMMEBCGEU3Ek__BackingField_12(),
	IGHKEEOAKDP_t3590090476::get_offset_of_U3CABDFNKGNCBCU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (CLMFPLGBKNK_t864559409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (GvrFPS_t750935016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[6] = 
{
	0,
	0,
	0,
	GvrFPS_t750935016::get_offset_of_AHMHPEMJCIN_5(),
	GvrFPS_t750935016::get_offset_of_CHFMJNJEGEC_6(),
	GvrFPS_t750935016::get_offset_of_cam_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (AEMKKCGEHBD_t322909534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (PJJNKINPAEK_t3708484331), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (GvrVideoPlayerTexture_t673526704), -1, sizeof(GvrVideoPlayerTexture_t673526704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2323[33] = 
{
	0,
	0,
	GvrVideoPlayerTexture_t673526704::get_offset_of_DCFCBLKFOBG_4(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_OCBIFHOKFFG_5(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_MKEKMCMGIEI_6(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_JMJBAFOGOLA_7(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_FFONIJKIJIL_8(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_EJOBGGJEDCF_9(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_NJCNGDNFKBG_10(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_DOIPEKBDDBA_11(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_EDBCILNODMO_12(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_LIJNLNFDPKD_13(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_ANPAPPHLJAN_14(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_KFODGKJCGDB_15(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_LBLCFHNPMPJ_16(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_HLHFHDJEKLO_17(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_GMKILJLIEAP_18(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_CGPOGMLIGNB_19(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_PBBBBJELCGJ_20(),
	GvrVideoPlayerTexture_t673526704_StaticFields::get_offset_of_HMAKNFMDNGJ_21(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_statusText_22(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_bufferSize_23(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoType_24(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoURL_25(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoContentID_26(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoProviderId_27(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_initialResolution_28(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_adjustAspectRatio_29(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_useSecurePath_30(),
	0,
	GvrVideoPlayerTexture_t673526704_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_32(),
	GvrVideoPlayerTexture_t673526704_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_33(),
	GvrVideoPlayerTexture_t673526704_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (NDBLEKFJACN_t774508329)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2324[4] = 
{
	NDBLEKFJACN_t774508329::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (MHDNMHAPLOB_t1088781421)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2325[6] = 
{
	MHDNMHAPLOB_t1088781421::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (OLFIBMLEAHI_t1971693957)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2326[6] = 
{
	OLFIBMLEAHI_t1971693957::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (FONBIOMKJEP_t138175225)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2327[6] = 
{
	FONBIOMKJEP_t138175225::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (EMHHHFBNMJA_t4234973779)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2328[8] = 
{
	EMHHHFBNMJA_t4234973779::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (NNKMLFAKIHO_t2285959827), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (PBCECLKMLFI_t589383309), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (U3CStartU3Ec__Iterator0_t2919987970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[4] = 
{
	U3CStartU3Ec__Iterator0_t2919987970::get_offset_of_AOOLEAHHMIH_0(),
	U3CStartU3Ec__Iterator0_t2919987970::get_offset_of_LGBFNMECDHC_1(),
	U3CStartU3Ec__Iterator0_t2919987970::get_offset_of_IMAGLIMLPFK_2(),
	U3CStartU3Ec__Iterator0_t2919987970::get_offset_of_EPCFNNGDBGC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[10] = 
{
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_BPHNLOBKGGN_0(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_AJDBPNNEJMP_1(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_IHFNOMJELBH_2(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_DGMMPFCFBDO_3(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_CGCGAGEDEIA_4(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_LDKJKNJKCJF_5(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_AOOLEAHHMIH_6(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_LGBFNMECDHC_7(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_IMAGLIMLPFK_8(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_EPCFNNGDBGC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[2] = 
{
	U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828::get_offset_of_OECALIBMPJK_0(),
	U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828::get_offset_of_DFDMADNLFGI_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[3] = 
{
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473::get_offset_of_OECALIBMPJK_0(),
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473::get_offset_of_LNNKFPBGOCK_1(),
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473::get_offset_of_PLLOJIMPMPE_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (ThreeDUI_t204342518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[1] = 
{
	ThreeDUI_t204342518::get_offset_of_Fonts_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (magicLetter_t3620962857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2336[5] = 
{
	magicLetter_t3620962857::get_offset_of_index_2(),
	magicLetter_t3620962857::get_offset_of_textMesh_3(),
	magicLetter_t3620962857::get_offset_of_renderer_4(),
	magicLetter_t3620962857::get_offset_of_boxCollider_5(),
	magicLetter_t3620962857::get_offset_of_NEENKLOMGFJ_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (U3CsetFontSizeU3Ec__Iterator0_t2254649741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[3] = 
{
	U3CsetFontSizeU3Ec__Iterator0_t2254649741::get_offset_of_LGBFNMECDHC_0(),
	U3CsetFontSizeU3Ec__Iterator0_t2254649741::get_offset_of_IMAGLIMLPFK_1(),
	U3CsetFontSizeU3Ec__Iterator0_t2254649741::get_offset_of_EPCFNNGDBGC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (magicText_t4012646348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[7] = 
{
	magicText_t4012646348::get_offset_of_HJDPLIAGFEP_2(),
	magicText_t4012646348::get_offset_of_IAPJACELLIJ_3(),
	magicText_t4012646348::get_offset_of_COLCJHPAGJN_4(),
	magicText_t4012646348::get_offset_of_HPPGEAFELHG_5(),
	magicText_t4012646348::get_offset_of_MOGMDCBMFHI_6(),
	magicText_t4012646348::get_offset_of_LKFNOALHHHL_7(),
	magicText_t4012646348::get_offset_of_BGHOBDEEOJE_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (CNKPOABMIJM_t1800042644)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2339[4] = 
{
	CNKPOABMIJM_t1800042644::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (VideoPlaybackBehaviour_t222960481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[17] = 
{
	VideoPlaybackBehaviour_t222960481::get_offset_of_m_path_2(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_m_playTexture_3(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_m_busyTexture_4(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_m_errorTexture_5(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_m_autoPlay_6(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_ACBEDADNILK_7(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_DCJGDAJJDKP_8(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_PKBAIDBGGCD_9(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_OPBMKDOIIHC_10(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_KCAGLDMAEOH_11(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_mKeyframeTexture_12(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_BGIEHPJLFLH_13(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_DGANMIDJBGM_14(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_JFBDMGKOFAK_15(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_NGAGMBBHLDI_16(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_GEMELIKMENM_17(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_MNJHGOPFMNL_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (U3CResetToPortraitSmoothlyU3Ec__Iterator0_t3955307691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[3] = 
{
	U3CResetToPortraitSmoothlyU3Ec__Iterator0_t3955307691::get_offset_of_LGBFNMECDHC_0(),
	U3CResetToPortraitSmoothlyU3Ec__Iterator0_t3955307691::get_offset_of_IMAGLIMLPFK_1(),
	U3CResetToPortraitSmoothlyU3Ec__Iterator0_t3955307691::get_offset_of_EPCFNNGDBGC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (LNMCBNCPBPG_t2969048680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[3] = 
{
	LNMCBNCPBPG_t2969048680::get_offset_of_GHJFMBOIPDE_0(),
	LNMCBNCPBPG_t2969048680::get_offset_of_ICKMGMCBBGA_1(),
	LNMCBNCPBPG_t2969048680::get_offset_of_POLCFLGGFAL_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (DGIJFIBFFPD_t968251196)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2343[9] = 
{
	DGIJFIBFFPD_t968251196::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (EPPHLMJAIBL_t3913485307)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2344[4] = 
{
	EPPHLMJAIBL_t3913485307::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (BackgroundPlaneBehaviour_t2431285219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (CloudRecoBehaviour_t3077176941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (CylinderTargetBehaviour_t2091399712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (DatabaseLoadBehaviour_t450246482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (DefaultInitializationErrorHandler_t965510117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[3] = 
{
	DefaultInitializationErrorHandler_t965510117::get_offset_of_DLMMNEGCAKE_2(),
	DefaultInitializationErrorHandler_t965510117::get_offset_of_MEHPMDDJBKE_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (DefaultSmartTerrainEventHandler_t870608571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[3] = 
{
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_NOIAJBFBBBP_2(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_PropTemplate_3(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_SurfaceTemplate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (DefaultTrackableEventHandler_t1082256726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[1] = 
{
	DefaultTrackableEventHandler_t1082256726::get_offset_of_MDEHCLGCIKP_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (DeviceTrackerBehaviour_t1884988499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (DigitalEyewearBehaviour_t495394589), -1, sizeof(DigitalEyewearBehaviour_t495394589_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2353[1] = 
{
	DigitalEyewearBehaviour_t495394589_StaticFields::get_offset_of_LAFNKGCHHID_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (GLErrorHandler_t3809113141), -1, sizeof(GLErrorHandler_t3809113141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2354[3] = 
{
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_DLMMNEGCAKE_2(),
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_MEHPMDDJBKE_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (HideExcessAreaBehaviour_t3495034315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (ImageTargetBehaviour_t2654589389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (OBLIGPGBOJF_t3834424063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[6] = 
{
	0,
	0,
	OBLIGPGBOJF_t3834424063::get_offset_of_KJNPPDBFOMG_2(),
	OBLIGPGBOJF_t3834424063::get_offset_of_POMHIFIBILE_3(),
	OBLIGPGBOJF_t3834424063::get_offset_of_IDCLCHMDDKB_4(),
	OBLIGPGBOJF_t3834424063::get_offset_of_IAFOLDJGNBL_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (ComponentFactoryStarterBehaviour_t3249343815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (MHAOKAIGBBF_t1137417231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[1] = 
{
	MHAOKAIGBBF_t1137417231::get_offset_of_KJNPPDBFOMG_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (JEAELKHNPJM_t4251970019), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (KDDDFNIEPOB_t809780732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[1] = 
{
	KDDDFNIEPOB_t809780732::get_offset_of_KJNPPDBFOMG_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (KeepAliveBehaviour_t1532923751), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (MarkerBehaviour_t1265232977), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (MaskOutBehaviour_t2994129365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (MultiTargetBehaviour_t3504654311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (ObjectTargetBehaviour_t3836044259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (PropBehaviour_t966064926), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (ReconstructionBehaviour_t4009935945), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (ReconstructionFromTargetBehaviour_t2111803406), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (SmartTerrainTrackerBehaviour_t3844158157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (SurfaceBehaviour_t2405314212), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (TextRecoBehaviour_t3400239837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (TurnOffBehaviour_t3058161409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (TurnOffWordBehaviour_t584991835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (UserDefinedTargetBuildingBehaviour_t4184040062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (VRIntegrationHelper_t556656694), -1, sizeof(VRIntegrationHelper_t556656694_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2376[12] = 
{
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_GCLNDKHKABD_2(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_CAECEMMGAEG_3(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_PPMOBGOPIML_4(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_PGKPEGHLGFI_5(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_BBAAELKOOAF_6(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_KAAJFCNDPMM_7(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_LCFGMPMDHKK_8(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_INDLCFFADED_9(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_GGHGBJJNCCC_10(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_LANCOANLELG_11(),
	VRIntegrationHelper_t556656694::get_offset_of_IsLeft_12(),
	VRIntegrationHelper_t556656694::get_offset_of_TrackableParent_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (VideoBackgroundBehaviour_t3161817952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (VideoBackgroundManager_t3515346924), -1, sizeof(VideoBackgroundManager_t3515346924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2378[1] = 
{
	VideoBackgroundManager_t3515346924_StaticFields::get_offset_of_LIHJMAGOOBH_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (VirtualButtonBehaviour_t2515041812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (VuMarkBehaviour_t2060629989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (VuforiaBehaviour_t359035403), -1, sizeof(VuforiaBehaviour_t359035403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2381[1] = 
{
	VuforiaBehaviour_t359035403_StaticFields::get_offset_of_MAKKKFJMJLI_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (WebCamBehaviour_t407765638), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (WireframeBehaviour_t2494532455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[4] = 
{
	WireframeBehaviour_t2494532455::get_offset_of_lineMaterial_2(),
	WireframeBehaviour_t2494532455::get_offset_of_ShowLines_3(),
	WireframeBehaviour_t2494532455::get_offset_of_LineColor_4(),
	WireframeBehaviour_t2494532455::get_offset_of_PFCCEKMCBGE_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (WireframeTrackableEventHandler_t1535150527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[1] = 
{
	WireframeTrackableEventHandler_t1535150527::get_offset_of_MDEHCLGCIKP_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (WordBehaviour_t3366478421), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (Text3D_t62691636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[8] = 
{
	Text3D_t62691636::get_offset_of_CCAGMENANPO_2(),
	Text3D_t62691636::get_offset_of_NFMIEIJLOHM_3(),
	Text3D_t62691636::get_offset_of_MOCEJEHIGAJ_4(),
	Text3D_t62691636::get_offset_of_myTransform_5(),
	Text3D_t62691636::get_offset_of_textMesh_6(),
	Text3D_t62691636::get_offset_of_DebugBounds_7(),
	Text3D_t62691636::get_offset_of_anchor_8(),
	Text3D_t62691636::get_offset_of_BHFNEOEPKGC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (EGCLHEJBIKM_t4071414764)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2387[4] = 
{
	EGCLHEJBIKM_t4071414764::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (Text3DExample_t192478882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[3] = 
{
	Text3DExample_t192478882::get_offset_of_NAJHBBJJKMP_2(),
	Text3DExample_t192478882::get_offset_of_COLCJHPAGJN_3(),
	Text3DExample_t192478882::get_offset_of_HJJPPJBGADF_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (Text3DManager_t1976449151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[4] = 
{
	Text3DManager_t1976449151::get_offset_of_Fonts_5(),
	Text3DManager_t1976449151::get_offset_of_pooledLetters_6(),
	Text3DManager_t1976449151::get_offset_of_letterObject_7(),
	Text3DManager_t1976449151::get_offset_of_poolCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (LetterSizes_t1582613310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[3] = 
{
	LetterSizes_t1582613310::get_offset_of_name_0(),
	LetterSizes_t1582613310::get_offset_of_font_1(),
	LetterSizes_t1582613310::get_offset_of_widths_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (HDNFKHHFDGG_t1814939731), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (FNIOBOKOKNO_t3395776259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (AEHNGNBNKNH_t1552223508), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (DataSetManager_t4144645295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[3] = 
{
	DataSetManager_t4144645295::get_offset_of_loadedDataSets_5(),
	DataSetManager_t4144645295::get_offset_of_aux_6(),
	DataSetManager_t4144645295::get_offset_of_databaseLoad_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (U3CWaitForLoadU3Ec__Iterator0_t2529810677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[4] = 
{
	U3CWaitForLoadU3Ec__Iterator0_t2529810677::get_offset_of_AOOLEAHHMIH_0(),
	U3CWaitForLoadU3Ec__Iterator0_t2529810677::get_offset_of_LGBFNMECDHC_1(),
	U3CWaitForLoadU3Ec__Iterator0_t2529810677::get_offset_of_IMAGLIMLPFK_2(),
	U3CWaitForLoadU3Ec__Iterator0_t2529810677::get_offset_of_EPCFNNGDBGC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (LoadedDataSet_t1410136239)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[2] = 
{
	LoadedDataSet_t1410136239::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LoadedDataSet_t1410136239::get_offset_of_dataSet_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (GimbalListener_t163996074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[5] = 
{
	GimbalListener_t163996074::get_offset_of_gimbal_5(),
	GimbalListener_t163996074::get_offset_of_BDGBMPDNEAF_6(),
	GimbalListener_t163996074::get_offset_of_PABHOAJMGFB_7(),
	GimbalListener_t163996074::get_offset_of_NNHPNBALPEE_8(),
	GimbalListener_t163996074::get_offset_of_currentPlace_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (LocationManager_t3181060254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[7] = 
{
	LocationManager_t3181060254::get_offset_of_KALMLPEGDFC_5(),
	LocationManager_t3181060254::get_offset_of_MCABJJDMKKD_6(),
	LocationManager_t3181060254::get_offset_of_NewPlace_7(),
	LocationManager_t3181060254::get_offset_of_NewLocation_8(),
	LocationManager_t3181060254::get_offset_of_NewTarget_9(),
	LocationManager_t3181060254::get_offset_of_previousTarget_10(),
	LocationManager_t3181060254::get_offset_of_MJMKEOPFCAM_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (GLDMKCBJKLK_t3811661921), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
