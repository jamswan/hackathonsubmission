﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{959a34e8-bda6-48f7-b3e7-b3f75a3a3202}.ArrayCopy148/$ArrayType$151
struct  U24ArrayTypeU24151_t3969012783 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24151_t3969012783__padding[151];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
