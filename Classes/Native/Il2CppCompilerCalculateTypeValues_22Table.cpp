﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GVR_Input_FloatEvent2213495270.h"
#include "AssemblyU2DCSharp_GVR_Input_BoolEvent555382268.h"
#include "AssemblyU2DCSharp_GVR_Input_ButtonEvent3014361476.h"
#include "AssemblyU2DCSharp_GVR_Input_TouchPadEvent1647781410.h"
#include "AssemblyU2DCSharp_GVR_Input_TransformEvent206501054.h"
#include "AssemblyU2DCSharp_GVR_Input_GameObjectEvent3653055841.h"
#include "AssemblyU2DCSharp_MenuHandler3829619697.h"
#include "AssemblyU2DCSharp_MenuHandler_U3CDoAppearU3Ec__Ite3667605745.h"
#include "AssemblyU2DCSharp_MenuHandler_U3CDoFadeU3Ec__Itera3187801445.h"
#include "AssemblyU2DCSharp_GVR_Events_PositionSwapper2793617445.h"
#include "AssemblyU2DCSharp_ScrubberEvents2429506345.h"
#include "AssemblyU2DCSharp_SwitchVideos3516593024.h"
#include "AssemblyU2DCSharp_SwitchVideos_U3CSetActiveDelayedU531920552.h"
#include "AssemblyU2DCSharp_GVR_Events_ToggleAction2865238344.h"
#include "AssemblyU2DCSharp_VideoControlsManager3010523296.h"
#include "AssemblyU2DCSharp_VideoControlsManager_U3CDoAppear2331568912.h"
#include "AssemblyU2DCSharp_VideoControlsManager_U3CDoFadeU34193299950.h"
#include "AssemblyU2DCSharp_VideoPlayerReference1150574547.h"
#include "AssemblyU2DCSharp_GvrHead3923315805.h"
#include "AssemblyU2DCSharp_GvrHead_GPJINOCLCJA4143869174.h"
#include "AssemblyU2DCSharp_FNIMJFIPFEK3591314519.h"
#include "AssemblyU2DCSharp_GvrEye3930157106.h"
#include "AssemblyU2DCSharp_GvrProfile2070273202.h"
#include "AssemblyU2DCSharp_GvrProfile_Screen839756045.h"
#include "AssemblyU2DCSharp_GvrProfile_Lenses2112994543.h"
#include "AssemblyU2DCSharp_GvrProfile_MaxFOV1743211906.h"
#include "AssemblyU2DCSharp_GvrProfile_Distortion550060296.h"
#include "AssemblyU2DCSharp_GvrProfile_Viewer1642017539.h"
#include "AssemblyU2DCSharp_GvrProfile_OJNFGEJBMOD3696210246.h"
#include "AssemblyU2DCSharp_GvrProfile_LICCAHLLNJF2188653983.h"
#include "AssemblyU2DCSharp_StereoController3144380552.h"
#include "AssemblyU2DCSharp_StereoController_U3CEndOfFrameU33626315335.h"
#include "AssemblyU2DCSharp_StereoRenderEffect958489249.h"
#include "AssemblyU2DCSharp_Gvr_Internal_MNAPGJPEPGN721519217.h"
#include "AssemblyU2DCSharp_Gvr_Internal_OAPLNBCDLIB4108484976.h"
#include "AssemblyU2DCSharp_Gvr_Internal_JCGMDGFLGOB1400499334.h"
#include "AssemblyU2DCSharp_GvrGaze2249568644.h"
#include "AssemblyU2DCSharp_GvrReticle1834592217.h"
#include "AssemblyU2DCSharp_OPGEDONPNEE1153938088.h"
#include "AssemblyU2DCSharp_OPGEDONPNEE_ICEFGGHBDDE1228156253.h"
#include "AssemblyU2DCSharp_OPGEDONPNEE_MPOFLJHICPJ3085196045.h"
#include "AssemblyU2DCSharp_OPGEDONPNEE_NIBCCNJNHOC1529433260.h"
#include "AssemblyU2DCSharp_OPGEDONPNEE_JIPMEOILOEH1380949674.h"
#include "AssemblyU2DCSharp_GvrAudioListener1521766837.h"
#include "AssemblyU2DCSharp_GvrAudioRoom1253442178.h"
#include "AssemblyU2DCSharp_GvrAudioRoom_LOLCNKGCDKE1199949914.h"
#include "AssemblyU2DCSharp_GvrAudioSoundfield1301118448.h"
#include "AssemblyU2DCSharp_GvrAudioSource2307460312.h"
#include "AssemblyU2DCSharp_GvrArmModel1664224602.h"
#include "AssemblyU2DCSharp_GvrArmModelOffsets2241056642.h"
#include "AssemblyU2DCSharp_GvrController1602869021.h"
#include "AssemblyU2DCSharp_GvrControllerVisual3328916665.h"
#include "AssemblyU2DCSharp_GvrControllerVisualManager1857939020.h"
#include "AssemblyU2DCSharp_GvrPointerManager2205699129.h"
#include "AssemblyU2DCSharp_GvrRecenterOnlyController2745329441.h"
#include "AssemblyU2DCSharp_GvrTooltip801170144.h"
#include "AssemblyU2DCSharp_Gvr_Internal_OBEJPHBKLEO1265277535.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorClientSocke2001911543.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorConfig616150261.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorConfig_BKFE3921329807.h"
#include "AssemblyU2DCSharp_Gvr_Internal_LNNAMOEPEAP2980568462.h"
#include "AssemblyU2DCSharp_Gvr_Internal_AMPNKIOMOGM1147901973.h"
#include "AssemblyU2DCSharp_Gvr_Internal_BADMPKCPJFF1360680984.h"
#include "AssemblyU2DCSharp_Gvr_Internal_BADMPKCPJFF_ODLEJHP4163010464.h"
#include "AssemblyU2DCSharp_Gvr_Internal_BADMPKCPJFF_FIDMFOF3974762026.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EHFLFEDGDEF1158703932.h"
#include "AssemblyU2DCSharp_Gvr_Internal_LCEDPLMOCBI3496077470.h"
#include "AssemblyU2DCSharp_Gvr_Internal_LCEDPLMOCBI_JBMAAKD2369530344.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager3364249716.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_EBN3366934859.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_HFE1480002485.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_PBB3727933525.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_BGI1108385459.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_AMG2280436239.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_U3C4253624923.h"
#include "AssemblyU2DCSharp_proto_Proto_PhoneEvent3882078222.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent2572128318.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types3648109718.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Type1530480861.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve4072706903.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve1262104803.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve1211758263.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve2701542133.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve3452538341.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_GyroscopeE182225200.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_GyroscopeEv33558588.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Accelerom1893725728.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Accelerom1480486140.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_DepthMapE1516604558.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_DepthMapE3483346914.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Orientati2038376807.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Orientati2561526853.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_KeyEvent639576718.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_KeyEvent_2056133158.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Builder2537253112.h"
#include "AssemblyU2DCSharp_FPFPADIHBIM767626572.h"
#include "AssemblyU2DCSharp_GvrBasePointerRaycaster1189534163.h"
#include "AssemblyU2DCSharp_GvrBasePointerRaycaster_NBEJNAKGD627056501.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (FloatEvent_t2213495270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (BoolEvent_t555382268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (ButtonEvent_t3014361476), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (TouchPadEvent_t1647781410), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (TransformEvent_t206501054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (GameObjectEvent_t3653055841), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (MenuHandler_t3829619697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[1] = 
{
	MenuHandler_t3829619697::get_offset_of_menuObjects_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (U3CDoAppearU3Ec__Iterator0_t3667605745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[5] = 
{
	U3CDoAppearU3Ec__Iterator0_t3667605745::get_offset_of_LJNGBIPCPNJ_0(),
	U3CDoAppearU3Ec__Iterator0_t3667605745::get_offset_of_AOOLEAHHMIH_1(),
	U3CDoAppearU3Ec__Iterator0_t3667605745::get_offset_of_LGBFNMECDHC_2(),
	U3CDoAppearU3Ec__Iterator0_t3667605745::get_offset_of_IMAGLIMLPFK_3(),
	U3CDoAppearU3Ec__Iterator0_t3667605745::get_offset_of_EPCFNNGDBGC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (U3CDoFadeU3Ec__Iterator1_t3187801445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[5] = 
{
	U3CDoFadeU3Ec__Iterator1_t3187801445::get_offset_of_LJNGBIPCPNJ_0(),
	U3CDoFadeU3Ec__Iterator1_t3187801445::get_offset_of_AOOLEAHHMIH_1(),
	U3CDoFadeU3Ec__Iterator1_t3187801445::get_offset_of_LGBFNMECDHC_2(),
	U3CDoFadeU3Ec__Iterator1_t3187801445::get_offset_of_IMAGLIMLPFK_3(),
	U3CDoFadeU3Ec__Iterator1_t3187801445::get_offset_of_EPCFNNGDBGC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (PositionSwapper_t2793617445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[2] = 
{
	PositionSwapper_t2793617445::get_offset_of_BABANNMDKAJ_2(),
	PositionSwapper_t2793617445::get_offset_of_Positions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (ScrubberEvents_t2429506345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[5] = 
{
	ScrubberEvents_t2429506345::get_offset_of_KKPDNPLIEIO_2(),
	ScrubberEvents_t2429506345::get_offset_of_BLALEKBHIPM_3(),
	ScrubberEvents_t2429506345::get_offset_of_IAGMGKCDFLC_4(),
	ScrubberEvents_t2429506345::get_offset_of_NAGPFCGNKJG_5(),
	ScrubberEvents_t2429506345::get_offset_of_KCIBEMJPAAL_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (SwitchVideos_t3516593024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[5] = 
{
	SwitchVideos_t3516593024::get_offset_of_localVideoSample_2(),
	SwitchVideos_t3516593024::get_offset_of_dashVideoSample_3(),
	SwitchVideos_t3516593024::get_offset_of_panoVideoSample_4(),
	SwitchVideos_t3516593024::get_offset_of_EPPODPCMDIN_5(),
	SwitchVideos_t3516593024::get_offset_of_missingLibText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (U3CSetActiveDelayedU3Ec__Iterator0_t531920552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[5] = 
{
	U3CSetActiveDelayedU3Ec__Iterator0_t531920552::get_offset_of_BIDNFMIKBOK_0(),
	U3CSetActiveDelayedU3Ec__Iterator0_t531920552::get_offset_of_CPLHMCELOLA_1(),
	U3CSetActiveDelayedU3Ec__Iterator0_t531920552::get_offset_of_LGBFNMECDHC_2(),
	U3CSetActiveDelayedU3Ec__Iterator0_t531920552::get_offset_of_IMAGLIMLPFK_3(),
	U3CSetActiveDelayedU3Ec__Iterator0_t531920552::get_offset_of_EPCFNNGDBGC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (ToggleAction_t2865238344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[7] = 
{
	ToggleAction_t2865238344::get_offset_of_JFEKJNLAKFN_2(),
	ToggleAction_t2865238344::get_offset_of_NEBBGJHHKIL_3(),
	ToggleAction_t2865238344::get_offset_of_OnToggleOn_4(),
	ToggleAction_t2865238344::get_offset_of_OnToggleOff_5(),
	ToggleAction_t2865238344::get_offset_of_InitialState_6(),
	ToggleAction_t2865238344::get_offset_of_RaiseEventForInitialState_7(),
	ToggleAction_t2865238344::get_offset_of_Cooldown_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (VideoControlsManager_t3010523296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[11] = 
{
	VideoControlsManager_t3010523296::get_offset_of_OGIMIFIJBHE_2(),
	VideoControlsManager_t3010523296::get_offset_of_IHDMIIKLJPN_3(),
	VideoControlsManager_t3010523296::get_offset_of_FCDACJEACOO_4(),
	VideoControlsManager_t3010523296::get_offset_of_KBCKBIKIEKI_5(),
	VideoControlsManager_t3010523296::get_offset_of_NILMBMLHEPI_6(),
	VideoControlsManager_t3010523296::get_offset_of_GIFEKIKJCGN_7(),
	VideoControlsManager_t3010523296::get_offset_of_DJCKNPMFNEO_8(),
	VideoControlsManager_t3010523296::get_offset_of_ECJHOEHFPEP_9(),
	VideoControlsManager_t3010523296::get_offset_of_EBGPHMIECCE_10(),
	VideoControlsManager_t3010523296::get_offset_of_BDBNLNHKGOL_11(),
	VideoControlsManager_t3010523296::get_offset_of_U3CLHKBFMHLFBFU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (U3CDoAppearU3Ec__Iterator0_t2331568912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[5] = 
{
	U3CDoAppearU3Ec__Iterator0_t2331568912::get_offset_of_LJNGBIPCPNJ_0(),
	U3CDoAppearU3Ec__Iterator0_t2331568912::get_offset_of_AOOLEAHHMIH_1(),
	U3CDoAppearU3Ec__Iterator0_t2331568912::get_offset_of_LGBFNMECDHC_2(),
	U3CDoAppearU3Ec__Iterator0_t2331568912::get_offset_of_IMAGLIMLPFK_3(),
	U3CDoAppearU3Ec__Iterator0_t2331568912::get_offset_of_EPCFNNGDBGC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (U3CDoFadeU3Ec__Iterator1_t4193299950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[5] = 
{
	U3CDoFadeU3Ec__Iterator1_t4193299950::get_offset_of_LJNGBIPCPNJ_0(),
	U3CDoFadeU3Ec__Iterator1_t4193299950::get_offset_of_AOOLEAHHMIH_1(),
	U3CDoFadeU3Ec__Iterator1_t4193299950::get_offset_of_LGBFNMECDHC_2(),
	U3CDoFadeU3Ec__Iterator1_t4193299950::get_offset_of_IMAGLIMLPFK_3(),
	U3CDoFadeU3Ec__Iterator1_t4193299950::get_offset_of_EPCFNNGDBGC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (VideoPlayerReference_t1150574547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[1] = 
{
	VideoPlayerReference_t1150574547::get_offset_of_player_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (GvrHead_t3923315805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[6] = 
{
	GvrHead_t3923315805::get_offset_of_trackRotation_2(),
	GvrHead_t3923315805::get_offset_of_trackPosition_3(),
	GvrHead_t3923315805::get_offset_of_target_4(),
	GvrHead_t3923315805::get_offset_of_updateEarly_5(),
	GvrHead_t3923315805::get_offset_of_OnHeadUpdated_6(),
	GvrHead_t3923315805::get_offset_of_KMEOJEOMJCH_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (GPJINOCLCJA_t4143869174), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (FNIMJFIPFEK_t3591314519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (GvrEye_t3930157106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[8] = 
{
	GvrEye_t3930157106::get_offset_of_eye_2(),
	GvrEye_t3930157106::get_offset_of_toggleCullingMask_3(),
	GvrEye_t3930157106::get_offset_of_GIGHINKJMLF_4(),
	GvrEye_t3930157106::get_offset_of_IBDCHBPFAIF_5(),
	GvrEye_t3930157106::get_offset_of_KLJHOLCNLED_6(),
	GvrEye_t3930157106::get_offset_of_CCFNHFMLDAK_7(),
	GvrEye_t3930157106::get_offset_of_CJKJMALCKCJ_8(),
	GvrEye_t3930157106::get_offset_of_U3CHIICJNOPLPFU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (GvrProfile_t2070273202), -1, sizeof(GvrProfile_t2070273202_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2222[15] = 
{
	GvrProfile_t2070273202::get_offset_of_screen_0(),
	GvrProfile_t2070273202::get_offset_of_viewer_1(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_Nexus5_2(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_Nexus6_3(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_GalaxyS6_4(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_GalaxyNote4_5(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_LGG3_6(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_iPhone4_7(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_iPhone5_8(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_iPhone6_9(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_iPhone6p_10(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_CardboardJun2014_11(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_CardboardMay2015_12(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_GoggleTechC1Glass_13(),
	GvrProfile_t2070273202_StaticFields::get_offset_of_Default_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (Screen_t839756045)+ sizeof (Il2CppObject), sizeof(Screen_t839756045 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2223[3] = 
{
	Screen_t839756045::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Screen_t839756045::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Screen_t839756045::get_offset_of_border_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (Lenses_t2112994543)+ sizeof (Il2CppObject), sizeof(Lenses_t2112994543 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2224[7] = 
{
	Lenses_t2112994543::get_offset_of_separation_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Lenses_t2112994543::get_offset_of_offset_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Lenses_t2112994543::get_offset_of_screenDistance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Lenses_t2112994543::get_offset_of_alignment_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (MaxFOV_t1743211906)+ sizeof (Il2CppObject), sizeof(MaxFOV_t1743211906 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2225[4] = 
{
	MaxFOV_t1743211906::get_offset_of_outer_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaxFOV_t1743211906::get_offset_of_inner_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaxFOV_t1743211906::get_offset_of_upper_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaxFOV_t1743211906::get_offset_of_lower_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (Distortion_t550060296)+ sizeof (Il2CppObject), sizeof(Distortion_t550060296_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2226[1] = 
{
	Distortion_t550060296::get_offset_of_coef_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (Viewer_t1642017539)+ sizeof (Il2CppObject), sizeof(Viewer_t1642017539_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2227[4] = 
{
	Viewer_t1642017539::get_offset_of_lenses_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Viewer_t1642017539::get_offset_of_maxFOV_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Viewer_t1642017539::get_offset_of_distortion_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Viewer_t1642017539::get_offset_of_inverse_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (OJNFGEJBMOD_t3696210246)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2228[10] = 
{
	OJNFGEJBMOD_t3696210246::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (LICCAHLLNJF_t2188653983)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2229[4] = 
{
	LICCAHLLNJF_t2188653983::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (StereoController_t3144380552), -1, sizeof(StereoController_t3144380552_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2230[17] = 
{
	StereoController_t3144380552::get_offset_of_directRender_2(),
	StereoController_t3144380552::get_offset_of_keepStereoUpdated_3(),
	StereoController_t3144380552::get_offset_of_stereoMultiplier_4(),
	StereoController_t3144380552::get_offset_of_matchMonoFOV_5(),
	StereoController_t3144380552::get_offset_of_matchByZoom_6(),
	StereoController_t3144380552::get_offset_of_centerOfInterest_7(),
	StereoController_t3144380552::get_offset_of_radiusOfInterest_8(),
	StereoController_t3144380552::get_offset_of_checkStereoComfort_9(),
	StereoController_t3144380552::get_offset_of_stereoAdjustSmoothing_10(),
	StereoController_t3144380552::get_offset_of_screenParallax_11(),
	StereoController_t3144380552::get_offset_of_stereoPaddingX_12(),
	StereoController_t3144380552::get_offset_of_stereoPaddingY_13(),
	StereoController_t3144380552::get_offset_of_PKCAKDJFMHM_14(),
	StereoController_t3144380552::get_offset_of_PPLJMNDAHCG_15(),
	StereoController_t3144380552::get_offset_of_KGMCAOECIPM_16(),
	StereoController_t3144380552::get_offset_of_U3CHIICJNOPLPFU3Ek__BackingField_17(),
	StereoController_t3144380552_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (U3CEndOfFrameU3Ec__Iterator0_t3626315335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[4] = 
{
	U3CEndOfFrameU3Ec__Iterator0_t3626315335::get_offset_of_AOOLEAHHMIH_0(),
	U3CEndOfFrameU3Ec__Iterator0_t3626315335::get_offset_of_LGBFNMECDHC_1(),
	U3CEndOfFrameU3Ec__Iterator0_t3626315335::get_offset_of_IMAGLIMLPFK_2(),
	U3CEndOfFrameU3Ec__Iterator0_t3626315335::get_offset_of_EPCFNNGDBGC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (StereoRenderEffect_t958489249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (MNAPGJPEPGN_t721519217), -1, sizeof(MNAPGJPEPGN_t721519217_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2233[19] = 
{
	MNAPGJPEPGN_t721519217_StaticFields::get_offset_of_FCEOCGPNFEC_0(),
	MNAPGJPEPGN_t721519217::get_offset_of_U3CKFMPCJJLEKHU3Ek__BackingField_1(),
	MNAPGJPEPGN_t721519217::get_offset_of_HNKDFALAEAM_2(),
	MNAPGJPEPGN_t721519217::get_offset_of_MDCAIGAFFMH_3(),
	MNAPGJPEPGN_t721519217::get_offset_of_HKAIEACNHEM_4(),
	MNAPGJPEPGN_t721519217::get_offset_of_GENCMNOKAHN_5(),
	MNAPGJPEPGN_t721519217::get_offset_of_PDECLGJCOFL_6(),
	MNAPGJPEPGN_t721519217::get_offset_of_IJJBAMPMNHA_7(),
	MNAPGJPEPGN_t721519217::get_offset_of_AEPECOPFMNO_8(),
	MNAPGJPEPGN_t721519217::get_offset_of_BEONPPLJDGF_9(),
	MNAPGJPEPGN_t721519217::get_offset_of_EJBOAKLLIJH_10(),
	MNAPGJPEPGN_t721519217::get_offset_of_FDLJGEIMBOK_11(),
	MNAPGJPEPGN_t721519217::get_offset_of_KJHLIAGKOBJ_12(),
	MNAPGJPEPGN_t721519217::get_offset_of_BFNLEKGOEFE_13(),
	MNAPGJPEPGN_t721519217::get_offset_of_POBPEGACDHK_14(),
	MNAPGJPEPGN_t721519217::get_offset_of_ELDGOGFMNOP_15(),
	MNAPGJPEPGN_t721519217::get_offset_of_FAPKBLPOPHP_16(),
	MNAPGJPEPGN_t721519217::get_offset_of_DFFBPELBEFE_17(),
	MNAPGJPEPGN_t721519217::get_offset_of_ALGNDFCEIOD_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (OAPLNBCDLIB_t4108484976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[13] = 
{
	0,
	0,
	0,
	0,
	OAPLNBCDLIB_t4108484976::get_offset_of_IPFDHFHGFNL_23(),
	OAPLNBCDLIB_t4108484976::get_offset_of_LPFIMOFHBAG_24(),
	OAPLNBCDLIB_t4108484976::get_offset_of_GMDNALCDCCP_25(),
	OAPLNBCDLIB_t4108484976::get_offset_of_ELLHLOEBGKN_26(),
	OAPLNBCDLIB_t4108484976::get_offset_of_KNIEEAMILHE_27(),
	OAPLNBCDLIB_t4108484976::get_offset_of_HHBEFPMOEIK_28(),
	OAPLNBCDLIB_t4108484976::get_offset_of_JJDIKBCLNGI_29(),
	OAPLNBCDLIB_t4108484976::get_offset_of_PLANKBDNDHL_30(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (JCGMDGFLGOB_t1400499334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[1] = 
{
	JCGMDGFLGOB_t1400499334::get_offset_of_INEMPHMNACI_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (GvrGaze_t2249568644), -1, sizeof(GvrGaze_t2249568644_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2236[11] = 
{
	GvrGaze_t2249568644::get_offset_of_pointerObject_2(),
	GvrGaze_t2249568644::get_offset_of_PGDIBLKFBDK_3(),
	GvrGaze_t2249568644::get_offset_of_U3CHIICJNOPLPFU3Ek__BackingField_4(),
	GvrGaze_t2249568644::get_offset_of_mask_5(),
	GvrGaze_t2249568644::get_offset_of_NDFPLFJJFMD_6(),
	GvrGaze_t2249568644::get_offset_of_IECMNHDFCEG_7(),
	GvrGaze_t2249568644::get_offset_of_FFPEMKIHNNN_8(),
	GvrGaze_t2249568644::get_offset_of_MPJPMMJANBI_9(),
	GvrGaze_t2249568644::get_offset_of_FCCNLMFLDJE_10(),
	GvrGaze_t2249568644_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
	GvrGaze_t2249568644_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (GvrReticle_t1834592217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (OPGEDONPNEE_t1153938088), -1, sizeof(OPGEDONPNEE_t1153938088_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2239[23] = 
{
	OPGEDONPNEE_t1153938088_StaticFields::get_offset_of_KCPLHLPNHFH_0(),
	OPGEDONPNEE_t1153938088_StaticFields::get_offset_of_KEGPMMCHGLH_1(),
	OPGEDONPNEE_t1153938088_StaticFields::get_offset_of_AEFCJNPMOBA_2(),
	OPGEDONPNEE_t1153938088_StaticFields::get_offset_of_DNMDLBEMGME_3(),
	OPGEDONPNEE_t1153938088_StaticFields::get_offset_of_MPDDLCPCGDH_4(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	OPGEDONPNEE_t1153938088_StaticFields::get_offset_of_KBGPBAHKIEF_16(),
	OPGEDONPNEE_t1153938088_StaticFields::get_offset_of_DAPEPOHIALH_17(),
	OPGEDONPNEE_t1153938088_StaticFields::get_offset_of_EJOBGGJEDCF_18(),
	OPGEDONPNEE_t1153938088_StaticFields::get_offset_of_KIJCBOIEBDB_19(),
	OPGEDONPNEE_t1153938088_StaticFields::get_offset_of_DMBPGHAHIHJ_20(),
	OPGEDONPNEE_t1153938088_StaticFields::get_offset_of_CAJJOBMIBMG_21(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (ICEFGGHBDDE_t1228156253)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2240[4] = 
{
	ICEFGGHBDDE_t1228156253::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (MPOFLJHICPJ_t3085196045)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2241[9] = 
{
	MPOFLJHICPJ_t3085196045::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (NIBCCNJNHOC_t1529433260)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2242[3] = 
{
	NIBCCNJNHOC_t1529433260::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (JIPMEOILOEH_t1380949674)+ sizeof (Il2CppObject), sizeof(JIPMEOILOEH_t1380949674 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2243[20] = 
{
	JIPMEOILOEH_t1380949674::get_offset_of_OFFLCJMANOF_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_KAODMLIEJAI_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_BIAPMBHCAGP_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_KDFDEMGLDAK_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_LBIELBFAJFB_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_LPGKIDBHBHA_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_JPHAMIOKOGL_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_HCFDHMLHGNB_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_GAHOOADHIFL_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_EKMKCCEBADC_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_KNBPEDDOJCK_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_IEEALBNNMKL_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_HKKEPLFEJKA_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_IGMDOGBKGKP_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_BCMOEOMGAGH_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_DPKBDFEHGJA_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_PBIFAKIBOGA_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_IBKIILPFNME_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_APCENFFNMBB_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JIPMEOILOEH_t1380949674::get_offset_of_AKMHGGLAKCH_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (GvrAudioListener_t1521766837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[3] = 
{
	GvrAudioListener_t1521766837::get_offset_of_globalGainDb_2(),
	GvrAudioListener_t1521766837::get_offset_of_occlusionMask_3(),
	GvrAudioListener_t1521766837::get_offset_of_quality_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (GvrAudioRoom_t1253442178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[11] = 
{
	GvrAudioRoom_t1253442178::get_offset_of_leftWall_2(),
	GvrAudioRoom_t1253442178::get_offset_of_rightWall_3(),
	GvrAudioRoom_t1253442178::get_offset_of_floor_4(),
	GvrAudioRoom_t1253442178::get_offset_of_ceiling_5(),
	GvrAudioRoom_t1253442178::get_offset_of_backWall_6(),
	GvrAudioRoom_t1253442178::get_offset_of_frontWall_7(),
	GvrAudioRoom_t1253442178::get_offset_of_reflectivity_8(),
	GvrAudioRoom_t1253442178::get_offset_of_reverbGainDb_9(),
	GvrAudioRoom_t1253442178::get_offset_of_reverbBrightness_10(),
	GvrAudioRoom_t1253442178::get_offset_of_reverbTime_11(),
	GvrAudioRoom_t1253442178::get_offset_of_size_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (LOLCNKGCDKE_t1199949914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2246[24] = 
{
	LOLCNKGCDKE_t1199949914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (GvrAudioSoundfield_t1301118448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[13] = 
{
	GvrAudioSoundfield_t1301118448::get_offset_of_bypassRoomEffects_2(),
	GvrAudioSoundfield_t1301118448::get_offset_of_gainDb_3(),
	GvrAudioSoundfield_t1301118448::get_offset_of_playOnAwake_4(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldClip0102_5(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldClip0304_6(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldLoop_7(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldMute_8(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldPitch_9(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldPriority_10(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldVolume_11(),
	GvrAudioSoundfield_t1301118448::get_offset_of_HFEDJBIALMB_12(),
	GvrAudioSoundfield_t1301118448::get_offset_of_MOPBDIKNMEN_13(),
	GvrAudioSoundfield_t1301118448::get_offset_of_OEPHEOPCAAH_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (GvrAudioSource_t2307460312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[25] = 
{
	GvrAudioSource_t2307460312::get_offset_of_bypassRoomEffects_2(),
	GvrAudioSource_t2307460312::get_offset_of_directivityAlpha_3(),
	GvrAudioSource_t2307460312::get_offset_of_directivitySharpness_4(),
	GvrAudioSource_t2307460312::get_offset_of_listenerDirectivityAlpha_5(),
	GvrAudioSource_t2307460312::get_offset_of_listenerDirectivitySharpness_6(),
	GvrAudioSource_t2307460312::get_offset_of_gainDb_7(),
	GvrAudioSource_t2307460312::get_offset_of_occlusionEnabled_8(),
	GvrAudioSource_t2307460312::get_offset_of_playOnAwake_9(),
	GvrAudioSource_t2307460312::get_offset_of_sourceClip_10(),
	GvrAudioSource_t2307460312::get_offset_of_sourceLoop_11(),
	GvrAudioSource_t2307460312::get_offset_of_sourceMute_12(),
	GvrAudioSource_t2307460312::get_offset_of_sourcePitch_13(),
	GvrAudioSource_t2307460312::get_offset_of_sourcePriority_14(),
	GvrAudioSource_t2307460312::get_offset_of_sourceDopplerLevel_15(),
	GvrAudioSource_t2307460312::get_offset_of_sourceSpread_16(),
	GvrAudioSource_t2307460312::get_offset_of_sourceVolume_17(),
	GvrAudioSource_t2307460312::get_offset_of_sourceRolloffMode_18(),
	GvrAudioSource_t2307460312::get_offset_of_sourceMaxDistance_19(),
	GvrAudioSource_t2307460312::get_offset_of_sourceMinDistance_20(),
	GvrAudioSource_t2307460312::get_offset_of_hrtfEnabled_21(),
	GvrAudioSource_t2307460312::get_offset_of_audioSource_22(),
	GvrAudioSource_t2307460312::get_offset_of_HFEDJBIALMB_23(),
	GvrAudioSource_t2307460312::get_offset_of_DEKCNHOFDCD_24(),
	GvrAudioSource_t2307460312::get_offset_of_AOFOLNHMCEF_25(),
	GvrAudioSource_t2307460312::get_offset_of_OEPHEOPCAAH_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (GvrArmModel_t1664224602), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (GvrArmModelOffsets_t2241056642), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (GvrController_t1602869021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (GvrControllerVisual_t3328916665), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (GvrControllerVisualManager_t1857939020), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (GvrPointerManager_t2205699129), -1, sizeof(GvrPointerManager_t2205699129_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2254[2] = 
{
	GvrPointerManager_t2205699129_StaticFields::get_offset_of_OLMOHEEKDMC_2(),
	GvrPointerManager_t2205699129::get_offset_of_PGDIBLKFBDK_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (GvrRecenterOnlyController_t2745329441), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (GvrTooltip_t801170144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (OBEJPHBKLEO_t1265277535), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (EmulatorClientSocket_t2001911543), -1, sizeof(EmulatorClientSocket_t2001911543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2258[9] = 
{
	EmulatorClientSocket_t2001911543_StaticFields::get_offset_of_IMGBFIPPOGO_2(),
	0,
	0,
	EmulatorClientSocket_t2001911543::get_offset_of_ONPKLOLLMED_5(),
	EmulatorClientSocket_t2001911543::get_offset_of_CMBKFJCMIIG_6(),
	EmulatorClientSocket_t2001911543::get_offset_of_JHLHEGHOFHP_7(),
	EmulatorClientSocket_t2001911543::get_offset_of_NOEALGHBFFL_8(),
	EmulatorClientSocket_t2001911543::get_offset_of_BAIPHHCGAPF_9(),
	EmulatorClientSocket_t2001911543::get_offset_of_U3COPLNEPJFOLPU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (EmulatorConfig_t616150261), -1, sizeof(EmulatorConfig_t616150261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2259[4] = 
{
	EmulatorConfig_t616150261_StaticFields::get_offset_of_OLMOHEEKDMC_2(),
	EmulatorConfig_t616150261::get_offset_of_PHONE_EVENT_MODE_3(),
	EmulatorConfig_t616150261_StaticFields::get_offset_of_USB_SERVER_IP_4(),
	EmulatorConfig_t616150261_StaticFields::get_offset_of_WIFI_SERVER_IP_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (BKFEJIAIMLM_t3921329807)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2260[4] = 
{
	BKFEJIAIMLM_t3921329807::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (LNNAMOEPEAP_t2980568462)+ sizeof (Il2CppObject), sizeof(LNNAMOEPEAP_t2980568462 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2261[2] = 
{
	LNNAMOEPEAP_t2980568462::get_offset_of_GJJAEACHFEK_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LNNAMOEPEAP_t2980568462::get_offset_of_PJMCJKFMGJI_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (AMPNKIOMOGM_t1147901973)+ sizeof (Il2CppObject), sizeof(AMPNKIOMOGM_t1147901973 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2262[2] = 
{
	AMPNKIOMOGM_t1147901973::get_offset_of_GJJAEACHFEK_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AMPNKIOMOGM_t1147901973::get_offset_of_PJMCJKFMGJI_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (BADMPKCPJFF_t1360680984)+ sizeof (Il2CppObject), -1, sizeof(BADMPKCPJFF_t1360680984_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2263[6] = 
{
	BADMPKCPJFF_t1360680984::get_offset_of_ANNIDELAEEC_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BADMPKCPJFF_t1360680984::get_offset_of_OPIBJJMGAOD_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BADMPKCPJFF_t1360680984::get_offset_of_JKCJGOGMNAF_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	BADMPKCPJFF_t1360680984_StaticFields::get_offset_of_NONPECLJJJB_3(),
	BADMPKCPJFF_t1360680984_StaticFields::get_offset_of_GIEEDIKMGIA_4(),
	BADMPKCPJFF_t1360680984_StaticFields::get_offset_of_OAOGMCMCNNK_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (ODLEJHPGFAC_t4163010464)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2264[10] = 
{
	ODLEJHPGFAC_t4163010464::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (FIDMFOFBGKL_t3974762026)+ sizeof (Il2CppObject), sizeof(FIDMFOFBGKL_t3974762026 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2265[3] = 
{
	FIDMFOFBGKL_t3974762026::get_offset_of_AGGPCOGGDCI_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FIDMFOFBGKL_t3974762026::get_offset_of_GIIKJDKOAGE_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FIDMFOFBGKL_t3974762026::get_offset_of_OHEEFIDMMON_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (EHFLFEDGDEF_t1158703932)+ sizeof (Il2CppObject), sizeof(EHFLFEDGDEF_t1158703932 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2266[2] = 
{
	EHFLFEDGDEF_t1158703932::get_offset_of_GJJAEACHFEK_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EHFLFEDGDEF_t1158703932::get_offset_of_PENFFOAGECN_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (LCEDPLMOCBI_t3496077470)+ sizeof (Il2CppObject), sizeof(LCEDPLMOCBI_t3496077470_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2267[2] = 
{
	LCEDPLMOCBI_t3496077470::get_offset_of_BAMGHCABHEK_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LCEDPLMOCBI_t3496077470::get_offset_of_BNBNGKONNKF_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (JBMAAKDFNJK_t2369530344)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2268[7] = 
{
	JBMAAKDFNJK_t2369530344::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (EmulatorManager_t3364249716), -1, sizeof(EmulatorManager_t3364249716_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2269[16] = 
{
	EmulatorManager_t3364249716::get_offset_of_OKPGJBDLLGN_2(),
	EmulatorManager_t3364249716::get_offset_of_KGLPCKKPJHB_3(),
	EmulatorManager_t3364249716_StaticFields::get_offset_of_OLMOHEEKDMC_4(),
	EmulatorManager_t3364249716::get_offset_of_IAKEONFNHAO_5(),
	EmulatorManager_t3364249716::get_offset_of_JFNCPDBJPEI_6(),
	EmulatorManager_t3364249716::get_offset_of_BJFGFMDKCNA_7(),
	EmulatorManager_t3364249716::get_offset_of_POPAFDBBHOC_8(),
	EmulatorManager_t3364249716::get_offset_of_NFHAFJMHDGO_9(),
	EmulatorManager_t3364249716::get_offset_of_gyroEventListenersInternal_10(),
	EmulatorManager_t3364249716::get_offset_of_accelEventListenersInternal_11(),
	EmulatorManager_t3364249716::get_offset_of_touchEventListenersInternal_12(),
	EmulatorManager_t3364249716::get_offset_of_orientationEventListenersInternal_13(),
	EmulatorManager_t3364249716::get_offset_of_buttonEventListenersInternal_14(),
	EmulatorManager_t3364249716::get_offset_of_KAKIBKIINOI_15(),
	EmulatorManager_t3364249716::get_offset_of_ICJHENHPDEI_16(),
	EmulatorManager_t3364249716::get_offset_of_PNJJEALMAFB_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (EBNNCJIJNIF_t3366934859), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (HFEGPIEMKOK_t1480002485), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (PBBMGAFIDCA_t3727933525), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (BGIHENCBNHH_t1108385459), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (AMGONBCFIKO_t2280436239), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (U3CEndOfFrameU3Ec__Iterator0_t4253624923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[5] = 
{
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_HCHAHDKBNGC_0(),
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_AOOLEAHHMIH_1(),
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_LGBFNMECDHC_2(),
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_IMAGLIMLPFK_3(),
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_EPCFNNGDBGC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (PhoneEvent_t3882078222), -1, sizeof(PhoneEvent_t3882078222_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2276[1] = 
{
	PhoneEvent_t3882078222_StaticFields::get_offset_of_HNBNJADPKML_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (PhoneEvent_t2572128318), -1, sizeof(PhoneEvent_t2572128318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2277[25] = 
{
	PhoneEvent_t2572128318_StaticFields::get_offset_of_CACHHOCBHAE_0(),
	PhoneEvent_t2572128318_StaticFields::get_offset_of_EOOGGAADBHL_1(),
	PhoneEvent_t2572128318_StaticFields::get_offset_of_HDBEOCNLPFD_2(),
	0,
	PhoneEvent_t2572128318::get_offset_of_NIHNHFPILMA_4(),
	PhoneEvent_t2572128318::get_offset_of_DOINLOMPNMA_5(),
	0,
	PhoneEvent_t2572128318::get_offset_of_FGGOIEMNPLG_7(),
	PhoneEvent_t2572128318::get_offset_of_PNANHMPMHBN_8(),
	0,
	PhoneEvent_t2572128318::get_offset_of_JHDDFCJDKAG_10(),
	PhoneEvent_t2572128318::get_offset_of_GPFHMBPFHCD_11(),
	0,
	PhoneEvent_t2572128318::get_offset_of_JEIKFAMKNBC_13(),
	PhoneEvent_t2572128318::get_offset_of_JKIMNBPOOFH_14(),
	0,
	PhoneEvent_t2572128318::get_offset_of_PMAMJBPPGBG_16(),
	PhoneEvent_t2572128318::get_offset_of_CHGLJAKDIOC_17(),
	0,
	PhoneEvent_t2572128318::get_offset_of_MCCAEHJIOKD_19(),
	PhoneEvent_t2572128318::get_offset_of_KOIDJIKCLAJ_20(),
	0,
	PhoneEvent_t2572128318::get_offset_of_DNDECDHEFBH_22(),
	PhoneEvent_t2572128318::get_offset_of_EFIOHEECJKI_23(),
	PhoneEvent_t2572128318::get_offset_of_BABCKJCECDJ_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (Types_t3648109718), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (Type_t1530480861)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2279[7] = 
{
	Type_t1530480861::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (MotionEvent_t4072706903), -1, sizeof(MotionEvent_t4072706903_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2280[12] = 
{
	MotionEvent_t4072706903_StaticFields::get_offset_of_CACHHOCBHAE_0(),
	MotionEvent_t4072706903_StaticFields::get_offset_of_NMNLDGPHJIL_1(),
	MotionEvent_t4072706903_StaticFields::get_offset_of_EOMOAOLBBIP_2(),
	0,
	MotionEvent_t4072706903::get_offset_of_MIGGKHHBAID_4(),
	MotionEvent_t4072706903::get_offset_of_HMBIGJIDGNB_5(),
	0,
	MotionEvent_t4072706903::get_offset_of_DECNNNLBPPF_7(),
	MotionEvent_t4072706903::get_offset_of_LADFMKMCMMH_8(),
	0,
	MotionEvent_t4072706903::get_offset_of_PCNPGHNFILL_10(),
	MotionEvent_t4072706903::get_offset_of_BABCKJCECDJ_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (Types_t1262104803), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (Pointer_t1211758263), -1, sizeof(Pointer_t1211758263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2282[13] = 
{
	Pointer_t1211758263_StaticFields::get_offset_of_CACHHOCBHAE_0(),
	Pointer_t1211758263_StaticFields::get_offset_of_BLJKKGIJBPB_1(),
	Pointer_t1211758263_StaticFields::get_offset_of_KJBMGPMKNOF_2(),
	0,
	Pointer_t1211758263::get_offset_of_AEEJBGMNBPG_4(),
	Pointer_t1211758263::get_offset_of_IBKNCIPJFEK_5(),
	0,
	Pointer_t1211758263::get_offset_of_DCGBGFAMFIG_7(),
	Pointer_t1211758263::get_offset_of_BGMOJHPMOJE_8(),
	0,
	Pointer_t1211758263::get_offset_of_GGMHCMHPELH_10(),
	Pointer_t1211758263::get_offset_of_LOHGDOHOBOL_11(),
	Pointer_t1211758263::get_offset_of_BABCKJCECDJ_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (Builder_t2701542133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[2] = 
{
	Builder_t2701542133::get_offset_of_LCFNHIBHFMO_0(),
	Builder_t2701542133::get_offset_of_JOMIOHAFLNI_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (Builder_t3452538341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[2] = 
{
	Builder_t3452538341::get_offset_of_LCFNHIBHFMO_0(),
	Builder_t3452538341::get_offset_of_JOMIOHAFLNI_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (GyroscopeEvent_t182225200), -1, sizeof(GyroscopeEvent_t182225200_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2285[16] = 
{
	GyroscopeEvent_t182225200_StaticFields::get_offset_of_CACHHOCBHAE_0(),
	GyroscopeEvent_t182225200_StaticFields::get_offset_of_NEKHOFLMDLE_1(),
	GyroscopeEvent_t182225200_StaticFields::get_offset_of_CIJJOMILCMJ_2(),
	0,
	GyroscopeEvent_t182225200::get_offset_of_MIGGKHHBAID_4(),
	GyroscopeEvent_t182225200::get_offset_of_HMBIGJIDGNB_5(),
	0,
	GyroscopeEvent_t182225200::get_offset_of_DAPIOCPOIID_7(),
	GyroscopeEvent_t182225200::get_offset_of_KKLEGOBOFFJ_8(),
	0,
	GyroscopeEvent_t182225200::get_offset_of_LGBJOJHEJDM_10(),
	GyroscopeEvent_t182225200::get_offset_of_NFCIJFNNNDL_11(),
	0,
	GyroscopeEvent_t182225200::get_offset_of_LPLIBMIPMFD_13(),
	GyroscopeEvent_t182225200::get_offset_of_PLILMNHLFED_14(),
	GyroscopeEvent_t182225200::get_offset_of_BABCKJCECDJ_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (Builder_t33558588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[2] = 
{
	Builder_t33558588::get_offset_of_LCFNHIBHFMO_0(),
	Builder_t33558588::get_offset_of_JOMIOHAFLNI_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (AccelerometerEvent_t1893725728), -1, sizeof(AccelerometerEvent_t1893725728_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2287[16] = 
{
	AccelerometerEvent_t1893725728_StaticFields::get_offset_of_CACHHOCBHAE_0(),
	AccelerometerEvent_t1893725728_StaticFields::get_offset_of_NLBFAPCOBBK_1(),
	AccelerometerEvent_t1893725728_StaticFields::get_offset_of_FNHKMMGHANC_2(),
	0,
	AccelerometerEvent_t1893725728::get_offset_of_MIGGKHHBAID_4(),
	AccelerometerEvent_t1893725728::get_offset_of_HMBIGJIDGNB_5(),
	0,
	AccelerometerEvent_t1893725728::get_offset_of_DAPIOCPOIID_7(),
	AccelerometerEvent_t1893725728::get_offset_of_KKLEGOBOFFJ_8(),
	0,
	AccelerometerEvent_t1893725728::get_offset_of_LGBJOJHEJDM_10(),
	AccelerometerEvent_t1893725728::get_offset_of_NFCIJFNNNDL_11(),
	0,
	AccelerometerEvent_t1893725728::get_offset_of_LPLIBMIPMFD_13(),
	AccelerometerEvent_t1893725728::get_offset_of_PLILMNHLFED_14(),
	AccelerometerEvent_t1893725728::get_offset_of_BABCKJCECDJ_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (Builder_t1480486140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[2] = 
{
	Builder_t1480486140::get_offset_of_LCFNHIBHFMO_0(),
	Builder_t1480486140::get_offset_of_JOMIOHAFLNI_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (DepthMapEvent_t1516604558), -1, sizeof(DepthMapEvent_t1516604558_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2289[16] = 
{
	DepthMapEvent_t1516604558_StaticFields::get_offset_of_CACHHOCBHAE_0(),
	DepthMapEvent_t1516604558_StaticFields::get_offset_of_JALKHEGJNPP_1(),
	DepthMapEvent_t1516604558_StaticFields::get_offset_of_AKGOEFJJEMA_2(),
	0,
	DepthMapEvent_t1516604558::get_offset_of_MIGGKHHBAID_4(),
	DepthMapEvent_t1516604558::get_offset_of_HMBIGJIDGNB_5(),
	0,
	DepthMapEvent_t1516604558::get_offset_of_BACCJGEKFJB_7(),
	DepthMapEvent_t1516604558::get_offset_of_AKLIGHJBHJD_8(),
	0,
	DepthMapEvent_t1516604558::get_offset_of_FACLGENOEIJ_10(),
	DepthMapEvent_t1516604558::get_offset_of_KAOMCBNGBBG_11(),
	0,
	DepthMapEvent_t1516604558::get_offset_of_AJGIEBFLLCM_13(),
	DepthMapEvent_t1516604558::get_offset_of_GBADDIAPLKK_14(),
	DepthMapEvent_t1516604558::get_offset_of_BABCKJCECDJ_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (Builder_t3483346914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[2] = 
{
	Builder_t3483346914::get_offset_of_LCFNHIBHFMO_0(),
	Builder_t3483346914::get_offset_of_JOMIOHAFLNI_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (OrientationEvent_t2038376807), -1, sizeof(OrientationEvent_t2038376807_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2291[19] = 
{
	OrientationEvent_t2038376807_StaticFields::get_offset_of_CACHHOCBHAE_0(),
	OrientationEvent_t2038376807_StaticFields::get_offset_of_ICMJABHGNBO_1(),
	OrientationEvent_t2038376807_StaticFields::get_offset_of_EMDLBCKFOML_2(),
	0,
	OrientationEvent_t2038376807::get_offset_of_MIGGKHHBAID_4(),
	OrientationEvent_t2038376807::get_offset_of_HMBIGJIDGNB_5(),
	0,
	OrientationEvent_t2038376807::get_offset_of_DAPIOCPOIID_7(),
	OrientationEvent_t2038376807::get_offset_of_KKLEGOBOFFJ_8(),
	0,
	OrientationEvent_t2038376807::get_offset_of_LGBJOJHEJDM_10(),
	OrientationEvent_t2038376807::get_offset_of_NFCIJFNNNDL_11(),
	0,
	OrientationEvent_t2038376807::get_offset_of_LPLIBMIPMFD_13(),
	OrientationEvent_t2038376807::get_offset_of_PLILMNHLFED_14(),
	0,
	OrientationEvent_t2038376807::get_offset_of_CKAAFMKPDBO_16(),
	OrientationEvent_t2038376807::get_offset_of_FFPNCMDJLGP_17(),
	OrientationEvent_t2038376807::get_offset_of_BABCKJCECDJ_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (Builder_t2561526853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[2] = 
{
	Builder_t2561526853::get_offset_of_LCFNHIBHFMO_0(),
	Builder_t2561526853::get_offset_of_JOMIOHAFLNI_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (KeyEvent_t639576718), -1, sizeof(KeyEvent_t639576718_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2293[10] = 
{
	KeyEvent_t639576718_StaticFields::get_offset_of_CACHHOCBHAE_0(),
	KeyEvent_t639576718_StaticFields::get_offset_of_ONFPAEDCDAG_1(),
	KeyEvent_t639576718_StaticFields::get_offset_of_KPNOHGJNJOA_2(),
	0,
	KeyEvent_t639576718::get_offset_of_DECNNNLBPPF_4(),
	KeyEvent_t639576718::get_offset_of_LADFMKMCMMH_5(),
	0,
	KeyEvent_t639576718::get_offset_of_HODDPFPKFII_7(),
	KeyEvent_t639576718::get_offset_of_FBHPFKCGLEB_8(),
	KeyEvent_t639576718::get_offset_of_BABCKJCECDJ_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (Builder_t2056133158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[2] = 
{
	Builder_t2056133158::get_offset_of_LCFNHIBHFMO_0(),
	Builder_t2056133158::get_offset_of_JOMIOHAFLNI_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (Builder_t2537253112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2295[2] = 
{
	Builder_t2537253112::get_offset_of_LCFNHIBHFMO_0(),
	Builder_t2537253112::get_offset_of_JOMIOHAFLNI_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (FPFPADIHBIM_t767626572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[2] = 
{
	FPFPADIHBIM_t767626572::get_offset_of_U3CCDLPGEPJPIDU3Ek__BackingField_0(),
	FPFPADIHBIM_t767626572::get_offset_of_U3CINNBOBHBEOLU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (GvrBasePointerRaycaster_t1189534163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[2] = 
{
	GvrBasePointerRaycaster_t1189534163::get_offset_of_raycastMode_2(),
	GvrBasePointerRaycaster_t1189534163::get_offset_of_DLBHMNOACHA_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (NBEJNAKGDCB_t627056501)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2299[3] = 
{
	NBEJNAKGDCB_t627056501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
