﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// UnityEngine.Sprite
struct Sprite_t309593783;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OverlayNewGraphic
struct  OverlayNewGraphic_t4179090742  : public NarrativeEffect_t3336735121
{
public:
	// UnityEngine.Sprite OverlayNewGraphic::sprite
	Sprite_t309593783 * ___sprite_3;
	// System.Boolean OverlayNewGraphic::clearMenu
	bool ___clearMenu_4;

public:
	inline static int32_t get_offset_of_sprite_3() { return static_cast<int32_t>(offsetof(OverlayNewGraphic_t4179090742, ___sprite_3)); }
	inline Sprite_t309593783 * get_sprite_3() const { return ___sprite_3; }
	inline Sprite_t309593783 ** get_address_of_sprite_3() { return &___sprite_3; }
	inline void set_sprite_3(Sprite_t309593783 * value)
	{
		___sprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___sprite_3, value);
	}

	inline static int32_t get_offset_of_clearMenu_4() { return static_cast<int32_t>(offsetof(OverlayNewGraphic_t4179090742, ___clearMenu_4)); }
	inline bool get_clearMenu_4() const { return ___clearMenu_4; }
	inline bool* get_address_of_clearMenu_4() { return &___clearMenu_4; }
	inline void set_clearMenu_4(bool value)
	{
		___clearMenu_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
