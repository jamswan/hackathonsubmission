﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"

// System.Collections.Generic.List`1<GvrAudioRoom>
struct List_1_t622563310;
// UnityEngine.Transform
struct Transform_t3275118058;
// ENOAJGBOEPC
struct ENOAJGBOEPC_t2312888837;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OPGEDONPNEE
struct  OPGEDONPNEE_t1153938088  : public Il2CppObject
{
public:

public:
};

struct OPGEDONPNEE_t1153938088_StaticFields
{
public:
	// System.Int32 OPGEDONPNEE::KCPLHLPNHFH
	int32_t ___KCPLHLPNHFH_0;
	// System.Int32 OPGEDONPNEE::KEGPMMCHGLH
	int32_t ___KEGPMMCHGLH_1;
	// System.Int32 OPGEDONPNEE::AEFCJNPMOBA
	int32_t ___AEFCJNPMOBA_2;
	// UnityEngine.Color OPGEDONPNEE::DNMDLBEMGME
	Color_t2020392075  ___DNMDLBEMGME_3;
	// UnityEngine.Color OPGEDONPNEE::MPDDLCPCGDH
	Color_t2020392075  ___MPDDLCPCGDH_4;
	// UnityEngine.Bounds OPGEDONPNEE::KBGPBAHKIEF
	Bounds_t3033363703  ___KBGPBAHKIEF_16;
	// System.Collections.Generic.List`1<GvrAudioRoom> OPGEDONPNEE::DAPEPOHIALH
	List_1_t622563310 * ___DAPEPOHIALH_17;
	// System.Boolean OPGEDONPNEE::EJOBGGJEDCF
	bool ___EJOBGGJEDCF_18;
	// UnityEngine.Transform OPGEDONPNEE::KIJCBOIEBDB
	Transform_t3275118058 * ___KIJCBOIEBDB_19;
	// System.Int32 OPGEDONPNEE::DMBPGHAHIHJ
	int32_t ___DMBPGHAHIHJ_20;
	// ENOAJGBOEPC OPGEDONPNEE::CAJJOBMIBMG
	ENOAJGBOEPC_t2312888837 * ___CAJJOBMIBMG_21;

public:
	inline static int32_t get_offset_of_KCPLHLPNHFH_0() { return static_cast<int32_t>(offsetof(OPGEDONPNEE_t1153938088_StaticFields, ___KCPLHLPNHFH_0)); }
	inline int32_t get_KCPLHLPNHFH_0() const { return ___KCPLHLPNHFH_0; }
	inline int32_t* get_address_of_KCPLHLPNHFH_0() { return &___KCPLHLPNHFH_0; }
	inline void set_KCPLHLPNHFH_0(int32_t value)
	{
		___KCPLHLPNHFH_0 = value;
	}

	inline static int32_t get_offset_of_KEGPMMCHGLH_1() { return static_cast<int32_t>(offsetof(OPGEDONPNEE_t1153938088_StaticFields, ___KEGPMMCHGLH_1)); }
	inline int32_t get_KEGPMMCHGLH_1() const { return ___KEGPMMCHGLH_1; }
	inline int32_t* get_address_of_KEGPMMCHGLH_1() { return &___KEGPMMCHGLH_1; }
	inline void set_KEGPMMCHGLH_1(int32_t value)
	{
		___KEGPMMCHGLH_1 = value;
	}

	inline static int32_t get_offset_of_AEFCJNPMOBA_2() { return static_cast<int32_t>(offsetof(OPGEDONPNEE_t1153938088_StaticFields, ___AEFCJNPMOBA_2)); }
	inline int32_t get_AEFCJNPMOBA_2() const { return ___AEFCJNPMOBA_2; }
	inline int32_t* get_address_of_AEFCJNPMOBA_2() { return &___AEFCJNPMOBA_2; }
	inline void set_AEFCJNPMOBA_2(int32_t value)
	{
		___AEFCJNPMOBA_2 = value;
	}

	inline static int32_t get_offset_of_DNMDLBEMGME_3() { return static_cast<int32_t>(offsetof(OPGEDONPNEE_t1153938088_StaticFields, ___DNMDLBEMGME_3)); }
	inline Color_t2020392075  get_DNMDLBEMGME_3() const { return ___DNMDLBEMGME_3; }
	inline Color_t2020392075 * get_address_of_DNMDLBEMGME_3() { return &___DNMDLBEMGME_3; }
	inline void set_DNMDLBEMGME_3(Color_t2020392075  value)
	{
		___DNMDLBEMGME_3 = value;
	}

	inline static int32_t get_offset_of_MPDDLCPCGDH_4() { return static_cast<int32_t>(offsetof(OPGEDONPNEE_t1153938088_StaticFields, ___MPDDLCPCGDH_4)); }
	inline Color_t2020392075  get_MPDDLCPCGDH_4() const { return ___MPDDLCPCGDH_4; }
	inline Color_t2020392075 * get_address_of_MPDDLCPCGDH_4() { return &___MPDDLCPCGDH_4; }
	inline void set_MPDDLCPCGDH_4(Color_t2020392075  value)
	{
		___MPDDLCPCGDH_4 = value;
	}

	inline static int32_t get_offset_of_KBGPBAHKIEF_16() { return static_cast<int32_t>(offsetof(OPGEDONPNEE_t1153938088_StaticFields, ___KBGPBAHKIEF_16)); }
	inline Bounds_t3033363703  get_KBGPBAHKIEF_16() const { return ___KBGPBAHKIEF_16; }
	inline Bounds_t3033363703 * get_address_of_KBGPBAHKIEF_16() { return &___KBGPBAHKIEF_16; }
	inline void set_KBGPBAHKIEF_16(Bounds_t3033363703  value)
	{
		___KBGPBAHKIEF_16 = value;
	}

	inline static int32_t get_offset_of_DAPEPOHIALH_17() { return static_cast<int32_t>(offsetof(OPGEDONPNEE_t1153938088_StaticFields, ___DAPEPOHIALH_17)); }
	inline List_1_t622563310 * get_DAPEPOHIALH_17() const { return ___DAPEPOHIALH_17; }
	inline List_1_t622563310 ** get_address_of_DAPEPOHIALH_17() { return &___DAPEPOHIALH_17; }
	inline void set_DAPEPOHIALH_17(List_1_t622563310 * value)
	{
		___DAPEPOHIALH_17 = value;
		Il2CppCodeGenWriteBarrier(&___DAPEPOHIALH_17, value);
	}

	inline static int32_t get_offset_of_EJOBGGJEDCF_18() { return static_cast<int32_t>(offsetof(OPGEDONPNEE_t1153938088_StaticFields, ___EJOBGGJEDCF_18)); }
	inline bool get_EJOBGGJEDCF_18() const { return ___EJOBGGJEDCF_18; }
	inline bool* get_address_of_EJOBGGJEDCF_18() { return &___EJOBGGJEDCF_18; }
	inline void set_EJOBGGJEDCF_18(bool value)
	{
		___EJOBGGJEDCF_18 = value;
	}

	inline static int32_t get_offset_of_KIJCBOIEBDB_19() { return static_cast<int32_t>(offsetof(OPGEDONPNEE_t1153938088_StaticFields, ___KIJCBOIEBDB_19)); }
	inline Transform_t3275118058 * get_KIJCBOIEBDB_19() const { return ___KIJCBOIEBDB_19; }
	inline Transform_t3275118058 ** get_address_of_KIJCBOIEBDB_19() { return &___KIJCBOIEBDB_19; }
	inline void set_KIJCBOIEBDB_19(Transform_t3275118058 * value)
	{
		___KIJCBOIEBDB_19 = value;
		Il2CppCodeGenWriteBarrier(&___KIJCBOIEBDB_19, value);
	}

	inline static int32_t get_offset_of_DMBPGHAHIHJ_20() { return static_cast<int32_t>(offsetof(OPGEDONPNEE_t1153938088_StaticFields, ___DMBPGHAHIHJ_20)); }
	inline int32_t get_DMBPGHAHIHJ_20() const { return ___DMBPGHAHIHJ_20; }
	inline int32_t* get_address_of_DMBPGHAHIHJ_20() { return &___DMBPGHAHIHJ_20; }
	inline void set_DMBPGHAHIHJ_20(int32_t value)
	{
		___DMBPGHAHIHJ_20 = value;
	}

	inline static int32_t get_offset_of_CAJJOBMIBMG_21() { return static_cast<int32_t>(offsetof(OPGEDONPNEE_t1153938088_StaticFields, ___CAJJOBMIBMG_21)); }
	inline ENOAJGBOEPC_t2312888837 * get_CAJJOBMIBMG_21() const { return ___CAJJOBMIBMG_21; }
	inline ENOAJGBOEPC_t2312888837 ** get_address_of_CAJJOBMIBMG_21() { return &___CAJJOBMIBMG_21; }
	inline void set_CAJJOBMIBMG_21(ENOAJGBOEPC_t2312888837 * value)
	{
		___CAJJOBMIBMG_21 = value;
		Il2CppCodeGenWriteBarrier(&___CAJJOBMIBMG_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
