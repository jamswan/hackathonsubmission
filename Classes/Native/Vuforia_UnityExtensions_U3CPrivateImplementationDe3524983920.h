﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{147A48C5-2BC1-47C3-8F82-CE56F4E53F6E}/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t3524983920 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t3524983920__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
