﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// Gvr.Internal.EmulatorManager
struct EmulatorManager_t3364249716;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorManager/<EndOfFrame>c__Iterator0
struct  U3CEndOfFrameU3Ec__Iterator0_t4253624923  : public Il2CppObject
{
public:
	// System.Object Gvr.Internal.EmulatorManager/<EndOfFrame>c__Iterator0::HCHAHDKBNGC
	Il2CppObject * ___HCHAHDKBNGC_0;
	// Gvr.Internal.EmulatorManager Gvr.Internal.EmulatorManager/<EndOfFrame>c__Iterator0::AOOLEAHHMIH
	EmulatorManager_t3364249716 * ___AOOLEAHHMIH_1;
	// System.Object Gvr.Internal.EmulatorManager/<EndOfFrame>c__Iterator0::LGBFNMECDHC
	Il2CppObject * ___LGBFNMECDHC_2;
	// System.Boolean Gvr.Internal.EmulatorManager/<EndOfFrame>c__Iterator0::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_3;
	// System.Int32 Gvr.Internal.EmulatorManager/<EndOfFrame>c__Iterator0::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_4;

public:
	inline static int32_t get_offset_of_HCHAHDKBNGC_0() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t4253624923, ___HCHAHDKBNGC_0)); }
	inline Il2CppObject * get_HCHAHDKBNGC_0() const { return ___HCHAHDKBNGC_0; }
	inline Il2CppObject ** get_address_of_HCHAHDKBNGC_0() { return &___HCHAHDKBNGC_0; }
	inline void set_HCHAHDKBNGC_0(Il2CppObject * value)
	{
		___HCHAHDKBNGC_0 = value;
		Il2CppCodeGenWriteBarrier(&___HCHAHDKBNGC_0, value);
	}

	inline static int32_t get_offset_of_AOOLEAHHMIH_1() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t4253624923, ___AOOLEAHHMIH_1)); }
	inline EmulatorManager_t3364249716 * get_AOOLEAHHMIH_1() const { return ___AOOLEAHHMIH_1; }
	inline EmulatorManager_t3364249716 ** get_address_of_AOOLEAHHMIH_1() { return &___AOOLEAHHMIH_1; }
	inline void set_AOOLEAHHMIH_1(EmulatorManager_t3364249716 * value)
	{
		___AOOLEAHHMIH_1 = value;
		Il2CppCodeGenWriteBarrier(&___AOOLEAHHMIH_1, value);
	}

	inline static int32_t get_offset_of_LGBFNMECDHC_2() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t4253624923, ___LGBFNMECDHC_2)); }
	inline Il2CppObject * get_LGBFNMECDHC_2() const { return ___LGBFNMECDHC_2; }
	inline Il2CppObject ** get_address_of_LGBFNMECDHC_2() { return &___LGBFNMECDHC_2; }
	inline void set_LGBFNMECDHC_2(Il2CppObject * value)
	{
		___LGBFNMECDHC_2 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_2, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_3() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t4253624923, ___IMAGLIMLPFK_3)); }
	inline bool get_IMAGLIMLPFK_3() const { return ___IMAGLIMLPFK_3; }
	inline bool* get_address_of_IMAGLIMLPFK_3() { return &___IMAGLIMLPFK_3; }
	inline void set_IMAGLIMLPFK_3(bool value)
	{
		___IMAGLIMLPFK_3 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_4() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t4253624923, ___EPCFNNGDBGC_4)); }
	inline int32_t get_EPCFNNGDBGC_4() const { return ___EPCFNNGDBGC_4; }
	inline int32_t* get_address_of_EPCFNNGDBGC_4() { return &___EPCFNNGDBGC_4; }
	inline void set_EPCFNNGDBGC_4(int32_t value)
	{
		___EPCFNNGDBGC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
