﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1779888572;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackableEventHandler
struct  TrackableEventHandler_t3812462983  : public MonoBehaviour_t1158329972
{
public:
	// Vuforia.TrackableBehaviour TrackableEventHandler::MDEHCLGCIKP
	TrackableBehaviour_t1779888572 * ___MDEHCLGCIKP_2;
	// System.Boolean TrackableEventHandler::CEKNEAIENHP
	bool ___CEKNEAIENHP_3;
	// System.Boolean TrackableEventHandler::FJAJCGLMJIN
	bool ___FJAJCGLMJIN_4;
	// System.Single TrackableEventHandler::JEAAIJFMKFE
	float ___JEAAIJFMKFE_5;

public:
	inline static int32_t get_offset_of_MDEHCLGCIKP_2() { return static_cast<int32_t>(offsetof(TrackableEventHandler_t3812462983, ___MDEHCLGCIKP_2)); }
	inline TrackableBehaviour_t1779888572 * get_MDEHCLGCIKP_2() const { return ___MDEHCLGCIKP_2; }
	inline TrackableBehaviour_t1779888572 ** get_address_of_MDEHCLGCIKP_2() { return &___MDEHCLGCIKP_2; }
	inline void set_MDEHCLGCIKP_2(TrackableBehaviour_t1779888572 * value)
	{
		___MDEHCLGCIKP_2 = value;
		Il2CppCodeGenWriteBarrier(&___MDEHCLGCIKP_2, value);
	}

	inline static int32_t get_offset_of_CEKNEAIENHP_3() { return static_cast<int32_t>(offsetof(TrackableEventHandler_t3812462983, ___CEKNEAIENHP_3)); }
	inline bool get_CEKNEAIENHP_3() const { return ___CEKNEAIENHP_3; }
	inline bool* get_address_of_CEKNEAIENHP_3() { return &___CEKNEAIENHP_3; }
	inline void set_CEKNEAIENHP_3(bool value)
	{
		___CEKNEAIENHP_3 = value;
	}

	inline static int32_t get_offset_of_FJAJCGLMJIN_4() { return static_cast<int32_t>(offsetof(TrackableEventHandler_t3812462983, ___FJAJCGLMJIN_4)); }
	inline bool get_FJAJCGLMJIN_4() const { return ___FJAJCGLMJIN_4; }
	inline bool* get_address_of_FJAJCGLMJIN_4() { return &___FJAJCGLMJIN_4; }
	inline void set_FJAJCGLMJIN_4(bool value)
	{
		___FJAJCGLMJIN_4 = value;
	}

	inline static int32_t get_offset_of_JEAAIJFMKFE_5() { return static_cast<int32_t>(offsetof(TrackableEventHandler_t3812462983, ___JEAAIJFMKFE_5)); }
	inline float get_JEAAIJFMKFE_5() const { return ___JEAAIJFMKFE_5; }
	inline float* get_address_of_JEAAIJFMKFE_5() { return &___JEAAIJFMKFE_5; }
	inline void set_JEAAIJFMKFE_5(float value)
	{
		___JEAAIJFMKFE_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
