﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TargetSeen
struct  TargetSeen_t1806735238  : public NarrativeCondition_t4123859457
{
public:
	// System.String TargetSeen::TargetName
	String_t* ___TargetName_13;

public:
	inline static int32_t get_offset_of_TargetName_13() { return static_cast<int32_t>(offsetof(TargetSeen_t1806735238, ___TargetName_13)); }
	inline String_t* get_TargetName_13() const { return ___TargetName_13; }
	inline String_t** get_address_of_TargetName_13() { return &___TargetName_13; }
	inline void set_TargetName_13(String_t* value)
	{
		___TargetName_13 = value;
		Il2CppCodeGenWriteBarrier(&___TargetName_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
