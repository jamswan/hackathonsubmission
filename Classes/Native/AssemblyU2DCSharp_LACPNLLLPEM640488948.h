﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_MMINFHHJGIF384011353.h"

// UnityEngine.Texture2D
struct Texture2D_t3542995729;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LACPNLLLPEM
struct  LACPNLLLPEM_t640488948  : public MMINFHHJGIF_t384011353
{
public:
	// UnityEngine.Texture2D LACPNLLLPEM::IDBKNFKOEBF
	Texture2D_t3542995729 * ___IDBKNFKOEBF_4;

public:
	inline static int32_t get_offset_of_IDBKNFKOEBF_4() { return static_cast<int32_t>(offsetof(LACPNLLLPEM_t640488948, ___IDBKNFKOEBF_4)); }
	inline Texture2D_t3542995729 * get_IDBKNFKOEBF_4() const { return ___IDBKNFKOEBF_4; }
	inline Texture2D_t3542995729 ** get_address_of_IDBKNFKOEBF_4() { return &___IDBKNFKOEBF_4; }
	inline void set_IDBKNFKOEBF_4(Texture2D_t3542995729 * value)
	{
		___IDBKNFKOEBF_4 = value;
		Il2CppCodeGenWriteBarrier(&___IDBKNFKOEBF_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
