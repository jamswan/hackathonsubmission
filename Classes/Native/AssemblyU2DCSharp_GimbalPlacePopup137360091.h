﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GimbalPlacePopup
struct  GimbalPlacePopup_t137360091  : public MonoBehaviour_t1158329972
{
public:
	// System.String GimbalPlacePopup::locationName
	String_t* ___locationName_2;
	// System.String GimbalPlacePopup::text
	String_t* ___text_3;
	// System.Single GimbalPlacePopup::duration
	float ___duration_4;

public:
	inline static int32_t get_offset_of_locationName_2() { return static_cast<int32_t>(offsetof(GimbalPlacePopup_t137360091, ___locationName_2)); }
	inline String_t* get_locationName_2() const { return ___locationName_2; }
	inline String_t** get_address_of_locationName_2() { return &___locationName_2; }
	inline void set_locationName_2(String_t* value)
	{
		___locationName_2 = value;
		Il2CppCodeGenWriteBarrier(&___locationName_2, value);
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(GimbalPlacePopup_t137360091, ___text_3)); }
	inline String_t* get_text_3() const { return ___text_3; }
	inline String_t** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(String_t* value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier(&___text_3, value);
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(GimbalPlacePopup_t137360091, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
