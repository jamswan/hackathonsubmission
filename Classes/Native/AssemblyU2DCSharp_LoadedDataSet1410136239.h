﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.String
struct String_t;
// Vuforia.DataSet
struct DataSet_t626511550;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadedDataSet
struct  LoadedDataSet_t1410136239 
{
public:
	// System.String LoadedDataSet::name
	String_t* ___name_0;
	// Vuforia.DataSet LoadedDataSet::dataSet
	DataSet_t626511550 * ___dataSet_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(LoadedDataSet_t1410136239, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_dataSet_1() { return static_cast<int32_t>(offsetof(LoadedDataSet_t1410136239, ___dataSet_1)); }
	inline DataSet_t626511550 * get_dataSet_1() const { return ___dataSet_1; }
	inline DataSet_t626511550 ** get_address_of_dataSet_1() { return &___dataSet_1; }
	inline void set_dataSet_1(DataSet_t626511550 * value)
	{
		___dataSet_1 = value;
		Il2CppCodeGenWriteBarrier(&___dataSet_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of LoadedDataSet
struct LoadedDataSet_t1410136239_marshaled_pinvoke
{
	char* ___name_0;
	DataSet_t626511550 * ___dataSet_1;
};
// Native definition for COM marshalling of LoadedDataSet
struct LoadedDataSet_t1410136239_marshaled_com
{
	Il2CppChar* ___name_0;
	DataSet_t626511550 * ___dataSet_1;
};
