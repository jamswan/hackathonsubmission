﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.BADMPKCPJFF/FIDMFOFBGKL
struct  FIDMFOFBGKL_t3974762026 
{
public:
	// System.Int32 Gvr.Internal.BADMPKCPJFF/FIDMFOFBGKL::AGGPCOGGDCI
	int32_t ___AGGPCOGGDCI_0;
	// System.Single Gvr.Internal.BADMPKCPJFF/FIDMFOFBGKL::GIIKJDKOAGE
	float ___GIIKJDKOAGE_1;
	// System.Single Gvr.Internal.BADMPKCPJFF/FIDMFOFBGKL::OHEEFIDMMON
	float ___OHEEFIDMMON_2;

public:
	inline static int32_t get_offset_of_AGGPCOGGDCI_0() { return static_cast<int32_t>(offsetof(FIDMFOFBGKL_t3974762026, ___AGGPCOGGDCI_0)); }
	inline int32_t get_AGGPCOGGDCI_0() const { return ___AGGPCOGGDCI_0; }
	inline int32_t* get_address_of_AGGPCOGGDCI_0() { return &___AGGPCOGGDCI_0; }
	inline void set_AGGPCOGGDCI_0(int32_t value)
	{
		___AGGPCOGGDCI_0 = value;
	}

	inline static int32_t get_offset_of_GIIKJDKOAGE_1() { return static_cast<int32_t>(offsetof(FIDMFOFBGKL_t3974762026, ___GIIKJDKOAGE_1)); }
	inline float get_GIIKJDKOAGE_1() const { return ___GIIKJDKOAGE_1; }
	inline float* get_address_of_GIIKJDKOAGE_1() { return &___GIIKJDKOAGE_1; }
	inline void set_GIIKJDKOAGE_1(float value)
	{
		___GIIKJDKOAGE_1 = value;
	}

	inline static int32_t get_offset_of_OHEEFIDMMON_2() { return static_cast<int32_t>(offsetof(FIDMFOFBGKL_t3974762026, ___OHEEFIDMMON_2)); }
	inline float get_OHEEFIDMMON_2() const { return ___OHEEFIDMMON_2; }
	inline float* get_address_of_OHEEFIDMMON_2() { return &___OHEEFIDMMON_2; }
	inline void set_OHEEFIDMMON_2(float value)
	{
		___OHEEFIDMMON_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
