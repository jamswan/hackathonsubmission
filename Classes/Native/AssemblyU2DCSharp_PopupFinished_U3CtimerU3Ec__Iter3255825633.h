﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// PopupFinished
struct PopupFinished_t3728197864;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopupFinished/<timer>c__Iterator0
struct  U3CtimerU3Ec__Iterator0_t3255825633  : public Il2CppObject
{
public:
	// System.Single PopupFinished/<timer>c__Iterator0::KJGAGDEJFFK
	float ___KJGAGDEJFFK_0;
	// PopupFinished PopupFinished/<timer>c__Iterator0::AOOLEAHHMIH
	PopupFinished_t3728197864 * ___AOOLEAHHMIH_1;
	// System.Object PopupFinished/<timer>c__Iterator0::LGBFNMECDHC
	Il2CppObject * ___LGBFNMECDHC_2;
	// System.Boolean PopupFinished/<timer>c__Iterator0::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_3;
	// System.Int32 PopupFinished/<timer>c__Iterator0::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_4;

public:
	inline static int32_t get_offset_of_KJGAGDEJFFK_0() { return static_cast<int32_t>(offsetof(U3CtimerU3Ec__Iterator0_t3255825633, ___KJGAGDEJFFK_0)); }
	inline float get_KJGAGDEJFFK_0() const { return ___KJGAGDEJFFK_0; }
	inline float* get_address_of_KJGAGDEJFFK_0() { return &___KJGAGDEJFFK_0; }
	inline void set_KJGAGDEJFFK_0(float value)
	{
		___KJGAGDEJFFK_0 = value;
	}

	inline static int32_t get_offset_of_AOOLEAHHMIH_1() { return static_cast<int32_t>(offsetof(U3CtimerU3Ec__Iterator0_t3255825633, ___AOOLEAHHMIH_1)); }
	inline PopupFinished_t3728197864 * get_AOOLEAHHMIH_1() const { return ___AOOLEAHHMIH_1; }
	inline PopupFinished_t3728197864 ** get_address_of_AOOLEAHHMIH_1() { return &___AOOLEAHHMIH_1; }
	inline void set_AOOLEAHHMIH_1(PopupFinished_t3728197864 * value)
	{
		___AOOLEAHHMIH_1 = value;
		Il2CppCodeGenWriteBarrier(&___AOOLEAHHMIH_1, value);
	}

	inline static int32_t get_offset_of_LGBFNMECDHC_2() { return static_cast<int32_t>(offsetof(U3CtimerU3Ec__Iterator0_t3255825633, ___LGBFNMECDHC_2)); }
	inline Il2CppObject * get_LGBFNMECDHC_2() const { return ___LGBFNMECDHC_2; }
	inline Il2CppObject ** get_address_of_LGBFNMECDHC_2() { return &___LGBFNMECDHC_2; }
	inline void set_LGBFNMECDHC_2(Il2CppObject * value)
	{
		___LGBFNMECDHC_2 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_2, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_3() { return static_cast<int32_t>(offsetof(U3CtimerU3Ec__Iterator0_t3255825633, ___IMAGLIMLPFK_3)); }
	inline bool get_IMAGLIMLPFK_3() const { return ___IMAGLIMLPFK_3; }
	inline bool* get_address_of_IMAGLIMLPFK_3() { return &___IMAGLIMLPFK_3; }
	inline void set_IMAGLIMLPFK_3(bool value)
	{
		___IMAGLIMLPFK_3 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_4() { return static_cast<int32_t>(offsetof(U3CtimerU3Ec__Iterator0_t3255825633, ___EPCFNNGDBGC_4)); }
	inline int32_t get_EPCFNNGDBGC_4() const { return ___EPCFNNGDBGC_4; }
	inline int32_t* get_address_of_EPCFNNGDBGC_4() { return &___EPCFNNGDBGC_4; }
	inline void set_EPCFNNGDBGC_4(int32_t value)
	{
		___EPCFNNGDBGC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
