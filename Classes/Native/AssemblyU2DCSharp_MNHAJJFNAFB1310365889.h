﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<OAEHGANBECD>
struct EventFunction_1_t2670640647;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MNHAJJFNAFB
struct  MNHAJJFNAFB_t1310365889  : public Il2CppObject
{
public:

public:
};

struct MNHAJJFNAFB_t1310365889_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<OAEHGANBECD> MNHAJJFNAFB::NBOIJKLFLJL
	EventFunction_1_t2670640647 * ___NBOIJKLFLJL_0;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<OAEHGANBECD> MNHAJJFNAFB::<>f__mg$cache0
	EventFunction_1_t2670640647 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_NBOIJKLFLJL_0() { return static_cast<int32_t>(offsetof(MNHAJJFNAFB_t1310365889_StaticFields, ___NBOIJKLFLJL_0)); }
	inline EventFunction_1_t2670640647 * get_NBOIJKLFLJL_0() const { return ___NBOIJKLFLJL_0; }
	inline EventFunction_1_t2670640647 ** get_address_of_NBOIJKLFLJL_0() { return &___NBOIJKLFLJL_0; }
	inline void set_NBOIJKLFLJL_0(EventFunction_1_t2670640647 * value)
	{
		___NBOIJKLFLJL_0 = value;
		Il2CppCodeGenWriteBarrier(&___NBOIJKLFLJL_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(MNHAJJFNAFB_t1310365889_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline EventFunction_1_t2670640647 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline EventFunction_1_t2670640647 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(EventFunction_1_t2670640647 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
