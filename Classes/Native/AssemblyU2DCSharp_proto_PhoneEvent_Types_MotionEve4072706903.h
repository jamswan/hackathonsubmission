﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Gene2989368125.h"

// proto.PhoneEvent/Types/MotionEvent
struct MotionEvent_t4072706903;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>
struct PopsicleList_1_t3701374430;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/MotionEvent
struct  MotionEvent_t4072706903  : public GeneratedMessageLite_2_t2989368125
{
public:
	// System.Boolean proto.PhoneEvent/Types/MotionEvent::MIGGKHHBAID
	bool ___MIGGKHHBAID_4;
	// System.Int64 proto.PhoneEvent/Types/MotionEvent::HMBIGJIDGNB
	int64_t ___HMBIGJIDGNB_5;
	// System.Boolean proto.PhoneEvent/Types/MotionEvent::DECNNNLBPPF
	bool ___DECNNNLBPPF_7;
	// System.Int32 proto.PhoneEvent/Types/MotionEvent::LADFMKMCMMH
	int32_t ___LADFMKMCMMH_8;
	// Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer> proto.PhoneEvent/Types/MotionEvent::PCNPGHNFILL
	PopsicleList_1_t3701374430 * ___PCNPGHNFILL_10;
	// System.Int32 proto.PhoneEvent/Types/MotionEvent::BABCKJCECDJ
	int32_t ___BABCKJCECDJ_11;

public:
	inline static int32_t get_offset_of_MIGGKHHBAID_4() { return static_cast<int32_t>(offsetof(MotionEvent_t4072706903, ___MIGGKHHBAID_4)); }
	inline bool get_MIGGKHHBAID_4() const { return ___MIGGKHHBAID_4; }
	inline bool* get_address_of_MIGGKHHBAID_4() { return &___MIGGKHHBAID_4; }
	inline void set_MIGGKHHBAID_4(bool value)
	{
		___MIGGKHHBAID_4 = value;
	}

	inline static int32_t get_offset_of_HMBIGJIDGNB_5() { return static_cast<int32_t>(offsetof(MotionEvent_t4072706903, ___HMBIGJIDGNB_5)); }
	inline int64_t get_HMBIGJIDGNB_5() const { return ___HMBIGJIDGNB_5; }
	inline int64_t* get_address_of_HMBIGJIDGNB_5() { return &___HMBIGJIDGNB_5; }
	inline void set_HMBIGJIDGNB_5(int64_t value)
	{
		___HMBIGJIDGNB_5 = value;
	}

	inline static int32_t get_offset_of_DECNNNLBPPF_7() { return static_cast<int32_t>(offsetof(MotionEvent_t4072706903, ___DECNNNLBPPF_7)); }
	inline bool get_DECNNNLBPPF_7() const { return ___DECNNNLBPPF_7; }
	inline bool* get_address_of_DECNNNLBPPF_7() { return &___DECNNNLBPPF_7; }
	inline void set_DECNNNLBPPF_7(bool value)
	{
		___DECNNNLBPPF_7 = value;
	}

	inline static int32_t get_offset_of_LADFMKMCMMH_8() { return static_cast<int32_t>(offsetof(MotionEvent_t4072706903, ___LADFMKMCMMH_8)); }
	inline int32_t get_LADFMKMCMMH_8() const { return ___LADFMKMCMMH_8; }
	inline int32_t* get_address_of_LADFMKMCMMH_8() { return &___LADFMKMCMMH_8; }
	inline void set_LADFMKMCMMH_8(int32_t value)
	{
		___LADFMKMCMMH_8 = value;
	}

	inline static int32_t get_offset_of_PCNPGHNFILL_10() { return static_cast<int32_t>(offsetof(MotionEvent_t4072706903, ___PCNPGHNFILL_10)); }
	inline PopsicleList_1_t3701374430 * get_PCNPGHNFILL_10() const { return ___PCNPGHNFILL_10; }
	inline PopsicleList_1_t3701374430 ** get_address_of_PCNPGHNFILL_10() { return &___PCNPGHNFILL_10; }
	inline void set_PCNPGHNFILL_10(PopsicleList_1_t3701374430 * value)
	{
		___PCNPGHNFILL_10 = value;
		Il2CppCodeGenWriteBarrier(&___PCNPGHNFILL_10, value);
	}

	inline static int32_t get_offset_of_BABCKJCECDJ_11() { return static_cast<int32_t>(offsetof(MotionEvent_t4072706903, ___BABCKJCECDJ_11)); }
	inline int32_t get_BABCKJCECDJ_11() const { return ___BABCKJCECDJ_11; }
	inline int32_t* get_address_of_BABCKJCECDJ_11() { return &___BABCKJCECDJ_11; }
	inline void set_BABCKJCECDJ_11(int32_t value)
	{
		___BABCKJCECDJ_11 = value;
	}
};

struct MotionEvent_t4072706903_StaticFields
{
public:
	// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::CACHHOCBHAE
	MotionEvent_t4072706903 * ___CACHHOCBHAE_0;
	// System.String[] proto.PhoneEvent/Types/MotionEvent::NMNLDGPHJIL
	StringU5BU5D_t1642385972* ___NMNLDGPHJIL_1;
	// System.UInt32[] proto.PhoneEvent/Types/MotionEvent::EOMOAOLBBIP
	UInt32U5BU5D_t59386216* ___EOMOAOLBBIP_2;

public:
	inline static int32_t get_offset_of_CACHHOCBHAE_0() { return static_cast<int32_t>(offsetof(MotionEvent_t4072706903_StaticFields, ___CACHHOCBHAE_0)); }
	inline MotionEvent_t4072706903 * get_CACHHOCBHAE_0() const { return ___CACHHOCBHAE_0; }
	inline MotionEvent_t4072706903 ** get_address_of_CACHHOCBHAE_0() { return &___CACHHOCBHAE_0; }
	inline void set_CACHHOCBHAE_0(MotionEvent_t4072706903 * value)
	{
		___CACHHOCBHAE_0 = value;
		Il2CppCodeGenWriteBarrier(&___CACHHOCBHAE_0, value);
	}

	inline static int32_t get_offset_of_NMNLDGPHJIL_1() { return static_cast<int32_t>(offsetof(MotionEvent_t4072706903_StaticFields, ___NMNLDGPHJIL_1)); }
	inline StringU5BU5D_t1642385972* get_NMNLDGPHJIL_1() const { return ___NMNLDGPHJIL_1; }
	inline StringU5BU5D_t1642385972** get_address_of_NMNLDGPHJIL_1() { return &___NMNLDGPHJIL_1; }
	inline void set_NMNLDGPHJIL_1(StringU5BU5D_t1642385972* value)
	{
		___NMNLDGPHJIL_1 = value;
		Il2CppCodeGenWriteBarrier(&___NMNLDGPHJIL_1, value);
	}

	inline static int32_t get_offset_of_EOMOAOLBBIP_2() { return static_cast<int32_t>(offsetof(MotionEvent_t4072706903_StaticFields, ___EOMOAOLBBIP_2)); }
	inline UInt32U5BU5D_t59386216* get_EOMOAOLBBIP_2() const { return ___EOMOAOLBBIP_2; }
	inline UInt32U5BU5D_t59386216** get_address_of_EOMOAOLBBIP_2() { return &___EOMOAOLBBIP_2; }
	inline void set_EOMOAOLBBIP_2(UInt32U5BU5D_t59386216* value)
	{
		___EOMOAOLBBIP_2 = value;
		Il2CppCodeGenWriteBarrier(&___EOMOAOLBBIP_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
