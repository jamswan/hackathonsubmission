﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// MMINFHHJGIF[]
struct MMINFHHJGIFU5BU5D_t1583571492;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GBMIMKMMFPO
struct  GBMIMKMMFPO_t601510760  : public Il2CppObject
{
public:
	// MMINFHHJGIF[] GBMIMKMMFPO::ENMDIGLCNEF
	MMINFHHJGIFU5BU5D_t1583571492* ___ENMDIGLCNEF_0;
	// UnityEngine.GUIStyle GBMIMKMMFPO::IECEJPLGHLG
	GUIStyle_t1799908754 * ___IECEJPLGHLG_1;
	// System.Int32 GBMIMKMMFPO::JNLEFANLCNC
	int32_t ___JNLEFANLCNC_2;
	// System.Int32 GBMIMKMMFPO::HPJMAGFMLBO
	int32_t ___HPJMAGFMLBO_3;
	// System.Int32 GBMIMKMMFPO::KLIMLJCLJNH
	int32_t ___KLIMLJCLJNH_4;
	// System.Single GBMIMKMMFPO::NOPKKJHHIOG
	float ___NOPKKJHHIOG_5;

public:
	inline static int32_t get_offset_of_ENMDIGLCNEF_0() { return static_cast<int32_t>(offsetof(GBMIMKMMFPO_t601510760, ___ENMDIGLCNEF_0)); }
	inline MMINFHHJGIFU5BU5D_t1583571492* get_ENMDIGLCNEF_0() const { return ___ENMDIGLCNEF_0; }
	inline MMINFHHJGIFU5BU5D_t1583571492** get_address_of_ENMDIGLCNEF_0() { return &___ENMDIGLCNEF_0; }
	inline void set_ENMDIGLCNEF_0(MMINFHHJGIFU5BU5D_t1583571492* value)
	{
		___ENMDIGLCNEF_0 = value;
		Il2CppCodeGenWriteBarrier(&___ENMDIGLCNEF_0, value);
	}

	inline static int32_t get_offset_of_IECEJPLGHLG_1() { return static_cast<int32_t>(offsetof(GBMIMKMMFPO_t601510760, ___IECEJPLGHLG_1)); }
	inline GUIStyle_t1799908754 * get_IECEJPLGHLG_1() const { return ___IECEJPLGHLG_1; }
	inline GUIStyle_t1799908754 ** get_address_of_IECEJPLGHLG_1() { return &___IECEJPLGHLG_1; }
	inline void set_IECEJPLGHLG_1(GUIStyle_t1799908754 * value)
	{
		___IECEJPLGHLG_1 = value;
		Il2CppCodeGenWriteBarrier(&___IECEJPLGHLG_1, value);
	}

	inline static int32_t get_offset_of_JNLEFANLCNC_2() { return static_cast<int32_t>(offsetof(GBMIMKMMFPO_t601510760, ___JNLEFANLCNC_2)); }
	inline int32_t get_JNLEFANLCNC_2() const { return ___JNLEFANLCNC_2; }
	inline int32_t* get_address_of_JNLEFANLCNC_2() { return &___JNLEFANLCNC_2; }
	inline void set_JNLEFANLCNC_2(int32_t value)
	{
		___JNLEFANLCNC_2 = value;
	}

	inline static int32_t get_offset_of_HPJMAGFMLBO_3() { return static_cast<int32_t>(offsetof(GBMIMKMMFPO_t601510760, ___HPJMAGFMLBO_3)); }
	inline int32_t get_HPJMAGFMLBO_3() const { return ___HPJMAGFMLBO_3; }
	inline int32_t* get_address_of_HPJMAGFMLBO_3() { return &___HPJMAGFMLBO_3; }
	inline void set_HPJMAGFMLBO_3(int32_t value)
	{
		___HPJMAGFMLBO_3 = value;
	}

	inline static int32_t get_offset_of_KLIMLJCLJNH_4() { return static_cast<int32_t>(offsetof(GBMIMKMMFPO_t601510760, ___KLIMLJCLJNH_4)); }
	inline int32_t get_KLIMLJCLJNH_4() const { return ___KLIMLJCLJNH_4; }
	inline int32_t* get_address_of_KLIMLJCLJNH_4() { return &___KLIMLJCLJNH_4; }
	inline void set_KLIMLJCLJNH_4(int32_t value)
	{
		___KLIMLJCLJNH_4 = value;
	}

	inline static int32_t get_offset_of_NOPKKJHHIOG_5() { return static_cast<int32_t>(offsetof(GBMIMKMMFPO_t601510760, ___NOPKKJHHIOG_5)); }
	inline float get_NOPKKJHHIOG_5() const { return ___NOPKKJHHIOG_5; }
	inline float* get_address_of_NOPKKJHHIOG_5() { return &___NOPKKJHHIOG_5; }
	inline void set_NOPKKJHHIOG_5(float value)
	{
		___NOPKKJHHIOG_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
