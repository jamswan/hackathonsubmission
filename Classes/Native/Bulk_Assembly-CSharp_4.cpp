﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour359035403.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "Vuforia_UnityExtensions_Vuforia_NullUnityPlayer754446093.h"
#include "AssemblyU2DCSharp_Vuforia_OBLIGPGBOJF3834424063.h"
#include "AssemblyU2DCSharp_Vuforia_MHAOKAIGBBF1137417231.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer918240325.h"
#include "AssemblyU2DCSharp_Vuforia_KDDDFNIEPOB809780732.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3319870759.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB3249343815.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "AssemblyU2DCSharp_Vuforia_VuMarkBehaviour2060629989.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkAbstractBeha1830666997.h"
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour407765638.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamAbstractBeha4104806356.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour2494532455.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager2424874861.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH1535150527.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour4057911311.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1779888572.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour3366478421.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725.h"
#include "AssemblyU2DCSharp_YesNoChoice3116058963.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "AssemblyU2DCSharp_EnableGameObject1635748018.h"
#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"
#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"

// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t359035403;
// Vuforia.NullUnityPlayer
struct NullUnityPlayer_t754446093;
// Vuforia.OBLIGPGBOJF
struct OBLIGPGBOJF_t3834424063;
// Vuforia.MHAOKAIGBBF
struct MHAOKAIGBBF_t1137417231;
// Vuforia.PlayModeUnityPlayer
struct PlayModeUnityPlayer_t918240325;
// Vuforia.KDDDFNIEPOB
struct KDDDFNIEPOB_t809780732;
// Vuforia.VuforiaAbstractBehaviour
struct VuforiaAbstractBehaviour_t3319870759;
// Vuforia.IUnityPlayer
struct IUnityPlayer_t2720985375;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Vuforia.ComponentFactoryStarterBehaviour
struct ComponentFactoryStarterBehaviour_t3249343815;
// System.Object
struct Il2CppObject;
// UnityEngine.Object
struct Object_t1021602117;
// Vuforia.VuMarkBehaviour
struct VuMarkBehaviour_t2060629989;
// Vuforia.VuMarkAbstractBehaviour
struct VuMarkAbstractBehaviour_t1830666997;
// Vuforia.WebCamBehaviour
struct WebCamBehaviour_t407765638;
// Vuforia.WebCamAbstractBehaviour
struct WebCamAbstractBehaviour_t4104806356;
// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t2494532455;
// UnityEngine.Material
struct Material_t193706927;
// Vuforia.VuforiaManager
struct VuforiaManager_t2424874861;
// UnityEngine.Camera[]
struct CameraU5BU5D_t3079764780;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;
// UnityEngine.Behaviour
struct Behaviour_t955675639;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// Vuforia.WireframeTrackableEventHandler
struct WireframeTrackableEventHandler_t1535150527;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1779888572;
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t696806248;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629;
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t2935582494;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Collider
struct Collider_t3497673348;
// Vuforia.WordBehaviour
struct WordBehaviour_t3366478421;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t2878458725;
// YesNoChoice
struct YesNoChoice_t3116058963;
// UnityEngine.GUISkin
struct GUISkin_t1436893342;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// NarrativeCondition
struct NarrativeCondition_t4123859457;
extern Il2CppClass* NullUnityPlayer_t754446093_il2cpp_TypeInfo_var;
extern Il2CppClass* OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var;
extern Il2CppClass* MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var;
extern Il2CppClass* KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var;
extern const uint32_t VuforiaBehaviour_PNCGFCJJGNL_m2329860686_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_EHGKCBDLMJO_m3000324330_MetadataUsageId;
extern Il2CppClass* VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var;
extern const uint32_t VuforiaBehaviour_DGENOCOMEKB_m1281060286_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_get_Instance_m790681192_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_CFELMCGNBHC_m1793512764_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_PDHCNDEDBPL_m1877488300_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_BBKHAACJHGD_m5466941_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_KIFLLMPNMNC_m1123554243_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_CGPLGMCGBCE_m1097455718_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_FEKBAIDKMBN_m3164767870_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_JKHMMNCFHJI_m2490924137_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_EDKAAPKGGOD_m3253621180_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_NKHDJMBCBEM_m694517645_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_MJBNINKEHFJ_m1346401912_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_Awake_m3772880569_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_CBJJNJENPCI_m1261556742_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_MKAJLMEAOAM_m1435281255_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_AKFJHJANLGB_m3356610398_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_AKJIJGJNFBN_m3992524508_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_FNBNJDGLFEF_m3441889726_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_GGIMPHGMBHF_m864314164_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_HEGGCOKFJJK_m2412924347_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_EJIGBOCHMJG_m2425568383_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_EFAGDFOEOCA_m3328127010_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_ELALFJNKHMJ_m286753846_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_OPFIGFLCCBP_m1212912359_MetadataUsageId;
extern const uint32_t VuforiaBehaviour_LGLIKDOFOGL_m2840130230_MetadataUsageId;
extern Il2CppClass* Material_t193706927_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral895546098;
extern const uint32_t WireframeBehaviour_HHOAADHHKNO_m314389997_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3321166683;
extern const uint32_t WireframeBehaviour_FMOLONLHIMO_m2357556614_MetadataUsageId;
extern Il2CppClass* VuforiaManager_t2424874861_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3039339873;
extern Il2CppCodeGenString* _stringLiteral558587201;
extern const uint32_t WireframeBehaviour_HNELGBIJBGC_m530229867_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral649327173;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t WireframeBehaviour_MOBPDGJMAMF_m1905335942_MetadataUsageId;
extern const uint32_t WireframeBehaviour_AFJHDFBMPGJ_m2334202949_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t WireframeBehaviour_CJHGABIIAJJ_m193737312_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral867673157;
extern Il2CppCodeGenString* _stringLiteral1731360412;
extern const uint32_t WireframeBehaviour_KNLKJDAFJBK_m916083466_MetadataUsageId;
extern const uint32_t WireframeBehaviour_LAHEJEDHCIA_m837675580_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2487108583;
extern const uint32_t WireframeBehaviour_OPBMNDCALIE_m2100828882_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral835756156;
extern const uint32_t WireframeBehaviour_BGOEOCDKBOI_m2543365546_MetadataUsageId;
extern const uint32_t WireframeBehaviour_GKLEHAEBPEB_m1938231880_MetadataUsageId;
extern const uint32_t WireframeBehaviour_PLAONFPMGHH_m2211354212_MetadataUsageId;
extern const uint32_t WireframeBehaviour_DLINDIPJEML_m287485272_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2413011947;
extern Il2CppCodeGenString* _stringLiteral3233100706;
extern const uint32_t WireframeBehaviour_ACBENIKCPJC_m386668307_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral522332253;
extern const uint32_t WireframeBehaviour_EAIAFPCACPM_m409168420_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2195302232;
extern const uint32_t WireframeBehaviour_PNLHBJLFPKG_m560360038_MetadataUsageId;
extern const uint32_t WireframeBehaviour_CIOBLBHMDMK_m2356793020_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4179972495;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t WireframeBehaviour_DHNMGMAMJAG_m1216929649_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral329175675;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern const uint32_t WireframeBehaviour_JMILLCALHLH_m2570396652_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2061384369;
extern const uint32_t WireframeBehaviour_IPFDMCLKBJB_m2511271304_MetadataUsageId;
extern const uint32_t WireframeBehaviour_MBEIOPGELLC_m579852765_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral751502816;
extern Il2CppCodeGenString* _stringLiteral3175066848;
extern const uint32_t WireframeBehaviour_LEDEPMFIMKI_m926826121_MetadataUsageId;
extern const uint32_t WireframeBehaviour_GBPIMIJCPCF_m2599532084_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2986049441;
extern const uint32_t WireframeBehaviour_MCHFCAKCJCD_m3268304879_MetadataUsageId;
extern const uint32_t WireframeBehaviour_JKIAOBIEHNH_m3073468284_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral375467474;
extern const uint32_t WireframeBehaviour_BLBHIBAIIMD_m705723127_MetadataUsageId;
extern const uint32_t WireframeBehaviour_AJMHKAKKEIL_m2136154278_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3809976310;
extern Il2CppCodeGenString* _stringLiteral1366542454;
extern const uint32_t WireframeBehaviour_OHNLCPIILJM_m3112319761_MetadataUsageId;
extern const uint32_t WireframeBehaviour_DDCAHMLLAAB_m1121749877_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2380008103;
extern const uint32_t WireframeBehaviour_BOKHKCNLFPC_m3976576613_MetadataUsageId;
extern const uint32_t WireframeBehaviour_DKFHAOLKNBF_m1894235920_MetadataUsageId;
extern const uint32_t WireframeBehaviour_DLDNLDFCLMM_m954184467_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral372029397;
extern const uint32_t WireframeBehaviour_MJLPLGCFHBB_m3666484813_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2927347315;
extern const uint32_t WireframeBehaviour_BCLGOEGLBKH_m978875162_MetadataUsageId;
extern const uint32_t WireframeBehaviour_MFCOGODLKKJ_m2906414397_MetadataUsageId;
extern const uint32_t WireframeBehaviour_HKELKKCEAIE_m1376041157_MetadataUsageId;
extern const uint32_t WireframeBehaviour_EHMDNANGGEM_m210280865_MetadataUsageId;
extern const uint32_t WireframeBehaviour_KLIALGFAKNK_m1667798701_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3808982001;
extern const uint32_t WireframeBehaviour_OBGFMPJKOIB_m2894498228_MetadataUsageId;
extern const uint32_t WireframeBehaviour_BGCIEMPBIIP_m4164002641_MetadataUsageId;
extern const uint32_t WireframeBehaviour_BOKLOGCJPFB_m1169001991_MetadataUsageId;
extern const uint32_t WireframeBehaviour_NLLIENONEJD_m1786817916_MetadataUsageId;
extern const uint32_t WireframeBehaviour_IDFGHNEPELJ_m1372447246_MetadataUsageId;
extern const uint32_t WireframeBehaviour_MCDPPLDPLOL_m1986778025_MetadataUsageId;
extern const uint32_t WireframeBehaviour_IKBFKIDKCCA_m1995790978_MetadataUsageId;
extern const uint32_t WireframeBehaviour_LBHMPINEGAH_m3848319783_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1345542693;
extern const uint32_t WireframeBehaviour_MAPFCDOLNBM_m3752887547_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2416570879;
extern const uint32_t WireframeBehaviour_CDOOAMIPCJP_m552301795_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4058986920;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern const uint32_t WireframeBehaviour_ANIFNALLOCP_m2471365939_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4181646808;
extern Il2CppCodeGenString* _stringLiteral2569923475;
extern const uint32_t WireframeBehaviour_IOIHDPNODLK_m128082013_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral874589821;
extern Il2CppCodeGenString* _stringLiteral2856410928;
extern const uint32_t WireframeBehaviour_HLLPDIIOIGJ_m2791082579_MetadataUsageId;
extern const uint32_t WireframeBehaviour_ICLFGOGGNMN_m2071320641_MetadataUsageId;
extern const uint32_t WireframeBehaviour_PIMBLOOKDPB_m1459775259_MetadataUsageId;
extern const uint32_t WireframeBehaviour_IJJBFLLHKLD_m1181171198_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3618020756;
extern const uint32_t WireframeBehaviour_MOJBNANCALO_m4155708600_MetadataUsageId;
extern const uint32_t WireframeBehaviour_KIDKBHKKIIK_m1835780632_MetadataUsageId;
extern const uint32_t WireframeBehaviour_IHPDKDHEINH_m3974879750_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern Il2CppCodeGenString* _stringLiteral1414245037;
extern const uint32_t WireframeBehaviour_OGBDMLNFBGL_m87610686_MetadataUsageId;
extern const uint32_t WireframeBehaviour_CBDCOKCNBNC_m3810949800_MetadataUsageId;
extern const uint32_t WireframeBehaviour_BNAFDCPEFNE_m1149095952_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3031879739;
extern const uint32_t WireframeBehaviour_IPENEKDFNCF_m3351966577_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2104855582;
extern const uint32_t WireframeBehaviour_GOIJOLDFFGE_m1315580118_MetadataUsageId;
extern const uint32_t WireframeBehaviour_MAAHACCBNOE_m13291592_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1757896673;
extern Il2CppCodeGenString* _stringLiteral1316399271;
extern const uint32_t WireframeBehaviour_PEHFGOBMBMI_m1891058994_MetadataUsageId;
extern const uint32_t WireframeBehaviour_MIBDDKLKOMP_m3832221708_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral372029372;
extern Il2CppCodeGenString* _stringLiteral1455280663;
extern const uint32_t WireframeBehaviour_OGBGALHMKFI_m2376258885_MetadataUsageId;
extern const uint32_t WireframeBehaviour_KEHMDLMJJBB_m1303729286_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4057172486;
extern const uint32_t WireframeBehaviour_BIBNCPMGMHB_m472765229_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1789014798;
extern const uint32_t WireframeBehaviour_FOBPMDGMBMK_m1508704514_MetadataUsageId;
extern const uint32_t WireframeBehaviour_PNCDFKJONMB_m617663918_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3632910556;
extern const uint32_t WireframeBehaviour_OHPFLFLNHFL_m1198768427_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral601409167;
extern const uint32_t WireframeBehaviour_MCNNCBJBALK_m3669813097_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4035478884;
extern Il2CppCodeGenString* _stringLiteral232714862;
extern const uint32_t WireframeBehaviour_FDOIPFCFBKE_m3161848843_MetadataUsageId;
extern const uint32_t WireframeBehaviour_EFECEHEFGCF_m348725949_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2882933152;
extern Il2CppCodeGenString* _stringLiteral174201204;
extern const uint32_t WireframeBehaviour_JOOMFINDFBD_m3753447744_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1987072475;
extern const uint32_t WireframeBehaviour_AEDFBFAKILI_m2513635732_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral185337353;
extern const uint32_t WireframeBehaviour_MDAOFBNICCH_m3350208466_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1992869258;
extern Il2CppCodeGenString* _stringLiteral3833952734;
extern const uint32_t WireframeBehaviour_PLMIMGEHAKM_m4001410418_MetadataUsageId;
extern const uint32_t WireframeBehaviour_PODEIBLPELK_m2670230051_MetadataUsageId;
extern const uint32_t WireframeBehaviour_LHKDHMNGLFD_m2674157117_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1887801112;
extern Il2CppCodeGenString* _stringLiteral4062258119;
extern const uint32_t WireframeBehaviour_DHKENODLLMK_m3971767325_MetadataUsageId;
extern const uint32_t WireframeBehaviour_ABJGLKBAHFG_m2922284023_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern Il2CppCodeGenString* _stringLiteral557470275;
extern const uint32_t WireframeBehaviour_EEMGGGBOONL_m390827842_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3249194866;
extern const uint32_t WireframeBehaviour_JKEBBCIBHHB_m3149500086_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral679394624;
extern Il2CppCodeGenString* _stringLiteral4250993590;
extern const uint32_t WireframeBehaviour_HHLBKKPKDOP_m1793292044_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral291633112;
extern Il2CppCodeGenString* _stringLiteral2876496943;
extern const uint32_t WireframeBehaviour_OGAGMLFACEI_m2916878735_MetadataUsageId;
extern const uint32_t WireframeBehaviour_BGCIPMPDJKB_m3140479233_MetadataUsageId;
extern const uint32_t WireframeBehaviour_DLPHNOJBCBH_m2457302654_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1929630113;
extern const uint32_t WireframeBehaviour_FJAJILOIHPI_m1689994309_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2222441915;
extern const uint32_t WireframeBehaviour_FELEKHGGHOC_m2743220153_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral318163179;
extern const uint32_t WireframeBehaviour_PEDEOEDOMKO_m2382624078_MetadataUsageId;
extern const uint32_t WireframeBehaviour_OnDrawGizmos_m4179942854_MetadataUsageId;
extern const uint32_t WireframeBehaviour_NCPHAPEKFHG_m1540413555_MetadataUsageId;
extern const uint32_t WireframeBehaviour_MCPGGDHELHK_m3222742170_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral599011397;
extern const uint32_t WireframeBehaviour_OnRenderObject_m2411339956_MetadataUsageId;
extern const uint32_t WireframeBehaviour_EKIJOIGNMDM_m2418535638_MetadataUsageId;
extern const uint32_t WireframeBehaviour_CDILOGPFICJ_m4091281244_MetadataUsageId;
extern const uint32_t WireframeBehaviour_CLCJHFINNGM_m1955471551_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3629399031;
extern const uint32_t WireframeBehaviour_MIHOBDOEDEI_m2408107447_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2218050368;
extern const uint32_t WireframeBehaviour_IHEOPGCENKF_m1547907625_MetadataUsageId;
extern const uint32_t WireframeBehaviour_NJEFDLIBDIF_m3734793107_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3209823468;
extern const uint32_t WireframeBehaviour_HPFKCKLMBJO_m2777694159_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral372029393;
extern Il2CppCodeGenString* _stringLiteral57475196;
extern const uint32_t WireframeBehaviour_EIGICDKBKBM_m1238950756_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2609891608;
extern const uint32_t WireframeBehaviour_JHPIGJOGLJD_m2989805612_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3033297088;
extern const uint32_t WireframeBehaviour_DNFCLPOELCO_m2327793637_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1991887629;
extern const uint32_t WireframeBehaviour_DBBODICGOPJ_m1024667819_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3442848064;
extern const uint32_t WireframeBehaviour_GGNLCGBLPLI_m2769584601_MetadataUsageId;
extern const uint32_t WireframeBehaviour_BPGAMBMJIAB_m1244812478_MetadataUsageId;
extern const uint32_t WireframeBehaviour_Start_m2184757344_MetadataUsageId;
extern const uint32_t WireframeBehaviour_MMCKMIIFBEC_m236885055_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4001967317;
extern const uint32_t WireframeBehaviour_HKLBFPAPKHD_m3427256221_MetadataUsageId;
extern const uint32_t WireframeBehaviour_GLEJCIBNNLL_m2669488302_MetadataUsageId;
extern const uint32_t WireframeBehaviour_NLNBMNPJINJ_m2049441042_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral115638372;
extern const uint32_t WireframeBehaviour_GJEHNMNJBLP_m49031537_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3495040156;
extern const uint32_t WireframeBehaviour_LJNKGAMLGFD_m4131286409_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3598345300;
extern const uint32_t WireframeBehaviour_GEGHFBJMALM_m3688575262_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var;
extern const uint32_t WireframeTrackableEventHandler_KKIAJHJCHMJ_m2198785808_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var;
extern const uint32_t WireframeTrackableEventHandler_LCPOKLKJFCE_m2446626476_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral253239906;
extern Il2CppCodeGenString* _stringLiteral1373918990;
extern const uint32_t WireframeTrackableEventHandler_IGABBLIOMHE_m1181148253_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern const uint32_t WireframeTrackableEventHandler_LOOMIIFHIMC_m782917760_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_JIBJGCDGPDI_m407693029_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_FCBAAPGIOIM_m4071878402_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3708513882;
extern const uint32_t WireframeTrackableEventHandler_LAEONDHFCMH_m833139037_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral787298419;
extern const uint32_t WireframeTrackableEventHandler_NIAFCJOMNCL_m4087719520_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral472785647;
extern const uint32_t WireframeTrackableEventHandler_FDJDHABNNCC_m671499079_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3364679155;
extern Il2CppCodeGenString* _stringLiteral102231524;
extern const uint32_t WireframeTrackableEventHandler_KNIFKOLAFBI_m1696818270_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_EOHMBHGJMNJ_m3945602073_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1417766541;
extern Il2CppCodeGenString* _stringLiteral816986750;
extern const uint32_t WireframeTrackableEventHandler_ACDOGKDFFIF_m2429399710_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_NLNBMNPJINJ_m1228672266_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_GKLEHAEBPEB_m2313315472_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1040926105;
extern Il2CppCodeGenString* _stringLiteral759218142;
extern const uint32_t WireframeTrackableEventHandler_ICMBCINNDGM_m110706081_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1493872207;
extern Il2CppCodeGenString* _stringLiteral425611190;
extern const uint32_t WireframeTrackableEventHandler_EDBCGPGHNBF_m1677182104_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2993583680;
extern const uint32_t WireframeTrackableEventHandler_NPLBCBNBIKE_m3569066392_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2375273008;
extern const uint32_t WireframeTrackableEventHandler_GCPMNKOIAGM_m2269296581_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1016630757;
extern const uint32_t WireframeTrackableEventHandler_FBKJJBFIGCM_m2697467891_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_FJPCAPDOEBC_m686633061_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_CNCABLEEGBL_m2114310396_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_DMDEOJCCMBE_m2103699731_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4234644335;
extern const uint32_t WireframeTrackableEventHandler_BIGOOPNEOPH_m1142678134_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern Il2CppCodeGenString* _stringLiteral737604599;
extern const uint32_t WireframeTrackableEventHandler_JELKBFLCAIK_m1170380144_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_KFFIKNBENJP_m3569997932_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral719456662;
extern Il2CppCodeGenString* _stringLiteral3033396940;
extern const uint32_t WireframeTrackableEventHandler_NOEBLEKPBOD_m2199512617_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_CGFACDCECPO_m674731180_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2440250040;
extern Il2CppCodeGenString* _stringLiteral1574707977;
extern const uint32_t WireframeTrackableEventHandler_OOADEGMJECE_m1566597351_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_NNJIDIACKEA_m488764539_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral212532649;
extern Il2CppCodeGenString* _stringLiteral3824521097;
extern const uint32_t WireframeTrackableEventHandler_DHDMLGADOHO_m1641672251_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2610385663;
extern const uint32_t WireframeTrackableEventHandler_CGOENOELDFO_m595842679_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3812735643;
extern const uint32_t WireframeTrackableEventHandler_FECCALMBHCB_m4181103262_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2041851080;
extern Il2CppCodeGenString* _stringLiteral403189246;
extern const uint32_t WireframeTrackableEventHandler_ILEELGACAOI_m4234151595_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_CJCJCCJIAJJ_m2331287578_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_IMAGALEFLGE_m184054848_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral819182375;
extern const uint32_t WireframeTrackableEventHandler_EBCPBMGNOOF_m4007887116_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1296085355;
extern const uint32_t WireframeTrackableEventHandler_PAKPNBLKGCB_m2253804083_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_IAMEHGDFBEO_m118164025_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_Start_m1475947192_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1093630588;
extern Il2CppCodeGenString* _stringLiteral2537971763;
extern const uint32_t WireframeTrackableEventHandler_FBDKKJLHIOD_m482125408_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_HEJKOLHLCNK_m1956377937_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral808027542;
extern const uint32_t WireframeTrackableEventHandler_COHNILNFJGH_m3628274274_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1409823797;
extern const uint32_t WireframeTrackableEventHandler_CLLGALDACLA_m1173486930_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1588531317;
extern const uint32_t WireframeTrackableEventHandler_CKOMKHHAGFK_m3147004680_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4252499464;
extern const uint32_t WireframeTrackableEventHandler_HDGLLAFAKEP_m3111200887_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2794766820;
extern const uint32_t WireframeTrackableEventHandler_ECGBLMKNAFH_m1968249532_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_AHNHMLGGFNG_m3712762473_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral949024030;
extern const uint32_t WireframeTrackableEventHandler_PJFOOOOGBDE_m1297153882_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1446756774;
extern Il2CppCodeGenString* _stringLiteral2812229188;
extern const uint32_t WireframeTrackableEventHandler_EDEOPGENFAJ_m2773645888_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_GOIJOLDFFGE_m63814910_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral2088361527;
extern const uint32_t WireframeTrackableEventHandler_DHKACIHBPPA_m139711023_MetadataUsageId;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t YesNoChoice_GKDAOBPHLNB_m3733691834_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3033402446;
extern Il2CppCodeGenString* _stringLiteral2267280474;
extern const uint32_t YesNoChoice_JFGPFABIMOB_m3795407005_MetadataUsageId;
extern const uint32_t YesNoChoice_IDBAOOPDLMH_m127698629_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral95490550;
extern const uint32_t YesNoChoice_FGHJDGNPAFI_m1512755462_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3114425798;
extern Il2CppCodeGenString* _stringLiteral2654155903;
extern const uint32_t YesNoChoice_HNDMPFCFFFE_m1628485279_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral372029399;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern const uint32_t YesNoChoice_COFPAJDOLHF_m1023226326_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral287061489;
extern Il2CppCodeGenString* _stringLiteral816988926;
extern const uint32_t YesNoChoice_PEJFEPFGJEN_m4037529232_MetadataUsageId;
extern const uint32_t YesNoChoice_GGKHNMGHKCC_m2035332920_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3993410431;
extern const uint32_t YesNoChoice_LKJCBHOHJEB_m1185547858_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3248387252;
extern const uint32_t YesNoChoice_CKHMOPOBELG_m3931102139_MetadataUsageId;
extern const uint32_t YesNoChoice_ELGLFIGOHOF_m2370230712_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2339396354;
extern const uint32_t YesNoChoice_PNDEANCEPFB_m2783877124_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3021629903;
extern Il2CppCodeGenString* _stringLiteral1496915101;
extern const uint32_t YesNoChoice_OnGUI_m1115652626_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral762922235;
extern Il2CppCodeGenString* _stringLiteral1982269700;
extern const uint32_t YesNoChoice_GKLFAFJANON_m2681160699_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t YesNoChoice_OPFOPPECKGB_m1131751710_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2879021919;
extern Il2CppCodeGenString* _stringLiteral3722207907;
extern const uint32_t YesNoChoice_MILJNKHOLLF_m538753334_MetadataUsageId;
extern const uint32_t YesNoChoice_HOCEJAMGLKH_m1141190081_MetadataUsageId;

// UnityEngine.Camera[]
struct CameraU5BU5D_t3079764780  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Camera_t189460977 * m_Items[1];

public:
	inline Camera_t189460977 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Camera_t189460977 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Camera_t189460977 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Camera_t189460977 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Camera_t189460977 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Camera_t189460977 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector3_t2243707580  m_Items[1];

public:
	inline Vector3_t2243707580  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2243707580  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t3030399641  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Renderer_t257310565 * m_Items[1];

public:
	inline Renderer_t257310565 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t257310565 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t257310565 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Renderer_t257310565 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t257310565 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t257310565 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Collider_t3497673348 * m_Items[1];

public:
	inline Collider_t3497673348 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t3497673348 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t3497673348 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Collider_t3497673348 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t3497673348 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t3497673348 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t2935582494  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WireframeBehaviour_t2494532455 * m_Items[1];

public:
	inline WireframeBehaviour_t2494532455 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WireframeBehaviour_t2494532455 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WireframeBehaviour_t2494532455 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline WireframeBehaviour_t2494532455 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WireframeBehaviour_t2494532455 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WireframeBehaviour_t2494532455 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m758847274_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInChildren_TisIl2CppObject_m1163966231_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInChildren_TisIl2CppObject_m4164830438_gshared (Component_t3819376471 * __this, bool p0, const MethodInfo* method);

// System.Void Vuforia.NullUnityPlayer::.ctor()
extern "C"  void NullUnityPlayer__ctor_m483624113 (NullUnityPlayer_t754446093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m3989224144 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OBLIGPGBOJF::.ctor()
extern "C"  void OBLIGPGBOJF__ctor_m2577981860 (OBLIGPGBOJF_t3834424063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MHAOKAIGBBF::.ctor()
extern "C"  void MHAOKAIGBBF__ctor_m2208160756 (MHAOKAIGBBF_t1137417231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaRuntimeUtilities::IsPlayMode()
extern "C"  bool VuforiaRuntimeUtilities_IsPlayMode_m2939358997 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::.ctor()
extern "C"  void PlayModeUnityPlayer__ctor_m2126346857 (PlayModeUnityPlayer_t918240325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaRuntimeUtilities::IsWSARuntime()
extern "C"  bool VuforiaRuntimeUtilities_IsWSARuntime_m3848252715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KDDDFNIEPOB::.ctor()
extern "C"  void KDDDFNIEPOB__ctor_m2457485235 (KDDDFNIEPOB_t809780732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaAbstractBehaviour::SetUnityPlayerImplementation(Vuforia.IUnityPlayer)
extern "C"  void VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552 (VuforiaAbstractBehaviour_t3319870759 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ComponentFactoryStarterBehaviour>()
#define GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(__this, method) ((  ComponentFactoryStarterBehaviour_t3249343815 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// System.Void Vuforia.VuforiaAbstractBehaviour::Awake()
extern "C"  void VuforiaAbstractBehaviour_Awake_m232471254 (VuforiaAbstractBehaviour_t3319870759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<Vuforia.VuforiaBehaviour>()
#define Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(__this /* static, unused */, method) ((  VuforiaBehaviour_t359035403 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m758847274_gshared)(__this /* static, unused */, method)
// System.Void Vuforia.VuforiaAbstractBehaviour::.ctor()
extern "C"  void VuforiaAbstractBehaviour__ctor_m3900338923 (VuforiaAbstractBehaviour_t3319870759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuMarkAbstractBehaviour::.ctor()
extern "C"  void VuMarkAbstractBehaviour__ctor_m326197713 (VuMarkAbstractBehaviour_t1830666997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::.ctor()
extern "C"  void WebCamAbstractBehaviour__ctor_m535376024 (WebCamAbstractBehaviour_t4104806356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern "C"  void Material__ctor_m1440882780 (Material_t193706927 * __this, Material_t193706927 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m2503577968 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaManager Vuforia.VuforiaManager::get_Instance()
extern "C"  VuforiaManager_t2424874861 * VuforiaManager_get_Instance_m425433003 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
#define GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(__this, method) ((  CameraU5BU5D_t3079764780* (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m1163966231_gshared)(__this, method)
// UnityEngine.Camera UnityEngine.Camera::get_current()
extern "C"  Camera_t189460977 * Camera_get_current_m2639890517 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, method) ((  MeshFilter_t3026937449 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2856731593 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C"  Mesh_t1356156583 * MeshFilter_get_sharedMesh_m1310789932 (MeshFilter_t3026937449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C"  Vector3U5BU5D_t1172311765* Mesh_get_vertices_m626989480 (Mesh_t1356156583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine.Mesh::get_triangles()
extern "C"  Int32U5BU5D_t3030399641* Mesh_get_triangles_m3988715512 (Mesh_t1356156583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::PushMatrix()
extern "C"  void GL_PushMatrix_m1979053131 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C"  Matrix4x4_t2933234003  Transform_get_localToWorldMatrix_m2868579006 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::MultMatrix(UnityEngine.Matrix4x4)
extern "C"  void GL_MultMatrix_m767401141 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::SetPass(System.Int32)
extern "C"  bool Material_SetPass_m2448940266 (Material_t193706927 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C"  void Material_SetColor_m650857509 (Material_t193706927 * __this, String_t* p0, Color_t2020392075  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Begin(System.Int32)
extern "C"  void GL_Begin_m3874173032 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Vertex(UnityEngine.Vector3)
extern "C"  void GL_Vertex_m4110027235 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::End()
extern "C"  void GL_End_m2374230645 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::PopMatrix()
extern "C"  void GL_PopMatrix_m856033754 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m4079055610 (Behaviour_t955675639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m1033555130 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern "C"  Vector3_t2243707580  Transform_get_lossyScale_m1638545862 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Matrix4x4_t2933234003  Matrix4x4_TRS_m1913765359 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Quaternion_t4030073918  p1, Vector3_t2243707580  p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_matrix(UnityEngine.Matrix4x4)
extern "C"  void Gizmos_set_matrix_m1590313986 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C"  void Gizmos_set_color_m494992840 (Il2CppObject * __this /* static, unused */, Color_t2020392075  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawLine_m1315654064 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C"  Color_t2020392075  Color_get_green_m2671273823 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::NIAFCJOMNCL()
extern "C"  void WireframeTrackableEventHandler_NIAFCJOMNCL_m4087719520 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::HDGLLAFAKEP()
extern "C"  void WireframeTrackableEventHandler_HDGLLAFAKEP_m3111200887 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
#define Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, method) ((  TrackableBehaviour_t1779888572 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void Vuforia.TrackableBehaviour::RegisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
extern "C"  void TrackableBehaviour_RegisterTrackableEventHandler_m1156666476 (TrackableBehaviour_t1779888572 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
#define Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, p0, method) ((  RendererU5BU5D_t2810717544* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m4164830438_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
#define Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, p0, method) ((  ColliderU5BU5D_t462843629* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m4164830438_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
#define Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, p0, method) ((  WireframeBehaviourU5BU5D_t2935582494* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m4164830438_gshared)(__this, p0, method)
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m142717579 (Renderer_t257310565 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
extern "C"  void Collider_set_enabled_m3489100454 (Collider_t3497673348 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m1796096907 (Behaviour_t955675639 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.TrackableBehaviour::get_TrackableName()
extern "C"  String_t* TrackableBehaviour_get_TrackableName_m3173853042 (TrackableBehaviour_t1779888572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::GCPMNKOIAGM()
extern "C"  void WireframeTrackableEventHandler_GCPMNKOIAGM_m2269296581 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::KFFIKNBENJP()
extern "C"  void WireframeTrackableEventHandler_KFFIKNBENJP_m3569997932 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::LOOMIIFHIMC()
extern "C"  void WireframeTrackableEventHandler_LOOMIIFHIMC_m782917760 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::DHKACIHBPPA()
extern "C"  void WireframeTrackableEventHandler_DHKACIHBPPA_m139711023 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::JELKBFLCAIK()
extern "C"  void WireframeTrackableEventHandler_JELKBFLCAIK_m1170380144 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::KNIFKOLAFBI()
extern "C"  void WireframeTrackableEventHandler_KNIFKOLAFBI_m1696818270 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::CGOENOELDFO()
extern "C"  void WireframeTrackableEventHandler_CGOENOELDFO_m595842679 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::FBKJJBFIGCM()
extern "C"  void WireframeTrackableEventHandler_FBKJJBFIGCM_m2697467891 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::ECGBLMKNAFH()
extern "C"  void WireframeTrackableEventHandler_ECGBLMKNAFH_m1968249532 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::PAKPNBLKGCB()
extern "C"  void WireframeTrackableEventHandler_PAKPNBLKGCB_m2253804083 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::EBCPBMGNOOF()
extern "C"  void WireframeTrackableEventHandler_EBCPBMGNOOF_m4007887116 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::CKOMKHHAGFK()
extern "C"  void WireframeTrackableEventHandler_CKOMKHHAGFK_m3147004680 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::ACDOGKDFFIF()
extern "C"  void WireframeTrackableEventHandler_ACDOGKDFFIF_m2429399710 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::CLLGALDACLA()
extern "C"  void WireframeTrackableEventHandler_CLLGALDACLA_m1173486930 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::LAEONDHFCMH()
extern "C"  void WireframeTrackableEventHandler_LAEONDHFCMH_m833139037 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::IGABBLIOMHE()
extern "C"  void WireframeTrackableEventHandler_IGABBLIOMHE_m1181148253 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::ICMBCINNDGM()
extern "C"  void WireframeTrackableEventHandler_ICMBCINNDGM_m110706081 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::NNJIDIACKEA()
extern "C"  void WireframeTrackableEventHandler_NNJIDIACKEA_m488764539 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::BIGOOPNEOPH()
extern "C"  void WireframeTrackableEventHandler_BIGOOPNEOPH_m1142678134 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::ILEELGACAOI()
extern "C"  void WireframeTrackableEventHandler_ILEELGACAOI_m4234151595 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::EDBCGPGHNBF()
extern "C"  void WireframeTrackableEventHandler_EDBCGPGHNBF_m1677182104 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::FDJDHABNNCC()
extern "C"  void WireframeTrackableEventHandler_FDJDHABNNCC_m671499079 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::PJFOOOOGBDE()
extern "C"  void WireframeTrackableEventHandler_PJFOOOOGBDE_m1297153882 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
extern "C"  void WordAbstractBehaviour__ctor_m1415816009 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m41137238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1051800773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
extern "C"  GUISkin_t1436893342 * GUI_get_skin_m2309570990 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_button()
extern "C"  GUIStyle_t1799908754 * GUISkin_get_button_m797402546 (GUISkin_t1436893342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
extern "C"  void GUIStyle_set_fontSize_m4015341543 (GUIStyle_t1799908754 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m1220545469 (Rect_t3681755626 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::BeginGroup(UnityEngine.Rect)
extern "C"  void GUI_BeginGroup_m2572373001 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String)
extern "C"  bool GUI_Button_m3054448581 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NarrativeCondition::set_isDone(System.Boolean)
extern "C"  void NarrativeCondition_set_isDone_m408944312 (NarrativeCondition_t4123859457 * __this, bool ___PJMCJKFMGJI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2887581199 (GameObject_t1756533147 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NarrativeCondition::ENHOMCGMINP(System.Boolean)
extern "C"  void NarrativeCondition_ENHOMCGMINP_m33130136 (NarrativeCondition_t4123859457 * __this, bool ___PJMCJKFMGJI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::EndGroup()
extern "C"  void GUI_EndGroup_m1672170830 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NarrativeCondition::BMJPDAHKIPO(System.Boolean)
extern "C"  void NarrativeCondition_BMJPDAHKIPO_m966518882 (NarrativeCondition_t4123859457 * __this, bool ___PJMCJKFMGJI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NarrativeCondition::LOJLMGCOMNE(System.Boolean)
extern "C"  void NarrativeCondition_LOJLMGCOMNE_m3469340218 (NarrativeCondition_t4123859457 * __this, bool ___PJMCJKFMGJI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NarrativeCondition::KPPPMJJLBNM(System.Boolean)
extern "C"  void NarrativeCondition_KPPPMJJLBNM_m2300095498 (NarrativeCondition_t4123859457 * __this, bool ___PJMCJKFMGJI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NarrativeCondition::LLPLKLIEGNH(System.Boolean)
extern "C"  void NarrativeCondition_LLPLKLIEGNH_m763528405 (NarrativeCondition_t4123859457 * __this, bool ___PJMCJKFMGJI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NarrativeCondition::GMKLCEALNKB(System.Boolean)
extern "C"  void NarrativeCondition_GMKLCEALNKB_m2255547382 (NarrativeCondition_t4123859457 * __this, bool ___PJMCJKFMGJI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NarrativeCondition::HIDFKJJGPLN(System.Boolean)
extern "C"  void NarrativeCondition_HIDFKJJGPLN_m739605056 (NarrativeCondition_t4123859457 * __this, bool ___PJMCJKFMGJI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NarrativeCondition::FLHPIGCGOND(System.Boolean)
extern "C"  void NarrativeCondition_FLHPIGCGOND_m691917992 (NarrativeCondition_t4123859457 * __this, bool ___PJMCJKFMGJI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NarrativeCondition::MKJPKLENHAG(System.Boolean)
extern "C"  void NarrativeCondition_MKJPKLENHAG_m3460246573 (NarrativeCondition_t4123859457 * __this, bool ___PJMCJKFMGJI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NarrativeCondition::.ctor()
extern "C"  void NarrativeCondition__ctor_m193474214 (NarrativeCondition_t4123859457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NarrativeCondition::POGHNANKPIB(System.Boolean)
extern "C"  void NarrativeCondition_POGHNANKPIB_m1855205988 (NarrativeCondition_t4123859457 * __this, bool ___PJMCJKFMGJI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NarrativeCondition::INPBCMCGFAH(System.Boolean)
extern "C"  void NarrativeCondition_INPBCMCGFAH_m2844416023 (NarrativeCondition_t4123859457 * __this, bool ___PJMCJKFMGJI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vuforia.VuforiaBehaviour::PNCGFCJJGNL()
extern "C"  void VuforiaBehaviour_PNCGFCJJGNL_m2329860686 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_PNCGFCJJGNL_m2329860686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)24)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::EHGKCBDLMJO()
extern "C"  void VuforiaBehaviour_EHGKCBDLMJO_m3000324330 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_EHGKCBDLMJO_m3000324330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-33)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)3))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::DGENOCOMEKB()
extern "C"  VuforiaBehaviour_t359035403 * VuforiaBehaviour_DGENOCOMEKB_m1281060286 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_DGENOCOMEKB_m1281060286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_0 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->set_MAKKKFJMJLI_43(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_3 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		return L_3;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::get_Instance()
extern "C"  VuforiaBehaviour_t359035403 * VuforiaBehaviour_get_Instance_m790681192 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_get_Instance_m790681192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_0 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->set_MAKKKFJMJLI_43(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_3 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		return L_3;
	}
}
// System.Void Vuforia.VuforiaBehaviour::CFELMCGNBHC()
extern "C"  void VuforiaBehaviour_CFELMCGNBHC_m1793512764 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_CFELMCGNBHC_m1793512764_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)103)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)5))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::PDHCNDEDBPL()
extern "C"  VuforiaBehaviour_t359035403 * VuforiaBehaviour_PDHCNDEDBPL_m1877488300 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_PDHCNDEDBPL_m1877488300_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_0 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->set_MAKKKFJMJLI_43(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_3 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		return L_3;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::BBKHAACJHGD()
extern "C"  VuforiaBehaviour_t359035403 * VuforiaBehaviour_BBKHAACJHGD_m5466941 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_BBKHAACJHGD_m5466941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_0 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->set_MAKKKFJMJLI_43(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_3 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		return L_3;
	}
}
// System.Void Vuforia.VuforiaBehaviour::KIFLLMPNMNC()
extern "C"  void VuforiaBehaviour_KIFLLMPNMNC_m1123554243 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_KIFLLMPNMNC_m1123554243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)6))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.cctor()
extern "C"  void VuforiaBehaviour__cctor_m1552098219 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::CGPLGMCGBCE()
extern "C"  void VuforiaBehaviour_CGPLGMCGBCE_m1097455718 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_CGPLGMCGBCE_m1097455718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-31)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::FEKBAIDKMBN()
extern "C"  void VuforiaBehaviour_FEKBAIDKMBN_m3164767870 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_FEKBAIDKMBN_m3164767870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)0))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::JKHMMNCFHJI()
extern "C"  void VuforiaBehaviour_JKHMMNCFHJI_m2490924137 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_JKHMMNCFHJI_m2490924137_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)3)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)7))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::EDKAAPKGGOD()
extern "C"  void VuforiaBehaviour_EDKAAPKGGOD_m3253621180 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_EDKAAPKGGOD_m3253621180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-40)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)7))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::NKHDJMBCBEM()
extern "C"  void VuforiaBehaviour_NKHDJMBCBEM_m694517645 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_NKHDJMBCBEM_m694517645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)119)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::MJBNINKEHFJ()
extern "C"  void VuforiaBehaviour_MJBNINKEHFJ_m1346401912 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_MJBNINKEHFJ_m1346401912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-83)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)7))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::Awake()
extern "C"  void VuforiaBehaviour_Awake_m3772880569 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_Awake_m3772880569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::CBJJNJENPCI()
extern "C"  void VuforiaBehaviour_CBJJNJENPCI_m1261556742 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_CBJJNJENPCI_m1261556742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)0))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::MKAJLMEAOAM()
extern "C"  void VuforiaBehaviour_MKAJLMEAOAM_m1435281255 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_MKAJLMEAOAM_m1435281255_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-126)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)6))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::AKFJHJANLGB()
extern "C"  VuforiaBehaviour_t359035403 * VuforiaBehaviour_AKFJHJANLGB_m3356610398 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_AKFJHJANLGB_m3356610398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_0 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->set_MAKKKFJMJLI_43(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_3 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		return L_3;
	}
}
// System.Void Vuforia.VuforiaBehaviour::AKJIJGJNFBN()
extern "C"  void VuforiaBehaviour_AKJIJGJNFBN_m3992524508 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_AKJIJGJNFBN_m3992524508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)111)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::FNBNJDGLFEF()
extern "C"  VuforiaBehaviour_t359035403 * VuforiaBehaviour_FNBNJDGLFEF_m3441889726 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_FNBNJDGLFEF_m3441889726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_0 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->set_MAKKKFJMJLI_43(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_3 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		return L_3;
	}
}
// System.Void Vuforia.VuforiaBehaviour::GGIMPHGMBHF()
extern "C"  void VuforiaBehaviour_GGIMPHGMBHF_m864314164 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_GGIMPHGMBHF_m864314164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-45)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)4))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.ctor()
extern "C"  void VuforiaBehaviour__ctor_m3143982076 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	{
		VuforiaAbstractBehaviour__ctor_m3900338923(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::HEGGCOKFJJK()
extern "C"  VuforiaBehaviour_t359035403 * VuforiaBehaviour_HEGGCOKFJJK_m2412924347 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_HEGGCOKFJJK_m2412924347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_0 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->set_MAKKKFJMJLI_43(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_3 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		return L_3;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::EJIGBOCHMJG()
extern "C"  VuforiaBehaviour_t359035403 * VuforiaBehaviour_EJIGBOCHMJG_m2425568383 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_EJIGBOCHMJG_m2425568383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_0 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->set_MAKKKFJMJLI_43(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_3 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		return L_3;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::EFAGDFOEOCA()
extern "C"  VuforiaBehaviour_t359035403 * VuforiaBehaviour_EFAGDFOEOCA_m3328127010 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_EFAGDFOEOCA_m3328127010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_0 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->set_MAKKKFJMJLI_43(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_3 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		return L_3;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::ELALFJNKHMJ()
extern "C"  VuforiaBehaviour_t359035403 * VuforiaBehaviour_ELALFJNKHMJ_m286753846 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_ELALFJNKHMJ_m286753846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_0 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->set_MAKKKFJMJLI_43(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_3 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		return L_3;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::OPFIGFLCCBP()
extern "C"  VuforiaBehaviour_t359035403 * VuforiaBehaviour_OPFIGFLCCBP_m1212912359 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_OPFIGFLCCBP_m1212912359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_0 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->set_MAKKKFJMJLI_43(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_3 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_MAKKKFJMJLI_43();
		return L_3;
	}
}
// System.Void Vuforia.VuforiaBehaviour::LGLIKDOFOGL()
extern "C"  void VuforiaBehaviour_LGLIKDOFOGL_m2840130230 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_LGLIKDOFOGL_m2840130230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-60)))))
		{
			goto IL_001d;
		}
	}
	{
		OBLIGPGBOJF_t3834424063 * L_2 = (OBLIGPGBOJF_t3834424063 *)il2cpp_codegen_object_new(OBLIGPGBOJF_t3834424063_il2cpp_TypeInfo_var);
		OBLIGPGBOJF__ctor_m2577981860(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)7))))
		{
			goto IL_0033;
		}
	}
	{
		MHAOKAIGBBF_t1137417231 * L_4 = (MHAOKAIGBBF_t1137417231 *)il2cpp_codegen_object_new(MHAOKAIGBBF_t1137417231_il2cpp_TypeInfo_var);
		MHAOKAIGBBF__ctor_m2208160756(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		KDDDFNIEPOB_t809780732 * L_8 = (KDDDFNIEPOB_t809780732 *)il2cpp_codegen_object_new(KDDDFNIEPOB_t809780732_il2cpp_TypeInfo_var);
		KDDDFNIEPOB__ctor_m2457485235(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		VuforiaAbstractBehaviour_SetUnityPlayerImplementation_m4082041552(__this, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_10, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuMarkBehaviour::.ctor()
extern "C"  void VuMarkBehaviour__ctor_m1415860126 (VuMarkBehaviour_t2060629989 * __this, const MethodInfo* method)
{
	{
		VuMarkAbstractBehaviour__ctor_m326197713(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WebCamBehaviour::.ctor()
extern "C"  void WebCamBehaviour__ctor_m2114979319 (WebCamBehaviour_t407765638 * __this, const MethodInfo* method)
{
	{
		WebCamAbstractBehaviour__ctor_m535376024(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::HHOAADHHKNO()
extern "C"  void WireframeBehaviour_HHOAADHHKNO_m314389997 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_HHOAADHHKNO_m314389997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral895546098, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::FMOLONLHIMO()
extern "C"  void WireframeBehaviour_FMOLONLHIMO_m2357556614 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_FMOLONLHIMO_m2357556614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3321166683, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::HNELGBIJBGC()
extern "C"  void WireframeBehaviour_HNELGBIJBGC_m530229867 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_HNELGBIJBGC_m530229867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3039339873, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral558587201, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)0));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)5));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MOBPDGJMAMF()
extern "C"  void WireframeBehaviour_MOBPDGJMAMF_m1905335942 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MOBPDGJMAMF_m1905335942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral649327173, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral372029425, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)4));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)4));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::AFJHDFBMPGJ()
extern "C"  void WireframeBehaviour_AFJHDFBMPGJ_m2334202949 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_AFJHDFBMPGJ_m2334202949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)4));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)8));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::CJHGABIIAJJ()
extern "C"  void WireframeBehaviour_CJHGABIIAJJ_m193737312 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_CJHGABIIAJJ_m193737312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1803325615, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::.ctor()
extern "C"  void WireframeBehaviour__ctor_m420914080 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	{
		__this->set_ShowLines_3((bool)1);
		Color_t2020392075  L_0 = Color_get_green_m2671273823(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_LineColor_4(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::KNLKJDAFJBK()
extern "C"  void WireframeBehaviour_KNLKJDAFJBK_m916083466 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_KNLKJDAFJBK_m916083466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral867673157, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral1731360412, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)0));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)0));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::LAHEJEDHCIA()
extern "C"  void WireframeBehaviour_LAHEJEDHCIA_m837675580 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_LAHEJEDHCIA_m837675580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)1));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OPBMNDCALIE()
extern "C"  void WireframeBehaviour_OPBMNDCALIE_m2100828882 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OPBMNDCALIE_m2100828882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2487108583, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::BGOEOCDKBOI()
extern "C"  void WireframeBehaviour_BGOEOCDKBOI_m2543365546 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_BGOEOCDKBOI_m2543365546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral835756156, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::GKLEHAEBPEB()
extern "C"  void WireframeBehaviour_GKLEHAEBPEB_m1938231880 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_GKLEHAEBPEB_m1938231880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3039339873, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::PLAONFPMGHH()
extern "C"  void WireframeBehaviour_PLAONFPMGHH_m2211354212 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_PLAONFPMGHH_m2211354212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)3));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)5));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::DLINDIPJEML()
extern "C"  void WireframeBehaviour_DLINDIPJEML_m287485272 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_DLINDIPJEML_m287485272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)8));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)7));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::ACBENIKCPJC()
extern "C"  void WireframeBehaviour_ACBENIKCPJC_m386668307 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_ACBENIKCPJC_m386668307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2413011947, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral3233100706, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)5));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)8));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::EAIAFPCACPM()
extern "C"  void WireframeBehaviour_EAIAFPCACPM_m409168420 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_EAIAFPCACPM_m409168420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral522332253, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::PNLHBJLFPKG()
extern "C"  void WireframeBehaviour_PNLHBJLFPKG_m560360038 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_PNLHBJLFPKG_m560360038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1803325615, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral2195302232, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)1));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)5));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::CIOBLBHMDMK()
extern "C"  void WireframeBehaviour_CIOBLBHMDMK_m2356793020 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_CIOBLBHMDMK_m2356793020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)4));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)4));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::DHNMGMAMJAG()
extern "C"  void WireframeBehaviour_DHNMGMAMJAG_m1216929649 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_DHNMGMAMJAG_m1216929649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral4179972495, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral372029352, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)5));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)3));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::JMILLCALHLH()
extern "C"  void WireframeBehaviour_JMILLCALHLH_m2570396652 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_JMILLCALHLH_m2570396652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral329175675, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral811305474, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)5));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)4));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::IPFDMCLKBJB()
extern "C"  void WireframeBehaviour_IPFDMCLKBJB_m2511271304 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_IPFDMCLKBJB_m2511271304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2061384369, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MBEIOPGELLC()
extern "C"  void WireframeBehaviour_MBEIOPGELLC_m579852765 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MBEIOPGELLC_m579852765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)4));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)7));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::LEDEPMFIMKI()
extern "C"  void WireframeBehaviour_LEDEPMFIMKI_m926826121 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_LEDEPMFIMKI_m926826121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral751502816, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral3175066848, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)7));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)1));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::GBPIMIJCPCF()
extern "C"  void WireframeBehaviour_GBPIMIJCPCF_m2599532084 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_GBPIMIJCPCF_m2599532084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)1));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)5));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MCHFCAKCJCD()
extern "C"  void WireframeBehaviour_MCHFCAKCJCD_m3268304879 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MCHFCAKCJCD_m3268304879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2986049441, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::JKIAOBIEHNH()
extern "C"  void WireframeBehaviour_JKIAOBIEHNH_m3073468284 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_JKIAOBIEHNH_m3073468284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)4));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)5));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::BLBHIBAIIMD()
extern "C"  void WireframeBehaviour_BLBHIBAIIMD_m705723127 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_BLBHIBAIIMD_m705723127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral375467474, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::AJMHKAKKEIL()
extern "C"  void WireframeBehaviour_AJMHKAKKEIL_m2136154278 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_AJMHKAKKEIL_m2136154278_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)3));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)6));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OHNLCPIILJM()
extern "C"  void WireframeBehaviour_OHNLCPIILJM_m3112319761 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OHNLCPIILJM_m3112319761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3809976310, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral1366542454, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)7));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)4));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::DDCAHMLLAAB()
extern "C"  void WireframeBehaviour_DDCAHMLLAAB_m1121749877 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_DDCAHMLLAAB_m1121749877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)7));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)7));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::BOKHKCNLFPC()
extern "C"  void WireframeBehaviour_BOKHKCNLFPC_m3976576613 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_BOKHKCNLFPC_m3976576613_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2380008103, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::DKFHAOLKNBF()
extern "C"  void WireframeBehaviour_DKFHAOLKNBF_m1894235920 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_DKFHAOLKNBF_m1894235920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::DLDNLDFCLMM()
extern "C"  void WireframeBehaviour_DLDNLDFCLMM_m954184467 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_DLDNLDFCLMM_m954184467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)1));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)0));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MJLPLGCFHBB()
extern "C"  void WireframeBehaviour_MJLPLGCFHBB_m3666484813 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MJLPLGCFHBB_m3666484813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral372029397, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral372029397, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)7));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)1));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::BCLGOEGLBKH()
extern "C"  void WireframeBehaviour_BCLGOEGLBKH_m978875162 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_BCLGOEGLBKH_m978875162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2927347315, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral1366542454, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)4));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)1));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MFCOGODLKKJ()
extern "C"  void WireframeBehaviour_MFCOGODLKKJ_m2906414397 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MFCOGODLKKJ_m2906414397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)2));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)7));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::HKELKKCEAIE()
extern "C"  void WireframeBehaviour_HKELKKCEAIE_m1376041157 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_HKELKKCEAIE_m1376041157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)7));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)4));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::EHMDNANGGEM()
extern "C"  void WireframeBehaviour_EHMDNANGGEM_m210280865 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_EHMDNANGGEM_m210280865_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1803325615, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::KLIALGFAKNK()
extern "C"  void WireframeBehaviour_KLIALGFAKNK_m1667798701 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_KLIALGFAKNK_m1667798701_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)7));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)5));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OBGFMPJKOIB()
extern "C"  void WireframeBehaviour_OBGFMPJKOIB_m2894498228 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OBGFMPJKOIB_m2894498228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3808982001, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::BGCIEMPBIIP()
extern "C"  void WireframeBehaviour_BGCIEMPBIIP_m4164002641 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_BGCIEMPBIIP_m4164002641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)2));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)2));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::BOKLOGCJPFB()
extern "C"  void WireframeBehaviour_BOKLOGCJPFB_m1169001991 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_BOKLOGCJPFB_m1169001991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)7));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)7));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::NLLIENONEJD()
extern "C"  void WireframeBehaviour_NLLIENONEJD_m1786817916 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_NLLIENONEJD_m1786817916_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)7));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)3));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::IDFGHNEPELJ()
extern "C"  void WireframeBehaviour_IDFGHNEPELJ_m1372447246 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_IDFGHNEPELJ_m1372447246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)5));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)3));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MCDPPLDPLOL()
extern "C"  void WireframeBehaviour_MCDPPLDPLOL_m1986778025 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MCDPPLDPLOL_m1986778025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)0));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)3));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::IKBFKIDKCCA()
extern "C"  void WireframeBehaviour_IKBFKIDKCCA_m1995790978 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_IKBFKIDKCCA_m1995790978_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)0));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)3));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::LBHMPINEGAH()
extern "C"  void WireframeBehaviour_LBHMPINEGAH_m3848319783 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_LBHMPINEGAH_m3848319783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)3));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)5));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MAPFCDOLNBM()
extern "C"  void WireframeBehaviour_MAPFCDOLNBM_m3752887547 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MAPFCDOLNBM_m3752887547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1345542693, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::CDOOAMIPCJP()
extern "C"  void WireframeBehaviour_CDOOAMIPCJP_m552301795 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_CDOOAMIPCJP_m552301795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2416570879, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::ANIFNALLOCP()
extern "C"  void WireframeBehaviour_ANIFNALLOCP_m2471365939 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_ANIFNALLOCP_m2471365939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral4058986920, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral372029431, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)4));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)7));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::IOIHDPNODLK()
extern "C"  void WireframeBehaviour_IOIHDPNODLK_m128082013 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_IOIHDPNODLK_m128082013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral4181646808, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral2569923475, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)8));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)6));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::HLLPDIIOIGJ()
extern "C"  void WireframeBehaviour_HLLPDIIOIGJ_m2791082579 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_HLLPDIIOIGJ_m2791082579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral874589821, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral2856410928, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)4));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)6));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::ICLFGOGGNMN()
extern "C"  void WireframeBehaviour_ICLFGOGGNMN_m2071320641 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_ICLFGOGGNMN_m2071320641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)0));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)8));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::PIMBLOOKDPB()
extern "C"  void WireframeBehaviour_PIMBLOOKDPB_m1459775259 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_PIMBLOOKDPB_m1459775259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)8));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)6));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::IJJBFLLHKLD()
extern "C"  void WireframeBehaviour_IJJBFLLHKLD_m1181171198 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_IJJBFLLHKLD_m1181171198_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3039339873, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MOJBNANCALO()
extern "C"  void WireframeBehaviour_MOJBNANCALO_m4155708600 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MOJBNANCALO_m4155708600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3618020756, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::KIDKBHKKIIK()
extern "C"  void WireframeBehaviour_KIDKBHKKIIK_m1835780632 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_KIDKBHKKIIK_m1835780632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)3));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)7));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::IHPDKDHEINH()
extern "C"  void WireframeBehaviour_IHPDKDHEINH_m3974879750 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_IHPDKDHEINH_m3974879750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)3));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)6));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OGBDMLNFBGL()
extern "C"  void WireframeBehaviour_OGBDMLNFBGL_m87610686 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OGBDMLNFBGL_m87610686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral372029316, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral1414245037, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)7));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)6));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::CBDCOKCNBNC()
extern "C"  void WireframeBehaviour_CBDCOKCNBNC_m3810949800 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_CBDCOKCNBNC_m3810949800_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)8));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)8));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::BNAFDCPEFNE()
extern "C"  void WireframeBehaviour_BNAFDCPEFNE_m1149095952 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_BNAFDCPEFNE_m1149095952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)5));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)3));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::IPENEKDFNCF()
extern "C"  void WireframeBehaviour_IPENEKDFNCF_m3351966577 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_IPENEKDFNCF_m3351966577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3031879739, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::GOIJOLDFFGE()
extern "C"  void WireframeBehaviour_GOIJOLDFFGE_m1315580118 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_GOIJOLDFFGE_m1315580118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2104855582, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MAAHACCBNOE()
extern "C"  void WireframeBehaviour_MAAHACCBNOE_m13291592 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MAAHACCBNOE_m13291592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)4));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)0));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::PEHFGOBMBMI()
extern "C"  void WireframeBehaviour_PEHFGOBMBMI_m1891058994 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_PEHFGOBMBMI_m1891058994_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1757896673, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral1316399271, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)6));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)4));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MIBDDKLKOMP()
extern "C"  void WireframeBehaviour_MIBDDKLKOMP_m3832221708 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MIBDDKLKOMP_m3832221708_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)0));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)0));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OGBGALHMKFI()
extern "C"  void WireframeBehaviour_OGBGALHMKFI_m2376258885 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OGBGALHMKFI_m2376258885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral372029372, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral1455280663, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)4));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)1));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::KEHMDLMJJBB()
extern "C"  void WireframeBehaviour_KEHMDLMJJBB_m1303729286 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_KEHMDLMJJBB_m1303729286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)1));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)0));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::BIBNCPMGMHB()
extern "C"  void WireframeBehaviour_BIBNCPMGMHB_m472765229 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_BIBNCPMGMHB_m472765229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral4057172486, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::FOBPMDGMBMK()
extern "C"  void WireframeBehaviour_FOBPMDGMBMK_m1508704514 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_FOBPMDGMBMK_m1508704514_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1789014798, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral4179972495, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)2));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)4));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::PNCDFKJONMB()
extern "C"  void WireframeBehaviour_PNCDFKJONMB_m617663918 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_PNCDFKJONMB_m617663918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral4057172486, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OHPFLFLNHFL()
extern "C"  void WireframeBehaviour_OHPFLFLNHFL_m1198768427 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OHPFLFLNHFL_m1198768427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3632910556, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MCNNCBJBALK()
extern "C"  void WireframeBehaviour_MCNNCBJBALK_m3669813097 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MCNNCBJBALK_m3669813097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral601409167, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::FDOIPFCFBKE()
extern "C"  void WireframeBehaviour_FDOIPFCFBKE_m3161848843 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_FDOIPFCFBKE_m3161848843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral4035478884, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral232714862, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)2));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)2));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::EFECEHEFGCF()
extern "C"  void WireframeBehaviour_EFECEHEFGCF_m348725949 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_EFECEHEFGCF_m348725949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral372029316, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::JOOMFINDFBD()
extern "C"  void WireframeBehaviour_JOOMFINDFBD_m3753447744 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_JOOMFINDFBD_m3753447744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2882933152, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral174201204, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)2));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)2));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::AEDFBFAKILI()
extern "C"  void WireframeBehaviour_AEDFBFAKILI_m2513635732 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_AEDFBFAKILI_m2513635732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1987072475, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral4058986920, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)3));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)6));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MDAOFBNICCH()
extern "C"  void WireframeBehaviour_MDAOFBNICCH_m3350208466 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MDAOFBNICCH_m3350208466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral185337353, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::PLMIMGEHAKM()
extern "C"  void WireframeBehaviour_PLMIMGEHAKM_m4001410418 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_PLMIMGEHAKM_m4001410418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1992869258, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral3833952734, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)0));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)8));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::PODEIBLPELK()
extern "C"  void WireframeBehaviour_PODEIBLPELK_m2670230051 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_PODEIBLPELK_m2670230051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3321166683, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::LHKDHMNGLFD()
extern "C"  void WireframeBehaviour_LHKDHMNGLFD_m2674157117 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_LHKDHMNGLFD_m2674157117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1789014798, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::DHKENODLLMK()
extern "C"  void WireframeBehaviour_DHKENODLLMK_m3971767325 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_DHKENODLLMK_m3971767325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1887801112, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral4062258119, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)2));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)3));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::ABJGLKBAHFG()
extern "C"  void WireframeBehaviour_ABJGLKBAHFG_m2922284023 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_ABJGLKBAHFG_m2922284023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)3));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)5));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::EEMGGGBOONL()
extern "C"  void WireframeBehaviour_EEMGGGBOONL_m390827842 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_EEMGGGBOONL_m390827842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral4026354833, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral557470275, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)6));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)0));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::JKEBBCIBHHB()
extern "C"  void WireframeBehaviour_JKEBBCIBHHB_m3149500086 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_JKEBBCIBHHB_m3149500086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3249194866, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::HHLBKKPKDOP()
extern "C"  void WireframeBehaviour_HHLBKKPKDOP_m1793292044 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_HHLBKKPKDOP_m1793292044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral679394624, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral4250993590, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)5));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)7));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OGAGMLFACEI()
extern "C"  void WireframeBehaviour_OGAGMLFACEI_m2916878735 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OGAGMLFACEI_m2916878735_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral291633112, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral2876496943, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)5));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)1));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::BGCIPMPDJKB()
extern "C"  void WireframeBehaviour_BGCIPMPDJKB_m3140479233 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_BGCIPMPDJKB_m3140479233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)2));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)6));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::DLPHNOJBCBH()
extern "C"  void WireframeBehaviour_DLPHNOJBCBH_m2457302654 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_DLPHNOJBCBH_m2457302654_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral4058986920, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::FJAJILOIHPI()
extern "C"  void WireframeBehaviour_FJAJILOIHPI_m1689994309 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_FJAJILOIHPI_m1689994309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1929630113, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral4062258119, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)2));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)2));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::FELEKHGGHOC()
extern "C"  void WireframeBehaviour_FELEKHGGHOC_m2743220153 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_FELEKHGGHOC_m2743220153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2222441915, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::PEDEOEDOMKO()
extern "C"  void WireframeBehaviour_PEDEOEDOMKO_m2382624078 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_PEDEOEDOMKO_m2382624078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral318163179, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern "C"  void WireframeBehaviour_OnDrawGizmos_m4179942854 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OnDrawGizmos_m4179942854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)2));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)3));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::NCPHAPEKFHG()
extern "C"  void WireframeBehaviour_NCPHAPEKFHG_m1540413555 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_NCPHAPEKFHG_m1540413555_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)6));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)0));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MCPGGDHELHK()
extern "C"  void WireframeBehaviour_MCPGGDHELHK_m3222742170 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MCPGGDHELHK_m3222742170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)8));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)2));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern "C"  void WireframeBehaviour_OnRenderObject_m2411339956 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OnRenderObject_m2411339956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral599011397, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral895546098, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)2));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)3));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::EKIJOIGNMDM()
extern "C"  void WireframeBehaviour_EKIJOIGNMDM_m2418535638 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_EKIJOIGNMDM_m2418535638_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)0));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)8));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::CDILOGPFICJ()
extern "C"  void WireframeBehaviour_CDILOGPFICJ_m4091281244 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_CDILOGPFICJ_m4091281244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)6));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)3));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::CLCJHFINNGM()
extern "C"  void WireframeBehaviour_CLCJHFINNGM_m1955471551 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_CLCJHFINNGM_m1955471551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)2));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)8));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MIHOBDOEDEI()
extern "C"  void WireframeBehaviour_MIHOBDOEDEI_m2408107447 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MIHOBDOEDEI_m2408107447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3629399031, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral3321166683, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)1));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)8));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::IHEOPGCENKF()
extern "C"  void WireframeBehaviour_IHEOPGCENKF_m1547907625 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_IHEOPGCENKF_m1547907625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2218050368, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::NJEFDLIBDIF()
extern "C"  void WireframeBehaviour_NJEFDLIBDIF_m3734793107 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_NJEFDLIBDIF_m3734793107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 1;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)6));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)0));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::HPFKCKLMBJO()
extern "C"  void WireframeBehaviour_HPFKCKLMBJO_m2777694159 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_HPFKCKLMBJO_m2777694159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral4026354833, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral3209823468, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)5));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)7));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::EIGICDKBKBM()
extern "C"  void WireframeBehaviour_EIGICDKBKBM_m1238950756 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_EIGICDKBKBM_m1238950756_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral372029393, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral57475196, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)0));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)2));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::JHPIGJOGLJD()
extern "C"  void WireframeBehaviour_JHPIGJOGLJD_m2989805612 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_JHPIGJOGLJD_m2989805612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2609891608, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::DNFCLPOELCO()
extern "C"  void WireframeBehaviour_DNFCLPOELCO_m2327793637 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_DNFCLPOELCO_m2327793637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3033297088, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::DBBODICGOPJ()
extern "C"  void WireframeBehaviour_DBBODICGOPJ_m1024667819 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_DBBODICGOPJ_m1024667819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1991887629, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral185337353, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)8));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)8));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::GGNLCGBLPLI()
extern "C"  void WireframeBehaviour_GGNLCGBLPLI_m2769584601 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_GGNLCGBLPLI_m2769584601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3442848064, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::BPGAMBMJIAB()
extern "C"  void WireframeBehaviour_BPGAMBMJIAB_m1244812478 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_BPGAMBMJIAB_m1244812478_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)0));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)0));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)4));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::Start()
extern "C"  void WireframeBehaviour_Start_m2184757344 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_Start_m2184757344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral599011397, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::MMCKMIIFBEC()
extern "C"  void WireframeBehaviour_MMCKMIIFBEC_m236885055 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_MMCKMIIFBEC_m236885055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)4));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::HKLBFPAPKHD()
extern "C"  void WireframeBehaviour_HKLBFPAPKHD_m3427256221 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_HKLBFPAPKHD_m3427256221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)0));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral4001967317, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral601409167, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 1;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)6));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)8));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::GLEJCIBNNLL()
extern "C"  void WireframeBehaviour_GLEJCIBNNLL_m2669488302 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_GLEJCIBNNLL_m2669488302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2487108583, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral372029397, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)0));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)1));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)5));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::NLNBMNPJINJ()
extern "C"  void WireframeBehaviour_NLNBMNPJINJ_m2049441042 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_NLNBMNPJINJ_m2049441042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2218050368, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::GJEHNMNJBLP()
extern "C"  void WireframeBehaviour_GJEHNMNJBLP_m49031537 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_GJEHNMNJBLP_m49031537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral115638372, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::LJNKGAMLGFD()
extern "C"  void WireframeBehaviour_LJNKGAMLGFD_m4131286409 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_LJNKGAMLGFD_m4131286409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)1;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 1;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)0;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_PFCCEKMCBGE_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3495040156, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_PFCCEKMCBGE_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_PFCCEKMCBGE_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral1731360412, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)5));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)5));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::GEGHFBJMALM()
extern "C"  void WireframeBehaviour_GEGHFBJMALM_m3688575262 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_GEGHFBJMALM_m3688575262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_PFCCEKMCBGE_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral3598345300, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::KFEEPKCPBCE(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_KFEEPKCPBCE_m667466021 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_NIAFCJOMNCL_m4087719520(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_HDGLLAFAKEP_m3111200887(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::KKIAJHJCHMJ()
extern "C"  void WireframeTrackableEventHandler_KKIAJHJCHMJ_m2198785808 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_KKIAJHJCHMJ_m2198785808_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::LCPOKLKJFCE()
extern "C"  void WireframeTrackableEventHandler_LCPOKLKJFCE_m2446626476 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_LCPOKLKJFCE_m2446626476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3039339873, L_31, _stringLiteral3039339873, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::IGABBLIOMHE()
extern "C"  void WireframeTrackableEventHandler_IGABBLIOMHE_m1181148253 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_IGABBLIOMHE_m1181148253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral253239906, L_31, _stringLiteral1373918990, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::LOOMIIFHIMC()
extern "C"  void WireframeTrackableEventHandler_LOOMIIFHIMC_m782917760 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_LOOMIIFHIMC_m782917760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral372029398, L_31, _stringLiteral372029316, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::NMJFDMKHALK(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_NMJFDMKHALK_m1174613403 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_GCPMNKOIAGM_m2269296581(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_KFFIKNBENJP_m3569997932(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::JIBJGCDGPDI()
extern "C"  void WireframeTrackableEventHandler_JIBJGCDGPDI_m407693029 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_JIBJGCDGPDI_m407693029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::FCBAAPGIOIM()
extern "C"  void WireframeTrackableEventHandler_FCBAAPGIOIM_m4071878402 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_FCBAAPGIOIM_m4071878402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::LAEONDHFCMH()
extern "C"  void WireframeTrackableEventHandler_LAEONDHFCMH_m833139037 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_LAEONDHFCMH_m833139037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3708513882, L_31, _stringLiteral1345542693, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::GCAKCGFCOCN(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_GCAKCGFCOCN_m3530363665 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_LOOMIIFHIMC_m782917760(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_DHKACIHBPPA_m139711023(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::GGDNBGDPEJJ(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_GGDNBGDPEJJ_m2026460368 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)5)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)6))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_JELKBFLCAIK_m1170380144(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_KNIFKOLAFBI_m1696818270(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::NIAFCJOMNCL()
extern "C"  void WireframeTrackableEventHandler_NIAFCJOMNCL_m4087719520 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_NIAFCJOMNCL_m4087719520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral372029398, L_31, _stringLiteral787298419, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::FDJDHABNNCC()
extern "C"  void WireframeTrackableEventHandler_FDJDHABNNCC_m671499079 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_FDJDHABNNCC_m671499079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral472785647, L_31, _stringLiteral4058986920, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::DIFHLOMHNHJ(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_DIFHLOMHNHJ_m2946226161 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)5)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_CGOENOELDFO_m595842679(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_FBKJJBFIGCM_m2697467891(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::KNIFKOLAFBI()
extern "C"  void WireframeTrackableEventHandler_KNIFKOLAFBI_m1696818270 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_KNIFKOLAFBI_m1696818270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3364679155, L_31, _stringLiteral102231524, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::EOHMBHGJMNJ()
extern "C"  void WireframeTrackableEventHandler_EOHMBHGJMNJ_m3945602073 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_EOHMBHGJMNJ_m3945602073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
extern "C"  void WireframeTrackableEventHandler__ctor_m4253736968 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::ACDOGKDFFIF()
extern "C"  void WireframeTrackableEventHandler_ACDOGKDFFIF_m2429399710 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_ACDOGKDFFIF_m2429399710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1417766541, L_31, _stringLiteral816986750, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::NLNBMNPJINJ()
extern "C"  void WireframeTrackableEventHandler_NLNBMNPJINJ_m1228672266 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_NLNBMNPJINJ_m1228672266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::GKLEHAEBPEB()
extern "C"  void WireframeTrackableEventHandler_GKLEHAEBPEB_m2313315472 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_GKLEHAEBPEB_m2313315472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::MACBCNKLKIJ(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_MACBCNKLKIJ_m1400026247 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_ECGBLMKNAFH_m1968249532(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_KFFIKNBENJP_m3569997932(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::DPANGCAGGEA(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_DPANGCAGGEA_m3715086270 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)0))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_PAKPNBLKGCB_m2253804083(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_KFFIKNBENJP_m3569997932(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::ICMBCINNDGM()
extern "C"  void WireframeTrackableEventHandler_ICMBCINNDGM_m110706081 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_ICMBCINNDGM_m110706081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1040926105, L_31, _stringLiteral759218142, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::EDBCGPGHNBF()
extern "C"  void WireframeTrackableEventHandler_EDBCGPGHNBF_m1677182104 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_EDBCGPGHNBF_m1677182104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1493872207, L_31, _stringLiteral425611190, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::NPLBCBNBIKE()
extern "C"  void WireframeTrackableEventHandler_NPLBCBNBIKE_m3569066392 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_NPLBCBNBIKE_m3569066392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2993583680, L_31, _stringLiteral2609891608, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::GCPMNKOIAGM()
extern "C"  void WireframeTrackableEventHandler_GCPMNKOIAGM_m2269296581 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_GCPMNKOIAGM_m2269296581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3321166683, L_31, _stringLiteral2375273008, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::FBKJJBFIGCM()
extern "C"  void WireframeTrackableEventHandler_FBKJJBFIGCM_m2697467891 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_FBKJJBFIGCM_m2697467891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1016630757, L_31, _stringLiteral372029398, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::FJPCAPDOEBC()
extern "C"  void WireframeTrackableEventHandler_FJPCAPDOEBC_m686633061 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_FJPCAPDOEBC_m686633061_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::CNCABLEEGBL()
extern "C"  void WireframeTrackableEventHandler_CNCABLEEGBL_m2114310396 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_CNCABLEEGBL_m2114310396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::LDHNJKJKMHC(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_LDHNJKJKMHC_m1513758338 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_EBCPBMGNOOF_m4007887116(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_CKOMKHHAGFK_m3147004680(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OLBJJCNKKFL(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_OLBJJCNKKFL_m1559561808 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)7)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_ACDOGKDFFIF_m2429399710(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_CLLGALDACLA_m1173486930(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::DMDEOJCCMBE()
extern "C"  void WireframeTrackableEventHandler_DMDEOJCCMBE_m2103699731 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_DMDEOJCCMBE_m2103699731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::GJCBMHBKBDO(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_GJCBMHBKBDO_m3560197325 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)6))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_LAEONDHFCMH_m833139037(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_IGABBLIOMHE_m1181148253(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::BIGOOPNEOPH()
extern "C"  void WireframeTrackableEventHandler_BIGOOPNEOPH_m1142678134 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_BIGOOPNEOPH_m1142678134_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral4234644335, L_31, _stringLiteral2487108583, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::JELKBFLCAIK()
extern "C"  void WireframeTrackableEventHandler_JELKBFLCAIK_m1170380144 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_JELKBFLCAIK_m1170380144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral372029310, L_31, _stringLiteral737604599, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::KFFIKNBENJP()
extern "C"  void WireframeTrackableEventHandler_KFFIKNBENJP_m3569997932 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_KFFIKNBENJP_m3569997932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3495040156, L_31, _stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::NOEBLEKPBOD()
extern "C"  void WireframeTrackableEventHandler_NOEBLEKPBOD_m2199512617 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_NOEBLEKPBOD_m2199512617_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral719456662, L_31, _stringLiteral3033396940, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::CGFACDCECPO()
extern "C"  void WireframeTrackableEventHandler_CGFACDCECPO_m674731180 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_CGFACDCECPO_m674731180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral4062258119, L_31, _stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OOADEGMJECE()
extern "C"  void WireframeTrackableEventHandler_OOADEGMJECE_m1566597351 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_OOADEGMJECE_m1566597351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2440250040, L_31, _stringLiteral1574707977, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::NNJIDIACKEA()
extern "C"  void WireframeTrackableEventHandler_NNJIDIACKEA_m488764539 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_NNJIDIACKEA_m488764539_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1040926105, L_31, _stringLiteral3033297088, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::DHDMLGADOHO()
extern "C"  void WireframeTrackableEventHandler_DHDMLGADOHO_m1641672251 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_DHDMLGADOHO_m1641672251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral212532649, L_31, _stringLiteral3824521097, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::CGOENOELDFO()
extern "C"  void WireframeTrackableEventHandler_CGOENOELDFO_m595842679 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_CGOENOELDFO_m595842679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2610385663, L_31, _stringLiteral1789014798, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::FECCALMBHCB()
extern "C"  void WireframeTrackableEventHandler_FECCALMBHCB_m4181103262 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_FECCALMBHCB_m4181103262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3039339873, L_31, _stringLiteral3812735643, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::ILEELGACAOI()
extern "C"  void WireframeTrackableEventHandler_ILEELGACAOI_m4234151595 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_ILEELGACAOI_m4234151595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2041851080, L_31, _stringLiteral403189246, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::CJCJCCJIAJJ()
extern "C"  void WireframeTrackableEventHandler_CJCJCCJIAJJ_m2331287578 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_CJCJCCJIAJJ_m2331287578_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::IMAGALEFLGE()
extern "C"  void WireframeTrackableEventHandler_IMAGALEFLGE_m184054848 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_IMAGALEFLGE_m184054848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::EBCPBMGNOOF()
extern "C"  void WireframeTrackableEventHandler_EBCPBMGNOOF_m4007887116 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_EBCPBMGNOOF_m4007887116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral819182375, L_31, _stringLiteral3039339873, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::PAKPNBLKGCB()
extern "C"  void WireframeTrackableEventHandler_PAKPNBLKGCB_m2253804083 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_PAKPNBLKGCB_m2253804083_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3629399031, L_31, _stringLiteral1296085355, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_OnTrackableStateChanged_m106630617 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_ICMBCINNDGM_m110706081(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_NNJIDIACKEA_m488764539(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::IAMEHGDFBEO()
extern "C"  void WireframeTrackableEventHandler_IAMEHGDFBEO_m118164025 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_IAMEHGDFBEO_m118164025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
extern "C"  void WireframeTrackableEventHandler_Start_m1475947192 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_Start_m1475947192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::KDPFEHMPGGK(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_KDPFEHMPGGK_m2539303454 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)0))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_BIGOOPNEOPH_m1142678134(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_CLLGALDACLA_m1173486930(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::FBDKKJLHIOD()
extern "C"  void WireframeTrackableEventHandler_FBDKKJLHIOD_m482125408 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_FBDKKJLHIOD_m482125408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1093630588, L_31, _stringLiteral2537971763, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::HEJKOLHLCNK()
extern "C"  void WireframeTrackableEventHandler_HEJKOLHLCNK_m1956377937 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_HEJKOLHLCNK_m1956377937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2218050368, L_31, _stringLiteral372029397, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::COHNILNFJGH()
extern "C"  void WireframeTrackableEventHandler_COHNILNFJGH_m3628274274 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_COHNILNFJGH_m3628274274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral808027542, L_31, _stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::CLLGALDACLA()
extern "C"  void WireframeTrackableEventHandler_CLLGALDACLA_m1173486930 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_CLLGALDACLA_m1173486930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1409823797, L_31, _stringLiteral372029316, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::NPLOAKBHIDI(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_NPLOAKBHIDI_m3302654845 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)8)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_JELKBFLCAIK_m1170380144(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_ILEELGACAOI_m4234151595(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::JGHMFCFADFL(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_JGHMFCFADFL_m35998590 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)0))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_JELKBFLCAIK_m1170380144(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_EDBCGPGHNBF_m1677182104(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::CKOMKHHAGFK()
extern "C"  void WireframeTrackableEventHandler_CKOMKHHAGFK_m3147004680 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_CKOMKHHAGFK_m3147004680_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1588531317, L_31, _stringLiteral3364679155, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::HDGLLAFAKEP()
extern "C"  void WireframeTrackableEventHandler_HDGLLAFAKEP_m3111200887 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_HDGLLAFAKEP_m3111200887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1803325615, L_31, _stringLiteral4252499464, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::ECGBLMKNAFH()
extern "C"  void WireframeTrackableEventHandler_ECGBLMKNAFH_m1968249532 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_ECGBLMKNAFH_m1968249532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2794766820, L_31, _stringLiteral1409823797, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::CCPKEOCCENB(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_CCPKEOCCENB_m3797815250 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)6))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_ECGBLMKNAFH_m1968249532(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_FDJDHABNNCC_m671499079(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::AHNHMLGGFNG()
extern "C"  void WireframeTrackableEventHandler_AHNHMLGGFNG_m3712762473 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_AHNHMLGGFNG_m3712762473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OHCIPKLOFGK(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_OHCIPKLOFGK_m1474654319 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___FCAFEOACNMO0, int32_t ___PIMNLEJBMCF1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___PIMNLEJBMCF1;
		if ((((int32_t)L_0) == ((int32_t)5)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___PIMNLEJBMCF1;
		if ((!(((uint32_t)L_1) == ((uint32_t)0))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_PJFOOOOGBDE_m1297153882(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_FBKJJBFIGCM_m2697467891(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::PJFOOOOGBDE()
extern "C"  void WireframeTrackableEventHandler_PJFOOOOGBDE_m1297153882 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_PJFOOOOGBDE_m1297153882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2537971763, L_31, _stringLiteral949024030, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::EDEOPGENFAJ()
extern "C"  void WireframeTrackableEventHandler_EDEOPGENFAJ_m2773645888 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_EDEOPGENFAJ_m2773645888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)0));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 1;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1446756774, L_31, _stringLiteral2812229188, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::GOIJOLDFFGE()
extern "C"  void WireframeTrackableEventHandler_GOIJOLDFFGE_m63814910 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_GOIJOLDFFGE_m63814910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_MDEHCLGCIKP_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_MDEHCLGCIKP_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::DHKACIHBPPA()
extern "C"  void WireframeTrackableEventHandler_DHKACIHBPPA_m139711023 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_DHKACIHBPPA_m139711023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 1;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)0));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 1;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)0));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_MDEHCLGCIKP_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2609877245, L_31, _stringLiteral2088361527, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordBehaviour::.ctor()
extern "C"  void WordBehaviour__ctor_m581909702 (WordBehaviour_t3366478421 * __this, const MethodInfo* method)
{
	{
		WordAbstractBehaviour__ctor_m1415816009(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::GKDAOBPHLNB()
extern "C"  void YesNoChoice_GKDAOBPHLNB_m3733691834 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_GKDAOBPHLNB_m3733691834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(115.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)0)))))-(float)((float)((float)L_9/(float)(13.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)2)))))-(float)((float)((float)L_11/(float)(576.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (1692.0f), (1151.0f), ((float)((float)L_15/(float)(1416.0f))), ((float)((float)L_16/(float)(552.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral3321166683, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)0);
		NarrativeCondition_set_isDone_m408944312(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)1, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(986.0f))), (1959.0f), ((float)((float)L_22/(float)(802.0f))), ((float)((float)L_23/(float)(1742.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral1345542693, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)1);
		NarrativeCondition_ENHOMCGMINP_m33130136(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)1, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::JFGPFABIMOB()
extern "C"  void YesNoChoice_JFGPFABIMOB_m3795407005 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_JFGPFABIMOB_m3795407005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(1654.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)7)))))-(float)((float)((float)L_9/(float)(297.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)6)))))-(float)((float)((float)L_11/(float)(718.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (1440.0f), (1507.0f), ((float)((float)L_15/(float)(1057.0f))), ((float)((float)L_16/(float)(465.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral3033402446, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)0);
		NarrativeCondition_BMJPDAHKIPO_m966518882(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)0, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(1922.0f))), (489.0f), ((float)((float)L_22/(float)(157.0f))), ((float)((float)L_23/(float)(1094.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral2267280474, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)0);
		NarrativeCondition_LOJLMGCOMNE_m3469340218(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)1, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::IDBAOOPDLMH()
extern "C"  void YesNoChoice_IDBAOOPDLMH_m127698629 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_IDBAOOPDLMH_m127698629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(873.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)7)))))-(float)((float)((float)L_9/(float)(8.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)3)))))-(float)((float)((float)L_11/(float)(1293.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (1590.0f), (669.0f), ((float)((float)L_15/(float)(1598.0f))), ((float)((float)L_16/(float)(1771.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral372029352, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)1);
		NarrativeCondition_KPPPMJJLBNM_m2300095498(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)1, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(4.0f))), (1338.0f), ((float)((float)L_22/(float)(1255.0f))), ((float)((float)L_23/(float)(57.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral808027542, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)1);
		NarrativeCondition_LOJLMGCOMNE_m3469340218(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)0, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::FGHJDGNPAFI()
extern "C"  void YesNoChoice_FGHJDGNPAFI_m1512755462 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_FGHJDGNPAFI_m1512755462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(1927.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)8)))))-(float)((float)((float)L_9/(float)(322.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)6)))))-(float)((float)((float)L_11/(float)(220.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (694.0f), (1821.0f), ((float)((float)L_15/(float)(650.0f))), ((float)((float)L_16/(float)(875.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral1296085355, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)0);
		NarrativeCondition_LLPLKLIEGNH_m763528405(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)1, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(281.0f))), (847.0f), ((float)((float)L_22/(float)(1174.0f))), ((float)((float)L_23/(float)(323.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral95490550, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)1);
		NarrativeCondition_GMKLCEALNKB_m2255547382(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)1, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::HNDMPFCFFFE()
extern "C"  void YesNoChoice_HNDMPFCFFFE_m1628485279 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_HNDMPFCFFFE_m1628485279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(287.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)6)))))-(float)((float)((float)L_9/(float)(819.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)1)))))-(float)((float)((float)L_11/(float)(1443.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (1365.0f), (1135.0f), ((float)((float)L_15/(float)(754.0f))), ((float)((float)L_16/(float)(645.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral3114425798, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)1);
		NarrativeCondition_LLPLKLIEGNH_m763528405(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)1, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(1646.0f))), (1793.0f), ((float)((float)L_22/(float)(764.0f))), ((float)((float)L_23/(float)(1569.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral2654155903, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)1);
		NarrativeCondition_HIDFKJJGPLN_m739605056(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)0, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::COFPAJDOLHF()
extern "C"  void YesNoChoice_COFPAJDOLHF_m1023226326 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_COFPAJDOLHF_m1023226326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(825.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)4)))))-(float)((float)((float)L_9/(float)(173.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)4)))))-(float)((float)((float)L_11/(float)(425.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (359.0f), (562.0f), ((float)((float)L_15/(float)(1514.0f))), ((float)((float)L_16/(float)(1628.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral372029399, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)1);
		NarrativeCondition_HIDFKJJGPLN_m739605056(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)1, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(70.0f))), (1792.0f), ((float)((float)L_22/(float)(1421.0f))), ((float)((float)L_23/(float)(186.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral372029314, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)0);
		NarrativeCondition_FLHPIGCGOND_m691917992(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)1, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::PEJFEPFGJEN()
extern "C"  void YesNoChoice_PEJFEPFGJEN_m4037529232 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_PEJFEPFGJEN_m4037529232_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(274.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)2)))))-(float)((float)((float)L_9/(float)(1456.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)6)))))-(float)((float)((float)L_11/(float)(1478.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (204.0f), (1121.0f), ((float)((float)L_15/(float)(322.0f))), ((float)((float)L_16/(float)(311.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral287061489, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)1);
		NarrativeCondition_MKJPKLENHAG_m3460246573(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)1, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(396.0f))), (223.0f), ((float)((float)L_22/(float)(1887.0f))), ((float)((float)L_23/(float)(514.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral816988926, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)1);
		NarrativeCondition_set_isDone_m408944312(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)1, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::.ctor()
extern "C"  void YesNoChoice__ctor_m2239614602 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	{
		NarrativeCondition__ctor_m193474214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::GGKHNMGHKCC()
extern "C"  void YesNoChoice_GGKHNMGHKCC_m2035332920 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_GGKHNMGHKCC_m2035332920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(1915.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)8)))))-(float)((float)((float)L_9/(float)(351.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)6)))))-(float)((float)((float)L_11/(float)(1701.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (1727.0f), (1134.0f), ((float)((float)L_15/(float)(1569.0f))), ((float)((float)L_16/(float)(91.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral57475196, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)1);
		NarrativeCondition_set_isDone_m408944312(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)1, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(50.0f))), (289.0f), ((float)((float)L_22/(float)(954.0f))), ((float)((float)L_23/(float)(439.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral3321166683, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)1);
		NarrativeCondition_LLPLKLIEGNH_m763528405(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)0, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::LKJCBHOHJEB()
extern "C"  void YesNoChoice_LKJCBHOHJEB_m1185547858 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_LKJCBHOHJEB_m1185547858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(1364.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)0)))))-(float)((float)((float)L_9/(float)(691.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)8)))))-(float)((float)((float)L_11/(float)(1187.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (571.0f), (169.0f), ((float)((float)L_15/(float)(1093.0f))), ((float)((float)L_16/(float)(1370.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral1040926105, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)1);
		NarrativeCondition_POGHNANKPIB_m1855205988(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)0, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(185.0f))), (310.0f), ((float)((float)L_22/(float)(1226.0f))), ((float)((float)L_23/(float)(425.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral3993410431, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)1);
		NarrativeCondition_GMKLCEALNKB_m2255547382(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)1, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::CKHMOPOBELG()
extern "C"  void YesNoChoice_CKHMOPOBELG_m3931102139 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_CKHMOPOBELG_m3931102139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(1006.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)3)))))-(float)((float)((float)L_9/(float)(904.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)7)))))-(float)((float)((float)L_11/(float)(235.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (249.0f), (471.0f), ((float)((float)L_15/(float)(744.0f))), ((float)((float)L_16/(float)(1766.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral3248387252, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)0);
		NarrativeCondition_MKJPKLENHAG_m3460246573(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)1, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(243.0f))), (1398.0f), ((float)((float)L_22/(float)(1229.0f))), ((float)((float)L_23/(float)(1284.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral4179972495, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)1);
		NarrativeCondition_INPBCMCGFAH_m2844416023(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)1, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::ELGLFIGOHOF()
extern "C"  void YesNoChoice_ELGLFIGOHOF_m2370230712 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_ELGLFIGOHOF_m2370230712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(392.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)1)))))-(float)((float)((float)L_9/(float)(378.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)0)))))-(float)((float)((float)L_11/(float)(960.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (962.0f), (448.0f), ((float)((float)L_15/(float)(1365.0f))), ((float)((float)L_16/(float)(1446.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral3039339873, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)0);
		NarrativeCondition_LOJLMGCOMNE_m3469340218(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)0, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(942.0f))), (638.0f), ((float)((float)L_22/(float)(988.0f))), ((float)((float)L_23/(float)(520.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral174201204, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)0);
		NarrativeCondition_INPBCMCGFAH_m2844416023(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)1, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::PNDEANCEPFB()
extern "C"  void YesNoChoice_PNDEANCEPFB_m2783877124 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_PNDEANCEPFB_m2783877124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(1631.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)8)))))-(float)((float)((float)L_9/(float)(282.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)4)))))-(float)((float)((float)L_11/(float)(1660.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (408.0f), (146.0f), ((float)((float)L_15/(float)(293.0f))), ((float)((float)L_16/(float)(532.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral557470275, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)0);
		NarrativeCondition_BMJPDAHKIPO_m966518882(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)0, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(1741.0f))), (924.0f), ((float)((float)L_22/(float)(1392.0f))), ((float)((float)L_23/(float)(1076.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral2339396354, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)0);
		NarrativeCondition_LLPLKLIEGNH_m763528405(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)1, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::OnGUI()
extern "C"  void YesNoChoice_OnGUI_m1115652626 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_OnGUI_m1115652626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(0.5f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)2)))))-(float)((float)((float)L_9/(float)(2.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)2)))))-(float)((float)((float)L_11/(float)(2.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (0.0f), (0.0f), ((float)((float)L_15/(float)(2.0f))), ((float)((float)L_16/(float)(2.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral3021629903, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)1);
		NarrativeCondition_set_isDone_m408944312(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)0, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(2.0f))), (0.0f), ((float)((float)L_22/(float)(2.0f))), ((float)((float)L_23/(float)(2.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral1496915101, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)1);
		NarrativeCondition_set_isDone_m408944312(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)0, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::GKLFAFJANON()
extern "C"  void YesNoChoice_GKLFAFJANON_m2681160699 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_GKLFAFJANON_m2681160699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(1503.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)5)))))-(float)((float)((float)L_9/(float)(1936.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)7)))))-(float)((float)((float)L_11/(float)(601.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (232.0f), (1607.0f), ((float)((float)L_15/(float)(707.0f))), ((float)((float)L_16/(float)(561.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral762922235, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)0);
		NarrativeCondition_GMKLCEALNKB_m2255547382(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)1, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(197.0f))), (879.0f), ((float)((float)L_22/(float)(11.0f))), ((float)((float)L_23/(float)(1886.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral1982269700, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)0);
		NarrativeCondition_GMKLCEALNKB_m2255547382(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)0, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::OPFOPPECKGB()
extern "C"  void YesNoChoice_OPFOPPECKGB_m1131751710 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_OPFOPPECKGB_m1131751710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(178.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)3)))))-(float)((float)((float)L_9/(float)(1496.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)4)))))-(float)((float)((float)L_11/(float)(384.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (1395.0f), (1505.0f), ((float)((float)L_15/(float)(886.0f))), ((float)((float)L_16/(float)(381.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral649327173, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)0);
		NarrativeCondition_INPBCMCGFAH_m2844416023(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)0, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(188.0f))), (668.0f), ((float)((float)L_22/(float)(1493.0f))), ((float)((float)L_23/(float)(1578.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral372029317, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)0);
		NarrativeCondition_INPBCMCGFAH_m2844416023(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)0, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::MILJNKHOLLF()
extern "C"  void YesNoChoice_MILJNKHOLLF_m538753334 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_MILJNKHOLLF_m538753334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(1822.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)5)))))-(float)((float)((float)L_9/(float)(1926.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)2)))))-(float)((float)((float)L_11/(float)(1584.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (1540.0f), (510.0f), ((float)((float)L_15/(float)(1993.0f))), ((float)((float)L_16/(float)(1808.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral2879021919, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)1);
		NarrativeCondition_LOJLMGCOMNE_m3469340218(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)1, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(281.0f))), (631.0f), ((float)((float)L_22/(float)(164.0f))), ((float)((float)L_23/(float)(1216.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral3722207907, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)1);
		NarrativeCondition_ENHOMCGMINP_m33130136(__this, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)0, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YesNoChoice::HOCEJAMGLKH()
extern "C"  void YesNoChoice_HOCEJAMGLKH_m1141190081 (YesNoChoice_t3116058963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (YesNoChoice_HOCEJAMGLKH_m1141190081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_choiceWidth_15();
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_1)))));
		float L_2 = __this->get_choiceHeight_16();
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2*(float)(((float)((float)L_3)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_4 = GUI_get_skin_m2309570990(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1799908754 * L_5 = GUISkin_get_button_m797402546(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = floorf(((float)((float)L_6*(float)(1951.0f))));
		NullCheck(L_5);
		GUIStyle_set_fontSize_m4015341543(L_5, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = V_0;
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = V_1;
		float L_12 = V_0;
		float L_13 = V_1;
		Rect_t3681755626  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Rect__ctor_m1220545469(&L_14, ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)0)))))-(float)((float)((float)L_9/(float)(448.0f))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)4)))))-(float)((float)((float)L_11/(float)(1241.0f))))), L_12, L_13, /*hidden argument*/NULL);
		GUI_BeginGroup_m2572373001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Rect_t3681755626  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m1220545469(&L_17, (1890.0f), (1443.0f), ((float)((float)L_15/(float)(725.0f))), ((float)((float)L_16/(float)(1108.0f))), /*hidden argument*/NULL);
		bool L_18 = GUI_Button_m3054448581(NULL /*static, unused*/, L_17, _stringLiteral372029317, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		EnableGameObject_t1635748018 * L_19 = __this->get_noEffect_14();
		NullCheck(L_19);
		((NarrativeEffect_t3336735121 *)L_19)->set_disableEffect_2((bool)0);
		NarrativeCondition_LOJLMGCOMNE_m3469340218(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)1, /*hidden argument*/NULL);
	}

IL_00af:
	{
		float L_21 = V_0;
		float L_22 = V_0;
		float L_23 = V_1;
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_21/(float)(1104.0f))), (505.0f), ((float)((float)L_22/(float)(1900.0f))), ((float)((float)L_23/(float)(1729.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_25 = GUI_Button_m3054448581(NULL /*static, unused*/, L_24, _stringLiteral811305474, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fc;
		}
	}
	{
		EnableGameObject_t1635748018 * L_26 = __this->get_yesEffect_13();
		NullCheck(L_26);
		((NarrativeEffect_t3336735121 *)L_26)->set_disableEffect_2((bool)1);
		NarrativeCondition_LOJLMGCOMNE_m3469340218(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)0, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_EndGroup_m1672170830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
