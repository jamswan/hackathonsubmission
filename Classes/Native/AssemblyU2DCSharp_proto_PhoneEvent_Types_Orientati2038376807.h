﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Gene2830295373.h"

// proto.PhoneEvent/Types/OrientationEvent
struct OrientationEvent_t2038376807;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/OrientationEvent
struct  OrientationEvent_t2038376807  : public GeneratedMessageLite_2_t2830295373
{
public:
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::MIGGKHHBAID
	bool ___MIGGKHHBAID_4;
	// System.Int64 proto.PhoneEvent/Types/OrientationEvent::HMBIGJIDGNB
	int64_t ___HMBIGJIDGNB_5;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::DAPIOCPOIID
	bool ___DAPIOCPOIID_7;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::KKLEGOBOFFJ
	float ___KKLEGOBOFFJ_8;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::LGBJOJHEJDM
	bool ___LGBJOJHEJDM_10;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::NFCIJFNNNDL
	float ___NFCIJFNNNDL_11;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::LPLIBMIPMFD
	bool ___LPLIBMIPMFD_13;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::PLILMNHLFED
	float ___PLILMNHLFED_14;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::CKAAFMKPDBO
	bool ___CKAAFMKPDBO_16;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::FFPNCMDJLGP
	float ___FFPNCMDJLGP_17;
	// System.Int32 proto.PhoneEvent/Types/OrientationEvent::BABCKJCECDJ
	int32_t ___BABCKJCECDJ_18;

public:
	inline static int32_t get_offset_of_MIGGKHHBAID_4() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807, ___MIGGKHHBAID_4)); }
	inline bool get_MIGGKHHBAID_4() const { return ___MIGGKHHBAID_4; }
	inline bool* get_address_of_MIGGKHHBAID_4() { return &___MIGGKHHBAID_4; }
	inline void set_MIGGKHHBAID_4(bool value)
	{
		___MIGGKHHBAID_4 = value;
	}

	inline static int32_t get_offset_of_HMBIGJIDGNB_5() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807, ___HMBIGJIDGNB_5)); }
	inline int64_t get_HMBIGJIDGNB_5() const { return ___HMBIGJIDGNB_5; }
	inline int64_t* get_address_of_HMBIGJIDGNB_5() { return &___HMBIGJIDGNB_5; }
	inline void set_HMBIGJIDGNB_5(int64_t value)
	{
		___HMBIGJIDGNB_5 = value;
	}

	inline static int32_t get_offset_of_DAPIOCPOIID_7() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807, ___DAPIOCPOIID_7)); }
	inline bool get_DAPIOCPOIID_7() const { return ___DAPIOCPOIID_7; }
	inline bool* get_address_of_DAPIOCPOIID_7() { return &___DAPIOCPOIID_7; }
	inline void set_DAPIOCPOIID_7(bool value)
	{
		___DAPIOCPOIID_7 = value;
	}

	inline static int32_t get_offset_of_KKLEGOBOFFJ_8() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807, ___KKLEGOBOFFJ_8)); }
	inline float get_KKLEGOBOFFJ_8() const { return ___KKLEGOBOFFJ_8; }
	inline float* get_address_of_KKLEGOBOFFJ_8() { return &___KKLEGOBOFFJ_8; }
	inline void set_KKLEGOBOFFJ_8(float value)
	{
		___KKLEGOBOFFJ_8 = value;
	}

	inline static int32_t get_offset_of_LGBJOJHEJDM_10() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807, ___LGBJOJHEJDM_10)); }
	inline bool get_LGBJOJHEJDM_10() const { return ___LGBJOJHEJDM_10; }
	inline bool* get_address_of_LGBJOJHEJDM_10() { return &___LGBJOJHEJDM_10; }
	inline void set_LGBJOJHEJDM_10(bool value)
	{
		___LGBJOJHEJDM_10 = value;
	}

	inline static int32_t get_offset_of_NFCIJFNNNDL_11() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807, ___NFCIJFNNNDL_11)); }
	inline float get_NFCIJFNNNDL_11() const { return ___NFCIJFNNNDL_11; }
	inline float* get_address_of_NFCIJFNNNDL_11() { return &___NFCIJFNNNDL_11; }
	inline void set_NFCIJFNNNDL_11(float value)
	{
		___NFCIJFNNNDL_11 = value;
	}

	inline static int32_t get_offset_of_LPLIBMIPMFD_13() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807, ___LPLIBMIPMFD_13)); }
	inline bool get_LPLIBMIPMFD_13() const { return ___LPLIBMIPMFD_13; }
	inline bool* get_address_of_LPLIBMIPMFD_13() { return &___LPLIBMIPMFD_13; }
	inline void set_LPLIBMIPMFD_13(bool value)
	{
		___LPLIBMIPMFD_13 = value;
	}

	inline static int32_t get_offset_of_PLILMNHLFED_14() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807, ___PLILMNHLFED_14)); }
	inline float get_PLILMNHLFED_14() const { return ___PLILMNHLFED_14; }
	inline float* get_address_of_PLILMNHLFED_14() { return &___PLILMNHLFED_14; }
	inline void set_PLILMNHLFED_14(float value)
	{
		___PLILMNHLFED_14 = value;
	}

	inline static int32_t get_offset_of_CKAAFMKPDBO_16() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807, ___CKAAFMKPDBO_16)); }
	inline bool get_CKAAFMKPDBO_16() const { return ___CKAAFMKPDBO_16; }
	inline bool* get_address_of_CKAAFMKPDBO_16() { return &___CKAAFMKPDBO_16; }
	inline void set_CKAAFMKPDBO_16(bool value)
	{
		___CKAAFMKPDBO_16 = value;
	}

	inline static int32_t get_offset_of_FFPNCMDJLGP_17() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807, ___FFPNCMDJLGP_17)); }
	inline float get_FFPNCMDJLGP_17() const { return ___FFPNCMDJLGP_17; }
	inline float* get_address_of_FFPNCMDJLGP_17() { return &___FFPNCMDJLGP_17; }
	inline void set_FFPNCMDJLGP_17(float value)
	{
		___FFPNCMDJLGP_17 = value;
	}

	inline static int32_t get_offset_of_BABCKJCECDJ_18() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807, ___BABCKJCECDJ_18)); }
	inline int32_t get_BABCKJCECDJ_18() const { return ___BABCKJCECDJ_18; }
	inline int32_t* get_address_of_BABCKJCECDJ_18() { return &___BABCKJCECDJ_18; }
	inline void set_BABCKJCECDJ_18(int32_t value)
	{
		___BABCKJCECDJ_18 = value;
	}
};

struct OrientationEvent_t2038376807_StaticFields
{
public:
	// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::CACHHOCBHAE
	OrientationEvent_t2038376807 * ___CACHHOCBHAE_0;
	// System.String[] proto.PhoneEvent/Types/OrientationEvent::ICMJABHGNBO
	StringU5BU5D_t1642385972* ___ICMJABHGNBO_1;
	// System.UInt32[] proto.PhoneEvent/Types/OrientationEvent::EMDLBCKFOML
	UInt32U5BU5D_t59386216* ___EMDLBCKFOML_2;

public:
	inline static int32_t get_offset_of_CACHHOCBHAE_0() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807_StaticFields, ___CACHHOCBHAE_0)); }
	inline OrientationEvent_t2038376807 * get_CACHHOCBHAE_0() const { return ___CACHHOCBHAE_0; }
	inline OrientationEvent_t2038376807 ** get_address_of_CACHHOCBHAE_0() { return &___CACHHOCBHAE_0; }
	inline void set_CACHHOCBHAE_0(OrientationEvent_t2038376807 * value)
	{
		___CACHHOCBHAE_0 = value;
		Il2CppCodeGenWriteBarrier(&___CACHHOCBHAE_0, value);
	}

	inline static int32_t get_offset_of_ICMJABHGNBO_1() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807_StaticFields, ___ICMJABHGNBO_1)); }
	inline StringU5BU5D_t1642385972* get_ICMJABHGNBO_1() const { return ___ICMJABHGNBO_1; }
	inline StringU5BU5D_t1642385972** get_address_of_ICMJABHGNBO_1() { return &___ICMJABHGNBO_1; }
	inline void set_ICMJABHGNBO_1(StringU5BU5D_t1642385972* value)
	{
		___ICMJABHGNBO_1 = value;
		Il2CppCodeGenWriteBarrier(&___ICMJABHGNBO_1, value);
	}

	inline static int32_t get_offset_of_EMDLBCKFOML_2() { return static_cast<int32_t>(offsetof(OrientationEvent_t2038376807_StaticFields, ___EMDLBCKFOML_2)); }
	inline UInt32U5BU5D_t59386216* get_EMDLBCKFOML_2() const { return ___EMDLBCKFOML_2; }
	inline UInt32U5BU5D_t59386216** get_address_of_EMDLBCKFOML_2() { return &___EMDLBCKFOML_2; }
	inline void set_EMDLBCKFOML_2(UInt32U5BU5D_t59386216* value)
	{
		___EMDLBCKFOML_2 = value;
		Il2CppCodeGenWriteBarrier(&___EMDLBCKFOML_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
