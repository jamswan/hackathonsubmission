﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SwitchVideos/<SetActiveDelayed>c__Iterator0
struct  U3CSetActiveDelayedU3Ec__Iterator0_t531920552  : public Il2CppObject
{
public:
	// UnityEngine.GameObject SwitchVideos/<SetActiveDelayed>c__Iterator0::BIDNFMIKBOK
	GameObject_t1756533147 * ___BIDNFMIKBOK_0;
	// System.Boolean SwitchVideos/<SetActiveDelayed>c__Iterator0::CPLHMCELOLA
	bool ___CPLHMCELOLA_1;
	// System.Object SwitchVideos/<SetActiveDelayed>c__Iterator0::LGBFNMECDHC
	Il2CppObject * ___LGBFNMECDHC_2;
	// System.Boolean SwitchVideos/<SetActiveDelayed>c__Iterator0::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_3;
	// System.Int32 SwitchVideos/<SetActiveDelayed>c__Iterator0::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_4;

public:
	inline static int32_t get_offset_of_BIDNFMIKBOK_0() { return static_cast<int32_t>(offsetof(U3CSetActiveDelayedU3Ec__Iterator0_t531920552, ___BIDNFMIKBOK_0)); }
	inline GameObject_t1756533147 * get_BIDNFMIKBOK_0() const { return ___BIDNFMIKBOK_0; }
	inline GameObject_t1756533147 ** get_address_of_BIDNFMIKBOK_0() { return &___BIDNFMIKBOK_0; }
	inline void set_BIDNFMIKBOK_0(GameObject_t1756533147 * value)
	{
		___BIDNFMIKBOK_0 = value;
		Il2CppCodeGenWriteBarrier(&___BIDNFMIKBOK_0, value);
	}

	inline static int32_t get_offset_of_CPLHMCELOLA_1() { return static_cast<int32_t>(offsetof(U3CSetActiveDelayedU3Ec__Iterator0_t531920552, ___CPLHMCELOLA_1)); }
	inline bool get_CPLHMCELOLA_1() const { return ___CPLHMCELOLA_1; }
	inline bool* get_address_of_CPLHMCELOLA_1() { return &___CPLHMCELOLA_1; }
	inline void set_CPLHMCELOLA_1(bool value)
	{
		___CPLHMCELOLA_1 = value;
	}

	inline static int32_t get_offset_of_LGBFNMECDHC_2() { return static_cast<int32_t>(offsetof(U3CSetActiveDelayedU3Ec__Iterator0_t531920552, ___LGBFNMECDHC_2)); }
	inline Il2CppObject * get_LGBFNMECDHC_2() const { return ___LGBFNMECDHC_2; }
	inline Il2CppObject ** get_address_of_LGBFNMECDHC_2() { return &___LGBFNMECDHC_2; }
	inline void set_LGBFNMECDHC_2(Il2CppObject * value)
	{
		___LGBFNMECDHC_2 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_2, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_3() { return static_cast<int32_t>(offsetof(U3CSetActiveDelayedU3Ec__Iterator0_t531920552, ___IMAGLIMLPFK_3)); }
	inline bool get_IMAGLIMLPFK_3() const { return ___IMAGLIMLPFK_3; }
	inline bool* get_address_of_IMAGLIMLPFK_3() { return &___IMAGLIMLPFK_3; }
	inline void set_IMAGLIMLPFK_3(bool value)
	{
		___IMAGLIMLPFK_3 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_4() { return static_cast<int32_t>(offsetof(U3CSetActiveDelayedU3Ec__Iterator0_t531920552, ___EPCFNNGDBGC_4)); }
	inline int32_t get_EPCFNNGDBGC_4() const { return ___EPCFNNGDBGC_4; }
	inline int32_t* get_address_of_EPCFNNGDBGC_4() { return &___EPCFNNGDBGC_4; }
	inline void set_EPCFNNGDBGC_4(int32_t value)
	{
		___EPCFNNGDBGC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
