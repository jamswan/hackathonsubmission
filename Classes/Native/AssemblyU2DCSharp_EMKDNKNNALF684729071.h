﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EMKDNKNNALF
struct  EMKDNKNNALF_t684729071  : public Il2CppObject
{
public:
	// System.String EMKDNKNNALF::COLCJHPAGJN
	String_t* ___COLCJHPAGJN_0;
	// System.Single EMKDNKNNALF::GAEKKNANIAP
	float ___GAEKKNANIAP_1;
	// System.Boolean EMKDNKNNALF::OCKNMJJGPAM
	bool ___OCKNMJJGPAM_2;
	// System.Action EMKDNKNNALF::NKNKLLNLJOI
	Action_t3226471752 * ___NKNKLLNLJOI_3;
	// System.Single EMKDNKNNALF::HPJMAGFMLBO
	float ___HPJMAGFMLBO_4;
	// System.Single EMKDNKNNALF::KLIMLJCLJNH
	float ___KLIMLJCLJNH_5;
	// System.Single EMKDNKNNALF::JPOCLFJIIMD
	float ___JPOCLFJIIMD_6;
	// System.Single EMKDNKNNALF::DAEHOJHKAAL
	float ___DAEHOJHKAAL_7;

public:
	inline static int32_t get_offset_of_COLCJHPAGJN_0() { return static_cast<int32_t>(offsetof(EMKDNKNNALF_t684729071, ___COLCJHPAGJN_0)); }
	inline String_t* get_COLCJHPAGJN_0() const { return ___COLCJHPAGJN_0; }
	inline String_t** get_address_of_COLCJHPAGJN_0() { return &___COLCJHPAGJN_0; }
	inline void set_COLCJHPAGJN_0(String_t* value)
	{
		___COLCJHPAGJN_0 = value;
		Il2CppCodeGenWriteBarrier(&___COLCJHPAGJN_0, value);
	}

	inline static int32_t get_offset_of_GAEKKNANIAP_1() { return static_cast<int32_t>(offsetof(EMKDNKNNALF_t684729071, ___GAEKKNANIAP_1)); }
	inline float get_GAEKKNANIAP_1() const { return ___GAEKKNANIAP_1; }
	inline float* get_address_of_GAEKKNANIAP_1() { return &___GAEKKNANIAP_1; }
	inline void set_GAEKKNANIAP_1(float value)
	{
		___GAEKKNANIAP_1 = value;
	}

	inline static int32_t get_offset_of_OCKNMJJGPAM_2() { return static_cast<int32_t>(offsetof(EMKDNKNNALF_t684729071, ___OCKNMJJGPAM_2)); }
	inline bool get_OCKNMJJGPAM_2() const { return ___OCKNMJJGPAM_2; }
	inline bool* get_address_of_OCKNMJJGPAM_2() { return &___OCKNMJJGPAM_2; }
	inline void set_OCKNMJJGPAM_2(bool value)
	{
		___OCKNMJJGPAM_2 = value;
	}

	inline static int32_t get_offset_of_NKNKLLNLJOI_3() { return static_cast<int32_t>(offsetof(EMKDNKNNALF_t684729071, ___NKNKLLNLJOI_3)); }
	inline Action_t3226471752 * get_NKNKLLNLJOI_3() const { return ___NKNKLLNLJOI_3; }
	inline Action_t3226471752 ** get_address_of_NKNKLLNLJOI_3() { return &___NKNKLLNLJOI_3; }
	inline void set_NKNKLLNLJOI_3(Action_t3226471752 * value)
	{
		___NKNKLLNLJOI_3 = value;
		Il2CppCodeGenWriteBarrier(&___NKNKLLNLJOI_3, value);
	}

	inline static int32_t get_offset_of_HPJMAGFMLBO_4() { return static_cast<int32_t>(offsetof(EMKDNKNNALF_t684729071, ___HPJMAGFMLBO_4)); }
	inline float get_HPJMAGFMLBO_4() const { return ___HPJMAGFMLBO_4; }
	inline float* get_address_of_HPJMAGFMLBO_4() { return &___HPJMAGFMLBO_4; }
	inline void set_HPJMAGFMLBO_4(float value)
	{
		___HPJMAGFMLBO_4 = value;
	}

	inline static int32_t get_offset_of_KLIMLJCLJNH_5() { return static_cast<int32_t>(offsetof(EMKDNKNNALF_t684729071, ___KLIMLJCLJNH_5)); }
	inline float get_KLIMLJCLJNH_5() const { return ___KLIMLJCLJNH_5; }
	inline float* get_address_of_KLIMLJCLJNH_5() { return &___KLIMLJCLJNH_5; }
	inline void set_KLIMLJCLJNH_5(float value)
	{
		___KLIMLJCLJNH_5 = value;
	}

	inline static int32_t get_offset_of_JPOCLFJIIMD_6() { return static_cast<int32_t>(offsetof(EMKDNKNNALF_t684729071, ___JPOCLFJIIMD_6)); }
	inline float get_JPOCLFJIIMD_6() const { return ___JPOCLFJIIMD_6; }
	inline float* get_address_of_JPOCLFJIIMD_6() { return &___JPOCLFJIIMD_6; }
	inline void set_JPOCLFJIIMD_6(float value)
	{
		___JPOCLFJIIMD_6 = value;
	}

	inline static int32_t get_offset_of_DAEHOJHKAAL_7() { return static_cast<int32_t>(offsetof(EMKDNKNNALF_t684729071, ___DAEHOJHKAAL_7)); }
	inline float get_DAEHOJHKAAL_7() const { return ___DAEHOJHKAAL_7; }
	inline float* get_address_of_DAEHOJHKAAL_7() { return &___DAEHOJHKAAL_7; }
	inline void set_DAEHOJHKAAL_7(float value)
	{
		___DAEHOJHKAAL_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
