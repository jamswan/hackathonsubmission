﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_NDBLEKFJACN774508329.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_MHDNMHAPLO1088781421.h"

// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2724090252;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// UnityEngine.Renderer
struct Renderer_t257310565;
// System.Collections.Generic.List`1<System.Action`1<System.Int32>>
struct List_1_t1242797962;
// System.Collections.Generic.List`1<System.Action`2<System.String,System.String>>
struct List_1_t3603663057;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t3046128587;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;
// GvrVideoPlayerTexture/NNKMLFAKIHO
struct NNKMLFAKIHO_t2285959827;
// GvrVideoPlayerTexture/PBCECLKMLFI
struct PBCECLKMLFI_t589383309;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture
struct  GvrVideoPlayerTexture_t673526704  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture2D[] GvrVideoPlayerTexture::DCFCBLKFOBG
	Texture2DU5BU5D_t2724090252* ___DCFCBLKFOBG_4;
	// System.Int32 GvrVideoPlayerTexture::OCBIFHOKFFG
	int32_t ___OCBIFHOKFFG_5;
	// System.IntPtr GvrVideoPlayerTexture::MKEKMCMGIEI
	IntPtr_t ___MKEKMCMGIEI_6;
	// System.Int32 GvrVideoPlayerTexture::JMJBAFOGOLA
	int32_t ___JMJBAFOGOLA_7;
	// UnityEngine.Texture GvrVideoPlayerTexture::FFONIJKIJIL
	Texture_t2243626319 * ___FFONIJKIJIL_8;
	// System.Boolean GvrVideoPlayerTexture::EJOBGGJEDCF
	bool ___EJOBGGJEDCF_9;
	// System.Int32 GvrVideoPlayerTexture::NJCNGDNFKBG
	int32_t ___NJCNGDNFKBG_10;
	// System.Int32 GvrVideoPlayerTexture::DOIPEKBDDBA
	int32_t ___DOIPEKBDDBA_11;
	// System.Int64 GvrVideoPlayerTexture::EDBCILNODMO
	int64_t ___EDBCILNODMO_12;
	// System.Single GvrVideoPlayerTexture::LIJNLNFDPKD
	float ___LIJNLNFDPKD_13;
	// UnityEngine.UI.Graphic GvrVideoPlayerTexture::ANPAPPHLJAN
	Graphic_t2426225576 * ___ANPAPPHLJAN_14;
	// UnityEngine.Renderer GvrVideoPlayerTexture::KFODGKJCGDB
	Renderer_t257310565 * ___KFODGKJCGDB_15;
	// System.IntPtr GvrVideoPlayerTexture::LBLCFHNPMPJ
	IntPtr_t ___LBLCFHNPMPJ_16;
	// System.Boolean GvrVideoPlayerTexture::HLHFHDJEKLO
	bool ___HLHFHDJEKLO_17;
	// System.Boolean GvrVideoPlayerTexture::GMKILJLIEAP
	bool ___GMKILJLIEAP_18;
	// System.Collections.Generic.List`1<System.Action`1<System.Int32>> GvrVideoPlayerTexture::CGPOGMLIGNB
	List_1_t1242797962 * ___CGPOGMLIGNB_19;
	// System.Collections.Generic.List`1<System.Action`2<System.String,System.String>> GvrVideoPlayerTexture::PBBBBJELCGJ
	List_1_t3603663057 * ___PBBBBJELCGJ_20;
	// UnityEngine.UI.Text GvrVideoPlayerTexture::statusText
	Text_t356221433 * ___statusText_22;
	// System.Int32 GvrVideoPlayerTexture::bufferSize
	int32_t ___bufferSize_23;
	// GvrVideoPlayerTexture/NDBLEKFJACN GvrVideoPlayerTexture::videoType
	int32_t ___videoType_24;
	// System.String GvrVideoPlayerTexture::videoURL
	String_t* ___videoURL_25;
	// System.String GvrVideoPlayerTexture::videoContentID
	String_t* ___videoContentID_26;
	// System.String GvrVideoPlayerTexture::videoProviderId
	String_t* ___videoProviderId_27;
	// GvrVideoPlayerTexture/MHDNMHAPLOB GvrVideoPlayerTexture::initialResolution
	int32_t ___initialResolution_28;
	// System.Boolean GvrVideoPlayerTexture::adjustAspectRatio
	bool ___adjustAspectRatio_29;
	// System.Boolean GvrVideoPlayerTexture::useSecurePath
	bool ___useSecurePath_30;

public:
	inline static int32_t get_offset_of_DCFCBLKFOBG_4() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___DCFCBLKFOBG_4)); }
	inline Texture2DU5BU5D_t2724090252* get_DCFCBLKFOBG_4() const { return ___DCFCBLKFOBG_4; }
	inline Texture2DU5BU5D_t2724090252** get_address_of_DCFCBLKFOBG_4() { return &___DCFCBLKFOBG_4; }
	inline void set_DCFCBLKFOBG_4(Texture2DU5BU5D_t2724090252* value)
	{
		___DCFCBLKFOBG_4 = value;
		Il2CppCodeGenWriteBarrier(&___DCFCBLKFOBG_4, value);
	}

	inline static int32_t get_offset_of_OCBIFHOKFFG_5() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___OCBIFHOKFFG_5)); }
	inline int32_t get_OCBIFHOKFFG_5() const { return ___OCBIFHOKFFG_5; }
	inline int32_t* get_address_of_OCBIFHOKFFG_5() { return &___OCBIFHOKFFG_5; }
	inline void set_OCBIFHOKFFG_5(int32_t value)
	{
		___OCBIFHOKFFG_5 = value;
	}

	inline static int32_t get_offset_of_MKEKMCMGIEI_6() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___MKEKMCMGIEI_6)); }
	inline IntPtr_t get_MKEKMCMGIEI_6() const { return ___MKEKMCMGIEI_6; }
	inline IntPtr_t* get_address_of_MKEKMCMGIEI_6() { return &___MKEKMCMGIEI_6; }
	inline void set_MKEKMCMGIEI_6(IntPtr_t value)
	{
		___MKEKMCMGIEI_6 = value;
	}

	inline static int32_t get_offset_of_JMJBAFOGOLA_7() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___JMJBAFOGOLA_7)); }
	inline int32_t get_JMJBAFOGOLA_7() const { return ___JMJBAFOGOLA_7; }
	inline int32_t* get_address_of_JMJBAFOGOLA_7() { return &___JMJBAFOGOLA_7; }
	inline void set_JMJBAFOGOLA_7(int32_t value)
	{
		___JMJBAFOGOLA_7 = value;
	}

	inline static int32_t get_offset_of_FFONIJKIJIL_8() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___FFONIJKIJIL_8)); }
	inline Texture_t2243626319 * get_FFONIJKIJIL_8() const { return ___FFONIJKIJIL_8; }
	inline Texture_t2243626319 ** get_address_of_FFONIJKIJIL_8() { return &___FFONIJKIJIL_8; }
	inline void set_FFONIJKIJIL_8(Texture_t2243626319 * value)
	{
		___FFONIJKIJIL_8 = value;
		Il2CppCodeGenWriteBarrier(&___FFONIJKIJIL_8, value);
	}

	inline static int32_t get_offset_of_EJOBGGJEDCF_9() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___EJOBGGJEDCF_9)); }
	inline bool get_EJOBGGJEDCF_9() const { return ___EJOBGGJEDCF_9; }
	inline bool* get_address_of_EJOBGGJEDCF_9() { return &___EJOBGGJEDCF_9; }
	inline void set_EJOBGGJEDCF_9(bool value)
	{
		___EJOBGGJEDCF_9 = value;
	}

	inline static int32_t get_offset_of_NJCNGDNFKBG_10() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___NJCNGDNFKBG_10)); }
	inline int32_t get_NJCNGDNFKBG_10() const { return ___NJCNGDNFKBG_10; }
	inline int32_t* get_address_of_NJCNGDNFKBG_10() { return &___NJCNGDNFKBG_10; }
	inline void set_NJCNGDNFKBG_10(int32_t value)
	{
		___NJCNGDNFKBG_10 = value;
	}

	inline static int32_t get_offset_of_DOIPEKBDDBA_11() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___DOIPEKBDDBA_11)); }
	inline int32_t get_DOIPEKBDDBA_11() const { return ___DOIPEKBDDBA_11; }
	inline int32_t* get_address_of_DOIPEKBDDBA_11() { return &___DOIPEKBDDBA_11; }
	inline void set_DOIPEKBDDBA_11(int32_t value)
	{
		___DOIPEKBDDBA_11 = value;
	}

	inline static int32_t get_offset_of_EDBCILNODMO_12() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___EDBCILNODMO_12)); }
	inline int64_t get_EDBCILNODMO_12() const { return ___EDBCILNODMO_12; }
	inline int64_t* get_address_of_EDBCILNODMO_12() { return &___EDBCILNODMO_12; }
	inline void set_EDBCILNODMO_12(int64_t value)
	{
		___EDBCILNODMO_12 = value;
	}

	inline static int32_t get_offset_of_LIJNLNFDPKD_13() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___LIJNLNFDPKD_13)); }
	inline float get_LIJNLNFDPKD_13() const { return ___LIJNLNFDPKD_13; }
	inline float* get_address_of_LIJNLNFDPKD_13() { return &___LIJNLNFDPKD_13; }
	inline void set_LIJNLNFDPKD_13(float value)
	{
		___LIJNLNFDPKD_13 = value;
	}

	inline static int32_t get_offset_of_ANPAPPHLJAN_14() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___ANPAPPHLJAN_14)); }
	inline Graphic_t2426225576 * get_ANPAPPHLJAN_14() const { return ___ANPAPPHLJAN_14; }
	inline Graphic_t2426225576 ** get_address_of_ANPAPPHLJAN_14() { return &___ANPAPPHLJAN_14; }
	inline void set_ANPAPPHLJAN_14(Graphic_t2426225576 * value)
	{
		___ANPAPPHLJAN_14 = value;
		Il2CppCodeGenWriteBarrier(&___ANPAPPHLJAN_14, value);
	}

	inline static int32_t get_offset_of_KFODGKJCGDB_15() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___KFODGKJCGDB_15)); }
	inline Renderer_t257310565 * get_KFODGKJCGDB_15() const { return ___KFODGKJCGDB_15; }
	inline Renderer_t257310565 ** get_address_of_KFODGKJCGDB_15() { return &___KFODGKJCGDB_15; }
	inline void set_KFODGKJCGDB_15(Renderer_t257310565 * value)
	{
		___KFODGKJCGDB_15 = value;
		Il2CppCodeGenWriteBarrier(&___KFODGKJCGDB_15, value);
	}

	inline static int32_t get_offset_of_LBLCFHNPMPJ_16() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___LBLCFHNPMPJ_16)); }
	inline IntPtr_t get_LBLCFHNPMPJ_16() const { return ___LBLCFHNPMPJ_16; }
	inline IntPtr_t* get_address_of_LBLCFHNPMPJ_16() { return &___LBLCFHNPMPJ_16; }
	inline void set_LBLCFHNPMPJ_16(IntPtr_t value)
	{
		___LBLCFHNPMPJ_16 = value;
	}

	inline static int32_t get_offset_of_HLHFHDJEKLO_17() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___HLHFHDJEKLO_17)); }
	inline bool get_HLHFHDJEKLO_17() const { return ___HLHFHDJEKLO_17; }
	inline bool* get_address_of_HLHFHDJEKLO_17() { return &___HLHFHDJEKLO_17; }
	inline void set_HLHFHDJEKLO_17(bool value)
	{
		___HLHFHDJEKLO_17 = value;
	}

	inline static int32_t get_offset_of_GMKILJLIEAP_18() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___GMKILJLIEAP_18)); }
	inline bool get_GMKILJLIEAP_18() const { return ___GMKILJLIEAP_18; }
	inline bool* get_address_of_GMKILJLIEAP_18() { return &___GMKILJLIEAP_18; }
	inline void set_GMKILJLIEAP_18(bool value)
	{
		___GMKILJLIEAP_18 = value;
	}

	inline static int32_t get_offset_of_CGPOGMLIGNB_19() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___CGPOGMLIGNB_19)); }
	inline List_1_t1242797962 * get_CGPOGMLIGNB_19() const { return ___CGPOGMLIGNB_19; }
	inline List_1_t1242797962 ** get_address_of_CGPOGMLIGNB_19() { return &___CGPOGMLIGNB_19; }
	inline void set_CGPOGMLIGNB_19(List_1_t1242797962 * value)
	{
		___CGPOGMLIGNB_19 = value;
		Il2CppCodeGenWriteBarrier(&___CGPOGMLIGNB_19, value);
	}

	inline static int32_t get_offset_of_PBBBBJELCGJ_20() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___PBBBBJELCGJ_20)); }
	inline List_1_t3603663057 * get_PBBBBJELCGJ_20() const { return ___PBBBBJELCGJ_20; }
	inline List_1_t3603663057 ** get_address_of_PBBBBJELCGJ_20() { return &___PBBBBJELCGJ_20; }
	inline void set_PBBBBJELCGJ_20(List_1_t3603663057 * value)
	{
		___PBBBBJELCGJ_20 = value;
		Il2CppCodeGenWriteBarrier(&___PBBBBJELCGJ_20, value);
	}

	inline static int32_t get_offset_of_statusText_22() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___statusText_22)); }
	inline Text_t356221433 * get_statusText_22() const { return ___statusText_22; }
	inline Text_t356221433 ** get_address_of_statusText_22() { return &___statusText_22; }
	inline void set_statusText_22(Text_t356221433 * value)
	{
		___statusText_22 = value;
		Il2CppCodeGenWriteBarrier(&___statusText_22, value);
	}

	inline static int32_t get_offset_of_bufferSize_23() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___bufferSize_23)); }
	inline int32_t get_bufferSize_23() const { return ___bufferSize_23; }
	inline int32_t* get_address_of_bufferSize_23() { return &___bufferSize_23; }
	inline void set_bufferSize_23(int32_t value)
	{
		___bufferSize_23 = value;
	}

	inline static int32_t get_offset_of_videoType_24() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___videoType_24)); }
	inline int32_t get_videoType_24() const { return ___videoType_24; }
	inline int32_t* get_address_of_videoType_24() { return &___videoType_24; }
	inline void set_videoType_24(int32_t value)
	{
		___videoType_24 = value;
	}

	inline static int32_t get_offset_of_videoURL_25() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___videoURL_25)); }
	inline String_t* get_videoURL_25() const { return ___videoURL_25; }
	inline String_t** get_address_of_videoURL_25() { return &___videoURL_25; }
	inline void set_videoURL_25(String_t* value)
	{
		___videoURL_25 = value;
		Il2CppCodeGenWriteBarrier(&___videoURL_25, value);
	}

	inline static int32_t get_offset_of_videoContentID_26() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___videoContentID_26)); }
	inline String_t* get_videoContentID_26() const { return ___videoContentID_26; }
	inline String_t** get_address_of_videoContentID_26() { return &___videoContentID_26; }
	inline void set_videoContentID_26(String_t* value)
	{
		___videoContentID_26 = value;
		Il2CppCodeGenWriteBarrier(&___videoContentID_26, value);
	}

	inline static int32_t get_offset_of_videoProviderId_27() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___videoProviderId_27)); }
	inline String_t* get_videoProviderId_27() const { return ___videoProviderId_27; }
	inline String_t** get_address_of_videoProviderId_27() { return &___videoProviderId_27; }
	inline void set_videoProviderId_27(String_t* value)
	{
		___videoProviderId_27 = value;
		Il2CppCodeGenWriteBarrier(&___videoProviderId_27, value);
	}

	inline static int32_t get_offset_of_initialResolution_28() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___initialResolution_28)); }
	inline int32_t get_initialResolution_28() const { return ___initialResolution_28; }
	inline int32_t* get_address_of_initialResolution_28() { return &___initialResolution_28; }
	inline void set_initialResolution_28(int32_t value)
	{
		___initialResolution_28 = value;
	}

	inline static int32_t get_offset_of_adjustAspectRatio_29() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___adjustAspectRatio_29)); }
	inline bool get_adjustAspectRatio_29() const { return ___adjustAspectRatio_29; }
	inline bool* get_address_of_adjustAspectRatio_29() { return &___adjustAspectRatio_29; }
	inline void set_adjustAspectRatio_29(bool value)
	{
		___adjustAspectRatio_29 = value;
	}

	inline static int32_t get_offset_of_useSecurePath_30() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704, ___useSecurePath_30)); }
	inline bool get_useSecurePath_30() const { return ___useSecurePath_30; }
	inline bool* get_address_of_useSecurePath_30() { return &___useSecurePath_30; }
	inline void set_useSecurePath_30(bool value)
	{
		___useSecurePath_30 = value;
	}
};

struct GvrVideoPlayerTexture_t673526704_StaticFields
{
public:
	// System.Collections.Generic.Queue`1<System.Action> GvrVideoPlayerTexture::HMAKNFMDNGJ
	Queue_1_t3046128587 * ___HMAKNFMDNGJ_21;
	// System.Action`2<System.String,System.String> GvrVideoPlayerTexture::<>f__am$cache0
	Action_2_t4234541925 * ___U3CU3Ef__amU24cache0_32;
	// GvrVideoPlayerTexture/NNKMLFAKIHO GvrVideoPlayerTexture::<>f__mg$cache0
	NNKMLFAKIHO_t2285959827 * ___U3CU3Ef__mgU24cache0_33;
	// GvrVideoPlayerTexture/PBCECLKMLFI GvrVideoPlayerTexture::<>f__mg$cache1
	PBCECLKMLFI_t589383309 * ___U3CU3Ef__mgU24cache1_34;

public:
	inline static int32_t get_offset_of_HMAKNFMDNGJ_21() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704_StaticFields, ___HMAKNFMDNGJ_21)); }
	inline Queue_1_t3046128587 * get_HMAKNFMDNGJ_21() const { return ___HMAKNFMDNGJ_21; }
	inline Queue_1_t3046128587 ** get_address_of_HMAKNFMDNGJ_21() { return &___HMAKNFMDNGJ_21; }
	inline void set_HMAKNFMDNGJ_21(Queue_1_t3046128587 * value)
	{
		___HMAKNFMDNGJ_21 = value;
		Il2CppCodeGenWriteBarrier(&___HMAKNFMDNGJ_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_32() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704_StaticFields, ___U3CU3Ef__amU24cache0_32)); }
	inline Action_2_t4234541925 * get_U3CU3Ef__amU24cache0_32() const { return ___U3CU3Ef__amU24cache0_32; }
	inline Action_2_t4234541925 ** get_address_of_U3CU3Ef__amU24cache0_32() { return &___U3CU3Ef__amU24cache0_32; }
	inline void set_U3CU3Ef__amU24cache0_32(Action_2_t4234541925 * value)
	{
		___U3CU3Ef__amU24cache0_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_32, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_33() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704_StaticFields, ___U3CU3Ef__mgU24cache0_33)); }
	inline NNKMLFAKIHO_t2285959827 * get_U3CU3Ef__mgU24cache0_33() const { return ___U3CU3Ef__mgU24cache0_33; }
	inline NNKMLFAKIHO_t2285959827 ** get_address_of_U3CU3Ef__mgU24cache0_33() { return &___U3CU3Ef__mgU24cache0_33; }
	inline void set_U3CU3Ef__mgU24cache0_33(NNKMLFAKIHO_t2285959827 * value)
	{
		___U3CU3Ef__mgU24cache0_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_33, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_34() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t673526704_StaticFields, ___U3CU3Ef__mgU24cache1_34)); }
	inline PBCECLKMLFI_t589383309 * get_U3CU3Ef__mgU24cache1_34() const { return ___U3CU3Ef__mgU24cache1_34; }
	inline PBCECLKMLFI_t589383309 ** get_address_of_U3CU3Ef__mgU24cache1_34() { return &___U3CU3Ef__mgU24cache1_34; }
	inline void set_U3CU3Ef__mgU24cache1_34(PBCECLKMLFI_t589383309 * value)
	{
		___U3CU3Ef__mgU24cache1_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
