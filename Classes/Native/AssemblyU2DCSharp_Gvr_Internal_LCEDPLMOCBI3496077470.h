﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_Gvr_Internal_LCEDPLMOCBI_JBMAAKD2369530344.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.LCEDPLMOCBI
struct  LCEDPLMOCBI_t3496077470 
{
public:
	// Gvr.Internal.LCEDPLMOCBI/JBMAAKDFNJK Gvr.Internal.LCEDPLMOCBI::BAMGHCABHEK
	int32_t ___BAMGHCABHEK_0;
	// System.Boolean Gvr.Internal.LCEDPLMOCBI::BNBNGKONNKF
	bool ___BNBNGKONNKF_1;

public:
	inline static int32_t get_offset_of_BAMGHCABHEK_0() { return static_cast<int32_t>(offsetof(LCEDPLMOCBI_t3496077470, ___BAMGHCABHEK_0)); }
	inline int32_t get_BAMGHCABHEK_0() const { return ___BAMGHCABHEK_0; }
	inline int32_t* get_address_of_BAMGHCABHEK_0() { return &___BAMGHCABHEK_0; }
	inline void set_BAMGHCABHEK_0(int32_t value)
	{
		___BAMGHCABHEK_0 = value;
	}

	inline static int32_t get_offset_of_BNBNGKONNKF_1() { return static_cast<int32_t>(offsetof(LCEDPLMOCBI_t3496077470, ___BNBNGKONNKF_1)); }
	inline bool get_BNBNGKONNKF_1() const { return ___BNBNGKONNKF_1; }
	inline bool* get_address_of_BNBNGKONNKF_1() { return &___BNBNGKONNKF_1; }
	inline void set_BNBNGKONNKF_1(bool value)
	{
		___BNBNGKONNKF_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Gvr.Internal.LCEDPLMOCBI
struct LCEDPLMOCBI_t3496077470_marshaled_pinvoke
{
	int32_t ___BAMGHCABHEK_0;
	int32_t ___BNBNGKONNKF_1;
};
// Native definition for COM marshalling of Gvr.Internal.LCEDPLMOCBI
struct LCEDPLMOCBI_t3496077470_marshaled_com
{
	int32_t ___BAMGHCABHEK_0;
	int32_t ___BNBNGKONNKF_1;
};
