﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"

// CLNIKPGODAH
struct CLNIKPGODAH_t660851912;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BKNEONNPPOE
struct  BKNEONNPPOE_t4035756105  : public Il2CppObject
{
public:
	// CLNIKPGODAH BKNEONNPPOE::MCABJJDMKKD
	CLNIKPGODAH_t660851912 * ___MCABJJDMKKD_0;
	// System.DateTime BKNEONNPPOE::DHFJOEBHKKO
	DateTime_t693205669  ___DHFJOEBHKKO_1;
	// System.Nullable`1<System.DateTime> BKNEONNPPOE::IEGICCCHPID
	Nullable_1_t3251239280  ___IEGICCCHPID_2;

public:
	inline static int32_t get_offset_of_MCABJJDMKKD_0() { return static_cast<int32_t>(offsetof(BKNEONNPPOE_t4035756105, ___MCABJJDMKKD_0)); }
	inline CLNIKPGODAH_t660851912 * get_MCABJJDMKKD_0() const { return ___MCABJJDMKKD_0; }
	inline CLNIKPGODAH_t660851912 ** get_address_of_MCABJJDMKKD_0() { return &___MCABJJDMKKD_0; }
	inline void set_MCABJJDMKKD_0(CLNIKPGODAH_t660851912 * value)
	{
		___MCABJJDMKKD_0 = value;
		Il2CppCodeGenWriteBarrier(&___MCABJJDMKKD_0, value);
	}

	inline static int32_t get_offset_of_DHFJOEBHKKO_1() { return static_cast<int32_t>(offsetof(BKNEONNPPOE_t4035756105, ___DHFJOEBHKKO_1)); }
	inline DateTime_t693205669  get_DHFJOEBHKKO_1() const { return ___DHFJOEBHKKO_1; }
	inline DateTime_t693205669 * get_address_of_DHFJOEBHKKO_1() { return &___DHFJOEBHKKO_1; }
	inline void set_DHFJOEBHKKO_1(DateTime_t693205669  value)
	{
		___DHFJOEBHKKO_1 = value;
	}

	inline static int32_t get_offset_of_IEGICCCHPID_2() { return static_cast<int32_t>(offsetof(BKNEONNPPOE_t4035756105, ___IEGICCCHPID_2)); }
	inline Nullable_1_t3251239280  get_IEGICCCHPID_2() const { return ___IEGICCCHPID_2; }
	inline Nullable_1_t3251239280 * get_address_of_IEGICCCHPID_2() { return &___IEGICCCHPID_2; }
	inline void set_IEGICCCHPID_2(Nullable_1_t3251239280  value)
	{
		___IEGICCCHPID_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
