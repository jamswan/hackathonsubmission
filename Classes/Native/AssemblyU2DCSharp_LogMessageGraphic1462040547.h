﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LogMessageGraphic
struct  LogMessageGraphic_t1462040547  : public NarrativeEffect_t3336735121
{
public:
	// System.String LogMessageGraphic::logName
	String_t* ___logName_3;
	// System.String LogMessageGraphic::message
	String_t* ___message_4;
	// UnityEngine.Texture2D LogMessageGraphic::image
	Texture2D_t3542995729 * ___image_5;
	// System.String LogMessageGraphic::overrideStyleName
	String_t* ___overrideStyleName_6;
	// UnityEngine.GUIStyle LogMessageGraphic::style
	GUIStyle_t1799908754 * ___style_7;

public:
	inline static int32_t get_offset_of_logName_3() { return static_cast<int32_t>(offsetof(LogMessageGraphic_t1462040547, ___logName_3)); }
	inline String_t* get_logName_3() const { return ___logName_3; }
	inline String_t** get_address_of_logName_3() { return &___logName_3; }
	inline void set_logName_3(String_t* value)
	{
		___logName_3 = value;
		Il2CppCodeGenWriteBarrier(&___logName_3, value);
	}

	inline static int32_t get_offset_of_message_4() { return static_cast<int32_t>(offsetof(LogMessageGraphic_t1462040547, ___message_4)); }
	inline String_t* get_message_4() const { return ___message_4; }
	inline String_t** get_address_of_message_4() { return &___message_4; }
	inline void set_message_4(String_t* value)
	{
		___message_4 = value;
		Il2CppCodeGenWriteBarrier(&___message_4, value);
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(LogMessageGraphic_t1462040547, ___image_5)); }
	inline Texture2D_t3542995729 * get_image_5() const { return ___image_5; }
	inline Texture2D_t3542995729 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(Texture2D_t3542995729 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier(&___image_5, value);
	}

	inline static int32_t get_offset_of_overrideStyleName_6() { return static_cast<int32_t>(offsetof(LogMessageGraphic_t1462040547, ___overrideStyleName_6)); }
	inline String_t* get_overrideStyleName_6() const { return ___overrideStyleName_6; }
	inline String_t** get_address_of_overrideStyleName_6() { return &___overrideStyleName_6; }
	inline void set_overrideStyleName_6(String_t* value)
	{
		___overrideStyleName_6 = value;
		Il2CppCodeGenWriteBarrier(&___overrideStyleName_6, value);
	}

	inline static int32_t get_offset_of_style_7() { return static_cast<int32_t>(offsetof(LogMessageGraphic_t1462040547, ___style_7)); }
	inline GUIStyle_t1799908754 * get_style_7() const { return ___style_7; }
	inline GUIStyle_t1799908754 ** get_address_of_style_7() { return &___style_7; }
	inline void set_style_7(GUIStyle_t1799908754 * value)
	{
		___style_7 = value;
		Il2CppCodeGenWriteBarrier(&___style_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
