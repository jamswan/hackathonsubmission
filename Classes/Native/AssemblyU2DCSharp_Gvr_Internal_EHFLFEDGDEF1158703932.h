﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EHFLFEDGDEF
struct  EHFLFEDGDEF_t1158703932 
{
public:
	// System.Int64 Gvr.Internal.EHFLFEDGDEF::GJJAEACHFEK
	int64_t ___GJJAEACHFEK_0;
	// UnityEngine.Quaternion Gvr.Internal.EHFLFEDGDEF::PENFFOAGECN
	Quaternion_t4030073918  ___PENFFOAGECN_1;

public:
	inline static int32_t get_offset_of_GJJAEACHFEK_0() { return static_cast<int32_t>(offsetof(EHFLFEDGDEF_t1158703932, ___GJJAEACHFEK_0)); }
	inline int64_t get_GJJAEACHFEK_0() const { return ___GJJAEACHFEK_0; }
	inline int64_t* get_address_of_GJJAEACHFEK_0() { return &___GJJAEACHFEK_0; }
	inline void set_GJJAEACHFEK_0(int64_t value)
	{
		___GJJAEACHFEK_0 = value;
	}

	inline static int32_t get_offset_of_PENFFOAGECN_1() { return static_cast<int32_t>(offsetof(EHFLFEDGDEF_t1158703932, ___PENFFOAGECN_1)); }
	inline Quaternion_t4030073918  get_PENFFOAGECN_1() const { return ___PENFFOAGECN_1; }
	inline Quaternion_t4030073918 * get_address_of_PENFFOAGECN_1() { return &___PENFFOAGECN_1; }
	inline void set_PENFFOAGECN_1(Quaternion_t4030073918  value)
	{
		___PENFFOAGECN_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
