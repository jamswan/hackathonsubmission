﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Mono.Math.BigInteger
struct BigInteger_t925946153;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{F4520A45-7DF5-40CA-B093-225450909C22}.ACGNDKCFFOM
struct  ACGNDKCFFOM_t3033861487  : public Il2CppObject
{
public:

public:
};

struct ACGNDKCFFOM_t3033861487_StaticFields
{
public:
	// System.Byte[] <PrivateImplementationDetails>{F4520A45-7DF5-40CA-B093-225450909C22}.ACGNDKCFFOM::publicKey
	ByteU5BU5D_t3397334013* ___publicKey_0;
	// System.Int32 <PrivateImplementationDetails>{F4520A45-7DF5-40CA-B093-225450909C22}.ACGNDKCFFOM::blockLengthField
	int32_t ___blockLengthField_1;
	// System.Int32 <PrivateImplementationDetails>{F4520A45-7DF5-40CA-B093-225450909C22}.ACGNDKCFFOM::exponentField
	int32_t ___exponentField_2;
	// Mono.Math.BigInteger <PrivateImplementationDetails>{F4520A45-7DF5-40CA-B093-225450909C22}.ACGNDKCFFOM::nField
	BigInteger_t925946153 * ___nField_3;

public:
	inline static int32_t get_offset_of_publicKey_0() { return static_cast<int32_t>(offsetof(ACGNDKCFFOM_t3033861487_StaticFields, ___publicKey_0)); }
	inline ByteU5BU5D_t3397334013* get_publicKey_0() const { return ___publicKey_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_publicKey_0() { return &___publicKey_0; }
	inline void set_publicKey_0(ByteU5BU5D_t3397334013* value)
	{
		___publicKey_0 = value;
		Il2CppCodeGenWriteBarrier(&___publicKey_0, value);
	}

	inline static int32_t get_offset_of_blockLengthField_1() { return static_cast<int32_t>(offsetof(ACGNDKCFFOM_t3033861487_StaticFields, ___blockLengthField_1)); }
	inline int32_t get_blockLengthField_1() const { return ___blockLengthField_1; }
	inline int32_t* get_address_of_blockLengthField_1() { return &___blockLengthField_1; }
	inline void set_blockLengthField_1(int32_t value)
	{
		___blockLengthField_1 = value;
	}

	inline static int32_t get_offset_of_exponentField_2() { return static_cast<int32_t>(offsetof(ACGNDKCFFOM_t3033861487_StaticFields, ___exponentField_2)); }
	inline int32_t get_exponentField_2() const { return ___exponentField_2; }
	inline int32_t* get_address_of_exponentField_2() { return &___exponentField_2; }
	inline void set_exponentField_2(int32_t value)
	{
		___exponentField_2 = value;
	}

	inline static int32_t get_offset_of_nField_3() { return static_cast<int32_t>(offsetof(ACGNDKCFFOM_t3033861487_StaticFields, ___nField_3)); }
	inline BigInteger_t925946153 * get_nField_3() const { return ___nField_3; }
	inline BigInteger_t925946153 ** get_address_of_nField_3() { return &___nField_3; }
	inline void set_nField_3(BigInteger_t925946153 * value)
	{
		___nField_3 = value;
		Il2CppCodeGenWriteBarrier(&___nField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
