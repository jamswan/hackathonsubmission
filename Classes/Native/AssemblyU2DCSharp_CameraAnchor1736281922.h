﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ImageTargetEvent3520571492.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraAnchor
struct  CameraAnchor_t1736281922  : public ImageTargetEvent_t3520571492
{
public:
	// UnityEngine.Transform CameraAnchor::AOEDENOGCID
	Transform_t3275118058 * ___AOEDENOGCID_6;
	// UnityEngine.Transform CameraAnchor::KJLBEHOKPME
	Transform_t3275118058 * ___KJLBEHOKPME_7;
	// UnityEngine.Transform CameraAnchor::APBGPHHPIPG
	Transform_t3275118058 * ___APBGPHHPIPG_8;
	// UnityEngine.Vector3 CameraAnchor::IOCLKJMCCFJ
	Vector3_t2243707580  ___IOCLKJMCCFJ_9;
	// UnityEngine.GameObject CameraAnchor::BCGIHGKLLOO
	GameObject_t1756533147 * ___BCGIHGKLLOO_10;
	// UnityEngine.Vector3 CameraAnchor::GJOLLCHAIMA
	Vector3_t2243707580  ___GJOLLCHAIMA_11;
	// System.Single CameraAnchor::DMFCGLDFDCF
	float ___DMFCGLDFDCF_12;
	// System.Single CameraAnchor::NOCBDOLGJLP
	float ___NOCBDOLGJLP_13;

public:
	inline static int32_t get_offset_of_AOEDENOGCID_6() { return static_cast<int32_t>(offsetof(CameraAnchor_t1736281922, ___AOEDENOGCID_6)); }
	inline Transform_t3275118058 * get_AOEDENOGCID_6() const { return ___AOEDENOGCID_6; }
	inline Transform_t3275118058 ** get_address_of_AOEDENOGCID_6() { return &___AOEDENOGCID_6; }
	inline void set_AOEDENOGCID_6(Transform_t3275118058 * value)
	{
		___AOEDENOGCID_6 = value;
		Il2CppCodeGenWriteBarrier(&___AOEDENOGCID_6, value);
	}

	inline static int32_t get_offset_of_KJLBEHOKPME_7() { return static_cast<int32_t>(offsetof(CameraAnchor_t1736281922, ___KJLBEHOKPME_7)); }
	inline Transform_t3275118058 * get_KJLBEHOKPME_7() const { return ___KJLBEHOKPME_7; }
	inline Transform_t3275118058 ** get_address_of_KJLBEHOKPME_7() { return &___KJLBEHOKPME_7; }
	inline void set_KJLBEHOKPME_7(Transform_t3275118058 * value)
	{
		___KJLBEHOKPME_7 = value;
		Il2CppCodeGenWriteBarrier(&___KJLBEHOKPME_7, value);
	}

	inline static int32_t get_offset_of_APBGPHHPIPG_8() { return static_cast<int32_t>(offsetof(CameraAnchor_t1736281922, ___APBGPHHPIPG_8)); }
	inline Transform_t3275118058 * get_APBGPHHPIPG_8() const { return ___APBGPHHPIPG_8; }
	inline Transform_t3275118058 ** get_address_of_APBGPHHPIPG_8() { return &___APBGPHHPIPG_8; }
	inline void set_APBGPHHPIPG_8(Transform_t3275118058 * value)
	{
		___APBGPHHPIPG_8 = value;
		Il2CppCodeGenWriteBarrier(&___APBGPHHPIPG_8, value);
	}

	inline static int32_t get_offset_of_IOCLKJMCCFJ_9() { return static_cast<int32_t>(offsetof(CameraAnchor_t1736281922, ___IOCLKJMCCFJ_9)); }
	inline Vector3_t2243707580  get_IOCLKJMCCFJ_9() const { return ___IOCLKJMCCFJ_9; }
	inline Vector3_t2243707580 * get_address_of_IOCLKJMCCFJ_9() { return &___IOCLKJMCCFJ_9; }
	inline void set_IOCLKJMCCFJ_9(Vector3_t2243707580  value)
	{
		___IOCLKJMCCFJ_9 = value;
	}

	inline static int32_t get_offset_of_BCGIHGKLLOO_10() { return static_cast<int32_t>(offsetof(CameraAnchor_t1736281922, ___BCGIHGKLLOO_10)); }
	inline GameObject_t1756533147 * get_BCGIHGKLLOO_10() const { return ___BCGIHGKLLOO_10; }
	inline GameObject_t1756533147 ** get_address_of_BCGIHGKLLOO_10() { return &___BCGIHGKLLOO_10; }
	inline void set_BCGIHGKLLOO_10(GameObject_t1756533147 * value)
	{
		___BCGIHGKLLOO_10 = value;
		Il2CppCodeGenWriteBarrier(&___BCGIHGKLLOO_10, value);
	}

	inline static int32_t get_offset_of_GJOLLCHAIMA_11() { return static_cast<int32_t>(offsetof(CameraAnchor_t1736281922, ___GJOLLCHAIMA_11)); }
	inline Vector3_t2243707580  get_GJOLLCHAIMA_11() const { return ___GJOLLCHAIMA_11; }
	inline Vector3_t2243707580 * get_address_of_GJOLLCHAIMA_11() { return &___GJOLLCHAIMA_11; }
	inline void set_GJOLLCHAIMA_11(Vector3_t2243707580  value)
	{
		___GJOLLCHAIMA_11 = value;
	}

	inline static int32_t get_offset_of_DMFCGLDFDCF_12() { return static_cast<int32_t>(offsetof(CameraAnchor_t1736281922, ___DMFCGLDFDCF_12)); }
	inline float get_DMFCGLDFDCF_12() const { return ___DMFCGLDFDCF_12; }
	inline float* get_address_of_DMFCGLDFDCF_12() { return &___DMFCGLDFDCF_12; }
	inline void set_DMFCGLDFDCF_12(float value)
	{
		___DMFCGLDFDCF_12 = value;
	}

	inline static int32_t get_offset_of_NOCBDOLGJLP_13() { return static_cast<int32_t>(offsetof(CameraAnchor_t1736281922, ___NOCBDOLGJLP_13)); }
	inline float get_NOCBDOLGJLP_13() const { return ___NOCBDOLGJLP_13; }
	inline float* get_address_of_NOCBDOLGJLP_13() { return &___NOCBDOLGJLP_13; }
	inline void set_NOCBDOLGJLP_13(float value)
	{
		___NOCBDOLGJLP_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
