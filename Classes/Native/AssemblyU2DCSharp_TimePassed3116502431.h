﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimePassed
struct  TimePassed_t3116502431  : public NarrativeCondition_t4123859457
{
public:
	// System.Single TimePassed::duration
	float ___duration_13;

public:
	inline static int32_t get_offset_of_duration_13() { return static_cast<int32_t>(offsetof(TimePassed_t3116502431, ___duration_13)); }
	inline float get_duration_13() const { return ___duration_13; }
	inline float* get_address_of_duration_13() { return &___duration_13; }
	inline void set_duration_13(float value)
	{
		___duration_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
