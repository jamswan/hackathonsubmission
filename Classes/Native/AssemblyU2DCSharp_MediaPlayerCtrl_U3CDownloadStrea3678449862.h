﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// MediaPlayerCtrl
struct MediaPlayerCtrl_t1284484152;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0
struct  U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862  : public Il2CppObject
{
public:
	// System.String MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0::MGLLOMDMBGD
	String_t* ___MGLLOMDMBGD_0;
	// UnityEngine.WWW MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0::MNNMDEABAIC
	WWW_t2919945039 * ___MNNMDEABAIC_1;
	// MediaPlayerCtrl MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0::AOOLEAHHMIH
	MediaPlayerCtrl_t1284484152 * ___AOOLEAHHMIH_2;
	// System.Object MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0::LGBFNMECDHC
	Il2CppObject * ___LGBFNMECDHC_3;
	// System.Boolean MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_4;
	// System.Int32 MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_5;

public:
	inline static int32_t get_offset_of_MGLLOMDMBGD_0() { return static_cast<int32_t>(offsetof(U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862, ___MGLLOMDMBGD_0)); }
	inline String_t* get_MGLLOMDMBGD_0() const { return ___MGLLOMDMBGD_0; }
	inline String_t** get_address_of_MGLLOMDMBGD_0() { return &___MGLLOMDMBGD_0; }
	inline void set_MGLLOMDMBGD_0(String_t* value)
	{
		___MGLLOMDMBGD_0 = value;
		Il2CppCodeGenWriteBarrier(&___MGLLOMDMBGD_0, value);
	}

	inline static int32_t get_offset_of_MNNMDEABAIC_1() { return static_cast<int32_t>(offsetof(U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862, ___MNNMDEABAIC_1)); }
	inline WWW_t2919945039 * get_MNNMDEABAIC_1() const { return ___MNNMDEABAIC_1; }
	inline WWW_t2919945039 ** get_address_of_MNNMDEABAIC_1() { return &___MNNMDEABAIC_1; }
	inline void set_MNNMDEABAIC_1(WWW_t2919945039 * value)
	{
		___MNNMDEABAIC_1 = value;
		Il2CppCodeGenWriteBarrier(&___MNNMDEABAIC_1, value);
	}

	inline static int32_t get_offset_of_AOOLEAHHMIH_2() { return static_cast<int32_t>(offsetof(U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862, ___AOOLEAHHMIH_2)); }
	inline MediaPlayerCtrl_t1284484152 * get_AOOLEAHHMIH_2() const { return ___AOOLEAHHMIH_2; }
	inline MediaPlayerCtrl_t1284484152 ** get_address_of_AOOLEAHHMIH_2() { return &___AOOLEAHHMIH_2; }
	inline void set_AOOLEAHHMIH_2(MediaPlayerCtrl_t1284484152 * value)
	{
		___AOOLEAHHMIH_2 = value;
		Il2CppCodeGenWriteBarrier(&___AOOLEAHHMIH_2, value);
	}

	inline static int32_t get_offset_of_LGBFNMECDHC_3() { return static_cast<int32_t>(offsetof(U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862, ___LGBFNMECDHC_3)); }
	inline Il2CppObject * get_LGBFNMECDHC_3() const { return ___LGBFNMECDHC_3; }
	inline Il2CppObject ** get_address_of_LGBFNMECDHC_3() { return &___LGBFNMECDHC_3; }
	inline void set_LGBFNMECDHC_3(Il2CppObject * value)
	{
		___LGBFNMECDHC_3 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_3, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_4() { return static_cast<int32_t>(offsetof(U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862, ___IMAGLIMLPFK_4)); }
	inline bool get_IMAGLIMLPFK_4() const { return ___IMAGLIMLPFK_4; }
	inline bool* get_address_of_IMAGLIMLPFK_4() { return &___IMAGLIMLPFK_4; }
	inline void set_IMAGLIMLPFK_4(bool value)
	{
		___IMAGLIMLPFK_4 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_5() { return static_cast<int32_t>(offsetof(U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862, ___EPCFNNGDBGC_5)); }
	inline int32_t get_EPCFNNGDBGC_5() const { return ___EPCFNNGDBGC_5; }
	inline int32_t* get_address_of_EPCFNNGDBGC_5() { return &___EPCFNNGDBGC_5; }
	inline void set_EPCFNNGDBGC_5(int32_t value)
	{
		___EPCFNNGDBGC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
