﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"
#include "AssemblyU2DCSharp_CNKPOABMIJM702222687.h"

// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnButtonPressed
struct  OnButtonPressed_t3387707835  : public NarrativeCondition_t4123859457
{
public:
	// System.String OnButtonPressed::buttonText
	String_t* ___buttonText_13;
	// System.String OnButtonPressed::styleOverride
	String_t* ___styleOverride_14;
	// UnityEngine.GUIStyle OnButtonPressed::style
	GUIStyle_t1799908754 * ___style_15;
	// CNKPOABMIJM OnButtonPressed::alignment
	int32_t ___alignment_16;
	// System.Single OnButtonPressed::percentageXPos
	float ___percentageXPos_17;
	// System.Single OnButtonPressed::percentageYPos
	float ___percentageYPos_18;
	// System.Single OnButtonPressed::buttonScreenPercentSize
	float ___buttonScreenPercentSize_19;
	// System.Single OnButtonPressed::buttonMaxInchSize
	float ___buttonMaxInchSize_20;
	// UnityEngine.GUIStyle OnButtonPressed::OKFCAAHCBBI
	GUIStyle_t1799908754 * ___OKFCAAHCBBI_21;
	// System.Int32 OnButtonPressed::JHKJOGKJIPH
	int32_t ___JHKJOGKJIPH_22;
	// System.Int32 OnButtonPressed::JNLEFANLCNC
	int32_t ___JNLEFANLCNC_23;
	// System.Int32 OnButtonPressed::HPJMAGFMLBO
	int32_t ___HPJMAGFMLBO_24;
	// System.Int32 OnButtonPressed::KLIMLJCLJNH
	int32_t ___KLIMLJCLJNH_25;

public:
	inline static int32_t get_offset_of_buttonText_13() { return static_cast<int32_t>(offsetof(OnButtonPressed_t3387707835, ___buttonText_13)); }
	inline String_t* get_buttonText_13() const { return ___buttonText_13; }
	inline String_t** get_address_of_buttonText_13() { return &___buttonText_13; }
	inline void set_buttonText_13(String_t* value)
	{
		___buttonText_13 = value;
		Il2CppCodeGenWriteBarrier(&___buttonText_13, value);
	}

	inline static int32_t get_offset_of_styleOverride_14() { return static_cast<int32_t>(offsetof(OnButtonPressed_t3387707835, ___styleOverride_14)); }
	inline String_t* get_styleOverride_14() const { return ___styleOverride_14; }
	inline String_t** get_address_of_styleOverride_14() { return &___styleOverride_14; }
	inline void set_styleOverride_14(String_t* value)
	{
		___styleOverride_14 = value;
		Il2CppCodeGenWriteBarrier(&___styleOverride_14, value);
	}

	inline static int32_t get_offset_of_style_15() { return static_cast<int32_t>(offsetof(OnButtonPressed_t3387707835, ___style_15)); }
	inline GUIStyle_t1799908754 * get_style_15() const { return ___style_15; }
	inline GUIStyle_t1799908754 ** get_address_of_style_15() { return &___style_15; }
	inline void set_style_15(GUIStyle_t1799908754 * value)
	{
		___style_15 = value;
		Il2CppCodeGenWriteBarrier(&___style_15, value);
	}

	inline static int32_t get_offset_of_alignment_16() { return static_cast<int32_t>(offsetof(OnButtonPressed_t3387707835, ___alignment_16)); }
	inline int32_t get_alignment_16() const { return ___alignment_16; }
	inline int32_t* get_address_of_alignment_16() { return &___alignment_16; }
	inline void set_alignment_16(int32_t value)
	{
		___alignment_16 = value;
	}

	inline static int32_t get_offset_of_percentageXPos_17() { return static_cast<int32_t>(offsetof(OnButtonPressed_t3387707835, ___percentageXPos_17)); }
	inline float get_percentageXPos_17() const { return ___percentageXPos_17; }
	inline float* get_address_of_percentageXPos_17() { return &___percentageXPos_17; }
	inline void set_percentageXPos_17(float value)
	{
		___percentageXPos_17 = value;
	}

	inline static int32_t get_offset_of_percentageYPos_18() { return static_cast<int32_t>(offsetof(OnButtonPressed_t3387707835, ___percentageYPos_18)); }
	inline float get_percentageYPos_18() const { return ___percentageYPos_18; }
	inline float* get_address_of_percentageYPos_18() { return &___percentageYPos_18; }
	inline void set_percentageYPos_18(float value)
	{
		___percentageYPos_18 = value;
	}

	inline static int32_t get_offset_of_buttonScreenPercentSize_19() { return static_cast<int32_t>(offsetof(OnButtonPressed_t3387707835, ___buttonScreenPercentSize_19)); }
	inline float get_buttonScreenPercentSize_19() const { return ___buttonScreenPercentSize_19; }
	inline float* get_address_of_buttonScreenPercentSize_19() { return &___buttonScreenPercentSize_19; }
	inline void set_buttonScreenPercentSize_19(float value)
	{
		___buttonScreenPercentSize_19 = value;
	}

	inline static int32_t get_offset_of_buttonMaxInchSize_20() { return static_cast<int32_t>(offsetof(OnButtonPressed_t3387707835, ___buttonMaxInchSize_20)); }
	inline float get_buttonMaxInchSize_20() const { return ___buttonMaxInchSize_20; }
	inline float* get_address_of_buttonMaxInchSize_20() { return &___buttonMaxInchSize_20; }
	inline void set_buttonMaxInchSize_20(float value)
	{
		___buttonMaxInchSize_20 = value;
	}

	inline static int32_t get_offset_of_OKFCAAHCBBI_21() { return static_cast<int32_t>(offsetof(OnButtonPressed_t3387707835, ___OKFCAAHCBBI_21)); }
	inline GUIStyle_t1799908754 * get_OKFCAAHCBBI_21() const { return ___OKFCAAHCBBI_21; }
	inline GUIStyle_t1799908754 ** get_address_of_OKFCAAHCBBI_21() { return &___OKFCAAHCBBI_21; }
	inline void set_OKFCAAHCBBI_21(GUIStyle_t1799908754 * value)
	{
		___OKFCAAHCBBI_21 = value;
		Il2CppCodeGenWriteBarrier(&___OKFCAAHCBBI_21, value);
	}

	inline static int32_t get_offset_of_JHKJOGKJIPH_22() { return static_cast<int32_t>(offsetof(OnButtonPressed_t3387707835, ___JHKJOGKJIPH_22)); }
	inline int32_t get_JHKJOGKJIPH_22() const { return ___JHKJOGKJIPH_22; }
	inline int32_t* get_address_of_JHKJOGKJIPH_22() { return &___JHKJOGKJIPH_22; }
	inline void set_JHKJOGKJIPH_22(int32_t value)
	{
		___JHKJOGKJIPH_22 = value;
	}

	inline static int32_t get_offset_of_JNLEFANLCNC_23() { return static_cast<int32_t>(offsetof(OnButtonPressed_t3387707835, ___JNLEFANLCNC_23)); }
	inline int32_t get_JNLEFANLCNC_23() const { return ___JNLEFANLCNC_23; }
	inline int32_t* get_address_of_JNLEFANLCNC_23() { return &___JNLEFANLCNC_23; }
	inline void set_JNLEFANLCNC_23(int32_t value)
	{
		___JNLEFANLCNC_23 = value;
	}

	inline static int32_t get_offset_of_HPJMAGFMLBO_24() { return static_cast<int32_t>(offsetof(OnButtonPressed_t3387707835, ___HPJMAGFMLBO_24)); }
	inline int32_t get_HPJMAGFMLBO_24() const { return ___HPJMAGFMLBO_24; }
	inline int32_t* get_address_of_HPJMAGFMLBO_24() { return &___HPJMAGFMLBO_24; }
	inline void set_HPJMAGFMLBO_24(int32_t value)
	{
		___HPJMAGFMLBO_24 = value;
	}

	inline static int32_t get_offset_of_KLIMLJCLJNH_25() { return static_cast<int32_t>(offsetof(OnButtonPressed_t3387707835, ___KLIMLJCLJNH_25)); }
	inline int32_t get_KLIMLJCLJNH_25() const { return ___KLIMLJCLJNH_25; }
	inline int32_t* get_address_of_KLIMLJCLJNH_25() { return &___KLIMLJCLJNH_25; }
	inline void set_KLIMLJCLJNH_25(int32_t value)
	{
		___KLIMLJCLJNH_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
