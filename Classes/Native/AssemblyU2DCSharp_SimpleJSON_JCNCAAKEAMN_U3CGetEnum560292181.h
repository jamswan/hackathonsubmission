﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2137623955.h"

// SimpleJSON.GFGBGCMOLKN
struct GFGBGCMOLKN_t3233773149;
// SimpleJSON.JCNCAAKEAMN
struct JCNCAAKEAMN_t875942704;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JCNCAAKEAMN/<GetEnumerator>c__Iterator1
struct  U3CGetEnumeratorU3Ec__Iterator1_t560292181  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<SimpleJSON.GFGBGCMOLKN> SimpleJSON.JCNCAAKEAMN/<GetEnumerator>c__Iterator1::HCHAHDKBNGC
	Enumerator_t2137623955  ___HCHAHDKBNGC_0;
	// SimpleJSON.GFGBGCMOLKN SimpleJSON.JCNCAAKEAMN/<GetEnumerator>c__Iterator1::BEBKPNABGFH
	GFGBGCMOLKN_t3233773149 * ___BEBKPNABGFH_1;
	// SimpleJSON.JCNCAAKEAMN SimpleJSON.JCNCAAKEAMN/<GetEnumerator>c__Iterator1::AOOLEAHHMIH
	JCNCAAKEAMN_t875942704 * ___AOOLEAHHMIH_2;
	// System.Object SimpleJSON.JCNCAAKEAMN/<GetEnumerator>c__Iterator1::LGBFNMECDHC
	Il2CppObject * ___LGBFNMECDHC_3;
	// System.Boolean SimpleJSON.JCNCAAKEAMN/<GetEnumerator>c__Iterator1::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_4;
	// System.Int32 SimpleJSON.JCNCAAKEAMN/<GetEnumerator>c__Iterator1::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_5;

public:
	inline static int32_t get_offset_of_HCHAHDKBNGC_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t560292181, ___HCHAHDKBNGC_0)); }
	inline Enumerator_t2137623955  get_HCHAHDKBNGC_0() const { return ___HCHAHDKBNGC_0; }
	inline Enumerator_t2137623955 * get_address_of_HCHAHDKBNGC_0() { return &___HCHAHDKBNGC_0; }
	inline void set_HCHAHDKBNGC_0(Enumerator_t2137623955  value)
	{
		___HCHAHDKBNGC_0 = value;
	}

	inline static int32_t get_offset_of_BEBKPNABGFH_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t560292181, ___BEBKPNABGFH_1)); }
	inline GFGBGCMOLKN_t3233773149 * get_BEBKPNABGFH_1() const { return ___BEBKPNABGFH_1; }
	inline GFGBGCMOLKN_t3233773149 ** get_address_of_BEBKPNABGFH_1() { return &___BEBKPNABGFH_1; }
	inline void set_BEBKPNABGFH_1(GFGBGCMOLKN_t3233773149 * value)
	{
		___BEBKPNABGFH_1 = value;
		Il2CppCodeGenWriteBarrier(&___BEBKPNABGFH_1, value);
	}

	inline static int32_t get_offset_of_AOOLEAHHMIH_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t560292181, ___AOOLEAHHMIH_2)); }
	inline JCNCAAKEAMN_t875942704 * get_AOOLEAHHMIH_2() const { return ___AOOLEAHHMIH_2; }
	inline JCNCAAKEAMN_t875942704 ** get_address_of_AOOLEAHHMIH_2() { return &___AOOLEAHHMIH_2; }
	inline void set_AOOLEAHHMIH_2(JCNCAAKEAMN_t875942704 * value)
	{
		___AOOLEAHHMIH_2 = value;
		Il2CppCodeGenWriteBarrier(&___AOOLEAHHMIH_2, value);
	}

	inline static int32_t get_offset_of_LGBFNMECDHC_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t560292181, ___LGBFNMECDHC_3)); }
	inline Il2CppObject * get_LGBFNMECDHC_3() const { return ___LGBFNMECDHC_3; }
	inline Il2CppObject ** get_address_of_LGBFNMECDHC_3() { return &___LGBFNMECDHC_3; }
	inline void set_LGBFNMECDHC_3(Il2CppObject * value)
	{
		___LGBFNMECDHC_3 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_3, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t560292181, ___IMAGLIMLPFK_4)); }
	inline bool get_IMAGLIMLPFK_4() const { return ___IMAGLIMLPFK_4; }
	inline bool* get_address_of_IMAGLIMLPFK_4() { return &___IMAGLIMLPFK_4; }
	inline void set_IMAGLIMLPFK_4(bool value)
	{
		___IMAGLIMLPFK_4 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_t560292181, ___EPCFNNGDBGC_5)); }
	inline int32_t get_EPCFNNGDBGC_5() const { return ___EPCFNNGDBGC_5; }
	inline int32_t* get_address_of_EPCFNNGDBGC_5() { return &___EPCFNNGDBGC_5; }
	inline void set_EPCFNNGDBGC_5(int32_t value)
	{
		___EPCFNNGDBGC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
