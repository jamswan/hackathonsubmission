﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundMan3765780423.h"

// Vuforia.VideoBackgroundManager
struct VideoBackgroundManager_t3515346924;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundManager
struct  VideoBackgroundManager_t3515346924  : public VideoBackgroundManagerAbstractBehaviour_t3765780423
{
public:

public:
};

struct VideoBackgroundManager_t3515346924_StaticFields
{
public:
	// Vuforia.VideoBackgroundManager Vuforia.VideoBackgroundManager::LIHJMAGOOBH
	VideoBackgroundManager_t3515346924 * ___LIHJMAGOOBH_9;

public:
	inline static int32_t get_offset_of_LIHJMAGOOBH_9() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t3515346924_StaticFields, ___LIHJMAGOOBH_9)); }
	inline VideoBackgroundManager_t3515346924 * get_LIHJMAGOOBH_9() const { return ___LIHJMAGOOBH_9; }
	inline VideoBackgroundManager_t3515346924 ** get_address_of_LIHJMAGOOBH_9() { return &___LIHJMAGOOBH_9; }
	inline void set_LIHJMAGOOBH_9(VideoBackgroundManager_t3515346924 * value)
	{
		___LIHJMAGOOBH_9 = value;
		Il2CppCodeGenWriteBarrier(&___LIHJMAGOOBH_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
