﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_GvrViewer_PIKNPGGIJMF2016830092.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.String
struct String_t;
// GvrViewer
struct GvrViewer_t2583885279;
// StereoController
struct StereoController_t3144380552;
// UnityEngine.Camera
struct Camera_t189460977;
// Gvr.Internal.MNAPGJPEPGN
struct MNAPGJPEPGN_t721519217;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// GvrViewer/BKCOFAHPJHN
struct BKCOFAHPJHN_t660377940;
// System.Uri
struct Uri_t19570940;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrViewer
struct  GvrViewer_t2583885279  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean GvrViewer::vrModeEnabled
	bool ___vrModeEnabled_6;
	// GvrViewer/PIKNPGGIJMF GvrViewer::distortionCorrection
	int32_t ___distortionCorrection_7;
	// System.Single GvrViewer::neckModelScale
	float ___neckModelScale_8;
	// System.Boolean GvrViewer::<GGHAFPCOIEO>k__BackingField
	bool ___U3CGGHAFPCOIEOU3Ek__BackingField_10;
	// System.Boolean GvrViewer::<IIMPFPMMONP>k__BackingField
	bool ___U3CIIMPFPMMONPU3Ek__BackingField_11;
	// System.Single GvrViewer::stereoScreenScale
	float ___stereoScreenScale_12;
	// GvrViewer/BKCOFAHPJHN GvrViewer::OnStereoScreenChanged
	BKCOFAHPJHN_t660377940 * ___OnStereoScreenChanged_14;
	// UnityEngine.Vector2 GvrViewer::CENNILMLHFB
	Vector2_t2243707579  ___CENNILMLHFB_15;
	// System.Uri GvrViewer::DefaultDeviceProfile
	Uri_t19570940 * ___DefaultDeviceProfile_16;
	// System.Boolean GvrViewer::<OBEGINALOIO>k__BackingField
	bool ___U3COBEGINALOIOU3Ek__BackingField_17;
	// System.Boolean GvrViewer::<PDMMEIFEFMP>k__BackingField
	bool ___U3CPDMMEIFEFMPU3Ek__BackingField_18;
	// System.Boolean GvrViewer::<JGIPJKKMJKE>k__BackingField
	bool ___U3CJGIPJKKMJKEU3Ek__BackingField_19;
	// System.Boolean GvrViewer::<GKLHCBNBOKL>k__BackingField
	bool ___U3CGKLHCBNBOKLU3Ek__BackingField_20;
	// System.Int32 GvrViewer::CJBBJHPNPOO
	int32_t ___CJBBJHPNPOO_21;

public:
	inline static int32_t get_offset_of_vrModeEnabled_6() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___vrModeEnabled_6)); }
	inline bool get_vrModeEnabled_6() const { return ___vrModeEnabled_6; }
	inline bool* get_address_of_vrModeEnabled_6() { return &___vrModeEnabled_6; }
	inline void set_vrModeEnabled_6(bool value)
	{
		___vrModeEnabled_6 = value;
	}

	inline static int32_t get_offset_of_distortionCorrection_7() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___distortionCorrection_7)); }
	inline int32_t get_distortionCorrection_7() const { return ___distortionCorrection_7; }
	inline int32_t* get_address_of_distortionCorrection_7() { return &___distortionCorrection_7; }
	inline void set_distortionCorrection_7(int32_t value)
	{
		___distortionCorrection_7 = value;
	}

	inline static int32_t get_offset_of_neckModelScale_8() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___neckModelScale_8)); }
	inline float get_neckModelScale_8() const { return ___neckModelScale_8; }
	inline float* get_address_of_neckModelScale_8() { return &___neckModelScale_8; }
	inline void set_neckModelScale_8(float value)
	{
		___neckModelScale_8 = value;
	}

	inline static int32_t get_offset_of_U3CGGHAFPCOIEOU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___U3CGGHAFPCOIEOU3Ek__BackingField_10)); }
	inline bool get_U3CGGHAFPCOIEOU3Ek__BackingField_10() const { return ___U3CGGHAFPCOIEOU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CGGHAFPCOIEOU3Ek__BackingField_10() { return &___U3CGGHAFPCOIEOU3Ek__BackingField_10; }
	inline void set_U3CGGHAFPCOIEOU3Ek__BackingField_10(bool value)
	{
		___U3CGGHAFPCOIEOU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CIIMPFPMMONPU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___U3CIIMPFPMMONPU3Ek__BackingField_11)); }
	inline bool get_U3CIIMPFPMMONPU3Ek__BackingField_11() const { return ___U3CIIMPFPMMONPU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIIMPFPMMONPU3Ek__BackingField_11() { return &___U3CIIMPFPMMONPU3Ek__BackingField_11; }
	inline void set_U3CIIMPFPMMONPU3Ek__BackingField_11(bool value)
	{
		___U3CIIMPFPMMONPU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_stereoScreenScale_12() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___stereoScreenScale_12)); }
	inline float get_stereoScreenScale_12() const { return ___stereoScreenScale_12; }
	inline float* get_address_of_stereoScreenScale_12() { return &___stereoScreenScale_12; }
	inline void set_stereoScreenScale_12(float value)
	{
		___stereoScreenScale_12 = value;
	}

	inline static int32_t get_offset_of_OnStereoScreenChanged_14() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___OnStereoScreenChanged_14)); }
	inline BKCOFAHPJHN_t660377940 * get_OnStereoScreenChanged_14() const { return ___OnStereoScreenChanged_14; }
	inline BKCOFAHPJHN_t660377940 ** get_address_of_OnStereoScreenChanged_14() { return &___OnStereoScreenChanged_14; }
	inline void set_OnStereoScreenChanged_14(BKCOFAHPJHN_t660377940 * value)
	{
		___OnStereoScreenChanged_14 = value;
		Il2CppCodeGenWriteBarrier(&___OnStereoScreenChanged_14, value);
	}

	inline static int32_t get_offset_of_CENNILMLHFB_15() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___CENNILMLHFB_15)); }
	inline Vector2_t2243707579  get_CENNILMLHFB_15() const { return ___CENNILMLHFB_15; }
	inline Vector2_t2243707579 * get_address_of_CENNILMLHFB_15() { return &___CENNILMLHFB_15; }
	inline void set_CENNILMLHFB_15(Vector2_t2243707579  value)
	{
		___CENNILMLHFB_15 = value;
	}

	inline static int32_t get_offset_of_DefaultDeviceProfile_16() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___DefaultDeviceProfile_16)); }
	inline Uri_t19570940 * get_DefaultDeviceProfile_16() const { return ___DefaultDeviceProfile_16; }
	inline Uri_t19570940 ** get_address_of_DefaultDeviceProfile_16() { return &___DefaultDeviceProfile_16; }
	inline void set_DefaultDeviceProfile_16(Uri_t19570940 * value)
	{
		___DefaultDeviceProfile_16 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultDeviceProfile_16, value);
	}

	inline static int32_t get_offset_of_U3COBEGINALOIOU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___U3COBEGINALOIOU3Ek__BackingField_17)); }
	inline bool get_U3COBEGINALOIOU3Ek__BackingField_17() const { return ___U3COBEGINALOIOU3Ek__BackingField_17; }
	inline bool* get_address_of_U3COBEGINALOIOU3Ek__BackingField_17() { return &___U3COBEGINALOIOU3Ek__BackingField_17; }
	inline void set_U3COBEGINALOIOU3Ek__BackingField_17(bool value)
	{
		___U3COBEGINALOIOU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CPDMMEIFEFMPU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___U3CPDMMEIFEFMPU3Ek__BackingField_18)); }
	inline bool get_U3CPDMMEIFEFMPU3Ek__BackingField_18() const { return ___U3CPDMMEIFEFMPU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CPDMMEIFEFMPU3Ek__BackingField_18() { return &___U3CPDMMEIFEFMPU3Ek__BackingField_18; }
	inline void set_U3CPDMMEIFEFMPU3Ek__BackingField_18(bool value)
	{
		___U3CPDMMEIFEFMPU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CJGIPJKKMJKEU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___U3CJGIPJKKMJKEU3Ek__BackingField_19)); }
	inline bool get_U3CJGIPJKKMJKEU3Ek__BackingField_19() const { return ___U3CJGIPJKKMJKEU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CJGIPJKKMJKEU3Ek__BackingField_19() { return &___U3CJGIPJKKMJKEU3Ek__BackingField_19; }
	inline void set_U3CJGIPJKKMJKEU3Ek__BackingField_19(bool value)
	{
		___U3CJGIPJKKMJKEU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CGKLHCBNBOKLU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___U3CGKLHCBNBOKLU3Ek__BackingField_20)); }
	inline bool get_U3CGKLHCBNBOKLU3Ek__BackingField_20() const { return ___U3CGKLHCBNBOKLU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CGKLHCBNBOKLU3Ek__BackingField_20() { return &___U3CGKLHCBNBOKLU3Ek__BackingField_20; }
	inline void set_U3CGKLHCBNBOKLU3Ek__BackingField_20(bool value)
	{
		___U3CGKLHCBNBOKLU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_CJBBJHPNPOO_21() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279, ___CJBBJHPNPOO_21)); }
	inline int32_t get_CJBBJHPNPOO_21() const { return ___CJBBJHPNPOO_21; }
	inline int32_t* get_address_of_CJBBJHPNPOO_21() { return &___CJBBJHPNPOO_21; }
	inline void set_CJBBJHPNPOO_21(int32_t value)
	{
		___CJBBJHPNPOO_21 = value;
	}
};

struct GvrViewer_t2583885279_StaticFields
{
public:
	// GvrViewer GvrViewer::OLMOHEEKDMC
	GvrViewer_t2583885279 * ___OLMOHEEKDMC_3;
	// StereoController GvrViewer::NNHNEIGFDPN
	StereoController_t3144380552 * ___NNHNEIGFDPN_4;
	// UnityEngine.Camera GvrViewer::EDEIECMOKKM
	Camera_t189460977 * ___EDEIECMOKKM_5;
	// Gvr.Internal.MNAPGJPEPGN GvrViewer::FCEOCGPNFEC
	MNAPGJPEPGN_t721519217 * ___FCEOCGPNFEC_9;
	// UnityEngine.RenderTexture GvrViewer::NKHOKKEKDJA
	RenderTexture_t2666733923 * ___NKHOKKEKDJA_13;

public:
	inline static int32_t get_offset_of_OLMOHEEKDMC_3() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279_StaticFields, ___OLMOHEEKDMC_3)); }
	inline GvrViewer_t2583885279 * get_OLMOHEEKDMC_3() const { return ___OLMOHEEKDMC_3; }
	inline GvrViewer_t2583885279 ** get_address_of_OLMOHEEKDMC_3() { return &___OLMOHEEKDMC_3; }
	inline void set_OLMOHEEKDMC_3(GvrViewer_t2583885279 * value)
	{
		___OLMOHEEKDMC_3 = value;
		Il2CppCodeGenWriteBarrier(&___OLMOHEEKDMC_3, value);
	}

	inline static int32_t get_offset_of_NNHNEIGFDPN_4() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279_StaticFields, ___NNHNEIGFDPN_4)); }
	inline StereoController_t3144380552 * get_NNHNEIGFDPN_4() const { return ___NNHNEIGFDPN_4; }
	inline StereoController_t3144380552 ** get_address_of_NNHNEIGFDPN_4() { return &___NNHNEIGFDPN_4; }
	inline void set_NNHNEIGFDPN_4(StereoController_t3144380552 * value)
	{
		___NNHNEIGFDPN_4 = value;
		Il2CppCodeGenWriteBarrier(&___NNHNEIGFDPN_4, value);
	}

	inline static int32_t get_offset_of_EDEIECMOKKM_5() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279_StaticFields, ___EDEIECMOKKM_5)); }
	inline Camera_t189460977 * get_EDEIECMOKKM_5() const { return ___EDEIECMOKKM_5; }
	inline Camera_t189460977 ** get_address_of_EDEIECMOKKM_5() { return &___EDEIECMOKKM_5; }
	inline void set_EDEIECMOKKM_5(Camera_t189460977 * value)
	{
		___EDEIECMOKKM_5 = value;
		Il2CppCodeGenWriteBarrier(&___EDEIECMOKKM_5, value);
	}

	inline static int32_t get_offset_of_FCEOCGPNFEC_9() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279_StaticFields, ___FCEOCGPNFEC_9)); }
	inline MNAPGJPEPGN_t721519217 * get_FCEOCGPNFEC_9() const { return ___FCEOCGPNFEC_9; }
	inline MNAPGJPEPGN_t721519217 ** get_address_of_FCEOCGPNFEC_9() { return &___FCEOCGPNFEC_9; }
	inline void set_FCEOCGPNFEC_9(MNAPGJPEPGN_t721519217 * value)
	{
		___FCEOCGPNFEC_9 = value;
		Il2CppCodeGenWriteBarrier(&___FCEOCGPNFEC_9, value);
	}

	inline static int32_t get_offset_of_NKHOKKEKDJA_13() { return static_cast<int32_t>(offsetof(GvrViewer_t2583885279_StaticFields, ___NKHOKKEKDJA_13)); }
	inline RenderTexture_t2666733923 * get_NKHOKKEKDJA_13() const { return ___NKHOKKEKDJA_13; }
	inline RenderTexture_t2666733923 ** get_address_of_NKHOKKEKDJA_13() { return &___NKHOKKEKDJA_13; }
	inline void set_NKHOKKEKDJA_13(RenderTexture_t2666733923 * value)
	{
		___NKHOKKEKDJA_13 = value;
		Il2CppCodeGenWriteBarrier(&___NKHOKKEKDJA_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
