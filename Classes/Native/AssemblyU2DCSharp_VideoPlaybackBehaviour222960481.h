﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_LNMCBNCPBPG_EPPHLMJAIBL3913485307.h"
#include "AssemblyU2DCSharp_LNMCBNCPBPG_DGIJFIBFFPD968251196.h"

// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2243626319;
// LNMCBNCPBPG
struct LNMCBNCPBPG_t2969048680;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoPlaybackBehaviour
struct  VideoPlaybackBehaviour_t222960481  : public MonoBehaviour_t1158329972
{
public:
	// System.String VideoPlaybackBehaviour::m_path
	String_t* ___m_path_2;
	// UnityEngine.Texture VideoPlaybackBehaviour::m_playTexture
	Texture_t2243626319 * ___m_playTexture_3;
	// UnityEngine.Texture VideoPlaybackBehaviour::m_busyTexture
	Texture_t2243626319 * ___m_busyTexture_4;
	// UnityEngine.Texture VideoPlaybackBehaviour::m_errorTexture
	Texture_t2243626319 * ___m_errorTexture_5;
	// System.Boolean VideoPlaybackBehaviour::m_autoPlay
	bool ___m_autoPlay_6;
	// LNMCBNCPBPG VideoPlaybackBehaviour::ACBEDADNILK
	LNMCBNCPBPG_t2969048680 * ___ACBEDADNILK_7;
	// System.Boolean VideoPlaybackBehaviour::DCJGDAJJDKP
	bool ___DCJGDAJJDKP_8;
	// System.Boolean VideoPlaybackBehaviour::PKBAIDBGGCD
	bool ___PKBAIDBGGCD_9;
	// System.Boolean VideoPlaybackBehaviour::OPBMKDOIIHC
	bool ___OPBMKDOIIHC_10;
	// UnityEngine.Texture2D VideoPlaybackBehaviour::KCAGLDMAEOH
	Texture2D_t3542995729 * ___KCAGLDMAEOH_11;
	// UnityEngine.Texture VideoPlaybackBehaviour::mKeyframeTexture
	Texture_t2243626319 * ___mKeyframeTexture_12;
	// LNMCBNCPBPG/EPPHLMJAIBL VideoPlaybackBehaviour::BGIEHPJLFLH
	int32_t ___BGIEHPJLFLH_13;
	// LNMCBNCPBPG/DGIJFIBFFPD VideoPlaybackBehaviour::DGANMIDJBGM
	int32_t ___DGANMIDJBGM_14;
	// System.Single VideoPlaybackBehaviour::JFBDMGKOFAK
	float ___JFBDMGKOFAK_15;
	// System.Boolean VideoPlaybackBehaviour::NGAGMBBHLDI
	bool ___NGAGMBBHLDI_16;
	// UnityEngine.GameObject VideoPlaybackBehaviour::GEMELIKMENM
	GameObject_t1756533147 * ___GEMELIKMENM_17;
	// System.Boolean VideoPlaybackBehaviour::MNJHGOPFMNL
	bool ___MNJHGOPFMNL_18;

public:
	inline static int32_t get_offset_of_m_path_2() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___m_path_2)); }
	inline String_t* get_m_path_2() const { return ___m_path_2; }
	inline String_t** get_address_of_m_path_2() { return &___m_path_2; }
	inline void set_m_path_2(String_t* value)
	{
		___m_path_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_path_2, value);
	}

	inline static int32_t get_offset_of_m_playTexture_3() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___m_playTexture_3)); }
	inline Texture_t2243626319 * get_m_playTexture_3() const { return ___m_playTexture_3; }
	inline Texture_t2243626319 ** get_address_of_m_playTexture_3() { return &___m_playTexture_3; }
	inline void set_m_playTexture_3(Texture_t2243626319 * value)
	{
		___m_playTexture_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_playTexture_3, value);
	}

	inline static int32_t get_offset_of_m_busyTexture_4() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___m_busyTexture_4)); }
	inline Texture_t2243626319 * get_m_busyTexture_4() const { return ___m_busyTexture_4; }
	inline Texture_t2243626319 ** get_address_of_m_busyTexture_4() { return &___m_busyTexture_4; }
	inline void set_m_busyTexture_4(Texture_t2243626319 * value)
	{
		___m_busyTexture_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_busyTexture_4, value);
	}

	inline static int32_t get_offset_of_m_errorTexture_5() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___m_errorTexture_5)); }
	inline Texture_t2243626319 * get_m_errorTexture_5() const { return ___m_errorTexture_5; }
	inline Texture_t2243626319 ** get_address_of_m_errorTexture_5() { return &___m_errorTexture_5; }
	inline void set_m_errorTexture_5(Texture_t2243626319 * value)
	{
		___m_errorTexture_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_errorTexture_5, value);
	}

	inline static int32_t get_offset_of_m_autoPlay_6() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___m_autoPlay_6)); }
	inline bool get_m_autoPlay_6() const { return ___m_autoPlay_6; }
	inline bool* get_address_of_m_autoPlay_6() { return &___m_autoPlay_6; }
	inline void set_m_autoPlay_6(bool value)
	{
		___m_autoPlay_6 = value;
	}

	inline static int32_t get_offset_of_ACBEDADNILK_7() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___ACBEDADNILK_7)); }
	inline LNMCBNCPBPG_t2969048680 * get_ACBEDADNILK_7() const { return ___ACBEDADNILK_7; }
	inline LNMCBNCPBPG_t2969048680 ** get_address_of_ACBEDADNILK_7() { return &___ACBEDADNILK_7; }
	inline void set_ACBEDADNILK_7(LNMCBNCPBPG_t2969048680 * value)
	{
		___ACBEDADNILK_7 = value;
		Il2CppCodeGenWriteBarrier(&___ACBEDADNILK_7, value);
	}

	inline static int32_t get_offset_of_DCJGDAJJDKP_8() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___DCJGDAJJDKP_8)); }
	inline bool get_DCJGDAJJDKP_8() const { return ___DCJGDAJJDKP_8; }
	inline bool* get_address_of_DCJGDAJJDKP_8() { return &___DCJGDAJJDKP_8; }
	inline void set_DCJGDAJJDKP_8(bool value)
	{
		___DCJGDAJJDKP_8 = value;
	}

	inline static int32_t get_offset_of_PKBAIDBGGCD_9() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___PKBAIDBGGCD_9)); }
	inline bool get_PKBAIDBGGCD_9() const { return ___PKBAIDBGGCD_9; }
	inline bool* get_address_of_PKBAIDBGGCD_9() { return &___PKBAIDBGGCD_9; }
	inline void set_PKBAIDBGGCD_9(bool value)
	{
		___PKBAIDBGGCD_9 = value;
	}

	inline static int32_t get_offset_of_OPBMKDOIIHC_10() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___OPBMKDOIIHC_10)); }
	inline bool get_OPBMKDOIIHC_10() const { return ___OPBMKDOIIHC_10; }
	inline bool* get_address_of_OPBMKDOIIHC_10() { return &___OPBMKDOIIHC_10; }
	inline void set_OPBMKDOIIHC_10(bool value)
	{
		___OPBMKDOIIHC_10 = value;
	}

	inline static int32_t get_offset_of_KCAGLDMAEOH_11() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___KCAGLDMAEOH_11)); }
	inline Texture2D_t3542995729 * get_KCAGLDMAEOH_11() const { return ___KCAGLDMAEOH_11; }
	inline Texture2D_t3542995729 ** get_address_of_KCAGLDMAEOH_11() { return &___KCAGLDMAEOH_11; }
	inline void set_KCAGLDMAEOH_11(Texture2D_t3542995729 * value)
	{
		___KCAGLDMAEOH_11 = value;
		Il2CppCodeGenWriteBarrier(&___KCAGLDMAEOH_11, value);
	}

	inline static int32_t get_offset_of_mKeyframeTexture_12() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___mKeyframeTexture_12)); }
	inline Texture_t2243626319 * get_mKeyframeTexture_12() const { return ___mKeyframeTexture_12; }
	inline Texture_t2243626319 ** get_address_of_mKeyframeTexture_12() { return &___mKeyframeTexture_12; }
	inline void set_mKeyframeTexture_12(Texture_t2243626319 * value)
	{
		___mKeyframeTexture_12 = value;
		Il2CppCodeGenWriteBarrier(&___mKeyframeTexture_12, value);
	}

	inline static int32_t get_offset_of_BGIEHPJLFLH_13() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___BGIEHPJLFLH_13)); }
	inline int32_t get_BGIEHPJLFLH_13() const { return ___BGIEHPJLFLH_13; }
	inline int32_t* get_address_of_BGIEHPJLFLH_13() { return &___BGIEHPJLFLH_13; }
	inline void set_BGIEHPJLFLH_13(int32_t value)
	{
		___BGIEHPJLFLH_13 = value;
	}

	inline static int32_t get_offset_of_DGANMIDJBGM_14() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___DGANMIDJBGM_14)); }
	inline int32_t get_DGANMIDJBGM_14() const { return ___DGANMIDJBGM_14; }
	inline int32_t* get_address_of_DGANMIDJBGM_14() { return &___DGANMIDJBGM_14; }
	inline void set_DGANMIDJBGM_14(int32_t value)
	{
		___DGANMIDJBGM_14 = value;
	}

	inline static int32_t get_offset_of_JFBDMGKOFAK_15() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___JFBDMGKOFAK_15)); }
	inline float get_JFBDMGKOFAK_15() const { return ___JFBDMGKOFAK_15; }
	inline float* get_address_of_JFBDMGKOFAK_15() { return &___JFBDMGKOFAK_15; }
	inline void set_JFBDMGKOFAK_15(float value)
	{
		___JFBDMGKOFAK_15 = value;
	}

	inline static int32_t get_offset_of_NGAGMBBHLDI_16() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___NGAGMBBHLDI_16)); }
	inline bool get_NGAGMBBHLDI_16() const { return ___NGAGMBBHLDI_16; }
	inline bool* get_address_of_NGAGMBBHLDI_16() { return &___NGAGMBBHLDI_16; }
	inline void set_NGAGMBBHLDI_16(bool value)
	{
		___NGAGMBBHLDI_16 = value;
	}

	inline static int32_t get_offset_of_GEMELIKMENM_17() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___GEMELIKMENM_17)); }
	inline GameObject_t1756533147 * get_GEMELIKMENM_17() const { return ___GEMELIKMENM_17; }
	inline GameObject_t1756533147 ** get_address_of_GEMELIKMENM_17() { return &___GEMELIKMENM_17; }
	inline void set_GEMELIKMENM_17(GameObject_t1756533147 * value)
	{
		___GEMELIKMENM_17 = value;
		Il2CppCodeGenWriteBarrier(&___GEMELIKMENM_17, value);
	}

	inline static int32_t get_offset_of_MNJHGOPFMNL_18() { return static_cast<int32_t>(offsetof(VideoPlaybackBehaviour_t222960481, ___MNJHGOPFMNL_18)); }
	inline bool get_MNJHGOPFMNL_18() const { return ___MNJHGOPFMNL_18; }
	inline bool* get_address_of_MNJHGOPFMNL_18() { return &___MNJHGOPFMNL_18; }
	inline void set_MNJHGOPFMNL_18(bool value)
	{
		___MNJHGOPFMNL_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
