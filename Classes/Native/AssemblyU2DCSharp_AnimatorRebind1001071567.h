﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// UnityEngine.Animator
struct Animator_t69676727;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimatorRebind
struct  AnimatorRebind_t1001071567  : public NarrativeEffect_t3336735121
{
public:
	// UnityEngine.Animator AnimatorRebind::animator
	Animator_t69676727 * ___animator_3;

public:
	inline static int32_t get_offset_of_animator_3() { return static_cast<int32_t>(offsetof(AnimatorRebind_t1001071567, ___animator_3)); }
	inline Animator_t69676727 * get_animator_3() const { return ___animator_3; }
	inline Animator_t69676727 ** get_address_of_animator_3() { return &___animator_3; }
	inline void set_animator_3(Animator_t69676727 * value)
	{
		___animator_3 = value;
		Il2CppCodeGenWriteBarrier(&___animator_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
