﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"

// EnableGameObject
struct EnableGameObject_t1635748018;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YesNoChoice
struct  YesNoChoice_t3116058963  : public NarrativeCondition_t4123859457
{
public:
	// EnableGameObject YesNoChoice::yesEffect
	EnableGameObject_t1635748018 * ___yesEffect_13;
	// EnableGameObject YesNoChoice::noEffect
	EnableGameObject_t1635748018 * ___noEffect_14;
	// System.Single YesNoChoice::choiceWidth
	float ___choiceWidth_15;
	// System.Single YesNoChoice::choiceHeight
	float ___choiceHeight_16;

public:
	inline static int32_t get_offset_of_yesEffect_13() { return static_cast<int32_t>(offsetof(YesNoChoice_t3116058963, ___yesEffect_13)); }
	inline EnableGameObject_t1635748018 * get_yesEffect_13() const { return ___yesEffect_13; }
	inline EnableGameObject_t1635748018 ** get_address_of_yesEffect_13() { return &___yesEffect_13; }
	inline void set_yesEffect_13(EnableGameObject_t1635748018 * value)
	{
		___yesEffect_13 = value;
		Il2CppCodeGenWriteBarrier(&___yesEffect_13, value);
	}

	inline static int32_t get_offset_of_noEffect_14() { return static_cast<int32_t>(offsetof(YesNoChoice_t3116058963, ___noEffect_14)); }
	inline EnableGameObject_t1635748018 * get_noEffect_14() const { return ___noEffect_14; }
	inline EnableGameObject_t1635748018 ** get_address_of_noEffect_14() { return &___noEffect_14; }
	inline void set_noEffect_14(EnableGameObject_t1635748018 * value)
	{
		___noEffect_14 = value;
		Il2CppCodeGenWriteBarrier(&___noEffect_14, value);
	}

	inline static int32_t get_offset_of_choiceWidth_15() { return static_cast<int32_t>(offsetof(YesNoChoice_t3116058963, ___choiceWidth_15)); }
	inline float get_choiceWidth_15() const { return ___choiceWidth_15; }
	inline float* get_address_of_choiceWidth_15() { return &___choiceWidth_15; }
	inline void set_choiceWidth_15(float value)
	{
		___choiceWidth_15 = value;
	}

	inline static int32_t get_offset_of_choiceHeight_16() { return static_cast<int32_t>(offsetof(YesNoChoice_t3116058963, ___choiceHeight_16)); }
	inline float get_choiceHeight_16() const { return ___choiceHeight_16; }
	inline float* get_address_of_choiceHeight_16() { return &___choiceHeight_16; }
	inline void set_choiceHeight_16(float value)
	{
		___choiceHeight_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
