﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Gvr_Internal_OAPLNBCDLIB4108484976.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.JCGMDGFLGOB
struct  JCGMDGFLGOB_t1400499334  : public OAPLNBCDLIB_t4108484976
{
public:
	// System.Boolean Gvr.Internal.JCGMDGFLGOB::INEMPHMNACI
	bool ___INEMPHMNACI_32;

public:
	inline static int32_t get_offset_of_INEMPHMNACI_32() { return static_cast<int32_t>(offsetof(JCGMDGFLGOB_t1400499334, ___INEMPHMNACI_32)); }
	inline bool get_INEMPHMNACI_32() const { return ___INEMPHMNACI_32; }
	inline bool* get_address_of_INEMPHMNACI_32() { return &___INEMPHMNACI_32; }
	inline void set_INEMPHMNACI_32(bool value)
	{
		___INEMPHMNACI_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
