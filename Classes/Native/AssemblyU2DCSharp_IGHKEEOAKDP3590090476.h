﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_FPFPADIHBIM767626572.h"

// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IGHKEEOAKDP
struct  IGHKEEOAKDP_t3590090476  : public FPFPADIHBIM_t767626572
{
public:
	// System.Single IGHKEEOAKDP::<CIFFNPAGDLB>k__BackingField
	float ___U3CCIFFNPAGDLBU3Ek__BackingField_7;
	// UnityEngine.Material IGHKEEOAKDP::<DCDPLHDGAHF>k__BackingField
	Material_t193706927 * ___U3CDCDPLHDGAHFU3Ek__BackingField_8;
	// System.Single IGHKEEOAKDP::<HAMJJGLDFNJ>k__BackingField
	float ___U3CHAMJJGLDFNJU3Ek__BackingField_9;
	// System.Single IGHKEEOAKDP::<LHFKJHOJJLI>k__BackingField
	float ___U3CLHFKJHOJJLIU3Ek__BackingField_10;
	// System.Single IGHKEEOAKDP::<MBKNCNKEDED>k__BackingField
	float ___U3CMBKNCNKEDEDU3Ek__BackingField_11;
	// System.Single IGHKEEOAKDP::<NIKGMMEBCGE>k__BackingField
	float ___U3CNIKGMMEBCGEU3Ek__BackingField_12;
	// System.Single IGHKEEOAKDP::<ABDFNKGNCBC>k__BackingField
	float ___U3CABDFNKGNCBCU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCIFFNPAGDLBU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(IGHKEEOAKDP_t3590090476, ___U3CCIFFNPAGDLBU3Ek__BackingField_7)); }
	inline float get_U3CCIFFNPAGDLBU3Ek__BackingField_7() const { return ___U3CCIFFNPAGDLBU3Ek__BackingField_7; }
	inline float* get_address_of_U3CCIFFNPAGDLBU3Ek__BackingField_7() { return &___U3CCIFFNPAGDLBU3Ek__BackingField_7; }
	inline void set_U3CCIFFNPAGDLBU3Ek__BackingField_7(float value)
	{
		___U3CCIFFNPAGDLBU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CDCDPLHDGAHFU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(IGHKEEOAKDP_t3590090476, ___U3CDCDPLHDGAHFU3Ek__BackingField_8)); }
	inline Material_t193706927 * get_U3CDCDPLHDGAHFU3Ek__BackingField_8() const { return ___U3CDCDPLHDGAHFU3Ek__BackingField_8; }
	inline Material_t193706927 ** get_address_of_U3CDCDPLHDGAHFU3Ek__BackingField_8() { return &___U3CDCDPLHDGAHFU3Ek__BackingField_8; }
	inline void set_U3CDCDPLHDGAHFU3Ek__BackingField_8(Material_t193706927 * value)
	{
		___U3CDCDPLHDGAHFU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDCDPLHDGAHFU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CHAMJJGLDFNJU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(IGHKEEOAKDP_t3590090476, ___U3CHAMJJGLDFNJU3Ek__BackingField_9)); }
	inline float get_U3CHAMJJGLDFNJU3Ek__BackingField_9() const { return ___U3CHAMJJGLDFNJU3Ek__BackingField_9; }
	inline float* get_address_of_U3CHAMJJGLDFNJU3Ek__BackingField_9() { return &___U3CHAMJJGLDFNJU3Ek__BackingField_9; }
	inline void set_U3CHAMJJGLDFNJU3Ek__BackingField_9(float value)
	{
		___U3CHAMJJGLDFNJU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CLHFKJHOJJLIU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(IGHKEEOAKDP_t3590090476, ___U3CLHFKJHOJJLIU3Ek__BackingField_10)); }
	inline float get_U3CLHFKJHOJJLIU3Ek__BackingField_10() const { return ___U3CLHFKJHOJJLIU3Ek__BackingField_10; }
	inline float* get_address_of_U3CLHFKJHOJJLIU3Ek__BackingField_10() { return &___U3CLHFKJHOJJLIU3Ek__BackingField_10; }
	inline void set_U3CLHFKJHOJJLIU3Ek__BackingField_10(float value)
	{
		___U3CLHFKJHOJJLIU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CMBKNCNKEDEDU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(IGHKEEOAKDP_t3590090476, ___U3CMBKNCNKEDEDU3Ek__BackingField_11)); }
	inline float get_U3CMBKNCNKEDEDU3Ek__BackingField_11() const { return ___U3CMBKNCNKEDEDU3Ek__BackingField_11; }
	inline float* get_address_of_U3CMBKNCNKEDEDU3Ek__BackingField_11() { return &___U3CMBKNCNKEDEDU3Ek__BackingField_11; }
	inline void set_U3CMBKNCNKEDEDU3Ek__BackingField_11(float value)
	{
		___U3CMBKNCNKEDEDU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CNIKGMMEBCGEU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(IGHKEEOAKDP_t3590090476, ___U3CNIKGMMEBCGEU3Ek__BackingField_12)); }
	inline float get_U3CNIKGMMEBCGEU3Ek__BackingField_12() const { return ___U3CNIKGMMEBCGEU3Ek__BackingField_12; }
	inline float* get_address_of_U3CNIKGMMEBCGEU3Ek__BackingField_12() { return &___U3CNIKGMMEBCGEU3Ek__BackingField_12; }
	inline void set_U3CNIKGMMEBCGEU3Ek__BackingField_12(float value)
	{
		___U3CNIKGMMEBCGEU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CABDFNKGNCBCU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(IGHKEEOAKDP_t3590090476, ___U3CABDFNKGNCBCU3Ek__BackingField_13)); }
	inline float get_U3CABDFNKGNCBCU3Ek__BackingField_13() const { return ___U3CABDFNKGNCBCU3Ek__BackingField_13; }
	inline float* get_address_of_U3CABDFNKGNCBCU3Ek__BackingField_13() { return &___U3CABDFNKGNCBCU3Ek__BackingField_13; }
	inline void set_U3CABDFNKGNCBCU3Ek__BackingField_13(float value)
	{
		___U3CABDFNKGNCBCU3Ek__BackingField_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
