﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagnifyingGlass
struct  MagnifyingGlass_t1447642303  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MagnifyingGlass::animationSpeed
	float ___animationSpeed_2;
	// System.Single MagnifyingGlass::rotationSpeed
	float ___rotationSpeed_3;
	// UnityEngine.Vector3 MagnifyingGlass::KALCNDOLONH
	Vector3_t2243707580  ___KALCNDOLONH_4;
	// UnityEngine.Vector3 MagnifyingGlass::AJNBKMJEGKK
	Vector3_t2243707580  ___AJNBKMJEGKK_5;
	// UnityEngine.Quaternion MagnifyingGlass::JKDKGPAEDKK
	Quaternion_t4030073918  ___JKDKGPAEDKK_6;
	// UnityEngine.Vector3 MagnifyingGlass::JBCPFANOKNE
	Vector3_t2243707580  ___JBCPFANOKNE_7;
	// UnityEngine.Quaternion MagnifyingGlass::LOCDEMAEFBP
	Quaternion_t4030073918  ___LOCDEMAEFBP_8;
	// UnityEngine.Transform MagnifyingGlass::KJLBEHOKPME
	Transform_t3275118058 * ___KJLBEHOKPME_9;
	// UnityEngine.Vector3 MagnifyingGlass::FGPFCKFBMAD
	Vector3_t2243707580  ___FGPFCKFBMAD_10;
	// System.Boolean MagnifyingGlass::AJILCIKCFEA
	bool ___AJILCIKCFEA_11;

public:
	inline static int32_t get_offset_of_animationSpeed_2() { return static_cast<int32_t>(offsetof(MagnifyingGlass_t1447642303, ___animationSpeed_2)); }
	inline float get_animationSpeed_2() const { return ___animationSpeed_2; }
	inline float* get_address_of_animationSpeed_2() { return &___animationSpeed_2; }
	inline void set_animationSpeed_2(float value)
	{
		___animationSpeed_2 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_3() { return static_cast<int32_t>(offsetof(MagnifyingGlass_t1447642303, ___rotationSpeed_3)); }
	inline float get_rotationSpeed_3() const { return ___rotationSpeed_3; }
	inline float* get_address_of_rotationSpeed_3() { return &___rotationSpeed_3; }
	inline void set_rotationSpeed_3(float value)
	{
		___rotationSpeed_3 = value;
	}

	inline static int32_t get_offset_of_KALCNDOLONH_4() { return static_cast<int32_t>(offsetof(MagnifyingGlass_t1447642303, ___KALCNDOLONH_4)); }
	inline Vector3_t2243707580  get_KALCNDOLONH_4() const { return ___KALCNDOLONH_4; }
	inline Vector3_t2243707580 * get_address_of_KALCNDOLONH_4() { return &___KALCNDOLONH_4; }
	inline void set_KALCNDOLONH_4(Vector3_t2243707580  value)
	{
		___KALCNDOLONH_4 = value;
	}

	inline static int32_t get_offset_of_AJNBKMJEGKK_5() { return static_cast<int32_t>(offsetof(MagnifyingGlass_t1447642303, ___AJNBKMJEGKK_5)); }
	inline Vector3_t2243707580  get_AJNBKMJEGKK_5() const { return ___AJNBKMJEGKK_5; }
	inline Vector3_t2243707580 * get_address_of_AJNBKMJEGKK_5() { return &___AJNBKMJEGKK_5; }
	inline void set_AJNBKMJEGKK_5(Vector3_t2243707580  value)
	{
		___AJNBKMJEGKK_5 = value;
	}

	inline static int32_t get_offset_of_JKDKGPAEDKK_6() { return static_cast<int32_t>(offsetof(MagnifyingGlass_t1447642303, ___JKDKGPAEDKK_6)); }
	inline Quaternion_t4030073918  get_JKDKGPAEDKK_6() const { return ___JKDKGPAEDKK_6; }
	inline Quaternion_t4030073918 * get_address_of_JKDKGPAEDKK_6() { return &___JKDKGPAEDKK_6; }
	inline void set_JKDKGPAEDKK_6(Quaternion_t4030073918  value)
	{
		___JKDKGPAEDKK_6 = value;
	}

	inline static int32_t get_offset_of_JBCPFANOKNE_7() { return static_cast<int32_t>(offsetof(MagnifyingGlass_t1447642303, ___JBCPFANOKNE_7)); }
	inline Vector3_t2243707580  get_JBCPFANOKNE_7() const { return ___JBCPFANOKNE_7; }
	inline Vector3_t2243707580 * get_address_of_JBCPFANOKNE_7() { return &___JBCPFANOKNE_7; }
	inline void set_JBCPFANOKNE_7(Vector3_t2243707580  value)
	{
		___JBCPFANOKNE_7 = value;
	}

	inline static int32_t get_offset_of_LOCDEMAEFBP_8() { return static_cast<int32_t>(offsetof(MagnifyingGlass_t1447642303, ___LOCDEMAEFBP_8)); }
	inline Quaternion_t4030073918  get_LOCDEMAEFBP_8() const { return ___LOCDEMAEFBP_8; }
	inline Quaternion_t4030073918 * get_address_of_LOCDEMAEFBP_8() { return &___LOCDEMAEFBP_8; }
	inline void set_LOCDEMAEFBP_8(Quaternion_t4030073918  value)
	{
		___LOCDEMAEFBP_8 = value;
	}

	inline static int32_t get_offset_of_KJLBEHOKPME_9() { return static_cast<int32_t>(offsetof(MagnifyingGlass_t1447642303, ___KJLBEHOKPME_9)); }
	inline Transform_t3275118058 * get_KJLBEHOKPME_9() const { return ___KJLBEHOKPME_9; }
	inline Transform_t3275118058 ** get_address_of_KJLBEHOKPME_9() { return &___KJLBEHOKPME_9; }
	inline void set_KJLBEHOKPME_9(Transform_t3275118058 * value)
	{
		___KJLBEHOKPME_9 = value;
		Il2CppCodeGenWriteBarrier(&___KJLBEHOKPME_9, value);
	}

	inline static int32_t get_offset_of_FGPFCKFBMAD_10() { return static_cast<int32_t>(offsetof(MagnifyingGlass_t1447642303, ___FGPFCKFBMAD_10)); }
	inline Vector3_t2243707580  get_FGPFCKFBMAD_10() const { return ___FGPFCKFBMAD_10; }
	inline Vector3_t2243707580 * get_address_of_FGPFCKFBMAD_10() { return &___FGPFCKFBMAD_10; }
	inline void set_FGPFCKFBMAD_10(Vector3_t2243707580  value)
	{
		___FGPFCKFBMAD_10 = value;
	}

	inline static int32_t get_offset_of_AJILCIKCFEA_11() { return static_cast<int32_t>(offsetof(MagnifyingGlass_t1447642303, ___AJILCIKCFEA_11)); }
	inline bool get_AJILCIKCFEA_11() const { return ___AJILCIKCFEA_11; }
	inline bool* get_address_of_AJILCIKCFEA_11() { return &___AJILCIKCFEA_11; }
	inline void set_AJILCIKCFEA_11(bool value)
	{
		___AJILCIKCFEA_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
