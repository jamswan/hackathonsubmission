﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopupFinished
struct  PopupFinished_t3728197864  : public NarrativeCondition_t4123859457
{
public:
	// System.String PopupFinished::logName
	String_t* ___logName_13;
	// System.String PopupFinished::message
	String_t* ___message_14;
	// System.Single PopupFinished::duration
	float ___duration_15;
	// System.Single PopupFinished::extraTime
	float ___extraTime_16;

public:
	inline static int32_t get_offset_of_logName_13() { return static_cast<int32_t>(offsetof(PopupFinished_t3728197864, ___logName_13)); }
	inline String_t* get_logName_13() const { return ___logName_13; }
	inline String_t** get_address_of_logName_13() { return &___logName_13; }
	inline void set_logName_13(String_t* value)
	{
		___logName_13 = value;
		Il2CppCodeGenWriteBarrier(&___logName_13, value);
	}

	inline static int32_t get_offset_of_message_14() { return static_cast<int32_t>(offsetof(PopupFinished_t3728197864, ___message_14)); }
	inline String_t* get_message_14() const { return ___message_14; }
	inline String_t** get_address_of_message_14() { return &___message_14; }
	inline void set_message_14(String_t* value)
	{
		___message_14 = value;
		Il2CppCodeGenWriteBarrier(&___message_14, value);
	}

	inline static int32_t get_offset_of_duration_15() { return static_cast<int32_t>(offsetof(PopupFinished_t3728197864, ___duration_15)); }
	inline float get_duration_15() const { return ___duration_15; }
	inline float* get_address_of_duration_15() { return &___duration_15; }
	inline void set_duration_15(float value)
	{
		___duration_15 = value;
	}

	inline static int32_t get_offset_of_extraTime_16() { return static_cast<int32_t>(offsetof(PopupFinished_t3728197864, ___extraTime_16)); }
	inline float get_extraTime_16() const { return ___extraTime_16; }
	inline float* get_address_of_extraTime_16() { return &___extraTime_16; }
	inline void set_extraTime_16(float value)
	{
		___extraTime_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
