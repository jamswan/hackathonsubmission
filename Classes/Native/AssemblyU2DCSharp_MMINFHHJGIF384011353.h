﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MMINFHHJGIF
struct  MMINFHHJGIF_t384011353  : public Il2CppObject
{
public:
	// UnityEngine.GUIStyle MMINFHHJGIF::IECEJPLGHLG
	GUIStyle_t1799908754 * ___IECEJPLGHLG_0;
	// System.Int32 MMINFHHJGIF::JHKJOGKJIPH
	int32_t ___JHKJOGKJIPH_1;
	// System.Int32 MMINFHHJGIF::HPJMAGFMLBO
	int32_t ___HPJMAGFMLBO_2;
	// System.Int32 MMINFHHJGIF::KLIMLJCLJNH
	int32_t ___KLIMLJCLJNH_3;

public:
	inline static int32_t get_offset_of_IECEJPLGHLG_0() { return static_cast<int32_t>(offsetof(MMINFHHJGIF_t384011353, ___IECEJPLGHLG_0)); }
	inline GUIStyle_t1799908754 * get_IECEJPLGHLG_0() const { return ___IECEJPLGHLG_0; }
	inline GUIStyle_t1799908754 ** get_address_of_IECEJPLGHLG_0() { return &___IECEJPLGHLG_0; }
	inline void set_IECEJPLGHLG_0(GUIStyle_t1799908754 * value)
	{
		___IECEJPLGHLG_0 = value;
		Il2CppCodeGenWriteBarrier(&___IECEJPLGHLG_0, value);
	}

	inline static int32_t get_offset_of_JHKJOGKJIPH_1() { return static_cast<int32_t>(offsetof(MMINFHHJGIF_t384011353, ___JHKJOGKJIPH_1)); }
	inline int32_t get_JHKJOGKJIPH_1() const { return ___JHKJOGKJIPH_1; }
	inline int32_t* get_address_of_JHKJOGKJIPH_1() { return &___JHKJOGKJIPH_1; }
	inline void set_JHKJOGKJIPH_1(int32_t value)
	{
		___JHKJOGKJIPH_1 = value;
	}

	inline static int32_t get_offset_of_HPJMAGFMLBO_2() { return static_cast<int32_t>(offsetof(MMINFHHJGIF_t384011353, ___HPJMAGFMLBO_2)); }
	inline int32_t get_HPJMAGFMLBO_2() const { return ___HPJMAGFMLBO_2; }
	inline int32_t* get_address_of_HPJMAGFMLBO_2() { return &___HPJMAGFMLBO_2; }
	inline void set_HPJMAGFMLBO_2(int32_t value)
	{
		___HPJMAGFMLBO_2 = value;
	}

	inline static int32_t get_offset_of_KLIMLJCLJNH_3() { return static_cast<int32_t>(offsetof(MMINFHHJGIF_t384011353, ___KLIMLJCLJNH_3)); }
	inline int32_t get_KLIMLJCLJNH_3() const { return ___KLIMLJCLJNH_3; }
	inline int32_t* get_address_of_KLIMLJCLJNH_3() { return &___KLIMLJCLJNH_3; }
	inline void set_KLIMLJCLJNH_3(int32_t value)
	{
		___KLIMLJCLJNH_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
