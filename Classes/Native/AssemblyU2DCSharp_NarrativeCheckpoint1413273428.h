﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// HKKKOKILPBA
struct HKKKOKILPBA_t3869624123;
// NarrativeCondition
struct NarrativeCondition_t4123859457;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NarrativeCheckpoint
struct  NarrativeCheckpoint_t1413273428  : public MonoBehaviour_t1158329972
{
public:
	// System.String NarrativeCheckpoint::checkpointName
	String_t* ___checkpointName_2;
	// HKKKOKILPBA NarrativeCheckpoint::KHGBALLNCLJ
	HKKKOKILPBA_t3869624123 * ___KHGBALLNCLJ_3;
	// NarrativeCondition NarrativeCheckpoint::GPLMPEHHMKG
	NarrativeCondition_t4123859457 * ___GPLMPEHHMKG_4;

public:
	inline static int32_t get_offset_of_checkpointName_2() { return static_cast<int32_t>(offsetof(NarrativeCheckpoint_t1413273428, ___checkpointName_2)); }
	inline String_t* get_checkpointName_2() const { return ___checkpointName_2; }
	inline String_t** get_address_of_checkpointName_2() { return &___checkpointName_2; }
	inline void set_checkpointName_2(String_t* value)
	{
		___checkpointName_2 = value;
		Il2CppCodeGenWriteBarrier(&___checkpointName_2, value);
	}

	inline static int32_t get_offset_of_KHGBALLNCLJ_3() { return static_cast<int32_t>(offsetof(NarrativeCheckpoint_t1413273428, ___KHGBALLNCLJ_3)); }
	inline HKKKOKILPBA_t3869624123 * get_KHGBALLNCLJ_3() const { return ___KHGBALLNCLJ_3; }
	inline HKKKOKILPBA_t3869624123 ** get_address_of_KHGBALLNCLJ_3() { return &___KHGBALLNCLJ_3; }
	inline void set_KHGBALLNCLJ_3(HKKKOKILPBA_t3869624123 * value)
	{
		___KHGBALLNCLJ_3 = value;
		Il2CppCodeGenWriteBarrier(&___KHGBALLNCLJ_3, value);
	}

	inline static int32_t get_offset_of_GPLMPEHHMKG_4() { return static_cast<int32_t>(offsetof(NarrativeCheckpoint_t1413273428, ___GPLMPEHHMKG_4)); }
	inline NarrativeCondition_t4123859457 * get_GPLMPEHHMKG_4() const { return ___GPLMPEHHMKG_4; }
	inline NarrativeCondition_t4123859457 ** get_address_of_GPLMPEHHMKG_4() { return &___GPLMPEHHMKG_4; }
	inline void set_GPLMPEHHMKG_4(NarrativeCondition_t4123859457 * value)
	{
		___GPLMPEHHMKG_4 = value;
		Il2CppCodeGenWriteBarrier(&___GPLMPEHHMKG_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
