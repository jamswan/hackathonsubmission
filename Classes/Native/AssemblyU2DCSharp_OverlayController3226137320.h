﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Singleton_1_gen107054411.h"

// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// OverlayController/GJBDJFFPGMG
struct GJBDJFFPGMG_t361342463;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OverlayController
struct  OverlayController_t3226137320  : public Singleton_1_t107054411
{
public:
	// UnityEngine.Camera OverlayController::cam
	Camera_t189460977 * ___cam_5;
	// System.Single OverlayController::depth
	float ___depth_6;
	// UnityEngine.SpriteRenderer OverlayController::BEBNPCCGHGK
	SpriteRenderer_t1209076198 * ___BEBNPCCGHGK_7;
	// UnityEngine.Transform OverlayController::KJLBEHOKPME
	Transform_t3275118058 * ___KJLBEHOKPME_8;
	// UnityEngine.Transform OverlayController::PIABGMNCHFE
	Transform_t3275118058 * ___PIABGMNCHFE_9;
	// UnityEngine.BoxCollider OverlayController::IJLNECIKGMF
	BoxCollider_t22920061 * ___IJLNECIKGMF_10;
	// System.Single OverlayController::FDDIFPENLPD
	float ___FDDIFPENLPD_11;
	// OverlayController/GJBDJFFPGMG OverlayController::OnOverlayTouched
	GJBDJFFPGMG_t361342463 * ___OnOverlayTouched_12;

public:
	inline static int32_t get_offset_of_cam_5() { return static_cast<int32_t>(offsetof(OverlayController_t3226137320, ___cam_5)); }
	inline Camera_t189460977 * get_cam_5() const { return ___cam_5; }
	inline Camera_t189460977 ** get_address_of_cam_5() { return &___cam_5; }
	inline void set_cam_5(Camera_t189460977 * value)
	{
		___cam_5 = value;
		Il2CppCodeGenWriteBarrier(&___cam_5, value);
	}

	inline static int32_t get_offset_of_depth_6() { return static_cast<int32_t>(offsetof(OverlayController_t3226137320, ___depth_6)); }
	inline float get_depth_6() const { return ___depth_6; }
	inline float* get_address_of_depth_6() { return &___depth_6; }
	inline void set_depth_6(float value)
	{
		___depth_6 = value;
	}

	inline static int32_t get_offset_of_BEBNPCCGHGK_7() { return static_cast<int32_t>(offsetof(OverlayController_t3226137320, ___BEBNPCCGHGK_7)); }
	inline SpriteRenderer_t1209076198 * get_BEBNPCCGHGK_7() const { return ___BEBNPCCGHGK_7; }
	inline SpriteRenderer_t1209076198 ** get_address_of_BEBNPCCGHGK_7() { return &___BEBNPCCGHGK_7; }
	inline void set_BEBNPCCGHGK_7(SpriteRenderer_t1209076198 * value)
	{
		___BEBNPCCGHGK_7 = value;
		Il2CppCodeGenWriteBarrier(&___BEBNPCCGHGK_7, value);
	}

	inline static int32_t get_offset_of_KJLBEHOKPME_8() { return static_cast<int32_t>(offsetof(OverlayController_t3226137320, ___KJLBEHOKPME_8)); }
	inline Transform_t3275118058 * get_KJLBEHOKPME_8() const { return ___KJLBEHOKPME_8; }
	inline Transform_t3275118058 ** get_address_of_KJLBEHOKPME_8() { return &___KJLBEHOKPME_8; }
	inline void set_KJLBEHOKPME_8(Transform_t3275118058 * value)
	{
		___KJLBEHOKPME_8 = value;
		Il2CppCodeGenWriteBarrier(&___KJLBEHOKPME_8, value);
	}

	inline static int32_t get_offset_of_PIABGMNCHFE_9() { return static_cast<int32_t>(offsetof(OverlayController_t3226137320, ___PIABGMNCHFE_9)); }
	inline Transform_t3275118058 * get_PIABGMNCHFE_9() const { return ___PIABGMNCHFE_9; }
	inline Transform_t3275118058 ** get_address_of_PIABGMNCHFE_9() { return &___PIABGMNCHFE_9; }
	inline void set_PIABGMNCHFE_9(Transform_t3275118058 * value)
	{
		___PIABGMNCHFE_9 = value;
		Il2CppCodeGenWriteBarrier(&___PIABGMNCHFE_9, value);
	}

	inline static int32_t get_offset_of_IJLNECIKGMF_10() { return static_cast<int32_t>(offsetof(OverlayController_t3226137320, ___IJLNECIKGMF_10)); }
	inline BoxCollider_t22920061 * get_IJLNECIKGMF_10() const { return ___IJLNECIKGMF_10; }
	inline BoxCollider_t22920061 ** get_address_of_IJLNECIKGMF_10() { return &___IJLNECIKGMF_10; }
	inline void set_IJLNECIKGMF_10(BoxCollider_t22920061 * value)
	{
		___IJLNECIKGMF_10 = value;
		Il2CppCodeGenWriteBarrier(&___IJLNECIKGMF_10, value);
	}

	inline static int32_t get_offset_of_FDDIFPENLPD_11() { return static_cast<int32_t>(offsetof(OverlayController_t3226137320, ___FDDIFPENLPD_11)); }
	inline float get_FDDIFPENLPD_11() const { return ___FDDIFPENLPD_11; }
	inline float* get_address_of_FDDIFPENLPD_11() { return &___FDDIFPENLPD_11; }
	inline void set_FDDIFPENLPD_11(float value)
	{
		___FDDIFPENLPD_11 = value;
	}

	inline static int32_t get_offset_of_OnOverlayTouched_12() { return static_cast<int32_t>(offsetof(OverlayController_t3226137320, ___OnOverlayTouched_12)); }
	inline GJBDJFFPGMG_t361342463 * get_OnOverlayTouched_12() const { return ___OnOverlayTouched_12; }
	inline GJBDJFFPGMG_t361342463 ** get_address_of_OnOverlayTouched_12() { return &___OnOverlayTouched_12; }
	inline void set_OnOverlayTouched_12(GJBDJFFPGMG_t361342463 * value)
	{
		___OnOverlayTouched_12 = value;
		Il2CppCodeGenWriteBarrier(&___OnOverlayTouched_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
