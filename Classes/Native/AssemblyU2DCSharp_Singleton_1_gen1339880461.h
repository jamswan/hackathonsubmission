﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GimbalListener
struct GimbalListener_t163996074;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<GimbalListener>
struct  Singleton_1_t1339880461  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t1339880461_StaticFields
{
public:
	// T Singleton`1::NNFGBDNEPJM
	GimbalListener_t163996074 * ___NNFGBDNEPJM_2;
	// System.Object Singleton`1::BJBBGNPOFBK
	Il2CppObject * ___BJBBGNPOFBK_3;
	// System.Boolean Singleton`1::JEPBBCJOKFL
	bool ___JEPBBCJOKFL_4;

public:
	inline static int32_t get_offset_of_NNFGBDNEPJM_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1339880461_StaticFields, ___NNFGBDNEPJM_2)); }
	inline GimbalListener_t163996074 * get_NNFGBDNEPJM_2() const { return ___NNFGBDNEPJM_2; }
	inline GimbalListener_t163996074 ** get_address_of_NNFGBDNEPJM_2() { return &___NNFGBDNEPJM_2; }
	inline void set_NNFGBDNEPJM_2(GimbalListener_t163996074 * value)
	{
		___NNFGBDNEPJM_2 = value;
		Il2CppCodeGenWriteBarrier(&___NNFGBDNEPJM_2, value);
	}

	inline static int32_t get_offset_of_BJBBGNPOFBK_3() { return static_cast<int32_t>(offsetof(Singleton_1_t1339880461_StaticFields, ___BJBBGNPOFBK_3)); }
	inline Il2CppObject * get_BJBBGNPOFBK_3() const { return ___BJBBGNPOFBK_3; }
	inline Il2CppObject ** get_address_of_BJBBGNPOFBK_3() { return &___BJBBGNPOFBK_3; }
	inline void set_BJBBGNPOFBK_3(Il2CppObject * value)
	{
		___BJBBGNPOFBK_3 = value;
		Il2CppCodeGenWriteBarrier(&___BJBBGNPOFBK_3, value);
	}

	inline static int32_t get_offset_of_JEPBBCJOKFL_4() { return static_cast<int32_t>(offsetof(Singleton_1_t1339880461_StaticFields, ___JEPBBCJOKFL_4)); }
	inline bool get_JEPBBCJOKFL_4() const { return ___JEPBBCJOKFL_4; }
	inline bool* get_address_of_JEPBBCJOKFL_4() { return &___JEPBBCJOKFL_4; }
	inline void set_JEPBBCJOKFL_4(bool value)
	{
		___JEPBBCJOKFL_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
