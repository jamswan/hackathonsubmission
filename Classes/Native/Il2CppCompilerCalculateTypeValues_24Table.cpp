﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LocationManager_NEPBPOIONDG194714732.h"
#include "AssemblyU2DCSharp_LocationManager_HKHFLGLBEKJ3398908651.h"
#include "AssemblyU2DCSharp_OverlayController3226137320.h"
#include "AssemblyU2DCSharp_OverlayController_GJBDJFFPGMG361342463.h"
#include "AssemblyU2DCSharp_EMKDNKNNALF684729071.h"
#include "AssemblyU2DCSharp_PlaceLogSwitcher3557142116.h"
#include "AssemblyU2DCSharp_APEPFIBKILA4136611566.h"
#include "AssemblyU2DCSharp_GBMIMKMMFPO601510760.h"
#include "AssemblyU2DCSharp_MMINFHHJGIF384011353.h"
#include "AssemblyU2DCSharp_PKFANFHEKDM3814445125.h"
#include "AssemblyU2DCSharp_FHOAHDEONLG2556966793.h"
#include "AssemblyU2DCSharp_LACPNLLLPEM640488948.h"
#include "AssemblyU2DCSharp_UIController2029583246.h"
#include "AssemblyU2DCSharp_MagnifyingGlass1447642303.h"
#include "AssemblyU2DCSharp_AutoFollowPlayerLocation3187216184.h"
#include "AssemblyU2DCSharp_ImageMap2947291373.h"
#include "AssemblyU2DCSharp_MapDrag1912005084.h"
#include "AssemblyU2DCSharp_MapHint3558901209.h"
#include "AssemblyU2DCSharp_MapLocations3433534602.h"
#include "AssemblyU2DCSharp_MapLocation3768883339.h"
#include "AssemblyU2DCSharp_NarrativeCheckpoint1413273428.h"
#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"
#include "AssemblyU2DCSharp_AudioFinished2595625456.h"
#include "AssemblyU2DCSharp_AugmentationFocusedOnTarget1552339247.h"
#include "AssemblyU2DCSharp_ButtonDisable334175964.h"
#include "AssemblyU2DCSharp_EnabledContainer4279066228.h"
#include "AssemblyU2DCSharp_LocationEntered3638976842.h"
#include "AssemblyU2DCSharp_OnButtonPressed3387707835.h"
#include "AssemblyU2DCSharp_CNKPOABMIJM702222687.h"
#include "AssemblyU2DCSharp_OverlayTouched2030538974.h"
#include "AssemblyU2DCSharp_OverlayTouched_U3CtimerU3Ec__Iter519015163.h"
#include "AssemblyU2DCSharp_OverlayWait4280792591.h"
#include "AssemblyU2DCSharp_OverlayWait_U3CtimerU3Ec__Iterat3479578004.h"
#include "AssemblyU2DCSharp_PopupFinished3728197864.h"
#include "AssemblyU2DCSharp_PopupFinished_U3CtimerU3Ec__Iter3255825633.h"
#include "AssemblyU2DCSharp_QuillMessageFinished3889532506.h"
#include "AssemblyU2DCSharp_StartButton3892719096.h"
#include "AssemblyU2DCSharp_TargetSeen1806735238.h"
#include "AssemblyU2DCSharp_TextRecognized2802831619.h"
#include "AssemblyU2DCSharp_TimePassed3116502431.h"
#include "AssemblyU2DCSharp_TimePassed_U3CtimerU3Ec__Iterator300358104.h"
#include "AssemblyU2DCSharp_YesNoChoice3116058963.h"
#include "AssemblyU2DCSharp_NarrativeController1535474994.h"
#include "AssemblyU2DCSharp_NarrativeController_DIKDKCCLMNH2086442123.h"
#include "AssemblyU2DCSharp_NarrativeController_EFPHLCGIJJG2265475206.h"
#include "AssemblyU2DCSharp_NarrativeController_APPPEKEOLLO1830122991.h"
#include "AssemblyU2DCSharp_HFHIMOBLMJG2996998969.h"
#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"
#include "AssemblyU2DCSharp_AnimatorRebind1001071567.h"
#include "AssemblyU2DCSharp_AnimatorTrigger1270260293.h"
#include "AssemblyU2DCSharp_AnyTouch456114821.h"
#include "AssemblyU2DCSharp_CameraSwitchEffect2333008950.h"
#include "AssemblyU2DCSharp_CameraTetherAdd734575530.h"
#include "AssemblyU2DCSharp_CameraTetherRemove423982543.h"
#include "AssemblyU2DCSharp_DisableGameObject929099789.h"
#include "AssemblyU2DCSharp_EnableGameObject1635748018.h"
#include "AssemblyU2DCSharp_LogMessage3320573177.h"
#include "AssemblyU2DCSharp_LogMessageGraphic1462040547.h"
#include "AssemblyU2DCSharp_LogTurnOff2700885558.h"
#include "AssemblyU2DCSharp_LogTurnOn2095167412.h"
#include "AssemblyU2DCSharp_MapFound3085251426.h"
#include "AssemblyU2DCSharp_MapHintAdd691601294.h"
#include "AssemblyU2DCSharp_MapHintRemove4179337195.h"
#include "AssemblyU2DCSharp_MapPlayerLocation2663631732.h"
#include "AssemblyU2DCSharp_MapTurnOff3456851750.h"
#include "AssemblyU2DCSharp_MapTurnOn1389436920.h"
#include "AssemblyU2DCSharp_ModeSetToPlay388334726.h"
#include "AssemblyU2DCSharp_ModeSetToStory333251123.h"
#include "AssemblyU2DCSharp_OverlayClear712249573.h"
#include "AssemblyU2DCSharp_OverlayNewGraphic4179090742.h"
#include "AssemblyU2DCSharp_PopupClosable2956162765.h"
#include "AssemblyU2DCSharp_PopupDuration1273651850.h"
#include "AssemblyU2DCSharp_QuillMessage937847432.h"
#include "AssemblyU2DCSharp_QuillReturn4068305857.h"
#include "AssemblyU2DCSharp_ResetCheckpoint3420044365.h"
#include "AssemblyU2DCSharp_RestartNarrative3582961693.h"
#include "AssemblyU2DCSharp_SetMeshRendererMaterial2687404289.h"
#include "AssemblyU2DCSharp_TetherActivate3288536165.h"
#include "AssemblyU2DCSharp_TetherDeactivate2659602610.h"
#include "AssemblyU2DCSharp_TextRecognizorDisable1433823199.h"
#include "AssemblyU2DCSharp_TextRecognizorEnable3741391952.h"
#include "AssemblyU2DCSharp_UIButtonActivate791039529.h"
#include "AssemblyU2DCSharp_UIButtonDeactivate535056236.h"
#include "AssemblyU2DCSharp_GimbalPlacePopup137360091.h"
#include "AssemblyU2DCSharp_QuillController2665191159.h"
#include "AssemblyU2DCSharp_QuillController_U3CdisableU3Ec__3079559271.h"
#include "AssemblyU2DCSharp_QuillController_U3CwaitforU3Ec__3642873752.h"
#include "AssemblyU2DCSharp_QuillController_U3CfadeTextU3Ec_3047042458.h"
#include "AssemblyU2DCSharp_QuaternionTest88039808.h"
#include "AssemblyU2DCSharp_SceneSwitchOnClick793957185.h"
#include "AssemblyU2DCSharp_TextEventHandler4116109227.h"
#include "AssemblyU2DCSharp_AutoLoad1292637803.h"
#include "AssemblyU2DCSharp_CCIGGFOILCD2687981402.h"
#include "AssemblyU2DCSharp_DisableAfterAwake2303007679.h"
#include "AssemblyU2DCSharp_DisableOnScene3768098929.h"
#include "AssemblyU2DCSharp_DisableScriptOnLoad2620751154.h"
#include "AssemblyU2DCSharp_DontDestroyOnLoad3235789354.h"
#include "AssemblyU2DCSharp_ScalePlaneToOrthographicCameraSi1221350851.h"
#include "AssemblyU2DCSharp_SetScreenResolution969167286.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (NEPBPOIONDG_t194714732), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (HKHFLGLBEKJ_t3398908651), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (OverlayController_t3226137320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[8] = 
{
	OverlayController_t3226137320::get_offset_of_cam_5(),
	OverlayController_t3226137320::get_offset_of_depth_6(),
	OverlayController_t3226137320::get_offset_of_BEBNPCCGHGK_7(),
	OverlayController_t3226137320::get_offset_of_KJLBEHOKPME_8(),
	OverlayController_t3226137320::get_offset_of_PIABGMNCHFE_9(),
	OverlayController_t3226137320::get_offset_of_IJLNECIKGMF_10(),
	OverlayController_t3226137320::get_offset_of_FDDIFPENLPD_11(),
	OverlayController_t3226137320::get_offset_of_OnOverlayTouched_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (GJBDJFFPGMG_t361342463), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (EMKDNKNNALF_t684729071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2404[8] = 
{
	EMKDNKNNALF_t684729071::get_offset_of_COLCJHPAGJN_0(),
	EMKDNKNNALF_t684729071::get_offset_of_GAEKKNANIAP_1(),
	EMKDNKNNALF_t684729071::get_offset_of_OCKNMJJGPAM_2(),
	EMKDNKNNALF_t684729071::get_offset_of_NKNKLLNLJOI_3(),
	EMKDNKNNALF_t684729071::get_offset_of_HPJMAGFMLBO_4(),
	EMKDNKNNALF_t684729071::get_offset_of_KLIMLJCLJNH_5(),
	EMKDNKNNALF_t684729071::get_offset_of_JPOCLFJIIMD_6(),
	EMKDNKNNALF_t684729071::get_offset_of_DAEHOJHKAAL_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (PlaceLogSwitcher_t3557142116), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (APEPFIBKILA_t4136611566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2406[18] = 
{
	APEPFIBKILA_t4136611566::get_offset_of_AJILCIKCFEA_0(),
	APEPFIBKILA_t4136611566::get_offset_of_BBCJADHMNPG_1(),
	APEPFIBKILA_t4136611566::get_offset_of_DIONBIDEDAB_2(),
	APEPFIBKILA_t4136611566::get_offset_of_DCJOKBNIADI_3(),
	APEPFIBKILA_t4136611566::get_offset_of_HBENJAFPHAM_4(),
	APEPFIBKILA_t4136611566::get_offset_of_OJNDJDOCAOH_5(),
	APEPFIBKILA_t4136611566::get_offset_of_CLNICLJBMNO_6(),
	APEPFIBKILA_t4136611566::get_offset_of_EKNMHBLCPFK_7(),
	APEPFIBKILA_t4136611566::get_offset_of_KGKJBBMMFHF_8(),
	APEPFIBKILA_t4136611566::get_offset_of_IECEJPLGHLG_9(),
	APEPFIBKILA_t4136611566::get_offset_of_IBGIKPJLEOE_10(),
	APEPFIBKILA_t4136611566::get_offset_of_DKFMAPPPOJF_11(),
	APEPFIBKILA_t4136611566::get_offset_of_HJNDDAGGJFB_12(),
	APEPFIBKILA_t4136611566::get_offset_of_LAIFECBAIIK_13(),
	APEPFIBKILA_t4136611566::get_offset_of_LEFAFPBDGIB_14(),
	APEPFIBKILA_t4136611566::get_offset_of_HFIHIMBLAFN_15(),
	APEPFIBKILA_t4136611566::get_offset_of_NLLIGEPDOBH_16(),
	APEPFIBKILA_t4136611566::get_offset_of_BMIBBEADOHI_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (GBMIMKMMFPO_t601510760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[6] = 
{
	GBMIMKMMFPO_t601510760::get_offset_of_ENMDIGLCNEF_0(),
	GBMIMKMMFPO_t601510760::get_offset_of_IECEJPLGHLG_1(),
	GBMIMKMMFPO_t601510760::get_offset_of_JNLEFANLCNC_2(),
	GBMIMKMMFPO_t601510760::get_offset_of_HPJMAGFMLBO_3(),
	GBMIMKMMFPO_t601510760::get_offset_of_KLIMLJCLJNH_4(),
	GBMIMKMMFPO_t601510760::get_offset_of_NOPKKJHHIOG_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (MMINFHHJGIF_t384011353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2408[4] = 
{
	MMINFHHJGIF_t384011353::get_offset_of_IECEJPLGHLG_0(),
	MMINFHHJGIF_t384011353::get_offset_of_JHKJOGKJIPH_1(),
	MMINFHHJGIF_t384011353::get_offset_of_HPJMAGFMLBO_2(),
	MMINFHHJGIF_t384011353::get_offset_of_KLIMLJCLJNH_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (PKFANFHEKDM_t3814445125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[3] = 
{
	PKFANFHEKDM_t3814445125::get_offset_of_ANNIDELAEEC_4(),
	PKFANFHEKDM_t3814445125::get_offset_of_MHAHJONGPBJ_5(),
	PKFANFHEKDM_t3814445125::get_offset_of_IDBKNFKOEBF_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (FHOAHDEONLG_t2556966793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2410[2] = 
{
	FHOAHDEONLG_t2556966793::get_offset_of_COLCJHPAGJN_4(),
	FHOAHDEONLG_t2556966793::get_offset_of_ACCKGOBDPLM_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (LACPNLLLPEM_t640488948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2411[1] = 
{
	LACPNLLLPEM_t640488948::get_offset_of_IDBKNFKOEBF_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (UIController_t2029583246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[34] = 
{
	UIController_t2029583246::get_offset_of_mainSkin_5(),
	UIController_t2029583246::get_offset_of_HKOJCMJPDNN_6(),
	UIController_t2029583246::get_offset_of_MapButtonStyleName_7(),
	UIController_t2029583246::get_offset_of_PJJNMAKANJM_8(),
	UIController_t2029583246::get_offset_of_LogButtonStyleName_9(),
	UIController_t2029583246::get_offset_of_FMENMEOCJNN_10(),
	UIController_t2029583246::get_offset_of_ReturnStyleName_11(),
	UIController_t2029583246::get_offset_of_AJPMLMBBNCJ_12(),
	UIController_t2029583246::get_offset_of_DownStyleName_13(),
	UIController_t2029583246::get_offset_of_CCIMIICKAKN_14(),
	UIController_t2029583246::get_offset_of_scrollLog_15(),
	UIController_t2029583246::get_offset_of_currentScrollLog_16(),
	UIController_t2029583246::get_offset_of_BBCJADHMNPG_17(),
	UIController_t2029583246::get_offset_of_globalLogItemStyleName_18(),
	UIController_t2029583246::get_offset_of_map_19(),
	UIController_t2029583246::get_offset_of_MaxButtonInchSize_20(),
	UIController_t2029583246::get_offset_of_ButtonScreenPercentSize_21(),
	UIController_t2029583246::get_offset_of_bufferInchSize_22(),
	UIController_t2029583246::get_offset_of_ALGOAFLCJLH_23(),
	UIController_t2029583246::get_offset_of_ADOLICJBCNN_24(),
	UIController_t2029583246::get_offset_of_CFLJPJJAIOA_25(),
	UIController_t2029583246::get_offset_of_popupWidth_26(),
	UIController_t2029583246::get_offset_of_popupHeight_27(),
	UIController_t2029583246::get_offset_of_HFIHIMBLAFN_28(),
	UIController_t2029583246::get_offset_of_LogHeight_29(),
	UIController_t2029583246::get_offset_of_LEFAFPBDGIB_30(),
	UIController_t2029583246::get_offset_of_MapFound_31(),
	UIController_t2029583246::get_offset_of_NEMPOOIIENI_32(),
	UIController_t2029583246::get_offset_of_showLog_33(),
	UIController_t2029583246::get_offset_of_debugLogVisible_34(),
	UIController_t2029583246::get_offset_of_HNMMOMKIPLL_35(),
	UIController_t2029583246::get_offset_of_LLMMHPGBGJJ_36(),
	UIController_t2029583246::get_offset_of_hasMap_37(),
	UIController_t2029583246::get_offset_of_mapview_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (MagnifyingGlass_t1447642303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[10] = 
{
	MagnifyingGlass_t1447642303::get_offset_of_animationSpeed_2(),
	MagnifyingGlass_t1447642303::get_offset_of_rotationSpeed_3(),
	MagnifyingGlass_t1447642303::get_offset_of_KALCNDOLONH_4(),
	MagnifyingGlass_t1447642303::get_offset_of_AJNBKMJEGKK_5(),
	MagnifyingGlass_t1447642303::get_offset_of_JKDKGPAEDKK_6(),
	MagnifyingGlass_t1447642303::get_offset_of_JBCPFANOKNE_7(),
	MagnifyingGlass_t1447642303::get_offset_of_LOCDEMAEFBP_8(),
	MagnifyingGlass_t1447642303::get_offset_of_KJLBEHOKPME_9(),
	MagnifyingGlass_t1447642303::get_offset_of_FGPFCKFBMAD_10(),
	MagnifyingGlass_t1447642303::get_offset_of_AJILCIKCFEA_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (AutoFollowPlayerLocation_t3187216184), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (ImageMap_t2947291373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[7] = 
{
	ImageMap_t2947291373::get_offset_of_rayDistance_2(),
	ImageMap_t2947291373::get_offset_of_layer_3(),
	ImageMap_t2947291373::get_offset_of_clickTexture_4(),
	ImageMap_t2947291373::get_offset_of_cam_5(),
	ImageMap_t2947291373::get_offset_of_colors_6(),
	ImageMap_t2947291373::get_offset_of_texts_7(),
	ImageMap_t2947291373::get_offset_of_OFLFFDAIAEI_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (MapDrag_t1912005084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[3] = 
{
	MapDrag_t1912005084::get_offset_of_KJLBEHOKPME_2(),
	MapDrag_t1912005084::get_offset_of_BBMILCGFAEN_3(),
	MapDrag_t1912005084::get_offset_of_KHFIGCOODNA_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (MapHint_t3558901209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[1] = 
{
	MapHint_t3558901209::get_offset_of_hintText_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (MapLocations_t3433534602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[5] = 
{
	MapLocations_t3433534602::get_offset_of_playerMarker_5(),
	MapLocations_t3433534602::get_offset_of_hintMarker_6(),
	MapLocations_t3433534602::get_offset_of_setMapLocations_7(),
	MapLocations_t3433534602::get_offset_of_HAOAJEOLMNG_8(),
	MapLocations_t3433534602::get_offset_of_IBIJEOFAMJA_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (MapLocation_t3768883339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[2] = 
{
	MapLocation_t3768883339::get_offset_of_name_0(),
	MapLocation_t3768883339::get_offset_of_location_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (NarrativeCheckpoint_t1413273428), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[3] = 
{
	NarrativeCheckpoint_t1413273428::get_offset_of_checkpointName_2(),
	NarrativeCheckpoint_t1413273428::get_offset_of_KHGBALLNCLJ_3(),
	NarrativeCheckpoint_t1413273428::get_offset_of_GPLMPEHHMKG_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (NarrativeCondition_t4123859457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[11] = 
{
	NarrativeCondition_t4123859457::get_offset_of_simulate_2(),
	NarrativeCondition_t4123859457::get_offset_of_reusable_3(),
	NarrativeCondition_t4123859457::get_offset_of_repeat_4(),
	NarrativeCondition_t4123859457::get_offset_of_narrativeObjectParent_5(),
	NarrativeCondition_t4123859457::get_offset_of_narrativeObjectChildren_6(),
	NarrativeCondition_t4123859457::get_offset_of_narrativeEffects_7(),
	NarrativeCondition_t4123859457::get_offset_of_HFBHJJDNKAF_8(),
	NarrativeCondition_t4123859457::get_offset_of_or_9(),
	NarrativeCondition_t4123859457::get_offset_of_nextToEnable_10(),
	NarrativeCondition_t4123859457::get_offset_of_BLKKEILFIPK_11(),
	NarrativeCondition_t4123859457::get_offset_of_ECPKIOBFGJD_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (AudioFinished_t2595625456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[3] = 
{
	AudioFinished_t2595625456::get_offset_of_audioClip_13(),
	AudioFinished_t2595625456::get_offset_of_audioSource_14(),
	AudioFinished_t2595625456::get_offset_of_PPIONFLEHAL_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (AugmentationFocusedOnTarget_t1552339247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2423[1] = 
{
	AugmentationFocusedOnTarget_t1552339247::get_offset_of_tether_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (ButtonDisable_t334175964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2424[3] = 
{
	ButtonDisable_t334175964::get_offset_of_location_13(),
	ButtonDisable_t334175964::get_offset_of_text_14(),
	ButtonDisable_t334175964::get_offset_of_Force_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (EnabledContainer_t4279066228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (LocationEntered_t3638976842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[1] = 
{
	LocationEntered_t3638976842::get_offset_of_location_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (OnButtonPressed_t3387707835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[13] = 
{
	OnButtonPressed_t3387707835::get_offset_of_buttonText_13(),
	OnButtonPressed_t3387707835::get_offset_of_styleOverride_14(),
	OnButtonPressed_t3387707835::get_offset_of_style_15(),
	OnButtonPressed_t3387707835::get_offset_of_alignment_16(),
	OnButtonPressed_t3387707835::get_offset_of_percentageXPos_17(),
	OnButtonPressed_t3387707835::get_offset_of_percentageYPos_18(),
	OnButtonPressed_t3387707835::get_offset_of_buttonScreenPercentSize_19(),
	OnButtonPressed_t3387707835::get_offset_of_buttonMaxInchSize_20(),
	OnButtonPressed_t3387707835::get_offset_of_OKFCAAHCBBI_21(),
	OnButtonPressed_t3387707835::get_offset_of_JHKJOGKJIPH_22(),
	OnButtonPressed_t3387707835::get_offset_of_JNLEFANLCNC_23(),
	OnButtonPressed_t3387707835::get_offset_of_HPJMAGFMLBO_24(),
	OnButtonPressed_t3387707835::get_offset_of_KLIMLJCLJNH_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (CNKPOABMIJM_t702222687)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2428[10] = 
{
	CNKPOABMIJM_t702222687::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (OverlayTouched_t2030538974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2429[3] = 
{
	OverlayTouched_t2030538974::get_offset_of_sprite_13(),
	OverlayTouched_t2030538974::get_offset_of_clearMenu_14(),
	OverlayTouched_t2030538974::get_offset_of_AutoTapTime_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (U3CtimerU3Ec__Iterator0_t519015163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[5] = 
{
	U3CtimerU3Ec__Iterator0_t519015163::get_offset_of_IGIHJBALJEK_0(),
	U3CtimerU3Ec__Iterator0_t519015163::get_offset_of_AOOLEAHHMIH_1(),
	U3CtimerU3Ec__Iterator0_t519015163::get_offset_of_LGBFNMECDHC_2(),
	U3CtimerU3Ec__Iterator0_t519015163::get_offset_of_IMAGLIMLPFK_3(),
	U3CtimerU3Ec__Iterator0_t519015163::get_offset_of_EPCFNNGDBGC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (OverlayWait_t4280792591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[3] = 
{
	OverlayWait_t4280792591::get_offset_of_sprite_13(),
	OverlayWait_t4280792591::get_offset_of_clearMenu_14(),
	OverlayWait_t4280792591::get_offset_of_duration_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (U3CtimerU3Ec__Iterator0_t3479578004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2432[5] = 
{
	U3CtimerU3Ec__Iterator0_t3479578004::get_offset_of_IGIHJBALJEK_0(),
	U3CtimerU3Ec__Iterator0_t3479578004::get_offset_of_AOOLEAHHMIH_1(),
	U3CtimerU3Ec__Iterator0_t3479578004::get_offset_of_LGBFNMECDHC_2(),
	U3CtimerU3Ec__Iterator0_t3479578004::get_offset_of_IMAGLIMLPFK_3(),
	U3CtimerU3Ec__Iterator0_t3479578004::get_offset_of_EPCFNNGDBGC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (PopupFinished_t3728197864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2433[4] = 
{
	PopupFinished_t3728197864::get_offset_of_logName_13(),
	PopupFinished_t3728197864::get_offset_of_message_14(),
	PopupFinished_t3728197864::get_offset_of_duration_15(),
	PopupFinished_t3728197864::get_offset_of_extraTime_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (U3CtimerU3Ec__Iterator0_t3255825633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[5] = 
{
	U3CtimerU3Ec__Iterator0_t3255825633::get_offset_of_KJGAGDEJFFK_0(),
	U3CtimerU3Ec__Iterator0_t3255825633::get_offset_of_AOOLEAHHMIH_1(),
	U3CtimerU3Ec__Iterator0_t3255825633::get_offset_of_LGBFNMECDHC_2(),
	U3CtimerU3Ec__Iterator0_t3255825633::get_offset_of_IMAGLIMLPFK_3(),
	U3CtimerU3Ec__Iterator0_t3255825633::get_offset_of_EPCFNNGDBGC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (QuillMessageFinished_t3889532506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2435[2] = 
{
	QuillMessageFinished_t3889532506::get_offset_of_messageIndex_13(),
	QuillMessageFinished_t3889532506::get_offset_of_MAOFEGMJEJD_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (StartButton_t3892719096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[2] = 
{
	StartButton_t3892719096::get_offset_of_HPJMAGFMLBO_13(),
	StartButton_t3892719096::get_offset_of_KLIMLJCLJNH_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (TargetSeen_t1806735238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2437[1] = 
{
	TargetSeen_t1806735238::get_offset_of_TargetName_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (TextRecognized_t2802831619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[1] = 
{
	TextRecognized_t2802831619::get_offset_of_words_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (TimePassed_t3116502431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2439[1] = 
{
	TimePassed_t3116502431::get_offset_of_duration_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (U3CtimerU3Ec__Iterator0_t300358104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2440[4] = 
{
	U3CtimerU3Ec__Iterator0_t300358104::get_offset_of_AOOLEAHHMIH_0(),
	U3CtimerU3Ec__Iterator0_t300358104::get_offset_of_LGBFNMECDHC_1(),
	U3CtimerU3Ec__Iterator0_t300358104::get_offset_of_IMAGLIMLPFK_2(),
	U3CtimerU3Ec__Iterator0_t300358104::get_offset_of_EPCFNNGDBGC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (YesNoChoice_t3116058963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2441[4] = 
{
	YesNoChoice_t3116058963::get_offset_of_yesEffect_13(),
	YesNoChoice_t3116058963::get_offset_of_noEffect_14(),
	YesNoChoice_t3116058963::get_offset_of_choiceWidth_15(),
	YesNoChoice_t3116058963::get_offset_of_choiceHeight_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (NarrativeController_t1535474994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[6] = 
{
	NarrativeController_t1535474994::get_offset_of__playMode_5(),
	NarrativeController_t1535474994::get_offset_of_augmentations_6(),
	NarrativeController_t1535474994::get_offset_of_augmentationTethers_7(),
	NarrativeController_t1535474994::get_offset_of_RestartNarrative_8(),
	NarrativeController_t1535474994::get_offset_of_OnFocusTarget_9(),
	NarrativeController_t1535474994::get_offset_of_OnUnFocusTarget_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (DIKDKCCLMNH_t2086442123), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (EFPHLCGIJJG_t2265475206), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (APPPEKEOLLO_t1830122991), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (HFHIMOBLMJG_t2996998969)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2446[3] = 
{
	HFHIMOBLMJG_t2996998969::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (NarrativeEffect_t3336735121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2447[1] = 
{
	NarrativeEffect_t3336735121::get_offset_of_disableEffect_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (AnimatorRebind_t1001071567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[1] = 
{
	AnimatorRebind_t1001071567::get_offset_of_animator_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (AnimatorTrigger_t1270260293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[2] = 
{
	AnimatorTrigger_t1270260293::get_offset_of_animator_3(),
	AnimatorTrigger_t1270260293::get_offset_of_triggerName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (AnyTouch_t456114821), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (CameraSwitchEffect_t2333008950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2451[2] = 
{
	CameraSwitchEffect_t2333008950::get_offset_of_Cam1_3(),
	CameraSwitchEffect_t2333008950::get_offset_of_Cam2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (CameraTetherAdd_t734575530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2452[1] = 
{
	CameraTetherAdd_t734575530::get_offset_of_cameraTether_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (CameraTetherRemove_t423982543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[1] = 
{
	CameraTetherRemove_t423982543::get_offset_of_cameraTether_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (DisableGameObject_t929099789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2454[1] = 
{
	DisableGameObject_t929099789::get_offset_of_gameobject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (EnableGameObject_t1635748018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2455[2] = 
{
	EnableGameObject_t1635748018::get_offset_of_targetObject_3(),
	EnableGameObject_t1635748018::get_offset_of_enableParents_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (LogMessage_t3320573177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2456[4] = 
{
	LogMessage_t3320573177::get_offset_of_logName_3(),
	LogMessage_t3320573177::get_offset_of_message_4(),
	LogMessage_t3320573177::get_offset_of_overrideStyleName_5(),
	LogMessage_t3320573177::get_offset_of_style_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (LogMessageGraphic_t1462040547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2457[5] = 
{
	LogMessageGraphic_t1462040547::get_offset_of_logName_3(),
	LogMessageGraphic_t1462040547::get_offset_of_message_4(),
	LogMessageGraphic_t1462040547::get_offset_of_image_5(),
	LogMessageGraphic_t1462040547::get_offset_of_overrideStyleName_6(),
	LogMessageGraphic_t1462040547::get_offset_of_style_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (LogTurnOff_t2700885558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (LogTurnOn_t2095167412), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (MapFound_t3085251426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (MapHintAdd_t691601294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2461[2] = 
{
	MapHintAdd_t691601294::get_offset_of_locationName_3(),
	MapHintAdd_t691601294::get_offset_of_hintText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (MapHintRemove_t4179337195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2462[1] = 
{
	MapHintRemove_t4179337195::get_offset_of_locationName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (MapPlayerLocation_t2663631732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2463[1] = 
{
	MapPlayerLocation_t2663631732::get_offset_of_locationName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (MapTurnOff_t3456851750), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (MapTurnOn_t1389436920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (ModeSetToPlay_t388334726), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (ModeSetToStory_t333251123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (OverlayClear_t712249573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (OverlayNewGraphic_t4179090742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[2] = 
{
	OverlayNewGraphic_t4179090742::get_offset_of_sprite_3(),
	OverlayNewGraphic_t4179090742::get_offset_of_clearMenu_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (PopupClosable_t2956162765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2470[1] = 
{
	PopupClosable_t2956162765::get_offset_of_popupMessage_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (PopupDuration_t1273651850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2471[2] = 
{
	PopupDuration_t1273651850::get_offset_of_popupMessage_3(),
	PopupDuration_t1273651850::get_offset_of_popupDuration_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (QuillMessage_t937847432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[2] = 
{
	QuillMessage_t937847432::get_offset_of_index_3(),
	QuillMessage_t937847432::get_offset_of_hasBeenSeen_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (QuillReturn_t4068305857), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (ResetCheckpoint_t3420044365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[1] = 
{
	ResetCheckpoint_t3420044365::get_offset_of_checkpointName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (RestartNarrative_t3582961693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[1] = 
{
	RestartNarrative_t3582961693::get_offset_of_CheckPointName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (SetMeshRendererMaterial_t2687404289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2476[2] = 
{
	SetMeshRendererMaterial_t2687404289::get_offset_of_material_3(),
	SetMeshRendererMaterial_t2687404289::get_offset_of_target_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (TetherActivate_t3288536165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[1] = 
{
	TetherActivate_t3288536165::get_offset_of_tether_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (TetherDeactivate_t2659602610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2478[1] = 
{
	TetherDeactivate_t2659602610::get_offset_of_tether_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (TextRecognizorDisable_t1433823199), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (TextRecognizorEnable_t3741391952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (UIButtonActivate_t791039529), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (UIButtonDeactivate_t535056236), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (GimbalPlacePopup_t137360091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[3] = 
{
	GimbalPlacePopup_t137360091::get_offset_of_locationName_2(),
	GimbalPlacePopup_t137360091::get_offset_of_text_3(),
	GimbalPlacePopup_t137360091::get_offset_of_duration_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (QuillController_t2665191159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2484[10] = 
{
	QuillController_t2665191159::get_offset_of_textObjects_5(),
	QuillController_t2665191159::get_offset_of_IIOBJLAHCPP_6(),
	QuillController_t2665191159::get_offset_of_animator_7(),
	QuillController_t2665191159::get_offset_of_writeAnimationClipName_8(),
	QuillController_t2665191159::get_offset_of_idleAnimationClipName_9(),
	QuillController_t2665191159::get_offset_of_idleNoPaperAnimationClipName_10(),
	QuillController_t2665191159::get_offset_of_pointAnimationClipName_11(),
	QuillController_t2665191159::get_offset_of_introAnimationClipName_12(),
	QuillController_t2665191159::get_offset_of_exitAnimationClipName_13(),
	QuillController_t2665191159::get_offset_of_LDJMKOICPCO_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (U3CdisableU3Ec__Iterator0_t3079559271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2485[4] = 
{
	U3CdisableU3Ec__Iterator0_t3079559271::get_offset_of_AOOLEAHHMIH_0(),
	U3CdisableU3Ec__Iterator0_t3079559271::get_offset_of_LGBFNMECDHC_1(),
	U3CdisableU3Ec__Iterator0_t3079559271::get_offset_of_IMAGLIMLPFK_2(),
	U3CdisableU3Ec__Iterator0_t3079559271::get_offset_of_EPCFNNGDBGC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (U3CwaitforU3Ec__Iterator1_t3642873752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2486[5] = 
{
	U3CwaitforU3Ec__Iterator1_t3642873752::get_offset_of_KJGAGDEJFFK_0(),
	U3CwaitforU3Ec__Iterator1_t3642873752::get_offset_of_PLLPPEEBFAP_1(),
	U3CwaitforU3Ec__Iterator1_t3642873752::get_offset_of_LGBFNMECDHC_2(),
	U3CwaitforU3Ec__Iterator1_t3642873752::get_offset_of_IMAGLIMLPFK_3(),
	U3CwaitforU3Ec__Iterator1_t3642873752::get_offset_of_EPCFNNGDBGC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (U3CfadeTextU3Ec__Iterator2_t3047042458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[10] = 
{
	U3CfadeTextU3Ec__Iterator2_t3047042458::get_offset_of_JGCPNFGKBOC_0(),
	U3CfadeTextU3Ec__Iterator2_t3047042458::get_offset_of_IGIHJBALJEK_1(),
	U3CfadeTextU3Ec__Iterator2_t3047042458::get_offset_of_KGKJGGMOJNE_2(),
	U3CfadeTextU3Ec__Iterator2_t3047042458::get_offset_of_OJJNNKBLEPC_3(),
	U3CfadeTextU3Ec__Iterator2_t3047042458::get_offset_of_FJKJHGHOJIC_4(),
	U3CfadeTextU3Ec__Iterator2_t3047042458::get_offset_of_PLLPPEEBFAP_5(),
	U3CfadeTextU3Ec__Iterator2_t3047042458::get_offset_of_AOOLEAHHMIH_6(),
	U3CfadeTextU3Ec__Iterator2_t3047042458::get_offset_of_LGBFNMECDHC_7(),
	U3CfadeTextU3Ec__Iterator2_t3047042458::get_offset_of_IMAGLIMLPFK_8(),
	U3CfadeTextU3Ec__Iterator2_t3047042458::get_offset_of_EPCFNNGDBGC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (QuaternionTest_t88039808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[3] = 
{
	QuaternionTest_t88039808::get_offset_of_target_2(),
	QuaternionTest_t88039808::get_offset_of_camera_3(),
	QuaternionTest_t88039808::get_offset_of_anchor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (SceneSwitchOnClick_t793957185), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (TextEventHandler_t4116109227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2490[14] = 
{
	TextEventHandler_t4116109227::get_offset_of_DGHIIGFLIBN_2(),
	TextEventHandler_t4116109227::get_offset_of_CBMMAHGIJDK_3(),
	TextEventHandler_t4116109227::get_offset_of_GICCCFHNIJP_4(),
	TextEventHandler_t4116109227::get_offset_of_IPPNHBNCMDG_5(),
	TextEventHandler_t4116109227::get_offset_of_EEGCDGMGMCA_6(),
	TextEventHandler_t4116109227::get_offset_of_LIBNLFLPENI_7(),
	TextEventHandler_t4116109227::get_offset_of_PGCBBCAOJHI_8(),
	TextEventHandler_t4116109227::get_offset_of_NCEMCAAKJLM_9(),
	TextEventHandler_t4116109227::get_offset_of_OIAOLNMENJC_10(),
	TextEventHandler_t4116109227::get_offset_of_PGLHJGGJHEI_11(),
	TextEventHandler_t4116109227::get_offset_of_NCNNHEHENKA_12(),
	TextEventHandler_t4116109227::get_offset_of_PODOCCPNBFB_13(),
	TextEventHandler_t4116109227::get_offset_of_boundingBoxMaterial_14(),
	TextEventHandler_t4116109227::get_offset_of_textRecoCanvas_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (AutoLoad_t1292637803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[1] = 
{
	AutoLoad_t1292637803::get_offset_of_levelName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (CCIGGFOILCD_t2687981402), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (DisableAfterAwake_t2303007679), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (DisableOnScene_t3768098929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2494[2] = 
{
	DisableOnScene_t3768098929::get_offset_of_SceneName_2(),
	DisableOnScene_t3768098929::get_offset_of_gO_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (DisableScriptOnLoad_t2620751154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[2] = 
{
	DisableScriptOnLoad_t2620751154::get_offset_of_mono_2(),
	DisableScriptOnLoad_t2620751154::get_offset_of_SceneName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (DontDestroyOnLoad_t3235789354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (ScalePlaneToOrthographicCameraSize_t1221350851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2497[1] = 
{
	ScalePlaneToOrthographicCameraSize_t1221350851::get_offset_of_cam_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (SetScreenResolution_t969167286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2498[1] = 
{
	SetScreenResolution_t969167286::get_offset_of_screenHeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[3] = 
{
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
