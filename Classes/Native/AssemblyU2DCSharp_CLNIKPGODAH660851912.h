﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CLNIKPGODAH
struct  CLNIKPGODAH_t660851912  : public Il2CppObject
{
public:
	// System.String CLNIKPGODAH::GANGPNMIIIF
	String_t* ___GANGPNMIIIF_0;
	// System.String CLNIKPGODAH::GAAMIIMINFA
	String_t* ___GAAMIIMINFA_1;

public:
	inline static int32_t get_offset_of_GANGPNMIIIF_0() { return static_cast<int32_t>(offsetof(CLNIKPGODAH_t660851912, ___GANGPNMIIIF_0)); }
	inline String_t* get_GANGPNMIIIF_0() const { return ___GANGPNMIIIF_0; }
	inline String_t** get_address_of_GANGPNMIIIF_0() { return &___GANGPNMIIIF_0; }
	inline void set_GANGPNMIIIF_0(String_t* value)
	{
		___GANGPNMIIIF_0 = value;
		Il2CppCodeGenWriteBarrier(&___GANGPNMIIIF_0, value);
	}

	inline static int32_t get_offset_of_GAAMIIMINFA_1() { return static_cast<int32_t>(offsetof(CLNIKPGODAH_t660851912, ___GAAMIIMINFA_1)); }
	inline String_t* get_GAAMIIMINFA_1() const { return ___GAAMIIMINFA_1; }
	inline String_t** get_address_of_GAAMIIMINFA_1() { return &___GAAMIIMINFA_1; }
	inline void set_GAAMIIMINFA_1(String_t* value)
	{
		___GAAMIIMINFA_1 = value;
		Il2CppCodeGenWriteBarrier(&___GAAMIIMINFA_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
