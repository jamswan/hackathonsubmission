﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Canvas
struct Canvas_t209405766;
// MenuOptions
struct MenuOptions_t3210604277;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuAnimator
struct  MenuAnimator_t2049002970  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 MenuAnimator::GMMCNAKGOGM
	Vector3_t2243707580  ___GMMCNAKGOGM_2;
	// UnityEngine.Vector3 MenuAnimator::FGFDJICIBAG
	Vector3_t2243707580  ___FGFDJICIBAG_3;
	// System.Single MenuAnimator::KHFPKDOJADK
	float ___KHFPKDOJADK_4;
	// System.Boolean MenuAnimator::FKGAMOOOIJN
	bool ___FKGAMOOOIJN_5;
	// UnityEngine.Canvas MenuAnimator::HJABAJAFDFM
	Canvas_t209405766 * ___HJABAJAFDFM_6;
	// MenuOptions MenuAnimator::CDLADKEPAKD
	MenuOptions_t3210604277 * ___CDLADKEPAKD_7;
	// System.Single MenuAnimator::SlidingTime
	float ___SlidingTime_8;

public:
	inline static int32_t get_offset_of_GMMCNAKGOGM_2() { return static_cast<int32_t>(offsetof(MenuAnimator_t2049002970, ___GMMCNAKGOGM_2)); }
	inline Vector3_t2243707580  get_GMMCNAKGOGM_2() const { return ___GMMCNAKGOGM_2; }
	inline Vector3_t2243707580 * get_address_of_GMMCNAKGOGM_2() { return &___GMMCNAKGOGM_2; }
	inline void set_GMMCNAKGOGM_2(Vector3_t2243707580  value)
	{
		___GMMCNAKGOGM_2 = value;
	}

	inline static int32_t get_offset_of_FGFDJICIBAG_3() { return static_cast<int32_t>(offsetof(MenuAnimator_t2049002970, ___FGFDJICIBAG_3)); }
	inline Vector3_t2243707580  get_FGFDJICIBAG_3() const { return ___FGFDJICIBAG_3; }
	inline Vector3_t2243707580 * get_address_of_FGFDJICIBAG_3() { return &___FGFDJICIBAG_3; }
	inline void set_FGFDJICIBAG_3(Vector3_t2243707580  value)
	{
		___FGFDJICIBAG_3 = value;
	}

	inline static int32_t get_offset_of_KHFPKDOJADK_4() { return static_cast<int32_t>(offsetof(MenuAnimator_t2049002970, ___KHFPKDOJADK_4)); }
	inline float get_KHFPKDOJADK_4() const { return ___KHFPKDOJADK_4; }
	inline float* get_address_of_KHFPKDOJADK_4() { return &___KHFPKDOJADK_4; }
	inline void set_KHFPKDOJADK_4(float value)
	{
		___KHFPKDOJADK_4 = value;
	}

	inline static int32_t get_offset_of_FKGAMOOOIJN_5() { return static_cast<int32_t>(offsetof(MenuAnimator_t2049002970, ___FKGAMOOOIJN_5)); }
	inline bool get_FKGAMOOOIJN_5() const { return ___FKGAMOOOIJN_5; }
	inline bool* get_address_of_FKGAMOOOIJN_5() { return &___FKGAMOOOIJN_5; }
	inline void set_FKGAMOOOIJN_5(bool value)
	{
		___FKGAMOOOIJN_5 = value;
	}

	inline static int32_t get_offset_of_HJABAJAFDFM_6() { return static_cast<int32_t>(offsetof(MenuAnimator_t2049002970, ___HJABAJAFDFM_6)); }
	inline Canvas_t209405766 * get_HJABAJAFDFM_6() const { return ___HJABAJAFDFM_6; }
	inline Canvas_t209405766 ** get_address_of_HJABAJAFDFM_6() { return &___HJABAJAFDFM_6; }
	inline void set_HJABAJAFDFM_6(Canvas_t209405766 * value)
	{
		___HJABAJAFDFM_6 = value;
		Il2CppCodeGenWriteBarrier(&___HJABAJAFDFM_6, value);
	}

	inline static int32_t get_offset_of_CDLADKEPAKD_7() { return static_cast<int32_t>(offsetof(MenuAnimator_t2049002970, ___CDLADKEPAKD_7)); }
	inline MenuOptions_t3210604277 * get_CDLADKEPAKD_7() const { return ___CDLADKEPAKD_7; }
	inline MenuOptions_t3210604277 ** get_address_of_CDLADKEPAKD_7() { return &___CDLADKEPAKD_7; }
	inline void set_CDLADKEPAKD_7(MenuOptions_t3210604277 * value)
	{
		___CDLADKEPAKD_7 = value;
		Il2CppCodeGenWriteBarrier(&___CDLADKEPAKD_7, value);
	}

	inline static int32_t get_offset_of_SlidingTime_8() { return static_cast<int32_t>(offsetof(MenuAnimator_t2049002970, ___SlidingTime_8)); }
	inline float get_SlidingTime_8() const { return ___SlidingTime_8; }
	inline float* get_address_of_SlidingTime_8() { return &___SlidingTime_8; }
	inline void set_SlidingTime_8(float value)
	{
		___SlidingTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
