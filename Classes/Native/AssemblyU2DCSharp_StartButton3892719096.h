﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartButton
struct  StartButton_t3892719096  : public NarrativeCondition_t4123859457
{
public:
	// System.Int32 StartButton::HPJMAGFMLBO
	int32_t ___HPJMAGFMLBO_13;
	// System.Int32 StartButton::KLIMLJCLJNH
	int32_t ___KLIMLJCLJNH_14;

public:
	inline static int32_t get_offset_of_HPJMAGFMLBO_13() { return static_cast<int32_t>(offsetof(StartButton_t3892719096, ___HPJMAGFMLBO_13)); }
	inline int32_t get_HPJMAGFMLBO_13() const { return ___HPJMAGFMLBO_13; }
	inline int32_t* get_address_of_HPJMAGFMLBO_13() { return &___HPJMAGFMLBO_13; }
	inline void set_HPJMAGFMLBO_13(int32_t value)
	{
		___HPJMAGFMLBO_13 = value;
	}

	inline static int32_t get_offset_of_KLIMLJCLJNH_14() { return static_cast<int32_t>(offsetof(StartButton_t3892719096, ___KLIMLJCLJNH_14)); }
	inline int32_t get_KLIMLJCLJNH_14() const { return ___KLIMLJCLJNH_14; }
	inline int32_t* get_address_of_KLIMLJCLJNH_14() { return &___KLIMLJCLJNH_14; }
	inline void set_KLIMLJCLJNH_14(int32_t value)
	{
		___KLIMLJCLJNH_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
