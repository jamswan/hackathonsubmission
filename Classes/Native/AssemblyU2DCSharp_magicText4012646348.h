﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<magicLetter>
struct List_1_t2990083989;
// LetterSizes
struct LetterSizes_t1582613310;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// magicText
struct  magicText_t4012646348  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<magicLetter> magicText::HJDPLIAGFEP
	List_1_t2990083989 * ___HJDPLIAGFEP_2;
	// LetterSizes magicText::IAPJACELLIJ
	LetterSizes_t1582613310 * ___IAPJACELLIJ_3;
	// System.String magicText::COLCJHPAGJN
	String_t* ___COLCJHPAGJN_4;
	// System.Single magicText::HPPGEAFELHG
	float ___HPPGEAFELHG_5;
	// System.Single magicText::MOGMDCBMFHI
	float ___MOGMDCBMFHI_6;
	// System.Single magicText::LKFNOALHHHL
	float ___LKFNOALHHHL_7;
	// System.Single magicText::BGHOBDEEOJE
	float ___BGHOBDEEOJE_8;

public:
	inline static int32_t get_offset_of_HJDPLIAGFEP_2() { return static_cast<int32_t>(offsetof(magicText_t4012646348, ___HJDPLIAGFEP_2)); }
	inline List_1_t2990083989 * get_HJDPLIAGFEP_2() const { return ___HJDPLIAGFEP_2; }
	inline List_1_t2990083989 ** get_address_of_HJDPLIAGFEP_2() { return &___HJDPLIAGFEP_2; }
	inline void set_HJDPLIAGFEP_2(List_1_t2990083989 * value)
	{
		___HJDPLIAGFEP_2 = value;
		Il2CppCodeGenWriteBarrier(&___HJDPLIAGFEP_2, value);
	}

	inline static int32_t get_offset_of_IAPJACELLIJ_3() { return static_cast<int32_t>(offsetof(magicText_t4012646348, ___IAPJACELLIJ_3)); }
	inline LetterSizes_t1582613310 * get_IAPJACELLIJ_3() const { return ___IAPJACELLIJ_3; }
	inline LetterSizes_t1582613310 ** get_address_of_IAPJACELLIJ_3() { return &___IAPJACELLIJ_3; }
	inline void set_IAPJACELLIJ_3(LetterSizes_t1582613310 * value)
	{
		___IAPJACELLIJ_3 = value;
		Il2CppCodeGenWriteBarrier(&___IAPJACELLIJ_3, value);
	}

	inline static int32_t get_offset_of_COLCJHPAGJN_4() { return static_cast<int32_t>(offsetof(magicText_t4012646348, ___COLCJHPAGJN_4)); }
	inline String_t* get_COLCJHPAGJN_4() const { return ___COLCJHPAGJN_4; }
	inline String_t** get_address_of_COLCJHPAGJN_4() { return &___COLCJHPAGJN_4; }
	inline void set_COLCJHPAGJN_4(String_t* value)
	{
		___COLCJHPAGJN_4 = value;
		Il2CppCodeGenWriteBarrier(&___COLCJHPAGJN_4, value);
	}

	inline static int32_t get_offset_of_HPPGEAFELHG_5() { return static_cast<int32_t>(offsetof(magicText_t4012646348, ___HPPGEAFELHG_5)); }
	inline float get_HPPGEAFELHG_5() const { return ___HPPGEAFELHG_5; }
	inline float* get_address_of_HPPGEAFELHG_5() { return &___HPPGEAFELHG_5; }
	inline void set_HPPGEAFELHG_5(float value)
	{
		___HPPGEAFELHG_5 = value;
	}

	inline static int32_t get_offset_of_MOGMDCBMFHI_6() { return static_cast<int32_t>(offsetof(magicText_t4012646348, ___MOGMDCBMFHI_6)); }
	inline float get_MOGMDCBMFHI_6() const { return ___MOGMDCBMFHI_6; }
	inline float* get_address_of_MOGMDCBMFHI_6() { return &___MOGMDCBMFHI_6; }
	inline void set_MOGMDCBMFHI_6(float value)
	{
		___MOGMDCBMFHI_6 = value;
	}

	inline static int32_t get_offset_of_LKFNOALHHHL_7() { return static_cast<int32_t>(offsetof(magicText_t4012646348, ___LKFNOALHHHL_7)); }
	inline float get_LKFNOALHHHL_7() const { return ___LKFNOALHHHL_7; }
	inline float* get_address_of_LKFNOALHHHL_7() { return &___LKFNOALHHHL_7; }
	inline void set_LKFNOALHHHL_7(float value)
	{
		___LKFNOALHHHL_7 = value;
	}

	inline static int32_t get_offset_of_BGHOBDEEOJE_8() { return static_cast<int32_t>(offsetof(magicText_t4012646348, ___BGHOBDEEOJE_8)); }
	inline float get_BGHOBDEEOJE_8() const { return ___BGHOBDEEOJE_8; }
	inline float* get_address_of_BGHOBDEEOJE_8() { return &___BGHOBDEEOJE_8; }
	inline void set_BGHOBDEEOJE_8(float value)
	{
		___BGHOBDEEOJE_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
