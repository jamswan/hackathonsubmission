﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Collections.Generic.List`1<GBMIMKMMFPO>
struct List_1_t4265599188;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APEPFIBKILA
struct  APEPFIBKILA_t4136611566  : public Il2CppObject
{
public:
	// System.Boolean APEPFIBKILA::AJILCIKCFEA
	bool ___AJILCIKCFEA_0;
	// System.Collections.Generic.List`1<GBMIMKMMFPO> APEPFIBKILA::BBCJADHMNPG
	List_1_t4265599188 * ___BBCJADHMNPG_1;
	// System.Collections.Generic.List`1<GBMIMKMMFPO> APEPFIBKILA::DIONBIDEDAB
	List_1_t4265599188 * ___DIONBIDEDAB_2;
	// System.Collections.Generic.List`1<GBMIMKMMFPO> APEPFIBKILA::DCJOKBNIADI
	List_1_t4265599188 * ___DCJOKBNIADI_3;
	// System.Collections.Generic.List`1<GBMIMKMMFPO> APEPFIBKILA::HBENJAFPHAM
	List_1_t4265599188 * ___HBENJAFPHAM_4;
	// System.Collections.Generic.List`1<GBMIMKMMFPO> APEPFIBKILA::OJNDJDOCAOH
	List_1_t4265599188 * ___OJNDJDOCAOH_5;
	// System.Int32 APEPFIBKILA::CLNICLJBMNO
	int32_t ___CLNICLJBMNO_6;
	// System.Int32 APEPFIBKILA::EKNMHBLCPFK
	int32_t ___EKNMHBLCPFK_7;
	// System.Single APEPFIBKILA::KGKJBBMMFHF
	float ___KGKJBBMMFHF_8;
	// UnityEngine.GUIStyle APEPFIBKILA::IECEJPLGHLG
	GUIStyle_t1799908754 * ___IECEJPLGHLG_9;
	// System.Single APEPFIBKILA::IBGIKPJLEOE
	float ___IBGIKPJLEOE_10;
	// System.Int32 APEPFIBKILA::DKFMAPPPOJF
	int32_t ___DKFMAPPPOJF_11;
	// System.Single APEPFIBKILA::HJNDDAGGJFB
	float ___HJNDDAGGJFB_12;
	// System.Single APEPFIBKILA::LAIFECBAIIK
	float ___LAIFECBAIIK_13;
	// UnityEngine.Vector3 APEPFIBKILA::LEFAFPBDGIB
	Vector3_t2243707580  ___LEFAFPBDGIB_14;
	// System.Boolean APEPFIBKILA::HFIHIMBLAFN
	bool ___HFIHIMBLAFN_15;
	// System.Single APEPFIBKILA::NLLIGEPDOBH
	float ___NLLIGEPDOBH_16;
	// System.Single APEPFIBKILA::BMIBBEADOHI
	float ___BMIBBEADOHI_17;

public:
	inline static int32_t get_offset_of_AJILCIKCFEA_0() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___AJILCIKCFEA_0)); }
	inline bool get_AJILCIKCFEA_0() const { return ___AJILCIKCFEA_0; }
	inline bool* get_address_of_AJILCIKCFEA_0() { return &___AJILCIKCFEA_0; }
	inline void set_AJILCIKCFEA_0(bool value)
	{
		___AJILCIKCFEA_0 = value;
	}

	inline static int32_t get_offset_of_BBCJADHMNPG_1() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___BBCJADHMNPG_1)); }
	inline List_1_t4265599188 * get_BBCJADHMNPG_1() const { return ___BBCJADHMNPG_1; }
	inline List_1_t4265599188 ** get_address_of_BBCJADHMNPG_1() { return &___BBCJADHMNPG_1; }
	inline void set_BBCJADHMNPG_1(List_1_t4265599188 * value)
	{
		___BBCJADHMNPG_1 = value;
		Il2CppCodeGenWriteBarrier(&___BBCJADHMNPG_1, value);
	}

	inline static int32_t get_offset_of_DIONBIDEDAB_2() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___DIONBIDEDAB_2)); }
	inline List_1_t4265599188 * get_DIONBIDEDAB_2() const { return ___DIONBIDEDAB_2; }
	inline List_1_t4265599188 ** get_address_of_DIONBIDEDAB_2() { return &___DIONBIDEDAB_2; }
	inline void set_DIONBIDEDAB_2(List_1_t4265599188 * value)
	{
		___DIONBIDEDAB_2 = value;
		Il2CppCodeGenWriteBarrier(&___DIONBIDEDAB_2, value);
	}

	inline static int32_t get_offset_of_DCJOKBNIADI_3() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___DCJOKBNIADI_3)); }
	inline List_1_t4265599188 * get_DCJOKBNIADI_3() const { return ___DCJOKBNIADI_3; }
	inline List_1_t4265599188 ** get_address_of_DCJOKBNIADI_3() { return &___DCJOKBNIADI_3; }
	inline void set_DCJOKBNIADI_3(List_1_t4265599188 * value)
	{
		___DCJOKBNIADI_3 = value;
		Il2CppCodeGenWriteBarrier(&___DCJOKBNIADI_3, value);
	}

	inline static int32_t get_offset_of_HBENJAFPHAM_4() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___HBENJAFPHAM_4)); }
	inline List_1_t4265599188 * get_HBENJAFPHAM_4() const { return ___HBENJAFPHAM_4; }
	inline List_1_t4265599188 ** get_address_of_HBENJAFPHAM_4() { return &___HBENJAFPHAM_4; }
	inline void set_HBENJAFPHAM_4(List_1_t4265599188 * value)
	{
		___HBENJAFPHAM_4 = value;
		Il2CppCodeGenWriteBarrier(&___HBENJAFPHAM_4, value);
	}

	inline static int32_t get_offset_of_OJNDJDOCAOH_5() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___OJNDJDOCAOH_5)); }
	inline List_1_t4265599188 * get_OJNDJDOCAOH_5() const { return ___OJNDJDOCAOH_5; }
	inline List_1_t4265599188 ** get_address_of_OJNDJDOCAOH_5() { return &___OJNDJDOCAOH_5; }
	inline void set_OJNDJDOCAOH_5(List_1_t4265599188 * value)
	{
		___OJNDJDOCAOH_5 = value;
		Il2CppCodeGenWriteBarrier(&___OJNDJDOCAOH_5, value);
	}

	inline static int32_t get_offset_of_CLNICLJBMNO_6() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___CLNICLJBMNO_6)); }
	inline int32_t get_CLNICLJBMNO_6() const { return ___CLNICLJBMNO_6; }
	inline int32_t* get_address_of_CLNICLJBMNO_6() { return &___CLNICLJBMNO_6; }
	inline void set_CLNICLJBMNO_6(int32_t value)
	{
		___CLNICLJBMNO_6 = value;
	}

	inline static int32_t get_offset_of_EKNMHBLCPFK_7() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___EKNMHBLCPFK_7)); }
	inline int32_t get_EKNMHBLCPFK_7() const { return ___EKNMHBLCPFK_7; }
	inline int32_t* get_address_of_EKNMHBLCPFK_7() { return &___EKNMHBLCPFK_7; }
	inline void set_EKNMHBLCPFK_7(int32_t value)
	{
		___EKNMHBLCPFK_7 = value;
	}

	inline static int32_t get_offset_of_KGKJBBMMFHF_8() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___KGKJBBMMFHF_8)); }
	inline float get_KGKJBBMMFHF_8() const { return ___KGKJBBMMFHF_8; }
	inline float* get_address_of_KGKJBBMMFHF_8() { return &___KGKJBBMMFHF_8; }
	inline void set_KGKJBBMMFHF_8(float value)
	{
		___KGKJBBMMFHF_8 = value;
	}

	inline static int32_t get_offset_of_IECEJPLGHLG_9() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___IECEJPLGHLG_9)); }
	inline GUIStyle_t1799908754 * get_IECEJPLGHLG_9() const { return ___IECEJPLGHLG_9; }
	inline GUIStyle_t1799908754 ** get_address_of_IECEJPLGHLG_9() { return &___IECEJPLGHLG_9; }
	inline void set_IECEJPLGHLG_9(GUIStyle_t1799908754 * value)
	{
		___IECEJPLGHLG_9 = value;
		Il2CppCodeGenWriteBarrier(&___IECEJPLGHLG_9, value);
	}

	inline static int32_t get_offset_of_IBGIKPJLEOE_10() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___IBGIKPJLEOE_10)); }
	inline float get_IBGIKPJLEOE_10() const { return ___IBGIKPJLEOE_10; }
	inline float* get_address_of_IBGIKPJLEOE_10() { return &___IBGIKPJLEOE_10; }
	inline void set_IBGIKPJLEOE_10(float value)
	{
		___IBGIKPJLEOE_10 = value;
	}

	inline static int32_t get_offset_of_DKFMAPPPOJF_11() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___DKFMAPPPOJF_11)); }
	inline int32_t get_DKFMAPPPOJF_11() const { return ___DKFMAPPPOJF_11; }
	inline int32_t* get_address_of_DKFMAPPPOJF_11() { return &___DKFMAPPPOJF_11; }
	inline void set_DKFMAPPPOJF_11(int32_t value)
	{
		___DKFMAPPPOJF_11 = value;
	}

	inline static int32_t get_offset_of_HJNDDAGGJFB_12() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___HJNDDAGGJFB_12)); }
	inline float get_HJNDDAGGJFB_12() const { return ___HJNDDAGGJFB_12; }
	inline float* get_address_of_HJNDDAGGJFB_12() { return &___HJNDDAGGJFB_12; }
	inline void set_HJNDDAGGJFB_12(float value)
	{
		___HJNDDAGGJFB_12 = value;
	}

	inline static int32_t get_offset_of_LAIFECBAIIK_13() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___LAIFECBAIIK_13)); }
	inline float get_LAIFECBAIIK_13() const { return ___LAIFECBAIIK_13; }
	inline float* get_address_of_LAIFECBAIIK_13() { return &___LAIFECBAIIK_13; }
	inline void set_LAIFECBAIIK_13(float value)
	{
		___LAIFECBAIIK_13 = value;
	}

	inline static int32_t get_offset_of_LEFAFPBDGIB_14() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___LEFAFPBDGIB_14)); }
	inline Vector3_t2243707580  get_LEFAFPBDGIB_14() const { return ___LEFAFPBDGIB_14; }
	inline Vector3_t2243707580 * get_address_of_LEFAFPBDGIB_14() { return &___LEFAFPBDGIB_14; }
	inline void set_LEFAFPBDGIB_14(Vector3_t2243707580  value)
	{
		___LEFAFPBDGIB_14 = value;
	}

	inline static int32_t get_offset_of_HFIHIMBLAFN_15() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___HFIHIMBLAFN_15)); }
	inline bool get_HFIHIMBLAFN_15() const { return ___HFIHIMBLAFN_15; }
	inline bool* get_address_of_HFIHIMBLAFN_15() { return &___HFIHIMBLAFN_15; }
	inline void set_HFIHIMBLAFN_15(bool value)
	{
		___HFIHIMBLAFN_15 = value;
	}

	inline static int32_t get_offset_of_NLLIGEPDOBH_16() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___NLLIGEPDOBH_16)); }
	inline float get_NLLIGEPDOBH_16() const { return ___NLLIGEPDOBH_16; }
	inline float* get_address_of_NLLIGEPDOBH_16() { return &___NLLIGEPDOBH_16; }
	inline void set_NLLIGEPDOBH_16(float value)
	{
		___NLLIGEPDOBH_16 = value;
	}

	inline static int32_t get_offset_of_BMIBBEADOHI_17() { return static_cast<int32_t>(offsetof(APEPFIBKILA_t4136611566, ___BMIBBEADOHI_17)); }
	inline float get_BMIBBEADOHI_17() const { return ___BMIBBEADOHI_17; }
	inline float* get_address_of_BMIBBEADOHI_17() { return &___BMIBBEADOHI_17; }
	inline void set_BMIBBEADOHI_17(float value)
	{
		___BMIBBEADOHI_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
