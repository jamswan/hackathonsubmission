﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// Text3D
struct Text3D_t62691636;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Text3DExample
struct  Text3DExample_t192478882  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Text3DExample::NAJHBBJJKMP
	int32_t ___NAJHBBJJKMP_2;
	// System.String Text3DExample::COLCJHPAGJN
	String_t* ___COLCJHPAGJN_3;
	// Text3D Text3DExample::HJJPPJBGADF
	Text3D_t62691636 * ___HJJPPJBGADF_4;

public:
	inline static int32_t get_offset_of_NAJHBBJJKMP_2() { return static_cast<int32_t>(offsetof(Text3DExample_t192478882, ___NAJHBBJJKMP_2)); }
	inline int32_t get_NAJHBBJJKMP_2() const { return ___NAJHBBJJKMP_2; }
	inline int32_t* get_address_of_NAJHBBJJKMP_2() { return &___NAJHBBJJKMP_2; }
	inline void set_NAJHBBJJKMP_2(int32_t value)
	{
		___NAJHBBJJKMP_2 = value;
	}

	inline static int32_t get_offset_of_COLCJHPAGJN_3() { return static_cast<int32_t>(offsetof(Text3DExample_t192478882, ___COLCJHPAGJN_3)); }
	inline String_t* get_COLCJHPAGJN_3() const { return ___COLCJHPAGJN_3; }
	inline String_t** get_address_of_COLCJHPAGJN_3() { return &___COLCJHPAGJN_3; }
	inline void set_COLCJHPAGJN_3(String_t* value)
	{
		___COLCJHPAGJN_3 = value;
		Il2CppCodeGenWriteBarrier(&___COLCJHPAGJN_3, value);
	}

	inline static int32_t get_offset_of_HJJPPJBGADF_4() { return static_cast<int32_t>(offsetof(Text3DExample_t192478882, ___HJJPPJBGADF_4)); }
	inline Text3D_t62691636 * get_HJJPPJBGADF_4() const { return ___HJJPPJBGADF_4; }
	inline Text3D_t62691636 ** get_address_of_HJJPPJBGADF_4() { return &___HJJPPJBGADF_4; }
	inline void set_HJJPPJBGADF_4(Text3D_t62691636 * value)
	{
		___HJJPPJBGADF_4 = value;
		Il2CppCodeGenWriteBarrier(&___HJJPPJBGADF_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
