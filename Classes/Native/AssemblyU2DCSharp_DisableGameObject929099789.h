﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisableGameObject
struct  DisableGameObject_t929099789  : public NarrativeEffect_t3336735121
{
public:
	// UnityEngine.GameObject DisableGameObject::gameobject
	GameObject_t1756533147 * ___gameobject_3;

public:
	inline static int32_t get_offset_of_gameobject_3() { return static_cast<int32_t>(offsetof(DisableGameObject_t929099789, ___gameobject_3)); }
	inline GameObject_t1756533147 * get_gameobject_3() const { return ___gameobject_3; }
	inline GameObject_t1756533147 ** get_address_of_gameobject_3() { return &___gameobject_3; }
	inline void set_gameobject_3(GameObject_t1756533147 * value)
	{
		___gameobject_3 = value;
		Il2CppCodeGenWriteBarrier(&___gameobject_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
