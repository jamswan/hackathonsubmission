﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1779888572;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageTargetEvent
struct  ImageTargetEvent_t3520571492  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ImageTargetEvent::active
	bool ___active_2;
	// Vuforia.TrackableBehaviour ImageTargetEvent::MDEHCLGCIKP
	TrackableBehaviour_t1779888572 * ___MDEHCLGCIKP_3;
	// UnityEngine.GameObject ImageTargetEvent::imageTarget
	GameObject_t1756533147 * ___imageTarget_4;
	// System.String ImageTargetEvent::targetName
	String_t* ___targetName_5;

public:
	inline static int32_t get_offset_of_active_2() { return static_cast<int32_t>(offsetof(ImageTargetEvent_t3520571492, ___active_2)); }
	inline bool get_active_2() const { return ___active_2; }
	inline bool* get_address_of_active_2() { return &___active_2; }
	inline void set_active_2(bool value)
	{
		___active_2 = value;
	}

	inline static int32_t get_offset_of_MDEHCLGCIKP_3() { return static_cast<int32_t>(offsetof(ImageTargetEvent_t3520571492, ___MDEHCLGCIKP_3)); }
	inline TrackableBehaviour_t1779888572 * get_MDEHCLGCIKP_3() const { return ___MDEHCLGCIKP_3; }
	inline TrackableBehaviour_t1779888572 ** get_address_of_MDEHCLGCIKP_3() { return &___MDEHCLGCIKP_3; }
	inline void set_MDEHCLGCIKP_3(TrackableBehaviour_t1779888572 * value)
	{
		___MDEHCLGCIKP_3 = value;
		Il2CppCodeGenWriteBarrier(&___MDEHCLGCIKP_3, value);
	}

	inline static int32_t get_offset_of_imageTarget_4() { return static_cast<int32_t>(offsetof(ImageTargetEvent_t3520571492, ___imageTarget_4)); }
	inline GameObject_t1756533147 * get_imageTarget_4() const { return ___imageTarget_4; }
	inline GameObject_t1756533147 ** get_address_of_imageTarget_4() { return &___imageTarget_4; }
	inline void set_imageTarget_4(GameObject_t1756533147 * value)
	{
		___imageTarget_4 = value;
		Il2CppCodeGenWriteBarrier(&___imageTarget_4, value);
	}

	inline static int32_t get_offset_of_targetName_5() { return static_cast<int32_t>(offsetof(ImageTargetEvent_t3520571492, ___targetName_5)); }
	inline String_t* get_targetName_5() const { return ___targetName_5; }
	inline String_t** get_address_of_targetName_5() { return &___targetName_5; }
	inline void set_targetName_5(String_t* value)
	{
		___targetName_5 = value;
		Il2CppCodeGenWriteBarrier(&___targetName_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
