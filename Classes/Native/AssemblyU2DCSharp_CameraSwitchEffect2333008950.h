﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSwitchEffect
struct  CameraSwitchEffect_t2333008950  : public NarrativeEffect_t3336735121
{
public:
	// UnityEngine.GameObject CameraSwitchEffect::Cam1
	GameObject_t1756533147 * ___Cam1_3;
	// UnityEngine.GameObject CameraSwitchEffect::Cam2
	GameObject_t1756533147 * ___Cam2_4;

public:
	inline static int32_t get_offset_of_Cam1_3() { return static_cast<int32_t>(offsetof(CameraSwitchEffect_t2333008950, ___Cam1_3)); }
	inline GameObject_t1756533147 * get_Cam1_3() const { return ___Cam1_3; }
	inline GameObject_t1756533147 ** get_address_of_Cam1_3() { return &___Cam1_3; }
	inline void set_Cam1_3(GameObject_t1756533147 * value)
	{
		___Cam1_3 = value;
		Il2CppCodeGenWriteBarrier(&___Cam1_3, value);
	}

	inline static int32_t get_offset_of_Cam2_4() { return static_cast<int32_t>(offsetof(CameraSwitchEffect_t2333008950, ___Cam2_4)); }
	inline GameObject_t1756533147 * get_Cam2_4() const { return ___Cam2_4; }
	inline GameObject_t1756533147 ** get_address_of_Cam2_4() { return &___Cam2_4; }
	inline void set_Cam2_4(GameObject_t1756533147 * value)
	{
		___Cam2_4 = value;
		Il2CppCodeGenWriteBarrier(&___Cam2_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
