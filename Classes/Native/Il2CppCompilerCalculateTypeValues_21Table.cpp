﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_TextureRenderer3312477626.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManager308318605.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManagerImpl381223961.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton3703236737.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton_Sens1678924861.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonImpl2449737797.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamImpl2771725761.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile3757625748.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1724666488.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof3644865120.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetAbstrac3327552701.h"
#include "Vuforia_UnityExtensions_Vuforia_MarkerAbstractBeha1456101953.h"
#include "Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeh3489038957.h"
#include "Vuforia_UnityExtensions_Vuforia_MultiTargetAbstrac3616801211.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity657456673.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitE2149396216.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Vufor3491240575.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Stora3897282321.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3319870759.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3132552034.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaMacros1884408435.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil3083157244.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil1916387570.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceUtilities4096327849.h"
#include "Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBe2386081773.h"
#include "Vuforia_UnityExtensions_Vuforia_SimpleTargetData3993525265.h"
#include "Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeh4084926705.h"
#include "Vuforia_UnityExtensions_Vuforia_UserDefinedTargetB3589690572.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst395384314.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundMan3765780423.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstr2478279366.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamAbstractBeha4104806356.h"
#include "Vuforia_UnityExtensions_Vuforia_WordTemplateMode1097144495.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725.h"
#include "Vuforia_UnityExtensions_Vuforia_WordFilterMode695600879.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDe3998171770.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDe3524983920.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_CameraSettings3536359094.h"
#include "AssemblyU2DCSharp_CameraSettings_U3CRestoreOrigina3562025758.h"
#include "AssemblyU2DCSharp_InitErrorHandler1791388012.h"
#include "AssemblyU2DCSharp_MenuAnimator2049002970.h"
#include "AssemblyU2DCSharp_MenuOptions3210604277.h"
#include "AssemblyU2DCSharp_AboutScreen3562380015.h"
#include "AssemblyU2DCSharp_AsyncSceneLoader2733707743.h"
#include "AssemblyU2DCSharp_AsyncSceneLoader_U3CLoadNextScen1824579776.h"
#include "AssemblyU2DCSharp_LoadingScreen2880017196.h"
#include "AssemblyU2DCSharp_TapHandler3409799063.h"
#include "AssemblyU2DCSharp_TrackableSettings4265251850.h"
#include "AssemblyU2DCSharp_PlayVideo2858951647.h"
#include "AssemblyU2DCSharp_PlayVideo_U3CPlayFullscreenVideo1003302819.h"
#include "AssemblyU2DCSharp_MedaiPlayerSampleGUI1330597946.h"
#include "AssemblyU2DCSharp_MedaiPlayerSampleSphereGUI391155485.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl1284484152.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_KBIFEBKFHJP9122301.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_DPHJBDJDHMD4102044392.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_NJENMFCKEPC2069907229.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_HDIIHNOEAOH1508400025.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_FAKGGJJJGNF2056791092.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_NOMEFGGFNGH2967056203.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_BCENGFHBBOI531979988.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_U3CDownloadStrea3678449862.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_U3CCopyStreaming2868877264.h"
#include "AssemblyU2DCSharp_MediaPlayerFullScreenCtrl2458194295.h"
#include "AssemblyU2DCSharp_SphereMirror3747354098.h"
#include "AssemblyU2DCSharp_VideoCopyTexture2834612149.h"
#include "AssemblyU2DCSharp_VideoPlaybackMenuOptions1188888539.h"
#include "AssemblyU2DCSharp_VideoPlaybackTapHandler4051201625.h"
#include "AssemblyU2DCSharp_VideoTexture_Lite4274040563.h"
#include "AssemblyU2DCSharp_VideoTexture_Lite_KPCHGDNDKNM41912157.h"
#include "AssemblyU2DCSharp_IJDGMEKNLGL3456803436.h"
#include "AssemblyU2DCSharp_DemoInputManager2776755480.h"
#include "AssemblyU2DCSharp_DemoSceneManager779426248.h"
#include "AssemblyU2DCSharp_Teleport282063519.h"
#include "AssemblyU2DCSharp_JumpToPage3783692930.h"
#include "AssemblyU2DCSharp_ChildrenPageProvider958136491.h"
#include "AssemblyU2DCSharp_PrefabPageProvider3978016920.h"
#include "AssemblyU2DCSharp_PagedScrollBar4073376237.h"
#include "AssemblyU2DCSharp_PagedScrollRect3048021378.h"
#include "AssemblyU2DCSharp_BaseScrollEffect2855282033.h"
#include "AssemblyU2DCSharp_BaseTile3549052087.h"
#include "AssemblyU2DCSharp_FadeScrollEffect2128935010.h"
#include "AssemblyU2DCSharp_FloatTile645192174.h"
#include "AssemblyU2DCSharp_MaskedTile4024183987.h"
#include "AssemblyU2DCSharp_ScaleScrollEffect3430758866.h"
#include "AssemblyU2DCSharp_TileScrollEffect2151509712.h"
#include "AssemblyU2DCSharp_TiledPage4183784445.h"
#include "AssemblyU2DCSharp_TranslateScrollEffect636656150.h"
#include "AssemblyU2DCSharp_Tab4262919697.h"
#include "AssemblyU2DCSharp_TabGroup2567651434.h"
#include "AssemblyU2DCSharp_UIFadeTransition1996000123.h"
#include "AssemblyU2DCSharp_GVR_Input_AppButtonInput1324551885.h"
#include "AssemblyU2DCSharp_GVRSample_AutoPlayVideo1314286476.h"
#include "AssemblyU2DCSharp_GVR_Input_Vector3Event2806921088.h"
#include "AssemblyU2DCSharp_GVR_Input_Vector2Event2806928513.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (TextureRenderer_t3312477626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[3] = 
{
	TextureRenderer_t3312477626::get_offset_of_mTextureBufferCamera_0(),
	TextureRenderer_t3312477626::get_offset_of_mTextureWidth_1(),
	TextureRenderer_t3312477626::get_offset_of_mTextureHeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (TrackerManager_t308318605), -1, sizeof(TrackerManager_t308318605_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2101[1] = 
{
	TrackerManager_t308318605_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (TrackerManagerImpl_t381223961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[6] = 
{
	TrackerManagerImpl_t381223961::get_offset_of_mObjectTracker_1(),
	TrackerManagerImpl_t381223961::get_offset_of_mMarkerTracker_2(),
	TrackerManagerImpl_t381223961::get_offset_of_mTextTracker_3(),
	TrackerManagerImpl_t381223961::get_offset_of_mSmartTerrainTracker_4(),
	TrackerManagerImpl_t381223961::get_offset_of_mDeviceTracker_5(),
	TrackerManagerImpl_t381223961::get_offset_of_mStateManager_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (VirtualButton_t3703236737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (Sensitivity_t1678924861)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2104[4] = 
{
	Sensitivity_t1678924861::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (VirtualButtonImpl_t2449737797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[6] = 
{
	VirtualButtonImpl_t2449737797::get_offset_of_mName_1(),
	VirtualButtonImpl_t2449737797::get_offset_of_mID_2(),
	VirtualButtonImpl_t2449737797::get_offset_of_mArea_3(),
	VirtualButtonImpl_t2449737797::get_offset_of_mIsEnabled_4(),
	VirtualButtonImpl_t2449737797::get_offset_of_mParentImageTarget_5(),
	VirtualButtonImpl_t2449737797::get_offset_of_mParentDataSet_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (WebCamImpl_t2771725761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[15] = 
{
	WebCamImpl_t2771725761::get_offset_of_mARCameras_0(),
	WebCamImpl_t2771725761::get_offset_of_mOriginalCameraCullMask_1(),
	WebCamImpl_t2771725761::get_offset_of_mWebCamTexture_2(),
	WebCamImpl_t2771725761::get_offset_of_mVideoModeData_3(),
	WebCamImpl_t2771725761::get_offset_of_mVideoTextureInfo_4(),
	WebCamImpl_t2771725761::get_offset_of_mTextureRenderer_5(),
	WebCamImpl_t2771725761::get_offset_of_mBufferReadTexture_6(),
	WebCamImpl_t2771725761::get_offset_of_mReadPixelsRect_7(),
	WebCamImpl_t2771725761::get_offset_of_mWebCamProfile_8(),
	WebCamImpl_t2771725761::get_offset_of_mFlipHorizontally_9(),
	WebCamImpl_t2771725761::get_offset_of_mIsDirty_10(),
	WebCamImpl_t2771725761::get_offset_of_mLastFrameIdx_11(),
	WebCamImpl_t2771725761::get_offset_of_mRenderTextureLayer_12(),
	WebCamImpl_t2771725761::get_offset_of_mWebcamPlaying_13(),
	WebCamImpl_t2771725761::get_offset_of_U3CIsTextureSizeAvailableU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (WebCamProfile_t3757625748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[1] = 
{
	WebCamProfile_t3757625748::get_offset_of_mProfileCollection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (ProfileData_t1724666488)+ sizeof (Il2CppObject), sizeof(ProfileData_t1724666488 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2108[3] = 
{
	ProfileData_t1724666488::get_offset_of_RequestedTextureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1724666488::get_offset_of_ResampledTextureSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1724666488::get_offset_of_RequestedFPS_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (ProfileCollection_t3644865120)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[2] = 
{
	ProfileCollection_t3644865120::get_offset_of_DefaultProfile_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileCollection_t3644865120::get_offset_of_Profiles_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (ImageTargetAbstractBehaviour_t3327552701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[8] = 
{
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mAspectRatio_20(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mImageTargetType_21(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mWidth_22(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mHeight_23(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mImageTarget_24(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mVirtualButtonBehaviours_25(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mLastTransformScale_26(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mLastSize_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (MarkerAbstractBehaviour_t1456101953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[2] = 
{
	MarkerAbstractBehaviour_t1456101953::get_offset_of_mMarkerID_10(),
	MarkerAbstractBehaviour_t1456101953::get_offset_of_mMarker_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (MaskOutAbstractBehaviour_t3489038957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[1] = 
{
	MaskOutAbstractBehaviour_t3489038957::get_offset_of_maskMaterial_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (MultiTargetAbstractBehaviour_t3616801211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2116[1] = 
{
	MultiTargetAbstractBehaviour_t3616801211::get_offset_of_mMultiTarget_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (VuforiaUnity_t657456673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (InitError_t2149396216)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2118[12] = 
{
	InitError_t2149396216::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (VuforiaHint_t3491240575)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2119[4] = 
{
	VuforiaHint_t3491240575::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (StorageType_t3897282321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2120[4] = 
{
	StorageType_t3897282321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (VuforiaAbstractBehaviour_t3319870759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[41] = 
{
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_VuforiaLicenseKey_2(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_CameraDeviceModeSetting_3(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_MaxSimultaneousImageTargets_4(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_MaxSimultaneousObjectTargets_5(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_UseDelayedLoadingObjectTargets_6(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_CameraDirection_7(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_MirrorVideoBackground_8(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mWorldCenterMode_9(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mWorldCenter_10(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mTrackerEventHandlers_11(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mVideoBgEventHandlers_12(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnVuforiaInitError_13(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnVuforiaInitialized_14(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnVuforiaStarted_15(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnTrackablesUpdated_16(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mRenderOnUpdate_17(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnPause_18(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnBackgroundTextureChanged_19(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mStartHasBeenInvoked_20(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mHasStarted_21(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mFailedToInitialize_22(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mBackgroundTextureHasChanged_23(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mInitError_24(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mCameraConfiguration_25(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mEyewearBehaviour_26(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mVideoBackgroundMgr_27(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mCheckStopCamera_28(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mClearMaterial_29(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMetalRendering_30(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mHasStartedOnce_31(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mWasEnabledBeforePause_32(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mObjectTrackerWasActiveBeforePause_33(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_34(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMarkerTrackerWasActiveBeforePause_35(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMarkerTrackerWasActiveBeforeDisabling_36(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mLastUpdatedFrame_37(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mTrackersRequestedToDeinit_38(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMissedToApplyLeftProjectionMatrix_39(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMissedToApplyRightProjectionMatrix_40(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mLeftProjectMatrixToApply_41(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mRightProjectMatrixToApply_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (WorldCenterMode_t3132552034)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2122[5] = 
{
	WorldCenterMode_t3132552034::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (VuforiaMacros_t1884408435)+ sizeof (Il2CppObject), sizeof(VuforiaMacros_t1884408435 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2123[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (VuforiaRuntimeUtilities_t3083157244), -1, sizeof(VuforiaRuntimeUtilities_t3083157244_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2124[2] = 
{
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sWebCamUsed_0(),
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sNativePluginSupport_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (InitializableBool_t1916387570)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2125[4] = 
{
	InitializableBool_t1916387570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (SurfaceUtilities_t4096327849), -1, sizeof(SurfaceUtilities_t4096327849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2126[1] = 
{
	SurfaceUtilities_t4096327849_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (TextRecoAbstractBehaviour_t2386081773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[12] = 
{
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mHasInitialized_2(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforePause_3(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforeDisabling_4(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordListFile_5(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mCustomWordListFile_6(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalCustomWords_7(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterMode_8(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterListFile_9(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalFilterWords_10(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordPrefabCreationMode_11(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mMaximumWordInstances_12(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTextRecoEventHandlers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (SimpleTargetData_t3993525265)+ sizeof (Il2CppObject), sizeof(SimpleTargetData_t3993525265 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2128[2] = 
{
	SimpleTargetData_t3993525265::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimpleTargetData_t3993525265::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (TurnOffAbstractBehaviour_t4084926705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (UserDefinedTargetBuildingAbstractBehaviour_t3589690572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[11] = 
{
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mObjectTracker_2(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mLastFrameQuality_3(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyScanning_4(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasScanningBeforeDisable_5(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyBuilding_6(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasBuildingBeforeDisable_7(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mOnInitializedCalled_8(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mHandlers_9(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopTrackerWhileScanning_10(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StartScanningAutomatically_11(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopScanningWhenFinshedBuilding_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (VideoBackgroundAbstractBehaviour_t395384314), -1, sizeof(VideoBackgroundAbstractBehaviour_t395384314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2131[12] = 
{
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mClearBuffers_2(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mSkipStateUpdates_3(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mVuforiaAbstractBehaviour_4(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mCamera_5(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mBackgroundBehaviour_6(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mStereoDepth_7(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mFrameCounter_8(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mRenderCounter_9(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mResetMatrix_10(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mVuforiaFrustumSkew_11(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mCenterToEyeAxis_12(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mDisabledMeshRenderers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (VideoBackgroundManagerAbstractBehaviour_t3765780423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[7] = 
{
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mTexture_2(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mVideoBgConfigChanged_3(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mNativeTexturePtr_4(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mClippingMode_5(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mMatteShader_6(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mVideoBackgroundEnabled_7(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mVuforiaBehaviour_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (VirtualButtonAbstractBehaviour_t2478279366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[15] = 
{
	0,
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mName_3(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivity_4(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHasUpdatedPose_5(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevTransform_6(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevParent_7(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivityDirty_8(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPreviousSensitivity_9(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPreviouslyEnabled_10(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPressed_11(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHandlers_12(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mLeftTop_13(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mRightBottom_14(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mUnregisterOnDestroy_15(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mVirtualButton_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (WebCamAbstractBehaviour_t4104806356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[6] = 
{
	WebCamAbstractBehaviour_t4104806356::get_offset_of_RenderTextureLayer_2(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mDeviceNameSetInEditor_3(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mFlipHorizontally_4(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mTurnOffWebCam_5(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mWebCamImpl_6(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mBackgroundCameraInstance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (WordTemplateMode_t1097144495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2135[3] = 
{
	WordTemplateMode_t1097144495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (WordAbstractBehaviour_t2878458725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[3] = 
{
	WordAbstractBehaviour_t2878458725::get_offset_of_mMode_10(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mSpecificWord_11(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mWord_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (WordFilterMode_t695600879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2137[4] = 
{
	WordFilterMode_t695600879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (U3CPrivateImplementationDetailsU3EU7B147A48C5U2D2BC1U2D47C3U2D8F82U2DCE56F4E53F6EU7D_t3998171770), -1, sizeof(U3CPrivateImplementationDetailsU3EU7B147A48C5U2D2BC1U2D47C3U2D8F82U2DCE56F4E53F6EU7D_t3998171770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2138[1] = 
{
	U3CPrivateImplementationDetailsU3EU7B147A48C5U2D2BC1U2D47C3U2D8F82U2DCE56F4E53F6EU7D_t3998171770_StaticFields::get_offset_of_U24U24method0x6000bb2U2D1_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (__StaticArrayInitTypeSizeU3D24_t3524983920)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D24_t3524983920 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (CameraSettings_t3536359094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[4] = 
{
	CameraSettings_t3536359094::get_offset_of_FCLGCBDLJAG_2(),
	CameraSettings_t3536359094::get_offset_of_OFPCBIFHDJL_3(),
	CameraSettings_t3536359094::get_offset_of_JMKLIMBIJFA_4(),
	CameraSettings_t3536359094::get_offset_of_GDLJHPPEELM_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[4] = 
{
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758::get_offset_of_AOOLEAHHMIH_0(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758::get_offset_of_LGBFNMECDHC_1(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758::get_offset_of_IMAGLIMLPFK_2(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758::get_offset_of_EPCFNNGDBGC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (InitErrorHandler_t1791388012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[2] = 
{
	InitErrorHandler_t1791388012::get_offset_of_errorText_2(),
	InitErrorHandler_t1791388012::get_offset_of_LKPFDPMMGMC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (MenuAnimator_t2049002970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[7] = 
{
	MenuAnimator_t2049002970::get_offset_of_GMMCNAKGOGM_2(),
	MenuAnimator_t2049002970::get_offset_of_FGFDJICIBAG_3(),
	MenuAnimator_t2049002970::get_offset_of_KHFPKDOJADK_4(),
	MenuAnimator_t2049002970::get_offset_of_FKGAMOOOIJN_5(),
	MenuAnimator_t2049002970::get_offset_of_HJABAJAFDFM_6(),
	MenuAnimator_t2049002970::get_offset_of_CDLADKEPAKD_7(),
	MenuAnimator_t2049002970::get_offset_of_SlidingTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (MenuOptions_t3210604277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[3] = 
{
	MenuOptions_t3210604277::get_offset_of_HHNJAPJPEON_2(),
	MenuOptions_t3210604277::get_offset_of_NKGJJGJKKIE_3(),
	MenuOptions_t3210604277::get_offset_of_KHNIGIIJIII_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (AboutScreen_t3562380015), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (AsyncSceneLoader_t2733707743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[1] = 
{
	AsyncSceneLoader_t2733707743::get_offset_of_loadingDelay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[4] = 
{
	U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776::get_offset_of_BFDJNAGEEBK_0(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776::get_offset_of_LGBFNMECDHC_1(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776::get_offset_of_IMAGLIMLPFK_2(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776::get_offset_of_EPCFNNGDBGC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (LoadingScreen_t2880017196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[2] = 
{
	LoadingScreen_t2880017196::get_offset_of_FCIOOAHFBOA_2(),
	LoadingScreen_t2880017196::get_offset_of_MFCENPLHKKE_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (TapHandler_t3409799063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[4] = 
{
	0,
	TapHandler_t3409799063::get_offset_of_GGEJIEBOLHP_3(),
	TapHandler_t3409799063::get_offset_of_KHNIGIIJIII_4(),
	TapHandler_t3409799063::get_offset_of_IEKGIFCHAJG_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (TrackableSettings_t4265251850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2151[1] = 
{
	TrackableSettings_t4265251850::get_offset_of_CHNDFIOCLFB_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (PlayVideo_t2858951647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[2] = 
{
	PlayVideo_t2858951647::get_offset_of_playFullscreen_2(),
	PlayVideo_t2858951647::get_offset_of_BFCHOFDBJPM_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[4] = 
{
	U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819::get_offset_of_PDDLMIFOBCN_0(),
	U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819::get_offset_of_LGBFNMECDHC_1(),
	U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819::get_offset_of_IMAGLIMLPFK_2(),
	U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819::get_offset_of_EPCFNNGDBGC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (MedaiPlayerSampleGUI_t1330597946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[2] = 
{
	MedaiPlayerSampleGUI_t1330597946::get_offset_of_scrMedia_2(),
	MedaiPlayerSampleGUI_t1330597946::get_offset_of_m_bFinish_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (MedaiPlayerSampleSphereGUI_t391155485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[1] = 
{
	MedaiPlayerSampleSphereGUI_t391155485::get_offset_of_scrMedia_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (MediaPlayerCtrl_t1284484152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[27] = 
{
	MediaPlayerCtrl_t1284484152::get_offset_of_m_strFileName_2(),
	MediaPlayerCtrl_t1284484152::get_offset_of_m_TargetMaterial_3(),
	MediaPlayerCtrl_t1284484152::get_offset_of_OJFFFGDDMIL_4(),
	MediaPlayerCtrl_t1284484152::get_offset_of_FGAMFEHIGBA_5(),
	MediaPlayerCtrl_t1284484152::get_offset_of_MLMOCCFIOMO_6(),
	MediaPlayerCtrl_t1284484152::get_offset_of_EMBDGBJEJPH_7(),
	MediaPlayerCtrl_t1284484152::get_offset_of_IENGHMEHDND_8(),
	MediaPlayerCtrl_t1284484152::get_offset_of_m_bFullScreen_9(),
	MediaPlayerCtrl_t1284484152::get_offset_of_m_bSupportRockchip_10(),
	MediaPlayerCtrl_t1284484152::get_offset_of_OnReady_11(),
	MediaPlayerCtrl_t1284484152::get_offset_of_OnEnd_12(),
	MediaPlayerCtrl_t1284484152::get_offset_of_OnVideoError_13(),
	MediaPlayerCtrl_t1284484152::get_offset_of_OnVideoFirstFrameReady_14(),
	MediaPlayerCtrl_t1284484152::get_offset_of_NMIEDBOEIJP_15(),
	MediaPlayerCtrl_t1284484152::get_offset_of_EEKCFBFHCKN_16(),
	MediaPlayerCtrl_t1284484152::get_offset_of_KPFIKJCMFHL_17(),
	MediaPlayerCtrl_t1284484152::get_offset_of_JHEPNDDEMKF_18(),
	MediaPlayerCtrl_t1284484152::get_offset_of_m_ScaleValue_19(),
	MediaPlayerCtrl_t1284484152::get_offset_of_m_objResize_20(),
	MediaPlayerCtrl_t1284484152::get_offset_of_m_bLoop_21(),
	MediaPlayerCtrl_t1284484152::get_offset_of_m_bAutoPlay_22(),
	MediaPlayerCtrl_t1284484152::get_offset_of_AHBCOEDBHPB_23(),
	MediaPlayerCtrl_t1284484152::get_offset_of_m_bInit_24(),
	MediaPlayerCtrl_t1284484152::get_offset_of_LBGKOPJKMLN_25(),
	MediaPlayerCtrl_t1284484152::get_offset_of_FDHFBCKLMIG_26(),
	MediaPlayerCtrl_t1284484152::get_offset_of_IKNNGFLLGPB_27(),
	MediaPlayerCtrl_t1284484152::get_offset_of_LPOPFENPPMN_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (KBIFEBKFHJP_t9122301), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (DPHJBDJDHMD_t4102044392), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (NJENMFCKEPC_t2069907229), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (HDIIHNOEAOH_t1508400025), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (FAKGGJJJGNF_t2056791092)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2161[8] = 
{
	FAKGGJJJGNF_t2056791092::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (NOMEFGGFNGH_t2967056203)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2162[8] = 
{
	NOMEFGGFNGH_t2967056203::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (BCENGFHBBOI_t531979988)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2163[8] = 
{
	BCENGFHBBOI_t531979988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[6] = 
{
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862::get_offset_of_MGLLOMDMBGD_0(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862::get_offset_of_MNNMDEABAIC_1(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862::get_offset_of_AOOLEAHHMIH_2(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862::get_offset_of_LGBFNMECDHC_3(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862::get_offset_of_IMAGLIMLPFK_4(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862::get_offset_of_EPCFNNGDBGC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[7] = 
{
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264::get_offset_of_MGLLOMDMBGD_0(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264::get_offset_of_FOLKNEJDGFP_1(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264::get_offset_of_ALLKOOONAMJ_2(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264::get_offset_of_AOOLEAHHMIH_3(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264::get_offset_of_LGBFNMECDHC_4(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264::get_offset_of_IMAGLIMLPFK_5(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264::get_offset_of_EPCFNNGDBGC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (MediaPlayerFullScreenCtrl_t2458194295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[3] = 
{
	MediaPlayerFullScreenCtrl_t2458194295::get_offset_of_m_objVideo_2(),
	MediaPlayerFullScreenCtrl_t2458194295::get_offset_of_MFJJLOCJEBL_3(),
	MediaPlayerFullScreenCtrl_t2458194295::get_offset_of_NPFMDFEONHP_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (SphereMirror_t3747354098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (VideoCopyTexture_t2834612149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[1] = 
{
	VideoCopyTexture_t2834612149::get_offset_of_m_srcVideo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (VideoPlaybackMenuOptions_t1188888539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[1] = 
{
	VideoPlaybackMenuOptions_t1188888539::get_offset_of_IBCPNAAFHFP_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (VideoPlaybackTapHandler_t4051201625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[1] = 
{
	VideoPlaybackTapHandler_t4051201625::get_offset_of_IBCPNAAFHFP_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (VideoTexture_Lite_t4274040563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[19] = 
{
	VideoTexture_Lite_t4274040563::get_offset_of_FPS_2(),
	VideoTexture_Lite_t4274040563::get_offset_of_firstFrame_3(),
	VideoTexture_Lite_t4274040563::get_offset_of_lastFrame_4(),
	VideoTexture_Lite_t4274040563::get_offset_of_FileName_5(),
	VideoTexture_Lite_t4274040563::get_offset_of_digitsFormat_6(),
	VideoTexture_Lite_t4274040563::get_offset_of_DigitsLocation_7(),
	VideoTexture_Lite_t4274040563::get_offset_of_aspectRatio_8(),
	VideoTexture_Lite_t4274040563::get_offset_of_enableAudio_9(),
	VideoTexture_Lite_t4274040563::get_offset_of_enableReplay_10(),
	VideoTexture_Lite_t4274040563::get_offset_of_showInstructions_11(),
	VideoTexture_Lite_t4274040563::get_offset_of_GAIHEKPNGKG_12(),
	VideoTexture_Lite_t4274040563::get_offset_of_BHPBIIBLNFK_13(),
	VideoTexture_Lite_t4274040563::get_offset_of_ADIACDNOBJA_14(),
	VideoTexture_Lite_t4274040563::get_offset_of_CFCNKEMCBAL_15(),
	VideoTexture_Lite_t4274040563::get_offset_of_BMLPPCDBECD_16(),
	VideoTexture_Lite_t4274040563::get_offset_of_KIDKLLOFBAB_17(),
	VideoTexture_Lite_t4274040563::get_offset_of_KDIEKAKPFPN_18(),
	VideoTexture_Lite_t4274040563::get_offset_of_GBBACOJGDJG_19(),
	VideoTexture_Lite_t4274040563::get_offset_of_LBGLAPDFIAK_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (KPCHGDNDKNM_t41912157)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2172[3] = 
{
	KPCHGDNDKNM_t41912157::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (IJDGMEKNLGL_t3456803436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[4] = 
{
	IJDGMEKNLGL_t3456803436::get_offset_of_FAMOEAKBKID_0(),
	IJDGMEKNLGL_t3456803436::get_offset_of_MKLFMGNDJBE_1(),
	IJDGMEKNLGL_t3456803436::get_offset_of_CHFMJNJEGEC_2(),
	IJDGMEKNLGL_t3456803436::get_offset_of_HMFINGPEEDN_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (DemoInputManager_t2776755480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (DemoSceneManager_t779426248), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (Teleport_t282063519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[3] = 
{
	Teleport_t282063519::get_offset_of_BFOJMNMBBBM_2(),
	Teleport_t282063519::get_offset_of_inactiveMaterial_3(),
	Teleport_t282063519::get_offset_of_gazedAtMaterial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (JumpToPage_t3783692930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (ChildrenPageProvider_t958136491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2178[2] = 
{
	ChildrenPageProvider_t958136491::get_offset_of_DKIMMFCGOHL_2(),
	ChildrenPageProvider_t958136491::get_offset_of_spacing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (PrefabPageProvider_t3978016920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[2] = 
{
	PrefabPageProvider_t3978016920::get_offset_of_prefabs_2(),
	PrefabPageProvider_t3978016920::get_offset_of_spacing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (PagedScrollBar_t4073376237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (PagedScrollRect_t3048021378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (BaseScrollEffect_t2855282033), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (BaseTile_t3549052087), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (FadeScrollEffect_t2128935010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (FloatTile_t645192174), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (MaskedTile_t4024183987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (ScaleScrollEffect_t3430758866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (TileScrollEffect_t2151509712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (TiledPage_t4183784445), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (TranslateScrollEffect_t636656150), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (Tab_t4262919697), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (TabGroup_t2567651434), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (UIFadeTransition_t1996000123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (AppButtonInput_t1324551885), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (AutoPlayVideo_t1314286476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2197[5] = 
{
	AutoPlayVideo_t1314286476::get_offset_of_CPEFGHEFDMK_2(),
	AutoPlayVideo_t1314286476::get_offset_of_BHFNEOEPKGC_3(),
	AutoPlayVideo_t1314286476::get_offset_of_OECALIBMPJK_4(),
	AutoPlayVideo_t1314286476::get_offset_of_delay_5(),
	AutoPlayVideo_t1314286476::get_offset_of_loop_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (Vector3Event_t2806921088), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (Vector2Event_t2806928513), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
