﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// VideoControlsManager
struct VideoControlsManager_t3010523296;
// GvrPointerInputModule
struct GvrPointerInputModule_t1603976810;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrubberEvents
struct  ScrubberEvents_t2429506345  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ScrubberEvents::KKPDNPLIEIO
	GameObject_t1756533147 * ___KKPDNPLIEIO_2;
	// UnityEngine.Vector3[] ScrubberEvents::BLALEKBHIPM
	Vector3U5BU5D_t1172311765* ___BLALEKBHIPM_3;
	// UnityEngine.UI.Slider ScrubberEvents::IAGMGKCDFLC
	Slider_t297367283 * ___IAGMGKCDFLC_4;
	// VideoControlsManager ScrubberEvents::NAGPFCGNKJG
	VideoControlsManager_t3010523296 * ___NAGPFCGNKJG_5;
	// GvrPointerInputModule ScrubberEvents::KCIBEMJPAAL
	GvrPointerInputModule_t1603976810 * ___KCIBEMJPAAL_6;

public:
	inline static int32_t get_offset_of_KKPDNPLIEIO_2() { return static_cast<int32_t>(offsetof(ScrubberEvents_t2429506345, ___KKPDNPLIEIO_2)); }
	inline GameObject_t1756533147 * get_KKPDNPLIEIO_2() const { return ___KKPDNPLIEIO_2; }
	inline GameObject_t1756533147 ** get_address_of_KKPDNPLIEIO_2() { return &___KKPDNPLIEIO_2; }
	inline void set_KKPDNPLIEIO_2(GameObject_t1756533147 * value)
	{
		___KKPDNPLIEIO_2 = value;
		Il2CppCodeGenWriteBarrier(&___KKPDNPLIEIO_2, value);
	}

	inline static int32_t get_offset_of_BLALEKBHIPM_3() { return static_cast<int32_t>(offsetof(ScrubberEvents_t2429506345, ___BLALEKBHIPM_3)); }
	inline Vector3U5BU5D_t1172311765* get_BLALEKBHIPM_3() const { return ___BLALEKBHIPM_3; }
	inline Vector3U5BU5D_t1172311765** get_address_of_BLALEKBHIPM_3() { return &___BLALEKBHIPM_3; }
	inline void set_BLALEKBHIPM_3(Vector3U5BU5D_t1172311765* value)
	{
		___BLALEKBHIPM_3 = value;
		Il2CppCodeGenWriteBarrier(&___BLALEKBHIPM_3, value);
	}

	inline static int32_t get_offset_of_IAGMGKCDFLC_4() { return static_cast<int32_t>(offsetof(ScrubberEvents_t2429506345, ___IAGMGKCDFLC_4)); }
	inline Slider_t297367283 * get_IAGMGKCDFLC_4() const { return ___IAGMGKCDFLC_4; }
	inline Slider_t297367283 ** get_address_of_IAGMGKCDFLC_4() { return &___IAGMGKCDFLC_4; }
	inline void set_IAGMGKCDFLC_4(Slider_t297367283 * value)
	{
		___IAGMGKCDFLC_4 = value;
		Il2CppCodeGenWriteBarrier(&___IAGMGKCDFLC_4, value);
	}

	inline static int32_t get_offset_of_NAGPFCGNKJG_5() { return static_cast<int32_t>(offsetof(ScrubberEvents_t2429506345, ___NAGPFCGNKJG_5)); }
	inline VideoControlsManager_t3010523296 * get_NAGPFCGNKJG_5() const { return ___NAGPFCGNKJG_5; }
	inline VideoControlsManager_t3010523296 ** get_address_of_NAGPFCGNKJG_5() { return &___NAGPFCGNKJG_5; }
	inline void set_NAGPFCGNKJG_5(VideoControlsManager_t3010523296 * value)
	{
		___NAGPFCGNKJG_5 = value;
		Il2CppCodeGenWriteBarrier(&___NAGPFCGNKJG_5, value);
	}

	inline static int32_t get_offset_of_KCIBEMJPAAL_6() { return static_cast<int32_t>(offsetof(ScrubberEvents_t2429506345, ___KCIBEMJPAAL_6)); }
	inline GvrPointerInputModule_t1603976810 * get_KCIBEMJPAAL_6() const { return ___KCIBEMJPAAL_6; }
	inline GvrPointerInputModule_t1603976810 ** get_address_of_KCIBEMJPAAL_6() { return &___KCIBEMJPAAL_6; }
	inline void set_KCIBEMJPAAL_6(GvrPointerInputModule_t1603976810 * value)
	{
		___KCIBEMJPAAL_6 = value;
		Il2CppCodeGenWriteBarrier(&___KCIBEMJPAAL_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
