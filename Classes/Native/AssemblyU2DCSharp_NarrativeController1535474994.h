﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Singleton_1_gen2711359381.h"
#include "AssemblyU2DCSharp_HFHIMOBLMJG2996998969.h"

// System.Collections.Generic.List`1<Augmentation>
struct List_1_t4194622440;
// System.Collections.Generic.List`1<Tether>
struct List_1_t3551859736;
// NarrativeController/DIKDKCCLMNH
struct DIKDKCCLMNH_t2086442123;
// NarrativeController/EFPHLCGIJJG
struct EFPHLCGIJJG_t2265475206;
// NarrativeController/APPPEKEOLLO
struct APPPEKEOLLO_t1830122991;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NarrativeController
struct  NarrativeController_t1535474994  : public Singleton_1_t2711359381
{
public:
	// HFHIMOBLMJG NarrativeController::_playMode
	int32_t ____playMode_5;
	// System.Collections.Generic.List`1<Augmentation> NarrativeController::augmentations
	List_1_t4194622440 * ___augmentations_6;
	// System.Collections.Generic.List`1<Tether> NarrativeController::augmentationTethers
	List_1_t3551859736 * ___augmentationTethers_7;
	// NarrativeController/DIKDKCCLMNH NarrativeController::RestartNarrative
	DIKDKCCLMNH_t2086442123 * ___RestartNarrative_8;
	// NarrativeController/EFPHLCGIJJG NarrativeController::OnFocusTarget
	EFPHLCGIJJG_t2265475206 * ___OnFocusTarget_9;
	// NarrativeController/APPPEKEOLLO NarrativeController::OnUnFocusTarget
	APPPEKEOLLO_t1830122991 * ___OnUnFocusTarget_10;

public:
	inline static int32_t get_offset_of__playMode_5() { return static_cast<int32_t>(offsetof(NarrativeController_t1535474994, ____playMode_5)); }
	inline int32_t get__playMode_5() const { return ____playMode_5; }
	inline int32_t* get_address_of__playMode_5() { return &____playMode_5; }
	inline void set__playMode_5(int32_t value)
	{
		____playMode_5 = value;
	}

	inline static int32_t get_offset_of_augmentations_6() { return static_cast<int32_t>(offsetof(NarrativeController_t1535474994, ___augmentations_6)); }
	inline List_1_t4194622440 * get_augmentations_6() const { return ___augmentations_6; }
	inline List_1_t4194622440 ** get_address_of_augmentations_6() { return &___augmentations_6; }
	inline void set_augmentations_6(List_1_t4194622440 * value)
	{
		___augmentations_6 = value;
		Il2CppCodeGenWriteBarrier(&___augmentations_6, value);
	}

	inline static int32_t get_offset_of_augmentationTethers_7() { return static_cast<int32_t>(offsetof(NarrativeController_t1535474994, ___augmentationTethers_7)); }
	inline List_1_t3551859736 * get_augmentationTethers_7() const { return ___augmentationTethers_7; }
	inline List_1_t3551859736 ** get_address_of_augmentationTethers_7() { return &___augmentationTethers_7; }
	inline void set_augmentationTethers_7(List_1_t3551859736 * value)
	{
		___augmentationTethers_7 = value;
		Il2CppCodeGenWriteBarrier(&___augmentationTethers_7, value);
	}

	inline static int32_t get_offset_of_RestartNarrative_8() { return static_cast<int32_t>(offsetof(NarrativeController_t1535474994, ___RestartNarrative_8)); }
	inline DIKDKCCLMNH_t2086442123 * get_RestartNarrative_8() const { return ___RestartNarrative_8; }
	inline DIKDKCCLMNH_t2086442123 ** get_address_of_RestartNarrative_8() { return &___RestartNarrative_8; }
	inline void set_RestartNarrative_8(DIKDKCCLMNH_t2086442123 * value)
	{
		___RestartNarrative_8 = value;
		Il2CppCodeGenWriteBarrier(&___RestartNarrative_8, value);
	}

	inline static int32_t get_offset_of_OnFocusTarget_9() { return static_cast<int32_t>(offsetof(NarrativeController_t1535474994, ___OnFocusTarget_9)); }
	inline EFPHLCGIJJG_t2265475206 * get_OnFocusTarget_9() const { return ___OnFocusTarget_9; }
	inline EFPHLCGIJJG_t2265475206 ** get_address_of_OnFocusTarget_9() { return &___OnFocusTarget_9; }
	inline void set_OnFocusTarget_9(EFPHLCGIJJG_t2265475206 * value)
	{
		___OnFocusTarget_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnFocusTarget_9, value);
	}

	inline static int32_t get_offset_of_OnUnFocusTarget_10() { return static_cast<int32_t>(offsetof(NarrativeController_t1535474994, ___OnUnFocusTarget_10)); }
	inline APPPEKEOLLO_t1830122991 * get_OnUnFocusTarget_10() const { return ___OnUnFocusTarget_10; }
	inline APPPEKEOLLO_t1830122991 ** get_address_of_OnUnFocusTarget_10() { return &___OnUnFocusTarget_10; }
	inline void set_OnUnFocusTarget_10(APPPEKEOLLO_t1830122991 * value)
	{
		___OnUnFocusTarget_10 = value;
		Il2CppCodeGenWriteBarrier(&___OnUnFocusTarget_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
