﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// GvrVideoPlayerTexture
struct GvrVideoPlayerTexture_t673526704;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey3
struct  U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473  : public Il2CppObject
{
public:
	// GvrVideoPlayerTexture GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey3::OECALIBMPJK
	GvrVideoPlayerTexture_t673526704 * ___OECALIBMPJK_0;
	// System.String GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey3::LNNKFPBGOCK
	String_t* ___LNNKFPBGOCK_1;
	// System.String GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey3::PLLOJIMPMPE
	String_t* ___PLLOJIMPMPE_2;

public:
	inline static int32_t get_offset_of_OECALIBMPJK_0() { return static_cast<int32_t>(offsetof(U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473, ___OECALIBMPJK_0)); }
	inline GvrVideoPlayerTexture_t673526704 * get_OECALIBMPJK_0() const { return ___OECALIBMPJK_0; }
	inline GvrVideoPlayerTexture_t673526704 ** get_address_of_OECALIBMPJK_0() { return &___OECALIBMPJK_0; }
	inline void set_OECALIBMPJK_0(GvrVideoPlayerTexture_t673526704 * value)
	{
		___OECALIBMPJK_0 = value;
		Il2CppCodeGenWriteBarrier(&___OECALIBMPJK_0, value);
	}

	inline static int32_t get_offset_of_LNNKFPBGOCK_1() { return static_cast<int32_t>(offsetof(U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473, ___LNNKFPBGOCK_1)); }
	inline String_t* get_LNNKFPBGOCK_1() const { return ___LNNKFPBGOCK_1; }
	inline String_t** get_address_of_LNNKFPBGOCK_1() { return &___LNNKFPBGOCK_1; }
	inline void set_LNNKFPBGOCK_1(String_t* value)
	{
		___LNNKFPBGOCK_1 = value;
		Il2CppCodeGenWriteBarrier(&___LNNKFPBGOCK_1, value);
	}

	inline static int32_t get_offset_of_PLLOJIMPMPE_2() { return static_cast<int32_t>(offsetof(U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473, ___PLLOJIMPMPE_2)); }
	inline String_t* get_PLLOJIMPMPE_2() const { return ___PLLOJIMPMPE_2; }
	inline String_t** get_address_of_PLLOJIMPMPE_2() { return &___PLLOJIMPMPE_2; }
	inline void set_PLLOJIMPMPE_2(String_t* value)
	{
		___PLLOJIMPMPE_2 = value;
		Il2CppCodeGenWriteBarrier(&___PLLOJIMPMPE_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
