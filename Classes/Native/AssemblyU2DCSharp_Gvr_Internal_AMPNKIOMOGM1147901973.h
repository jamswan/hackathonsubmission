﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.AMPNKIOMOGM
struct  AMPNKIOMOGM_t1147901973 
{
public:
	// System.Int64 Gvr.Internal.AMPNKIOMOGM::GJJAEACHFEK
	int64_t ___GJJAEACHFEK_0;
	// UnityEngine.Vector3 Gvr.Internal.AMPNKIOMOGM::PJMCJKFMGJI
	Vector3_t2243707580  ___PJMCJKFMGJI_1;

public:
	inline static int32_t get_offset_of_GJJAEACHFEK_0() { return static_cast<int32_t>(offsetof(AMPNKIOMOGM_t1147901973, ___GJJAEACHFEK_0)); }
	inline int64_t get_GJJAEACHFEK_0() const { return ___GJJAEACHFEK_0; }
	inline int64_t* get_address_of_GJJAEACHFEK_0() { return &___GJJAEACHFEK_0; }
	inline void set_GJJAEACHFEK_0(int64_t value)
	{
		___GJJAEACHFEK_0 = value;
	}

	inline static int32_t get_offset_of_PJMCJKFMGJI_1() { return static_cast<int32_t>(offsetof(AMPNKIOMOGM_t1147901973, ___PJMCJKFMGJI_1)); }
	inline Vector3_t2243707580  get_PJMCJKFMGJI_1() const { return ___PJMCJKFMGJI_1; }
	inline Vector3_t2243707580 * get_address_of_PJMCJKFMGJI_1() { return &___PJMCJKFMGJI_1; }
	inline void set_PJMCJKFMGJI_1(Vector3_t2243707580  value)
	{
		___PJMCJKFMGJI_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
