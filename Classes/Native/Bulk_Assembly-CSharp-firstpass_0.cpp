﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Beebyte_Obfuscator_D1792108601.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Attribute542643598.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Beebyte_Obfuscator_Ob492095300.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Beebyte_Obfuscator_Re583505052.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Beebyte_Obfuscator_R1927323899.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Beebyte_Obfuscator_S3608595407.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Beebyte_Obfuscator_S2535354723.h"

// Beebyte.Obfuscator.DoNotFakeAttribute
struct DoNotFakeAttribute_t1792108601;
// System.Attribute
struct Attribute_t542643598;
// Beebyte.Obfuscator.ObfuscateLiteralsAttribute
struct ObfuscateLiteralsAttribute_t492095300;
// Beebyte.Obfuscator.RenameAttribute
struct RenameAttribute_t583505052;
// System.String
struct String_t;
// Beebyte.Obfuscator.ReplaceLiteralsWithNameAttribute
struct ReplaceLiteralsWithNameAttribute_t1927323899;
// Beebyte.Obfuscator.SkipAttribute
struct SkipAttribute_t3608595407;
// Beebyte.Obfuscator.SkipRenameAttribute
struct SkipRenameAttribute_t2535354723;




// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1730479323 (Attribute_t542643598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Beebyte.Obfuscator.DoNotFakeAttribute::.ctor()
extern "C"  void DoNotFakeAttribute__ctor_m1590746 (DoNotFakeAttribute_t1792108601 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Beebyte.Obfuscator.ObfuscateLiteralsAttribute::.ctor()
extern "C"  void ObfuscateLiteralsAttribute__ctor_m2566873701 (ObfuscateLiteralsAttribute_t492095300 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Beebyte.Obfuscator.RenameAttribute::.ctor()
extern "C"  void RenameAttribute__ctor_m1026968991 (RenameAttribute_t583505052 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Beebyte.Obfuscator.RenameAttribute::.ctor(System.String)
extern "C"  void RenameAttribute__ctor_m2151326289 (RenameAttribute_t583505052 * __this, String_t* ___target0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___target0;
		__this->set_target_0(L_0);
		return;
	}
}
// System.String Beebyte.Obfuscator.RenameAttribute::GetTarget()
extern "C"  String_t* RenameAttribute_GetTarget_m1560197893 (RenameAttribute_t583505052 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_target_0();
		return L_0;
	}
}
// System.Void Beebyte.Obfuscator.ReplaceLiteralsWithNameAttribute::.ctor()
extern "C"  void ReplaceLiteralsWithNameAttribute__ctor_m3178145394 (ReplaceLiteralsWithNameAttribute_t1927323899 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Beebyte.Obfuscator.SkipAttribute::.ctor()
extern "C"  void SkipAttribute__ctor_m2857305682 (SkipAttribute_t3608595407 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Beebyte.Obfuscator.SkipRenameAttribute::.ctor()
extern "C"  void SkipRenameAttribute__ctor_m3966014824 (SkipRenameAttribute_t2535354723 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
