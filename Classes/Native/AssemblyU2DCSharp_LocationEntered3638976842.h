﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocationEntered
struct  LocationEntered_t3638976842  : public NarrativeCondition_t4123859457
{
public:
	// System.String LocationEntered::location
	String_t* ___location_13;

public:
	inline static int32_t get_offset_of_location_13() { return static_cast<int32_t>(offsetof(LocationEntered_t3638976842, ___location_13)); }
	inline String_t* get_location_13() const { return ___location_13; }
	inline String_t** get_address_of_location_13() { return &___location_13; }
	inline void set_location_13(String_t* value)
	{
		___location_13 = value;
		Il2CppCodeGenWriteBarrier(&___location_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
