﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// IGHKEEOAKDP
struct IGHKEEOAKDP_t3590090476;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrReticlePointer
struct  GvrReticlePointer_t2836438988  : public MonoBehaviour_t1158329972
{
public:
	// IGHKEEOAKDP GvrReticlePointer::JAMHFIIINGC
	IGHKEEOAKDP_t3590090476 * ___JAMHFIIINGC_2;
	// System.Int32 GvrReticlePointer::reticleSegments
	int32_t ___reticleSegments_3;
	// System.Single GvrReticlePointer::reticleGrowthSpeed
	float ___reticleGrowthSpeed_4;

public:
	inline static int32_t get_offset_of_JAMHFIIINGC_2() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2836438988, ___JAMHFIIINGC_2)); }
	inline IGHKEEOAKDP_t3590090476 * get_JAMHFIIINGC_2() const { return ___JAMHFIIINGC_2; }
	inline IGHKEEOAKDP_t3590090476 ** get_address_of_JAMHFIIINGC_2() { return &___JAMHFIIINGC_2; }
	inline void set_JAMHFIIINGC_2(IGHKEEOAKDP_t3590090476 * value)
	{
		___JAMHFIIINGC_2 = value;
		Il2CppCodeGenWriteBarrier(&___JAMHFIIINGC_2, value);
	}

	inline static int32_t get_offset_of_reticleSegments_3() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2836438988, ___reticleSegments_3)); }
	inline int32_t get_reticleSegments_3() const { return ___reticleSegments_3; }
	inline int32_t* get_address_of_reticleSegments_3() { return &___reticleSegments_3; }
	inline void set_reticleSegments_3(int32_t value)
	{
		___reticleSegments_3 = value;
	}

	inline static int32_t get_offset_of_reticleGrowthSpeed_4() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2836438988, ___reticleGrowthSpeed_4)); }
	inline float get_reticleGrowthSpeed_4() const { return ___reticleGrowthSpeed_4; }
	inline float* get_address_of_reticleGrowthSpeed_4() { return &___reticleGrowthSpeed_4; }
	inline void set_reticleGrowthSpeed_4(float value)
	{
		___reticleGrowthSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
