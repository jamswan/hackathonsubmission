﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Gene1917433415.h"

// proto.PhoneEvent/Types/AccelerometerEvent
struct AccelerometerEvent_t1893725728;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/AccelerometerEvent
struct  AccelerometerEvent_t1893725728  : public GeneratedMessageLite_2_t1917433415
{
public:
	// System.Boolean proto.PhoneEvent/Types/AccelerometerEvent::MIGGKHHBAID
	bool ___MIGGKHHBAID_4;
	// System.Int64 proto.PhoneEvent/Types/AccelerometerEvent::HMBIGJIDGNB
	int64_t ___HMBIGJIDGNB_5;
	// System.Boolean proto.PhoneEvent/Types/AccelerometerEvent::DAPIOCPOIID
	bool ___DAPIOCPOIID_7;
	// System.Single proto.PhoneEvent/Types/AccelerometerEvent::KKLEGOBOFFJ
	float ___KKLEGOBOFFJ_8;
	// System.Boolean proto.PhoneEvent/Types/AccelerometerEvent::LGBJOJHEJDM
	bool ___LGBJOJHEJDM_10;
	// System.Single proto.PhoneEvent/Types/AccelerometerEvent::NFCIJFNNNDL
	float ___NFCIJFNNNDL_11;
	// System.Boolean proto.PhoneEvent/Types/AccelerometerEvent::LPLIBMIPMFD
	bool ___LPLIBMIPMFD_13;
	// System.Single proto.PhoneEvent/Types/AccelerometerEvent::PLILMNHLFED
	float ___PLILMNHLFED_14;
	// System.Int32 proto.PhoneEvent/Types/AccelerometerEvent::BABCKJCECDJ
	int32_t ___BABCKJCECDJ_15;

public:
	inline static int32_t get_offset_of_MIGGKHHBAID_4() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1893725728, ___MIGGKHHBAID_4)); }
	inline bool get_MIGGKHHBAID_4() const { return ___MIGGKHHBAID_4; }
	inline bool* get_address_of_MIGGKHHBAID_4() { return &___MIGGKHHBAID_4; }
	inline void set_MIGGKHHBAID_4(bool value)
	{
		___MIGGKHHBAID_4 = value;
	}

	inline static int32_t get_offset_of_HMBIGJIDGNB_5() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1893725728, ___HMBIGJIDGNB_5)); }
	inline int64_t get_HMBIGJIDGNB_5() const { return ___HMBIGJIDGNB_5; }
	inline int64_t* get_address_of_HMBIGJIDGNB_5() { return &___HMBIGJIDGNB_5; }
	inline void set_HMBIGJIDGNB_5(int64_t value)
	{
		___HMBIGJIDGNB_5 = value;
	}

	inline static int32_t get_offset_of_DAPIOCPOIID_7() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1893725728, ___DAPIOCPOIID_7)); }
	inline bool get_DAPIOCPOIID_7() const { return ___DAPIOCPOIID_7; }
	inline bool* get_address_of_DAPIOCPOIID_7() { return &___DAPIOCPOIID_7; }
	inline void set_DAPIOCPOIID_7(bool value)
	{
		___DAPIOCPOIID_7 = value;
	}

	inline static int32_t get_offset_of_KKLEGOBOFFJ_8() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1893725728, ___KKLEGOBOFFJ_8)); }
	inline float get_KKLEGOBOFFJ_8() const { return ___KKLEGOBOFFJ_8; }
	inline float* get_address_of_KKLEGOBOFFJ_8() { return &___KKLEGOBOFFJ_8; }
	inline void set_KKLEGOBOFFJ_8(float value)
	{
		___KKLEGOBOFFJ_8 = value;
	}

	inline static int32_t get_offset_of_LGBJOJHEJDM_10() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1893725728, ___LGBJOJHEJDM_10)); }
	inline bool get_LGBJOJHEJDM_10() const { return ___LGBJOJHEJDM_10; }
	inline bool* get_address_of_LGBJOJHEJDM_10() { return &___LGBJOJHEJDM_10; }
	inline void set_LGBJOJHEJDM_10(bool value)
	{
		___LGBJOJHEJDM_10 = value;
	}

	inline static int32_t get_offset_of_NFCIJFNNNDL_11() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1893725728, ___NFCIJFNNNDL_11)); }
	inline float get_NFCIJFNNNDL_11() const { return ___NFCIJFNNNDL_11; }
	inline float* get_address_of_NFCIJFNNNDL_11() { return &___NFCIJFNNNDL_11; }
	inline void set_NFCIJFNNNDL_11(float value)
	{
		___NFCIJFNNNDL_11 = value;
	}

	inline static int32_t get_offset_of_LPLIBMIPMFD_13() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1893725728, ___LPLIBMIPMFD_13)); }
	inline bool get_LPLIBMIPMFD_13() const { return ___LPLIBMIPMFD_13; }
	inline bool* get_address_of_LPLIBMIPMFD_13() { return &___LPLIBMIPMFD_13; }
	inline void set_LPLIBMIPMFD_13(bool value)
	{
		___LPLIBMIPMFD_13 = value;
	}

	inline static int32_t get_offset_of_PLILMNHLFED_14() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1893725728, ___PLILMNHLFED_14)); }
	inline float get_PLILMNHLFED_14() const { return ___PLILMNHLFED_14; }
	inline float* get_address_of_PLILMNHLFED_14() { return &___PLILMNHLFED_14; }
	inline void set_PLILMNHLFED_14(float value)
	{
		___PLILMNHLFED_14 = value;
	}

	inline static int32_t get_offset_of_BABCKJCECDJ_15() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1893725728, ___BABCKJCECDJ_15)); }
	inline int32_t get_BABCKJCECDJ_15() const { return ___BABCKJCECDJ_15; }
	inline int32_t* get_address_of_BABCKJCECDJ_15() { return &___BABCKJCECDJ_15; }
	inline void set_BABCKJCECDJ_15(int32_t value)
	{
		___BABCKJCECDJ_15 = value;
	}
};

struct AccelerometerEvent_t1893725728_StaticFields
{
public:
	// proto.PhoneEvent/Types/AccelerometerEvent proto.PhoneEvent/Types/AccelerometerEvent::CACHHOCBHAE
	AccelerometerEvent_t1893725728 * ___CACHHOCBHAE_0;
	// System.String[] proto.PhoneEvent/Types/AccelerometerEvent::NLBFAPCOBBK
	StringU5BU5D_t1642385972* ___NLBFAPCOBBK_1;
	// System.UInt32[] proto.PhoneEvent/Types/AccelerometerEvent::FNHKMMGHANC
	UInt32U5BU5D_t59386216* ___FNHKMMGHANC_2;

public:
	inline static int32_t get_offset_of_CACHHOCBHAE_0() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1893725728_StaticFields, ___CACHHOCBHAE_0)); }
	inline AccelerometerEvent_t1893725728 * get_CACHHOCBHAE_0() const { return ___CACHHOCBHAE_0; }
	inline AccelerometerEvent_t1893725728 ** get_address_of_CACHHOCBHAE_0() { return &___CACHHOCBHAE_0; }
	inline void set_CACHHOCBHAE_0(AccelerometerEvent_t1893725728 * value)
	{
		___CACHHOCBHAE_0 = value;
		Il2CppCodeGenWriteBarrier(&___CACHHOCBHAE_0, value);
	}

	inline static int32_t get_offset_of_NLBFAPCOBBK_1() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1893725728_StaticFields, ___NLBFAPCOBBK_1)); }
	inline StringU5BU5D_t1642385972* get_NLBFAPCOBBK_1() const { return ___NLBFAPCOBBK_1; }
	inline StringU5BU5D_t1642385972** get_address_of_NLBFAPCOBBK_1() { return &___NLBFAPCOBBK_1; }
	inline void set_NLBFAPCOBBK_1(StringU5BU5D_t1642385972* value)
	{
		___NLBFAPCOBBK_1 = value;
		Il2CppCodeGenWriteBarrier(&___NLBFAPCOBBK_1, value);
	}

	inline static int32_t get_offset_of_FNHKMMGHANC_2() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1893725728_StaticFields, ___FNHKMMGHANC_2)); }
	inline UInt32U5BU5D_t59386216* get_FNHKMMGHANC_2() const { return ___FNHKMMGHANC_2; }
	inline UInt32U5BU5D_t59386216** get_address_of_FNHKMMGHANC_2() { return &___FNHKMMGHANC_2; }
	inline void set_FNHKMMGHANC_2(UInt32U5BU5D_t59386216* value)
	{
		___FNHKMMGHANC_2 = value;
		Il2CppCodeGenWriteBarrier(&___FNHKMMGHANC_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
