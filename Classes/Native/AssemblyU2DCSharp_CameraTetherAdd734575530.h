﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// CameraTether
struct CameraTether_t1360283009;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraTetherAdd
struct  CameraTetherAdd_t734575530  : public NarrativeEffect_t3336735121
{
public:
	// CameraTether CameraTetherAdd::cameraTether
	CameraTether_t1360283009 * ___cameraTether_3;

public:
	inline static int32_t get_offset_of_cameraTether_3() { return static_cast<int32_t>(offsetof(CameraTetherAdd_t734575530, ___cameraTether_3)); }
	inline CameraTether_t1360283009 * get_cameraTether_3() const { return ___cameraTether_3; }
	inline CameraTether_t1360283009 ** get_address_of_cameraTether_3() { return &___cameraTether_3; }
	inline void set_cameraTether_3(CameraTether_t1360283009 * value)
	{
		___cameraTether_3 = value;
		Il2CppCodeGenWriteBarrier(&___cameraTether_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
