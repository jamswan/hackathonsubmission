﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.UI.Text
struct Text_t356221433;
// GvrVideoPlayerTexture
struct GvrVideoPlayerTexture_t673526704;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoControlsManager
struct  VideoControlsManager_t3010523296  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject VideoControlsManager::OGIMIFIJBHE
	GameObject_t1756533147 * ___OGIMIFIJBHE_2;
	// UnityEngine.GameObject VideoControlsManager::IHDMIIKLJPN
	GameObject_t1756533147 * ___IHDMIIKLJPN_3;
	// UnityEngine.UI.Slider VideoControlsManager::FCDACJEACOO
	Slider_t297367283 * ___FCDACJEACOO_4;
	// UnityEngine.UI.Slider VideoControlsManager::KBCKBIKIEKI
	Slider_t297367283 * ___KBCKBIKIEKI_5;
	// UnityEngine.GameObject VideoControlsManager::NILMBMLHEPI
	GameObject_t1756533147 * ___NILMBMLHEPI_6;
	// UnityEngine.GameObject VideoControlsManager::GIFEKIKJCGN
	GameObject_t1756533147 * ___GIFEKIKJCGN_7;
	// UnityEngine.GameObject VideoControlsManager::DJCKNPMFNEO
	GameObject_t1756533147 * ___DJCKNPMFNEO_8;
	// UnityEngine.Vector3 VideoControlsManager::ECJHOEHFPEP
	Vector3_t2243707580  ___ECJHOEHFPEP_9;
	// UnityEngine.UI.Text VideoControlsManager::EBGPHMIECCE
	Text_t356221433 * ___EBGPHMIECCE_10;
	// UnityEngine.UI.Text VideoControlsManager::BDBNLNHKGOL
	Text_t356221433 * ___BDBNLNHKGOL_11;
	// GvrVideoPlayerTexture VideoControlsManager::<LHKBFMHLFBF>k__BackingField
	GvrVideoPlayerTexture_t673526704 * ___U3CLHKBFMHLFBFU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_OGIMIFIJBHE_2() { return static_cast<int32_t>(offsetof(VideoControlsManager_t3010523296, ___OGIMIFIJBHE_2)); }
	inline GameObject_t1756533147 * get_OGIMIFIJBHE_2() const { return ___OGIMIFIJBHE_2; }
	inline GameObject_t1756533147 ** get_address_of_OGIMIFIJBHE_2() { return &___OGIMIFIJBHE_2; }
	inline void set_OGIMIFIJBHE_2(GameObject_t1756533147 * value)
	{
		___OGIMIFIJBHE_2 = value;
		Il2CppCodeGenWriteBarrier(&___OGIMIFIJBHE_2, value);
	}

	inline static int32_t get_offset_of_IHDMIIKLJPN_3() { return static_cast<int32_t>(offsetof(VideoControlsManager_t3010523296, ___IHDMIIKLJPN_3)); }
	inline GameObject_t1756533147 * get_IHDMIIKLJPN_3() const { return ___IHDMIIKLJPN_3; }
	inline GameObject_t1756533147 ** get_address_of_IHDMIIKLJPN_3() { return &___IHDMIIKLJPN_3; }
	inline void set_IHDMIIKLJPN_3(GameObject_t1756533147 * value)
	{
		___IHDMIIKLJPN_3 = value;
		Il2CppCodeGenWriteBarrier(&___IHDMIIKLJPN_3, value);
	}

	inline static int32_t get_offset_of_FCDACJEACOO_4() { return static_cast<int32_t>(offsetof(VideoControlsManager_t3010523296, ___FCDACJEACOO_4)); }
	inline Slider_t297367283 * get_FCDACJEACOO_4() const { return ___FCDACJEACOO_4; }
	inline Slider_t297367283 ** get_address_of_FCDACJEACOO_4() { return &___FCDACJEACOO_4; }
	inline void set_FCDACJEACOO_4(Slider_t297367283 * value)
	{
		___FCDACJEACOO_4 = value;
		Il2CppCodeGenWriteBarrier(&___FCDACJEACOO_4, value);
	}

	inline static int32_t get_offset_of_KBCKBIKIEKI_5() { return static_cast<int32_t>(offsetof(VideoControlsManager_t3010523296, ___KBCKBIKIEKI_5)); }
	inline Slider_t297367283 * get_KBCKBIKIEKI_5() const { return ___KBCKBIKIEKI_5; }
	inline Slider_t297367283 ** get_address_of_KBCKBIKIEKI_5() { return &___KBCKBIKIEKI_5; }
	inline void set_KBCKBIKIEKI_5(Slider_t297367283 * value)
	{
		___KBCKBIKIEKI_5 = value;
		Il2CppCodeGenWriteBarrier(&___KBCKBIKIEKI_5, value);
	}

	inline static int32_t get_offset_of_NILMBMLHEPI_6() { return static_cast<int32_t>(offsetof(VideoControlsManager_t3010523296, ___NILMBMLHEPI_6)); }
	inline GameObject_t1756533147 * get_NILMBMLHEPI_6() const { return ___NILMBMLHEPI_6; }
	inline GameObject_t1756533147 ** get_address_of_NILMBMLHEPI_6() { return &___NILMBMLHEPI_6; }
	inline void set_NILMBMLHEPI_6(GameObject_t1756533147 * value)
	{
		___NILMBMLHEPI_6 = value;
		Il2CppCodeGenWriteBarrier(&___NILMBMLHEPI_6, value);
	}

	inline static int32_t get_offset_of_GIFEKIKJCGN_7() { return static_cast<int32_t>(offsetof(VideoControlsManager_t3010523296, ___GIFEKIKJCGN_7)); }
	inline GameObject_t1756533147 * get_GIFEKIKJCGN_7() const { return ___GIFEKIKJCGN_7; }
	inline GameObject_t1756533147 ** get_address_of_GIFEKIKJCGN_7() { return &___GIFEKIKJCGN_7; }
	inline void set_GIFEKIKJCGN_7(GameObject_t1756533147 * value)
	{
		___GIFEKIKJCGN_7 = value;
		Il2CppCodeGenWriteBarrier(&___GIFEKIKJCGN_7, value);
	}

	inline static int32_t get_offset_of_DJCKNPMFNEO_8() { return static_cast<int32_t>(offsetof(VideoControlsManager_t3010523296, ___DJCKNPMFNEO_8)); }
	inline GameObject_t1756533147 * get_DJCKNPMFNEO_8() const { return ___DJCKNPMFNEO_8; }
	inline GameObject_t1756533147 ** get_address_of_DJCKNPMFNEO_8() { return &___DJCKNPMFNEO_8; }
	inline void set_DJCKNPMFNEO_8(GameObject_t1756533147 * value)
	{
		___DJCKNPMFNEO_8 = value;
		Il2CppCodeGenWriteBarrier(&___DJCKNPMFNEO_8, value);
	}

	inline static int32_t get_offset_of_ECJHOEHFPEP_9() { return static_cast<int32_t>(offsetof(VideoControlsManager_t3010523296, ___ECJHOEHFPEP_9)); }
	inline Vector3_t2243707580  get_ECJHOEHFPEP_9() const { return ___ECJHOEHFPEP_9; }
	inline Vector3_t2243707580 * get_address_of_ECJHOEHFPEP_9() { return &___ECJHOEHFPEP_9; }
	inline void set_ECJHOEHFPEP_9(Vector3_t2243707580  value)
	{
		___ECJHOEHFPEP_9 = value;
	}

	inline static int32_t get_offset_of_EBGPHMIECCE_10() { return static_cast<int32_t>(offsetof(VideoControlsManager_t3010523296, ___EBGPHMIECCE_10)); }
	inline Text_t356221433 * get_EBGPHMIECCE_10() const { return ___EBGPHMIECCE_10; }
	inline Text_t356221433 ** get_address_of_EBGPHMIECCE_10() { return &___EBGPHMIECCE_10; }
	inline void set_EBGPHMIECCE_10(Text_t356221433 * value)
	{
		___EBGPHMIECCE_10 = value;
		Il2CppCodeGenWriteBarrier(&___EBGPHMIECCE_10, value);
	}

	inline static int32_t get_offset_of_BDBNLNHKGOL_11() { return static_cast<int32_t>(offsetof(VideoControlsManager_t3010523296, ___BDBNLNHKGOL_11)); }
	inline Text_t356221433 * get_BDBNLNHKGOL_11() const { return ___BDBNLNHKGOL_11; }
	inline Text_t356221433 ** get_address_of_BDBNLNHKGOL_11() { return &___BDBNLNHKGOL_11; }
	inline void set_BDBNLNHKGOL_11(Text_t356221433 * value)
	{
		___BDBNLNHKGOL_11 = value;
		Il2CppCodeGenWriteBarrier(&___BDBNLNHKGOL_11, value);
	}

	inline static int32_t get_offset_of_U3CLHKBFMHLFBFU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(VideoControlsManager_t3010523296, ___U3CLHKBFMHLFBFU3Ek__BackingField_12)); }
	inline GvrVideoPlayerTexture_t673526704 * get_U3CLHKBFMHLFBFU3Ek__BackingField_12() const { return ___U3CLHKBFMHLFBFU3Ek__BackingField_12; }
	inline GvrVideoPlayerTexture_t673526704 ** get_address_of_U3CLHKBFMHLFBFU3Ek__BackingField_12() { return &___U3CLHKBFMHLFBFU3Ek__BackingField_12; }
	inline void set_U3CLHKBFMHLFBFU3Ek__BackingField_12(GvrVideoPlayerTexture_t673526704 * value)
	{
		___U3CLHKBFMHLFBFU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLHKBFMHLFBFU3Ek__BackingField_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
