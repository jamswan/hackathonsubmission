﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Tether4182738604.h"

// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1779888572;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AugmentationTether
struct  AugmentationTether_t3007013576  : public Tether_t4182738604
{
public:
	// Vuforia.TrackableBehaviour AugmentationTether::MDEHCLGCIKP
	TrackableBehaviour_t1779888572 * ___MDEHCLGCIKP_6;
	// UnityEngine.GameObject AugmentationTether::imageTarget
	GameObject_t1756533147 * ___imageTarget_7;

public:
	inline static int32_t get_offset_of_MDEHCLGCIKP_6() { return static_cast<int32_t>(offsetof(AugmentationTether_t3007013576, ___MDEHCLGCIKP_6)); }
	inline TrackableBehaviour_t1779888572 * get_MDEHCLGCIKP_6() const { return ___MDEHCLGCIKP_6; }
	inline TrackableBehaviour_t1779888572 ** get_address_of_MDEHCLGCIKP_6() { return &___MDEHCLGCIKP_6; }
	inline void set_MDEHCLGCIKP_6(TrackableBehaviour_t1779888572 * value)
	{
		___MDEHCLGCIKP_6 = value;
		Il2CppCodeGenWriteBarrier(&___MDEHCLGCIKP_6, value);
	}

	inline static int32_t get_offset_of_imageTarget_7() { return static_cast<int32_t>(offsetof(AugmentationTether_t3007013576, ___imageTarget_7)); }
	inline GameObject_t1756533147 * get_imageTarget_7() const { return ___imageTarget_7; }
	inline GameObject_t1756533147 ** get_address_of_imageTarget_7() { return &___imageTarget_7; }
	inline void set_imageTarget_7(GameObject_t1756533147 * value)
	{
		___imageTarget_7 = value;
		Il2CppCodeGenWriteBarrier(&___imageTarget_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
