﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Canvas
struct Canvas_t209405766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitErrorHandler
struct  InitErrorHandler_t1791388012  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text InitErrorHandler::errorText
	Text_t356221433 * ___errorText_2;
	// UnityEngine.Canvas InitErrorHandler::LKPFDPMMGMC
	Canvas_t209405766 * ___LKPFDPMMGMC_3;

public:
	inline static int32_t get_offset_of_errorText_2() { return static_cast<int32_t>(offsetof(InitErrorHandler_t1791388012, ___errorText_2)); }
	inline Text_t356221433 * get_errorText_2() const { return ___errorText_2; }
	inline Text_t356221433 ** get_address_of_errorText_2() { return &___errorText_2; }
	inline void set_errorText_2(Text_t356221433 * value)
	{
		___errorText_2 = value;
		Il2CppCodeGenWriteBarrier(&___errorText_2, value);
	}

	inline static int32_t get_offset_of_LKPFDPMMGMC_3() { return static_cast<int32_t>(offsetof(InitErrorHandler_t1791388012, ___LKPFDPMMGMC_3)); }
	inline Canvas_t209405766 * get_LKPFDPMMGMC_3() const { return ___LKPFDPMMGMC_3; }
	inline Canvas_t209405766 ** get_address_of_LKPFDPMMGMC_3() { return &___LKPFDPMMGMC_3; }
	inline void set_LKPFDPMMGMC_3(Canvas_t209405766 * value)
	{
		___LKPFDPMMGMC_3 = value;
		Il2CppCodeGenWriteBarrier(&___LKPFDPMMGMC_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
