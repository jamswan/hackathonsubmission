﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_MMINFHHJGIF384011353.h"

// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FHOAHDEONLG
struct  FHOAHDEONLG_t2556966793  : public MMINFHHJGIF_t384011353
{
public:
	// System.String FHOAHDEONLG::COLCJHPAGJN
	String_t* ___COLCJHPAGJN_4;
	// UnityEngine.GUIStyle FHOAHDEONLG::ACCKGOBDPLM
	GUIStyle_t1799908754 * ___ACCKGOBDPLM_5;

public:
	inline static int32_t get_offset_of_COLCJHPAGJN_4() { return static_cast<int32_t>(offsetof(FHOAHDEONLG_t2556966793, ___COLCJHPAGJN_4)); }
	inline String_t* get_COLCJHPAGJN_4() const { return ___COLCJHPAGJN_4; }
	inline String_t** get_address_of_COLCJHPAGJN_4() { return &___COLCJHPAGJN_4; }
	inline void set_COLCJHPAGJN_4(String_t* value)
	{
		___COLCJHPAGJN_4 = value;
		Il2CppCodeGenWriteBarrier(&___COLCJHPAGJN_4, value);
	}

	inline static int32_t get_offset_of_ACCKGOBDPLM_5() { return static_cast<int32_t>(offsetof(FHOAHDEONLG_t2556966793, ___ACCKGOBDPLM_5)); }
	inline GUIStyle_t1799908754 * get_ACCKGOBDPLM_5() const { return ___ACCKGOBDPLM_5; }
	inline GUIStyle_t1799908754 ** get_address_of_ACCKGOBDPLM_5() { return &___ACCKGOBDPLM_5; }
	inline void set_ACCKGOBDPLM_5(GUIStyle_t1799908754 * value)
	{
		___ACCKGOBDPLM_5 = value;
		Il2CppCodeGenWriteBarrier(&___ACCKGOBDPLM_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
