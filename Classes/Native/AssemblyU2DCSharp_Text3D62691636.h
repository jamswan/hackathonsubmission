﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Text3D_EGCLHEJBIKM4071414764.h"

// Text3D
struct Text3D_t62691636;
// System.Collections.Generic.List`1<Text3D>
struct List_1_t3726780064;
// LetterSizes
struct LetterSizes_t1582613310;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Text3D
struct  Text3D_t62691636  : public MonoBehaviour_t1158329972
{
public:
	// Text3D Text3D::CCAGMENANPO
	Text3D_t62691636 * ___CCAGMENANPO_2;
	// System.Collections.Generic.List`1<Text3D> Text3D::NFMIEIJLOHM
	List_1_t3726780064 * ___NFMIEIJLOHM_3;
	// LetterSizes Text3D::MOCEJEHIGAJ
	LetterSizes_t1582613310 * ___MOCEJEHIGAJ_4;
	// UnityEngine.Transform Text3D::myTransform
	Transform_t3275118058 * ___myTransform_5;
	// UnityEngine.TextMesh Text3D::textMesh
	TextMesh_t1641806576 * ___textMesh_6;
	// System.Boolean Text3D::DebugBounds
	bool ___DebugBounds_7;
	// Text3D/EGCLHEJBIKM Text3D::anchor
	int32_t ___anchor_8;
	// System.String Text3D::BHFNEOEPKGC
	String_t* ___BHFNEOEPKGC_9;

public:
	inline static int32_t get_offset_of_CCAGMENANPO_2() { return static_cast<int32_t>(offsetof(Text3D_t62691636, ___CCAGMENANPO_2)); }
	inline Text3D_t62691636 * get_CCAGMENANPO_2() const { return ___CCAGMENANPO_2; }
	inline Text3D_t62691636 ** get_address_of_CCAGMENANPO_2() { return &___CCAGMENANPO_2; }
	inline void set_CCAGMENANPO_2(Text3D_t62691636 * value)
	{
		___CCAGMENANPO_2 = value;
		Il2CppCodeGenWriteBarrier(&___CCAGMENANPO_2, value);
	}

	inline static int32_t get_offset_of_NFMIEIJLOHM_3() { return static_cast<int32_t>(offsetof(Text3D_t62691636, ___NFMIEIJLOHM_3)); }
	inline List_1_t3726780064 * get_NFMIEIJLOHM_3() const { return ___NFMIEIJLOHM_3; }
	inline List_1_t3726780064 ** get_address_of_NFMIEIJLOHM_3() { return &___NFMIEIJLOHM_3; }
	inline void set_NFMIEIJLOHM_3(List_1_t3726780064 * value)
	{
		___NFMIEIJLOHM_3 = value;
		Il2CppCodeGenWriteBarrier(&___NFMIEIJLOHM_3, value);
	}

	inline static int32_t get_offset_of_MOCEJEHIGAJ_4() { return static_cast<int32_t>(offsetof(Text3D_t62691636, ___MOCEJEHIGAJ_4)); }
	inline LetterSizes_t1582613310 * get_MOCEJEHIGAJ_4() const { return ___MOCEJEHIGAJ_4; }
	inline LetterSizes_t1582613310 ** get_address_of_MOCEJEHIGAJ_4() { return &___MOCEJEHIGAJ_4; }
	inline void set_MOCEJEHIGAJ_4(LetterSizes_t1582613310 * value)
	{
		___MOCEJEHIGAJ_4 = value;
		Il2CppCodeGenWriteBarrier(&___MOCEJEHIGAJ_4, value);
	}

	inline static int32_t get_offset_of_myTransform_5() { return static_cast<int32_t>(offsetof(Text3D_t62691636, ___myTransform_5)); }
	inline Transform_t3275118058 * get_myTransform_5() const { return ___myTransform_5; }
	inline Transform_t3275118058 ** get_address_of_myTransform_5() { return &___myTransform_5; }
	inline void set_myTransform_5(Transform_t3275118058 * value)
	{
		___myTransform_5 = value;
		Il2CppCodeGenWriteBarrier(&___myTransform_5, value);
	}

	inline static int32_t get_offset_of_textMesh_6() { return static_cast<int32_t>(offsetof(Text3D_t62691636, ___textMesh_6)); }
	inline TextMesh_t1641806576 * get_textMesh_6() const { return ___textMesh_6; }
	inline TextMesh_t1641806576 ** get_address_of_textMesh_6() { return &___textMesh_6; }
	inline void set_textMesh_6(TextMesh_t1641806576 * value)
	{
		___textMesh_6 = value;
		Il2CppCodeGenWriteBarrier(&___textMesh_6, value);
	}

	inline static int32_t get_offset_of_DebugBounds_7() { return static_cast<int32_t>(offsetof(Text3D_t62691636, ___DebugBounds_7)); }
	inline bool get_DebugBounds_7() const { return ___DebugBounds_7; }
	inline bool* get_address_of_DebugBounds_7() { return &___DebugBounds_7; }
	inline void set_DebugBounds_7(bool value)
	{
		___DebugBounds_7 = value;
	}

	inline static int32_t get_offset_of_anchor_8() { return static_cast<int32_t>(offsetof(Text3D_t62691636, ___anchor_8)); }
	inline int32_t get_anchor_8() const { return ___anchor_8; }
	inline int32_t* get_address_of_anchor_8() { return &___anchor_8; }
	inline void set_anchor_8(int32_t value)
	{
		___anchor_8 = value;
	}

	inline static int32_t get_offset_of_BHFNEOEPKGC_9() { return static_cast<int32_t>(offsetof(Text3D_t62691636, ___BHFNEOEPKGC_9)); }
	inline String_t* get_BHFNEOEPKGC_9() const { return ___BHFNEOEPKGC_9; }
	inline String_t** get_address_of_BHFNEOEPKGC_9() { return &___BHFNEOEPKGC_9; }
	inline void set_BHFNEOEPKGC_9(String_t* value)
	{
		___BHFNEOEPKGC_9 = value;
		Il2CppCodeGenWriteBarrier(&___BHFNEOEPKGC_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
