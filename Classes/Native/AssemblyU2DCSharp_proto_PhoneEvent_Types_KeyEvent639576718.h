﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Gene3695499659.h"

// proto.PhoneEvent/Types/KeyEvent
struct KeyEvent_t639576718;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/KeyEvent
struct  KeyEvent_t639576718  : public GeneratedMessageLite_2_t3695499659
{
public:
	// System.Boolean proto.PhoneEvent/Types/KeyEvent::DECNNNLBPPF
	bool ___DECNNNLBPPF_4;
	// System.Int32 proto.PhoneEvent/Types/KeyEvent::LADFMKMCMMH
	int32_t ___LADFMKMCMMH_5;
	// System.Boolean proto.PhoneEvent/Types/KeyEvent::HODDPFPKFII
	bool ___HODDPFPKFII_7;
	// System.Int32 proto.PhoneEvent/Types/KeyEvent::FBHPFKCGLEB
	int32_t ___FBHPFKCGLEB_8;
	// System.Int32 proto.PhoneEvent/Types/KeyEvent::BABCKJCECDJ
	int32_t ___BABCKJCECDJ_9;

public:
	inline static int32_t get_offset_of_DECNNNLBPPF_4() { return static_cast<int32_t>(offsetof(KeyEvent_t639576718, ___DECNNNLBPPF_4)); }
	inline bool get_DECNNNLBPPF_4() const { return ___DECNNNLBPPF_4; }
	inline bool* get_address_of_DECNNNLBPPF_4() { return &___DECNNNLBPPF_4; }
	inline void set_DECNNNLBPPF_4(bool value)
	{
		___DECNNNLBPPF_4 = value;
	}

	inline static int32_t get_offset_of_LADFMKMCMMH_5() { return static_cast<int32_t>(offsetof(KeyEvent_t639576718, ___LADFMKMCMMH_5)); }
	inline int32_t get_LADFMKMCMMH_5() const { return ___LADFMKMCMMH_5; }
	inline int32_t* get_address_of_LADFMKMCMMH_5() { return &___LADFMKMCMMH_5; }
	inline void set_LADFMKMCMMH_5(int32_t value)
	{
		___LADFMKMCMMH_5 = value;
	}

	inline static int32_t get_offset_of_HODDPFPKFII_7() { return static_cast<int32_t>(offsetof(KeyEvent_t639576718, ___HODDPFPKFII_7)); }
	inline bool get_HODDPFPKFII_7() const { return ___HODDPFPKFII_7; }
	inline bool* get_address_of_HODDPFPKFII_7() { return &___HODDPFPKFII_7; }
	inline void set_HODDPFPKFII_7(bool value)
	{
		___HODDPFPKFII_7 = value;
	}

	inline static int32_t get_offset_of_FBHPFKCGLEB_8() { return static_cast<int32_t>(offsetof(KeyEvent_t639576718, ___FBHPFKCGLEB_8)); }
	inline int32_t get_FBHPFKCGLEB_8() const { return ___FBHPFKCGLEB_8; }
	inline int32_t* get_address_of_FBHPFKCGLEB_8() { return &___FBHPFKCGLEB_8; }
	inline void set_FBHPFKCGLEB_8(int32_t value)
	{
		___FBHPFKCGLEB_8 = value;
	}

	inline static int32_t get_offset_of_BABCKJCECDJ_9() { return static_cast<int32_t>(offsetof(KeyEvent_t639576718, ___BABCKJCECDJ_9)); }
	inline int32_t get_BABCKJCECDJ_9() const { return ___BABCKJCECDJ_9; }
	inline int32_t* get_address_of_BABCKJCECDJ_9() { return &___BABCKJCECDJ_9; }
	inline void set_BABCKJCECDJ_9(int32_t value)
	{
		___BABCKJCECDJ_9 = value;
	}
};

struct KeyEvent_t639576718_StaticFields
{
public:
	// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::CACHHOCBHAE
	KeyEvent_t639576718 * ___CACHHOCBHAE_0;
	// System.String[] proto.PhoneEvent/Types/KeyEvent::ONFPAEDCDAG
	StringU5BU5D_t1642385972* ___ONFPAEDCDAG_1;
	// System.UInt32[] proto.PhoneEvent/Types/KeyEvent::KPNOHGJNJOA
	UInt32U5BU5D_t59386216* ___KPNOHGJNJOA_2;

public:
	inline static int32_t get_offset_of_CACHHOCBHAE_0() { return static_cast<int32_t>(offsetof(KeyEvent_t639576718_StaticFields, ___CACHHOCBHAE_0)); }
	inline KeyEvent_t639576718 * get_CACHHOCBHAE_0() const { return ___CACHHOCBHAE_0; }
	inline KeyEvent_t639576718 ** get_address_of_CACHHOCBHAE_0() { return &___CACHHOCBHAE_0; }
	inline void set_CACHHOCBHAE_0(KeyEvent_t639576718 * value)
	{
		___CACHHOCBHAE_0 = value;
		Il2CppCodeGenWriteBarrier(&___CACHHOCBHAE_0, value);
	}

	inline static int32_t get_offset_of_ONFPAEDCDAG_1() { return static_cast<int32_t>(offsetof(KeyEvent_t639576718_StaticFields, ___ONFPAEDCDAG_1)); }
	inline StringU5BU5D_t1642385972* get_ONFPAEDCDAG_1() const { return ___ONFPAEDCDAG_1; }
	inline StringU5BU5D_t1642385972** get_address_of_ONFPAEDCDAG_1() { return &___ONFPAEDCDAG_1; }
	inline void set_ONFPAEDCDAG_1(StringU5BU5D_t1642385972* value)
	{
		___ONFPAEDCDAG_1 = value;
		Il2CppCodeGenWriteBarrier(&___ONFPAEDCDAG_1, value);
	}

	inline static int32_t get_offset_of_KPNOHGJNJOA_2() { return static_cast<int32_t>(offsetof(KeyEvent_t639576718_StaticFields, ___KPNOHGJNJOA_2)); }
	inline UInt32U5BU5D_t59386216* get_KPNOHGJNJOA_2() const { return ___KPNOHGJNJOA_2; }
	inline UInt32U5BU5D_t59386216** get_address_of_KPNOHGJNJOA_2() { return &___KPNOHGJNJOA_2; }
	inline void set_KPNOHGJNJOA_2(UInt32U5BU5D_t59386216* value)
	{
		___KPNOHGJNJOA_2 = value;
		Il2CppCodeGenWriteBarrier(&___KPNOHGJNJOA_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
