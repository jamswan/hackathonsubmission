﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Collections.Generic.List`1<AugmentationEffect>
struct List_1_t2065651177;
// System.Collections.Generic.List`1<Tether>
struct List_1_t3551859736;
// UnityEngine.Transform
struct Transform_t3275118058;
// Tether
struct Tether_t4182738604;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Augmentation
struct  Augmentation_t530534012  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<AugmentationEffect> Augmentation::augmentationEffects
	List_1_t2065651177 * ___augmentationEffects_2;
	// System.Collections.Generic.List`1<Tether> Augmentation::tethers
	List_1_t3551859736 * ___tethers_3;
	// System.Boolean Augmentation::newTargetPriority
	bool ___newTargetPriority_4;
	// UnityEngine.Transform Augmentation::KJLBEHOKPME
	Transform_t3275118058 * ___KJLBEHOKPME_5;
	// UnityEngine.Vector3 Augmentation::IIDJHDBHGHG
	Vector3_t2243707580  ___IIDJHDBHGHG_6;
	// UnityEngine.Vector3 Augmentation::GJOLLCHAIMA
	Vector3_t2243707580  ___GJOLLCHAIMA_7;
	// System.Single Augmentation::positionTime
	float ___positionTime_8;
	// System.Single Augmentation::rotationTime
	float ___rotationTime_9;
	// Tether Augmentation::BCCAFCJDLIG
	Tether_t4182738604 * ___BCCAFCJDLIG_10;

public:
	inline static int32_t get_offset_of_augmentationEffects_2() { return static_cast<int32_t>(offsetof(Augmentation_t530534012, ___augmentationEffects_2)); }
	inline List_1_t2065651177 * get_augmentationEffects_2() const { return ___augmentationEffects_2; }
	inline List_1_t2065651177 ** get_address_of_augmentationEffects_2() { return &___augmentationEffects_2; }
	inline void set_augmentationEffects_2(List_1_t2065651177 * value)
	{
		___augmentationEffects_2 = value;
		Il2CppCodeGenWriteBarrier(&___augmentationEffects_2, value);
	}

	inline static int32_t get_offset_of_tethers_3() { return static_cast<int32_t>(offsetof(Augmentation_t530534012, ___tethers_3)); }
	inline List_1_t3551859736 * get_tethers_3() const { return ___tethers_3; }
	inline List_1_t3551859736 ** get_address_of_tethers_3() { return &___tethers_3; }
	inline void set_tethers_3(List_1_t3551859736 * value)
	{
		___tethers_3 = value;
		Il2CppCodeGenWriteBarrier(&___tethers_3, value);
	}

	inline static int32_t get_offset_of_newTargetPriority_4() { return static_cast<int32_t>(offsetof(Augmentation_t530534012, ___newTargetPriority_4)); }
	inline bool get_newTargetPriority_4() const { return ___newTargetPriority_4; }
	inline bool* get_address_of_newTargetPriority_4() { return &___newTargetPriority_4; }
	inline void set_newTargetPriority_4(bool value)
	{
		___newTargetPriority_4 = value;
	}

	inline static int32_t get_offset_of_KJLBEHOKPME_5() { return static_cast<int32_t>(offsetof(Augmentation_t530534012, ___KJLBEHOKPME_5)); }
	inline Transform_t3275118058 * get_KJLBEHOKPME_5() const { return ___KJLBEHOKPME_5; }
	inline Transform_t3275118058 ** get_address_of_KJLBEHOKPME_5() { return &___KJLBEHOKPME_5; }
	inline void set_KJLBEHOKPME_5(Transform_t3275118058 * value)
	{
		___KJLBEHOKPME_5 = value;
		Il2CppCodeGenWriteBarrier(&___KJLBEHOKPME_5, value);
	}

	inline static int32_t get_offset_of_IIDJHDBHGHG_6() { return static_cast<int32_t>(offsetof(Augmentation_t530534012, ___IIDJHDBHGHG_6)); }
	inline Vector3_t2243707580  get_IIDJHDBHGHG_6() const { return ___IIDJHDBHGHG_6; }
	inline Vector3_t2243707580 * get_address_of_IIDJHDBHGHG_6() { return &___IIDJHDBHGHG_6; }
	inline void set_IIDJHDBHGHG_6(Vector3_t2243707580  value)
	{
		___IIDJHDBHGHG_6 = value;
	}

	inline static int32_t get_offset_of_GJOLLCHAIMA_7() { return static_cast<int32_t>(offsetof(Augmentation_t530534012, ___GJOLLCHAIMA_7)); }
	inline Vector3_t2243707580  get_GJOLLCHAIMA_7() const { return ___GJOLLCHAIMA_7; }
	inline Vector3_t2243707580 * get_address_of_GJOLLCHAIMA_7() { return &___GJOLLCHAIMA_7; }
	inline void set_GJOLLCHAIMA_7(Vector3_t2243707580  value)
	{
		___GJOLLCHAIMA_7 = value;
	}

	inline static int32_t get_offset_of_positionTime_8() { return static_cast<int32_t>(offsetof(Augmentation_t530534012, ___positionTime_8)); }
	inline float get_positionTime_8() const { return ___positionTime_8; }
	inline float* get_address_of_positionTime_8() { return &___positionTime_8; }
	inline void set_positionTime_8(float value)
	{
		___positionTime_8 = value;
	}

	inline static int32_t get_offset_of_rotationTime_9() { return static_cast<int32_t>(offsetof(Augmentation_t530534012, ___rotationTime_9)); }
	inline float get_rotationTime_9() const { return ___rotationTime_9; }
	inline float* get_address_of_rotationTime_9() { return &___rotationTime_9; }
	inline void set_rotationTime_9(float value)
	{
		___rotationTime_9 = value;
	}

	inline static int32_t get_offset_of_BCCAFCJDLIG_10() { return static_cast<int32_t>(offsetof(Augmentation_t530534012, ___BCCAFCJDLIG_10)); }
	inline Tether_t4182738604 * get_BCCAFCJDLIG_10() const { return ___BCCAFCJDLIG_10; }
	inline Tether_t4182738604 ** get_address_of_BCCAFCJDLIG_10() { return &___BCCAFCJDLIG_10; }
	inline void set_BCCAFCJDLIG_10(Tether_t4182738604 * value)
	{
		___BCCAFCJDLIG_10 = value;
		Il2CppCodeGenWriteBarrier(&___BCCAFCJDLIG_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
