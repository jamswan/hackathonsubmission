﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"

// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextRecognized
struct  TextRecognized_t2802831619  : public NarrativeCondition_t4123859457
{
public:
	// System.String[] TextRecognized::words
	StringU5BU5D_t1642385972* ___words_13;

public:
	inline static int32_t get_offset_of_words_13() { return static_cast<int32_t>(offsetof(TextRecognized_t2802831619, ___words_13)); }
	inline StringU5BU5D_t1642385972* get_words_13() const { return ___words_13; }
	inline StringU5BU5D_t1642385972** get_address_of_words_13() { return &___words_13; }
	inline void set_words_13(StringU5BU5D_t1642385972* value)
	{
		___words_13 = value;
		Il2CppCodeGenWriteBarrier(&___words_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
