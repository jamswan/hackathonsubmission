﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"

// UnityEngine.Sprite
struct Sprite_t309593783;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OverlayWait
struct  OverlayWait_t4280792591  : public NarrativeCondition_t4123859457
{
public:
	// UnityEngine.Sprite OverlayWait::sprite
	Sprite_t309593783 * ___sprite_13;
	// System.Boolean OverlayWait::clearMenu
	bool ___clearMenu_14;
	// System.Single OverlayWait::duration
	float ___duration_15;

public:
	inline static int32_t get_offset_of_sprite_13() { return static_cast<int32_t>(offsetof(OverlayWait_t4280792591, ___sprite_13)); }
	inline Sprite_t309593783 * get_sprite_13() const { return ___sprite_13; }
	inline Sprite_t309593783 ** get_address_of_sprite_13() { return &___sprite_13; }
	inline void set_sprite_13(Sprite_t309593783 * value)
	{
		___sprite_13 = value;
		Il2CppCodeGenWriteBarrier(&___sprite_13, value);
	}

	inline static int32_t get_offset_of_clearMenu_14() { return static_cast<int32_t>(offsetof(OverlayWait_t4280792591, ___clearMenu_14)); }
	inline bool get_clearMenu_14() const { return ___clearMenu_14; }
	inline bool* get_address_of_clearMenu_14() { return &___clearMenu_14; }
	inline void set_clearMenu_14(bool value)
	{
		___clearMenu_14 = value;
	}

	inline static int32_t get_offset_of_duration_15() { return static_cast<int32_t>(offsetof(OverlayWait_t4280792591, ___duration_15)); }
	inline float get_duration_15() const { return ___duration_15; }
	inline float* get_address_of_duration_15() { return &___duration_15; }
	inline void set_duration_15(float value)
	{
		___duration_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
