﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NarrativeEffect
struct  NarrativeEffect_t3336735121  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean NarrativeEffect::disableEffect
	bool ___disableEffect_2;

public:
	inline static int32_t get_offset_of_disableEffect_2() { return static_cast<int32_t>(offsetof(NarrativeEffect_t3336735121, ___disableEffect_2)); }
	inline bool get_disableEffect_2() const { return ___disableEffect_2; }
	inline bool* get_address_of_disableEffect_2() { return &___disableEffect_2; }
	inline void set_disableEffect_2(bool value)
	{
		___disableEffect_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
