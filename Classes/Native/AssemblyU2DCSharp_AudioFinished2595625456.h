﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeCondition4123859457.h"

// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioFinished
struct  AudioFinished_t2595625456  : public NarrativeCondition_t4123859457
{
public:
	// UnityEngine.AudioClip AudioFinished::audioClip
	AudioClip_t1932558630 * ___audioClip_13;
	// UnityEngine.AudioSource AudioFinished::audioSource
	AudioSource_t1135106623 * ___audioSource_14;
	// System.Boolean AudioFinished::PPIONFLEHAL
	bool ___PPIONFLEHAL_15;

public:
	inline static int32_t get_offset_of_audioClip_13() { return static_cast<int32_t>(offsetof(AudioFinished_t2595625456, ___audioClip_13)); }
	inline AudioClip_t1932558630 * get_audioClip_13() const { return ___audioClip_13; }
	inline AudioClip_t1932558630 ** get_address_of_audioClip_13() { return &___audioClip_13; }
	inline void set_audioClip_13(AudioClip_t1932558630 * value)
	{
		___audioClip_13 = value;
		Il2CppCodeGenWriteBarrier(&___audioClip_13, value);
	}

	inline static int32_t get_offset_of_audioSource_14() { return static_cast<int32_t>(offsetof(AudioFinished_t2595625456, ___audioSource_14)); }
	inline AudioSource_t1135106623 * get_audioSource_14() const { return ___audioSource_14; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_14() { return &___audioSource_14; }
	inline void set_audioSource_14(AudioSource_t1135106623 * value)
	{
		___audioSource_14 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_14, value);
	}

	inline static int32_t get_offset_of_PPIONFLEHAL_15() { return static_cast<int32_t>(offsetof(AudioFinished_t2595625456, ___PPIONFLEHAL_15)); }
	inline bool get_PPIONFLEHAL_15() const { return ___PPIONFLEHAL_15; }
	inline bool* get_address_of_PPIONFLEHAL_15() { return &___PPIONFLEHAL_15; }
	inline void set_PPIONFLEHAL_15(bool value)
	{
		___PPIONFLEHAL_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
