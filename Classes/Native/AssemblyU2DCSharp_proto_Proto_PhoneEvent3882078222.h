﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.Proto.PhoneEvent
struct  PhoneEvent_t3882078222  : public Il2CppObject
{
public:

public:
};

struct PhoneEvent_t3882078222_StaticFields
{
public:
	// System.Object proto.Proto.PhoneEvent::HNBNJADPKML
	Il2CppObject * ___HNBNJADPKML_0;

public:
	inline static int32_t get_offset_of_HNBNJADPKML_0() { return static_cast<int32_t>(offsetof(PhoneEvent_t3882078222_StaticFields, ___HNBNJADPKML_0)); }
	inline Il2CppObject * get_HNBNJADPKML_0() const { return ___HNBNJADPKML_0; }
	inline Il2CppObject ** get_address_of_HNBNJADPKML_0() { return &___HNBNJADPKML_0; }
	inline void set_HNBNJADPKML_0(Il2CppObject * value)
	{
		___HNBNJADPKML_0 = value;
		Il2CppCodeGenWriteBarrier(&___HNBNJADPKML_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
