﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuaternionTest
struct  QuaternionTest_t88039808  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject QuaternionTest::target
	GameObject_t1756533147 * ___target_2;
	// UnityEngine.GameObject QuaternionTest::camera
	GameObject_t1756533147 * ___camera_3;
	// UnityEngine.GameObject QuaternionTest::anchor
	GameObject_t1756533147 * ___anchor_4;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(QuaternionTest_t88039808, ___target_2)); }
	inline GameObject_t1756533147 * get_target_2() const { return ___target_2; }
	inline GameObject_t1756533147 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(GameObject_t1756533147 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier(&___target_2, value);
	}

	inline static int32_t get_offset_of_camera_3() { return static_cast<int32_t>(offsetof(QuaternionTest_t88039808, ___camera_3)); }
	inline GameObject_t1756533147 * get_camera_3() const { return ___camera_3; }
	inline GameObject_t1756533147 ** get_address_of_camera_3() { return &___camera_3; }
	inline void set_camera_3(GameObject_t1756533147 * value)
	{
		___camera_3 = value;
		Il2CppCodeGenWriteBarrier(&___camera_3, value);
	}

	inline static int32_t get_offset_of_anchor_4() { return static_cast<int32_t>(offsetof(QuaternionTest_t88039808, ___anchor_4)); }
	inline GameObject_t1756533147 * get_anchor_4() const { return ___anchor_4; }
	inline GameObject_t1756533147 ** get_address_of_anchor_4() { return &___anchor_4; }
	inline void set_anchor_4(GameObject_t1756533147 * value)
	{
		___anchor_4 = value;
		Il2CppCodeGenWriteBarrier(&___anchor_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
