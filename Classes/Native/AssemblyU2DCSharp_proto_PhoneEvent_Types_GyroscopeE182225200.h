﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Google_ProtocolBuffers_Google_ProtocolBuffers_Gener164448311.h"

// proto.PhoneEvent/Types/GyroscopeEvent
struct GyroscopeEvent_t182225200;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/GyroscopeEvent
struct  GyroscopeEvent_t182225200  : public GeneratedMessageLite_2_t164448311
{
public:
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::MIGGKHHBAID
	bool ___MIGGKHHBAID_4;
	// System.Int64 proto.PhoneEvent/Types/GyroscopeEvent::HMBIGJIDGNB
	int64_t ___HMBIGJIDGNB_5;
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::DAPIOCPOIID
	bool ___DAPIOCPOIID_7;
	// System.Single proto.PhoneEvent/Types/GyroscopeEvent::KKLEGOBOFFJ
	float ___KKLEGOBOFFJ_8;
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::LGBJOJHEJDM
	bool ___LGBJOJHEJDM_10;
	// System.Single proto.PhoneEvent/Types/GyroscopeEvent::NFCIJFNNNDL
	float ___NFCIJFNNNDL_11;
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::LPLIBMIPMFD
	bool ___LPLIBMIPMFD_13;
	// System.Single proto.PhoneEvent/Types/GyroscopeEvent::PLILMNHLFED
	float ___PLILMNHLFED_14;
	// System.Int32 proto.PhoneEvent/Types/GyroscopeEvent::BABCKJCECDJ
	int32_t ___BABCKJCECDJ_15;

public:
	inline static int32_t get_offset_of_MIGGKHHBAID_4() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t182225200, ___MIGGKHHBAID_4)); }
	inline bool get_MIGGKHHBAID_4() const { return ___MIGGKHHBAID_4; }
	inline bool* get_address_of_MIGGKHHBAID_4() { return &___MIGGKHHBAID_4; }
	inline void set_MIGGKHHBAID_4(bool value)
	{
		___MIGGKHHBAID_4 = value;
	}

	inline static int32_t get_offset_of_HMBIGJIDGNB_5() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t182225200, ___HMBIGJIDGNB_5)); }
	inline int64_t get_HMBIGJIDGNB_5() const { return ___HMBIGJIDGNB_5; }
	inline int64_t* get_address_of_HMBIGJIDGNB_5() { return &___HMBIGJIDGNB_5; }
	inline void set_HMBIGJIDGNB_5(int64_t value)
	{
		___HMBIGJIDGNB_5 = value;
	}

	inline static int32_t get_offset_of_DAPIOCPOIID_7() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t182225200, ___DAPIOCPOIID_7)); }
	inline bool get_DAPIOCPOIID_7() const { return ___DAPIOCPOIID_7; }
	inline bool* get_address_of_DAPIOCPOIID_7() { return &___DAPIOCPOIID_7; }
	inline void set_DAPIOCPOIID_7(bool value)
	{
		___DAPIOCPOIID_7 = value;
	}

	inline static int32_t get_offset_of_KKLEGOBOFFJ_8() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t182225200, ___KKLEGOBOFFJ_8)); }
	inline float get_KKLEGOBOFFJ_8() const { return ___KKLEGOBOFFJ_8; }
	inline float* get_address_of_KKLEGOBOFFJ_8() { return &___KKLEGOBOFFJ_8; }
	inline void set_KKLEGOBOFFJ_8(float value)
	{
		___KKLEGOBOFFJ_8 = value;
	}

	inline static int32_t get_offset_of_LGBJOJHEJDM_10() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t182225200, ___LGBJOJHEJDM_10)); }
	inline bool get_LGBJOJHEJDM_10() const { return ___LGBJOJHEJDM_10; }
	inline bool* get_address_of_LGBJOJHEJDM_10() { return &___LGBJOJHEJDM_10; }
	inline void set_LGBJOJHEJDM_10(bool value)
	{
		___LGBJOJHEJDM_10 = value;
	}

	inline static int32_t get_offset_of_NFCIJFNNNDL_11() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t182225200, ___NFCIJFNNNDL_11)); }
	inline float get_NFCIJFNNNDL_11() const { return ___NFCIJFNNNDL_11; }
	inline float* get_address_of_NFCIJFNNNDL_11() { return &___NFCIJFNNNDL_11; }
	inline void set_NFCIJFNNNDL_11(float value)
	{
		___NFCIJFNNNDL_11 = value;
	}

	inline static int32_t get_offset_of_LPLIBMIPMFD_13() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t182225200, ___LPLIBMIPMFD_13)); }
	inline bool get_LPLIBMIPMFD_13() const { return ___LPLIBMIPMFD_13; }
	inline bool* get_address_of_LPLIBMIPMFD_13() { return &___LPLIBMIPMFD_13; }
	inline void set_LPLIBMIPMFD_13(bool value)
	{
		___LPLIBMIPMFD_13 = value;
	}

	inline static int32_t get_offset_of_PLILMNHLFED_14() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t182225200, ___PLILMNHLFED_14)); }
	inline float get_PLILMNHLFED_14() const { return ___PLILMNHLFED_14; }
	inline float* get_address_of_PLILMNHLFED_14() { return &___PLILMNHLFED_14; }
	inline void set_PLILMNHLFED_14(float value)
	{
		___PLILMNHLFED_14 = value;
	}

	inline static int32_t get_offset_of_BABCKJCECDJ_15() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t182225200, ___BABCKJCECDJ_15)); }
	inline int32_t get_BABCKJCECDJ_15() const { return ___BABCKJCECDJ_15; }
	inline int32_t* get_address_of_BABCKJCECDJ_15() { return &___BABCKJCECDJ_15; }
	inline void set_BABCKJCECDJ_15(int32_t value)
	{
		___BABCKJCECDJ_15 = value;
	}
};

struct GyroscopeEvent_t182225200_StaticFields
{
public:
	// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::CACHHOCBHAE
	GyroscopeEvent_t182225200 * ___CACHHOCBHAE_0;
	// System.String[] proto.PhoneEvent/Types/GyroscopeEvent::NEKHOFLMDLE
	StringU5BU5D_t1642385972* ___NEKHOFLMDLE_1;
	// System.UInt32[] proto.PhoneEvent/Types/GyroscopeEvent::CIJJOMILCMJ
	UInt32U5BU5D_t59386216* ___CIJJOMILCMJ_2;

public:
	inline static int32_t get_offset_of_CACHHOCBHAE_0() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t182225200_StaticFields, ___CACHHOCBHAE_0)); }
	inline GyroscopeEvent_t182225200 * get_CACHHOCBHAE_0() const { return ___CACHHOCBHAE_0; }
	inline GyroscopeEvent_t182225200 ** get_address_of_CACHHOCBHAE_0() { return &___CACHHOCBHAE_0; }
	inline void set_CACHHOCBHAE_0(GyroscopeEvent_t182225200 * value)
	{
		___CACHHOCBHAE_0 = value;
		Il2CppCodeGenWriteBarrier(&___CACHHOCBHAE_0, value);
	}

	inline static int32_t get_offset_of_NEKHOFLMDLE_1() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t182225200_StaticFields, ___NEKHOFLMDLE_1)); }
	inline StringU5BU5D_t1642385972* get_NEKHOFLMDLE_1() const { return ___NEKHOFLMDLE_1; }
	inline StringU5BU5D_t1642385972** get_address_of_NEKHOFLMDLE_1() { return &___NEKHOFLMDLE_1; }
	inline void set_NEKHOFLMDLE_1(StringU5BU5D_t1642385972* value)
	{
		___NEKHOFLMDLE_1 = value;
		Il2CppCodeGenWriteBarrier(&___NEKHOFLMDLE_1, value);
	}

	inline static int32_t get_offset_of_CIJJOMILCMJ_2() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t182225200_StaticFields, ___CIJJOMILCMJ_2)); }
	inline UInt32U5BU5D_t59386216* get_CIJJOMILCMJ_2() const { return ___CIJJOMILCMJ_2; }
	inline UInt32U5BU5D_t59386216** get_address_of_CIJJOMILCMJ_2() { return &___CIJJOMILCMJ_2; }
	inline void set_CIJJOMILCMJ_2(UInt32U5BU5D_t59386216* value)
	{
		___CIJJOMILCMJ_2 = value;
		Il2CppCodeGenWriteBarrier(&___CIJJOMILCMJ_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
