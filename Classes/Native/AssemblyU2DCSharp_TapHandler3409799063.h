﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// MenuAnimator
struct MenuAnimator_t2049002970;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TapHandler
struct  TapHandler_t3409799063  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TapHandler::GGEJIEBOLHP
	float ___GGEJIEBOLHP_3;
	// MenuAnimator TapHandler::KHNIGIIJIII
	MenuAnimator_t2049002970 * ___KHNIGIIJIII_4;
	// System.Int32 TapHandler::IEKGIFCHAJG
	int32_t ___IEKGIFCHAJG_5;

public:
	inline static int32_t get_offset_of_GGEJIEBOLHP_3() { return static_cast<int32_t>(offsetof(TapHandler_t3409799063, ___GGEJIEBOLHP_3)); }
	inline float get_GGEJIEBOLHP_3() const { return ___GGEJIEBOLHP_3; }
	inline float* get_address_of_GGEJIEBOLHP_3() { return &___GGEJIEBOLHP_3; }
	inline void set_GGEJIEBOLHP_3(float value)
	{
		___GGEJIEBOLHP_3 = value;
	}

	inline static int32_t get_offset_of_KHNIGIIJIII_4() { return static_cast<int32_t>(offsetof(TapHandler_t3409799063, ___KHNIGIIJIII_4)); }
	inline MenuAnimator_t2049002970 * get_KHNIGIIJIII_4() const { return ___KHNIGIIJIII_4; }
	inline MenuAnimator_t2049002970 ** get_address_of_KHNIGIIJIII_4() { return &___KHNIGIIJIII_4; }
	inline void set_KHNIGIIJIII_4(MenuAnimator_t2049002970 * value)
	{
		___KHNIGIIJIII_4 = value;
		Il2CppCodeGenWriteBarrier(&___KHNIGIIJIII_4, value);
	}

	inline static int32_t get_offset_of_IEKGIFCHAJG_5() { return static_cast<int32_t>(offsetof(TapHandler_t3409799063, ___IEKGIFCHAJG_5)); }
	inline int32_t get_IEKGIFCHAJG_5() const { return ___IEKGIFCHAJG_5; }
	inline int32_t* get_address_of_IEKGIFCHAJG_5() { return &___IEKGIFCHAJG_5; }
	inline void set_IEKGIFCHAJG_5(int32_t value)
	{
		___IEKGIFCHAJG_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
