﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisableOnScene
struct  DisableOnScene_t3768098929  : public MonoBehaviour_t1158329972
{
public:
	// System.String DisableOnScene::SceneName
	String_t* ___SceneName_2;
	// UnityEngine.GameObject DisableOnScene::gO
	GameObject_t1756533147 * ___gO_3;

public:
	inline static int32_t get_offset_of_SceneName_2() { return static_cast<int32_t>(offsetof(DisableOnScene_t3768098929, ___SceneName_2)); }
	inline String_t* get_SceneName_2() const { return ___SceneName_2; }
	inline String_t** get_address_of_SceneName_2() { return &___SceneName_2; }
	inline void set_SceneName_2(String_t* value)
	{
		___SceneName_2 = value;
		Il2CppCodeGenWriteBarrier(&___SceneName_2, value);
	}

	inline static int32_t get_offset_of_gO_3() { return static_cast<int32_t>(offsetof(DisableOnScene_t3768098929, ___gO_3)); }
	inline GameObject_t1756533147 * get_gO_3() const { return ___gO_3; }
	inline GameObject_t1756533147 ** get_address_of_gO_3() { return &___gO_3; }
	inline void set_gO_3(GameObject_t1756533147 * value)
	{
		___gO_3 = value;
		Il2CppCodeGenWriteBarrier(&___gO_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
