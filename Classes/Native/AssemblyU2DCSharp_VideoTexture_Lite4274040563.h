﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_VideoTexture_Lite_KPCHGDNDKNM41912157.h"

// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2243626319;
// IJDGMEKNLGL
struct IJDGMEKNLGL_t3456803436;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoTexture_Lite
struct  VideoTexture_Lite_t4274040563  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VideoTexture_Lite::FPS
	float ___FPS_2;
	// System.Int32 VideoTexture_Lite::firstFrame
	int32_t ___firstFrame_3;
	// System.Int32 VideoTexture_Lite::lastFrame
	int32_t ___lastFrame_4;
	// System.String VideoTexture_Lite::FileName
	String_t* ___FileName_5;
	// System.String VideoTexture_Lite::digitsFormat
	String_t* ___digitsFormat_6;
	// VideoTexture_Lite/KPCHGDNDKNM VideoTexture_Lite::DigitsLocation
	int32_t ___DigitsLocation_7;
	// System.Single VideoTexture_Lite::aspectRatio
	float ___aspectRatio_8;
	// System.Boolean VideoTexture_Lite::enableAudio
	bool ___enableAudio_9;
	// System.Boolean VideoTexture_Lite::enableReplay
	bool ___enableReplay_10;
	// System.Boolean VideoTexture_Lite::showInstructions
	bool ___showInstructions_11;
	// System.Boolean VideoTexture_Lite::GAIHEKPNGKG
	bool ___GAIHEKPNGKG_12;
	// System.Boolean VideoTexture_Lite::BHPBIIBLNFK
	bool ___BHPBIIBLNFK_13;
	// System.String VideoTexture_Lite::ADIACDNOBJA
	String_t* ___ADIACDNOBJA_14;
	// UnityEngine.Texture VideoTexture_Lite::CFCNKEMCBAL
	Texture_t2243626319 * ___CFCNKEMCBAL_15;
	// UnityEngine.Texture VideoTexture_Lite::BMLPPCDBECD
	Texture_t2243626319 * ___BMLPPCDBECD_16;
	// System.Single VideoTexture_Lite::KIDKLLOFBAB
	float ___KIDKLLOFBAB_17;
	// System.Int32 VideoTexture_Lite::KDIEKAKPFPN
	int32_t ___KDIEKAKPFPN_18;
	// System.Int32 VideoTexture_Lite::GBBACOJGDJG
	int32_t ___GBBACOJGDJG_19;
	// IJDGMEKNLGL VideoTexture_Lite::LBGLAPDFIAK
	IJDGMEKNLGL_t3456803436 * ___LBGLAPDFIAK_20;

public:
	inline static int32_t get_offset_of_FPS_2() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___FPS_2)); }
	inline float get_FPS_2() const { return ___FPS_2; }
	inline float* get_address_of_FPS_2() { return &___FPS_2; }
	inline void set_FPS_2(float value)
	{
		___FPS_2 = value;
	}

	inline static int32_t get_offset_of_firstFrame_3() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___firstFrame_3)); }
	inline int32_t get_firstFrame_3() const { return ___firstFrame_3; }
	inline int32_t* get_address_of_firstFrame_3() { return &___firstFrame_3; }
	inline void set_firstFrame_3(int32_t value)
	{
		___firstFrame_3 = value;
	}

	inline static int32_t get_offset_of_lastFrame_4() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___lastFrame_4)); }
	inline int32_t get_lastFrame_4() const { return ___lastFrame_4; }
	inline int32_t* get_address_of_lastFrame_4() { return &___lastFrame_4; }
	inline void set_lastFrame_4(int32_t value)
	{
		___lastFrame_4 = value;
	}

	inline static int32_t get_offset_of_FileName_5() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___FileName_5)); }
	inline String_t* get_FileName_5() const { return ___FileName_5; }
	inline String_t** get_address_of_FileName_5() { return &___FileName_5; }
	inline void set_FileName_5(String_t* value)
	{
		___FileName_5 = value;
		Il2CppCodeGenWriteBarrier(&___FileName_5, value);
	}

	inline static int32_t get_offset_of_digitsFormat_6() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___digitsFormat_6)); }
	inline String_t* get_digitsFormat_6() const { return ___digitsFormat_6; }
	inline String_t** get_address_of_digitsFormat_6() { return &___digitsFormat_6; }
	inline void set_digitsFormat_6(String_t* value)
	{
		___digitsFormat_6 = value;
		Il2CppCodeGenWriteBarrier(&___digitsFormat_6, value);
	}

	inline static int32_t get_offset_of_DigitsLocation_7() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___DigitsLocation_7)); }
	inline int32_t get_DigitsLocation_7() const { return ___DigitsLocation_7; }
	inline int32_t* get_address_of_DigitsLocation_7() { return &___DigitsLocation_7; }
	inline void set_DigitsLocation_7(int32_t value)
	{
		___DigitsLocation_7 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_8() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___aspectRatio_8)); }
	inline float get_aspectRatio_8() const { return ___aspectRatio_8; }
	inline float* get_address_of_aspectRatio_8() { return &___aspectRatio_8; }
	inline void set_aspectRatio_8(float value)
	{
		___aspectRatio_8 = value;
	}

	inline static int32_t get_offset_of_enableAudio_9() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___enableAudio_9)); }
	inline bool get_enableAudio_9() const { return ___enableAudio_9; }
	inline bool* get_address_of_enableAudio_9() { return &___enableAudio_9; }
	inline void set_enableAudio_9(bool value)
	{
		___enableAudio_9 = value;
	}

	inline static int32_t get_offset_of_enableReplay_10() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___enableReplay_10)); }
	inline bool get_enableReplay_10() const { return ___enableReplay_10; }
	inline bool* get_address_of_enableReplay_10() { return &___enableReplay_10; }
	inline void set_enableReplay_10(bool value)
	{
		___enableReplay_10 = value;
	}

	inline static int32_t get_offset_of_showInstructions_11() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___showInstructions_11)); }
	inline bool get_showInstructions_11() const { return ___showInstructions_11; }
	inline bool* get_address_of_showInstructions_11() { return &___showInstructions_11; }
	inline void set_showInstructions_11(bool value)
	{
		___showInstructions_11 = value;
	}

	inline static int32_t get_offset_of_GAIHEKPNGKG_12() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___GAIHEKPNGKG_12)); }
	inline bool get_GAIHEKPNGKG_12() const { return ___GAIHEKPNGKG_12; }
	inline bool* get_address_of_GAIHEKPNGKG_12() { return &___GAIHEKPNGKG_12; }
	inline void set_GAIHEKPNGKG_12(bool value)
	{
		___GAIHEKPNGKG_12 = value;
	}

	inline static int32_t get_offset_of_BHPBIIBLNFK_13() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___BHPBIIBLNFK_13)); }
	inline bool get_BHPBIIBLNFK_13() const { return ___BHPBIIBLNFK_13; }
	inline bool* get_address_of_BHPBIIBLNFK_13() { return &___BHPBIIBLNFK_13; }
	inline void set_BHPBIIBLNFK_13(bool value)
	{
		___BHPBIIBLNFK_13 = value;
	}

	inline static int32_t get_offset_of_ADIACDNOBJA_14() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___ADIACDNOBJA_14)); }
	inline String_t* get_ADIACDNOBJA_14() const { return ___ADIACDNOBJA_14; }
	inline String_t** get_address_of_ADIACDNOBJA_14() { return &___ADIACDNOBJA_14; }
	inline void set_ADIACDNOBJA_14(String_t* value)
	{
		___ADIACDNOBJA_14 = value;
		Il2CppCodeGenWriteBarrier(&___ADIACDNOBJA_14, value);
	}

	inline static int32_t get_offset_of_CFCNKEMCBAL_15() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___CFCNKEMCBAL_15)); }
	inline Texture_t2243626319 * get_CFCNKEMCBAL_15() const { return ___CFCNKEMCBAL_15; }
	inline Texture_t2243626319 ** get_address_of_CFCNKEMCBAL_15() { return &___CFCNKEMCBAL_15; }
	inline void set_CFCNKEMCBAL_15(Texture_t2243626319 * value)
	{
		___CFCNKEMCBAL_15 = value;
		Il2CppCodeGenWriteBarrier(&___CFCNKEMCBAL_15, value);
	}

	inline static int32_t get_offset_of_BMLPPCDBECD_16() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___BMLPPCDBECD_16)); }
	inline Texture_t2243626319 * get_BMLPPCDBECD_16() const { return ___BMLPPCDBECD_16; }
	inline Texture_t2243626319 ** get_address_of_BMLPPCDBECD_16() { return &___BMLPPCDBECD_16; }
	inline void set_BMLPPCDBECD_16(Texture_t2243626319 * value)
	{
		___BMLPPCDBECD_16 = value;
		Il2CppCodeGenWriteBarrier(&___BMLPPCDBECD_16, value);
	}

	inline static int32_t get_offset_of_KIDKLLOFBAB_17() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___KIDKLLOFBAB_17)); }
	inline float get_KIDKLLOFBAB_17() const { return ___KIDKLLOFBAB_17; }
	inline float* get_address_of_KIDKLLOFBAB_17() { return &___KIDKLLOFBAB_17; }
	inline void set_KIDKLLOFBAB_17(float value)
	{
		___KIDKLLOFBAB_17 = value;
	}

	inline static int32_t get_offset_of_KDIEKAKPFPN_18() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___KDIEKAKPFPN_18)); }
	inline int32_t get_KDIEKAKPFPN_18() const { return ___KDIEKAKPFPN_18; }
	inline int32_t* get_address_of_KDIEKAKPFPN_18() { return &___KDIEKAKPFPN_18; }
	inline void set_KDIEKAKPFPN_18(int32_t value)
	{
		___KDIEKAKPFPN_18 = value;
	}

	inline static int32_t get_offset_of_GBBACOJGDJG_19() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___GBBACOJGDJG_19)); }
	inline int32_t get_GBBACOJGDJG_19() const { return ___GBBACOJGDJG_19; }
	inline int32_t* get_address_of_GBBACOJGDJG_19() { return &___GBBACOJGDJG_19; }
	inline void set_GBBACOJGDJG_19(int32_t value)
	{
		___GBBACOJGDJG_19 = value;
	}

	inline static int32_t get_offset_of_LBGLAPDFIAK_20() { return static_cast<int32_t>(offsetof(VideoTexture_Lite_t4274040563, ___LBGLAPDFIAK_20)); }
	inline IJDGMEKNLGL_t3456803436 * get_LBGLAPDFIAK_20() const { return ___LBGLAPDFIAK_20; }
	inline IJDGMEKNLGL_t3456803436 ** get_address_of_LBGLAPDFIAK_20() { return &___LBGLAPDFIAK_20; }
	inline void set_LBGLAPDFIAK_20(IJDGMEKNLGL_t3456803436 * value)
	{
		___LBGLAPDFIAK_20 = value;
		Il2CppCodeGenWriteBarrier(&___LBGLAPDFIAK_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
