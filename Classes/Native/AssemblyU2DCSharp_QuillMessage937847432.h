﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuillMessage
struct  QuillMessage_t937847432  : public NarrativeEffect_t3336735121
{
public:
	// System.Int32 QuillMessage::index
	int32_t ___index_3;
	// System.Boolean QuillMessage::hasBeenSeen
	bool ___hasBeenSeen_4;

public:
	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(QuillMessage_t937847432, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_hasBeenSeen_4() { return static_cast<int32_t>(offsetof(QuillMessage_t937847432, ___hasBeenSeen_4)); }
	inline bool get_hasBeenSeen_4() const { return ___hasBeenSeen_4; }
	inline bool* get_address_of_hasBeenSeen_4() { return &___hasBeenSeen_4; }
	inline void set_hasBeenSeen_4(bool value)
	{
		___hasBeenSeen_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
