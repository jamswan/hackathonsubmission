﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// VideoPlaybackBehaviour
struct VideoPlaybackBehaviour_t222960481;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayVideo/<PlayFullscreenVideoAtEndOfFrame>c__Iterator0
struct  U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819  : public Il2CppObject
{
public:
	// VideoPlaybackBehaviour PlayVideo/<PlayFullscreenVideoAtEndOfFrame>c__Iterator0::PDDLMIFOBCN
	VideoPlaybackBehaviour_t222960481 * ___PDDLMIFOBCN_0;
	// System.Object PlayVideo/<PlayFullscreenVideoAtEndOfFrame>c__Iterator0::LGBFNMECDHC
	Il2CppObject * ___LGBFNMECDHC_1;
	// System.Boolean PlayVideo/<PlayFullscreenVideoAtEndOfFrame>c__Iterator0::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_2;
	// System.Int32 PlayVideo/<PlayFullscreenVideoAtEndOfFrame>c__Iterator0::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_3;

public:
	inline static int32_t get_offset_of_PDDLMIFOBCN_0() { return static_cast<int32_t>(offsetof(U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819, ___PDDLMIFOBCN_0)); }
	inline VideoPlaybackBehaviour_t222960481 * get_PDDLMIFOBCN_0() const { return ___PDDLMIFOBCN_0; }
	inline VideoPlaybackBehaviour_t222960481 ** get_address_of_PDDLMIFOBCN_0() { return &___PDDLMIFOBCN_0; }
	inline void set_PDDLMIFOBCN_0(VideoPlaybackBehaviour_t222960481 * value)
	{
		___PDDLMIFOBCN_0 = value;
		Il2CppCodeGenWriteBarrier(&___PDDLMIFOBCN_0, value);
	}

	inline static int32_t get_offset_of_LGBFNMECDHC_1() { return static_cast<int32_t>(offsetof(U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819, ___LGBFNMECDHC_1)); }
	inline Il2CppObject * get_LGBFNMECDHC_1() const { return ___LGBFNMECDHC_1; }
	inline Il2CppObject ** get_address_of_LGBFNMECDHC_1() { return &___LGBFNMECDHC_1; }
	inline void set_LGBFNMECDHC_1(Il2CppObject * value)
	{
		___LGBFNMECDHC_1 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_1, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_2() { return static_cast<int32_t>(offsetof(U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819, ___IMAGLIMLPFK_2)); }
	inline bool get_IMAGLIMLPFK_2() const { return ___IMAGLIMLPFK_2; }
	inline bool* get_address_of_IMAGLIMLPFK_2() { return &___IMAGLIMLPFK_2; }
	inline void set_IMAGLIMLPFK_2(bool value)
	{
		___IMAGLIMLPFK_2 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_3() { return static_cast<int32_t>(offsetof(U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819, ___EPCFNNGDBGC_3)); }
	inline int32_t get_EPCFNNGDBGC_3() const { return ___EPCFNNGDBGC_3; }
	inline int32_t* get_address_of_EPCFNNGDBGC_3() { return &___EPCFNNGDBGC_3; }
	inline void set_EPCFNNGDBGC_3(int32_t value)
	{
		___EPCFNNGDBGC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
