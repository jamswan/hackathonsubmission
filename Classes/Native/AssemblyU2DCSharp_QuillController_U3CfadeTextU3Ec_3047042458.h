﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Material
struct Material_t193706927;
// System.Action
struct Action_t3226471752;
// QuillController
struct QuillController_t2665191159;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuillController/<fadeText>c__Iterator2
struct  U3CfadeTextU3Ec__Iterator2_t3047042458  : public Il2CppObject
{
public:
	// UnityEngine.Material QuillController/<fadeText>c__Iterator2::JGCPNFGKBOC
	Material_t193706927 * ___JGCPNFGKBOC_0;
	// System.Single QuillController/<fadeText>c__Iterator2::IGIHJBALJEK
	float ___IGIHJBALJEK_1;
	// System.Single QuillController/<fadeText>c__Iterator2::KGKJGGMOJNE
	float ___KGKJGGMOJNE_2;
	// System.Single QuillController/<fadeText>c__Iterator2::OJJNNKBLEPC
	float ___OJJNNKBLEPC_3;
	// System.Single QuillController/<fadeText>c__Iterator2::FJKJHGHOJIC
	float ___FJKJHGHOJIC_4;
	// System.Action QuillController/<fadeText>c__Iterator2::PLLPPEEBFAP
	Action_t3226471752 * ___PLLPPEEBFAP_5;
	// QuillController QuillController/<fadeText>c__Iterator2::AOOLEAHHMIH
	QuillController_t2665191159 * ___AOOLEAHHMIH_6;
	// System.Object QuillController/<fadeText>c__Iterator2::LGBFNMECDHC
	Il2CppObject * ___LGBFNMECDHC_7;
	// System.Boolean QuillController/<fadeText>c__Iterator2::IMAGLIMLPFK
	bool ___IMAGLIMLPFK_8;
	// System.Int32 QuillController/<fadeText>c__Iterator2::EPCFNNGDBGC
	int32_t ___EPCFNNGDBGC_9;

public:
	inline static int32_t get_offset_of_JGCPNFGKBOC_0() { return static_cast<int32_t>(offsetof(U3CfadeTextU3Ec__Iterator2_t3047042458, ___JGCPNFGKBOC_0)); }
	inline Material_t193706927 * get_JGCPNFGKBOC_0() const { return ___JGCPNFGKBOC_0; }
	inline Material_t193706927 ** get_address_of_JGCPNFGKBOC_0() { return &___JGCPNFGKBOC_0; }
	inline void set_JGCPNFGKBOC_0(Material_t193706927 * value)
	{
		___JGCPNFGKBOC_0 = value;
		Il2CppCodeGenWriteBarrier(&___JGCPNFGKBOC_0, value);
	}

	inline static int32_t get_offset_of_IGIHJBALJEK_1() { return static_cast<int32_t>(offsetof(U3CfadeTextU3Ec__Iterator2_t3047042458, ___IGIHJBALJEK_1)); }
	inline float get_IGIHJBALJEK_1() const { return ___IGIHJBALJEK_1; }
	inline float* get_address_of_IGIHJBALJEK_1() { return &___IGIHJBALJEK_1; }
	inline void set_IGIHJBALJEK_1(float value)
	{
		___IGIHJBALJEK_1 = value;
	}

	inline static int32_t get_offset_of_KGKJGGMOJNE_2() { return static_cast<int32_t>(offsetof(U3CfadeTextU3Ec__Iterator2_t3047042458, ___KGKJGGMOJNE_2)); }
	inline float get_KGKJGGMOJNE_2() const { return ___KGKJGGMOJNE_2; }
	inline float* get_address_of_KGKJGGMOJNE_2() { return &___KGKJGGMOJNE_2; }
	inline void set_KGKJGGMOJNE_2(float value)
	{
		___KGKJGGMOJNE_2 = value;
	}

	inline static int32_t get_offset_of_OJJNNKBLEPC_3() { return static_cast<int32_t>(offsetof(U3CfadeTextU3Ec__Iterator2_t3047042458, ___OJJNNKBLEPC_3)); }
	inline float get_OJJNNKBLEPC_3() const { return ___OJJNNKBLEPC_3; }
	inline float* get_address_of_OJJNNKBLEPC_3() { return &___OJJNNKBLEPC_3; }
	inline void set_OJJNNKBLEPC_3(float value)
	{
		___OJJNNKBLEPC_3 = value;
	}

	inline static int32_t get_offset_of_FJKJHGHOJIC_4() { return static_cast<int32_t>(offsetof(U3CfadeTextU3Ec__Iterator2_t3047042458, ___FJKJHGHOJIC_4)); }
	inline float get_FJKJHGHOJIC_4() const { return ___FJKJHGHOJIC_4; }
	inline float* get_address_of_FJKJHGHOJIC_4() { return &___FJKJHGHOJIC_4; }
	inline void set_FJKJHGHOJIC_4(float value)
	{
		___FJKJHGHOJIC_4 = value;
	}

	inline static int32_t get_offset_of_PLLPPEEBFAP_5() { return static_cast<int32_t>(offsetof(U3CfadeTextU3Ec__Iterator2_t3047042458, ___PLLPPEEBFAP_5)); }
	inline Action_t3226471752 * get_PLLPPEEBFAP_5() const { return ___PLLPPEEBFAP_5; }
	inline Action_t3226471752 ** get_address_of_PLLPPEEBFAP_5() { return &___PLLPPEEBFAP_5; }
	inline void set_PLLPPEEBFAP_5(Action_t3226471752 * value)
	{
		___PLLPPEEBFAP_5 = value;
		Il2CppCodeGenWriteBarrier(&___PLLPPEEBFAP_5, value);
	}

	inline static int32_t get_offset_of_AOOLEAHHMIH_6() { return static_cast<int32_t>(offsetof(U3CfadeTextU3Ec__Iterator2_t3047042458, ___AOOLEAHHMIH_6)); }
	inline QuillController_t2665191159 * get_AOOLEAHHMIH_6() const { return ___AOOLEAHHMIH_6; }
	inline QuillController_t2665191159 ** get_address_of_AOOLEAHHMIH_6() { return &___AOOLEAHHMIH_6; }
	inline void set_AOOLEAHHMIH_6(QuillController_t2665191159 * value)
	{
		___AOOLEAHHMIH_6 = value;
		Il2CppCodeGenWriteBarrier(&___AOOLEAHHMIH_6, value);
	}

	inline static int32_t get_offset_of_LGBFNMECDHC_7() { return static_cast<int32_t>(offsetof(U3CfadeTextU3Ec__Iterator2_t3047042458, ___LGBFNMECDHC_7)); }
	inline Il2CppObject * get_LGBFNMECDHC_7() const { return ___LGBFNMECDHC_7; }
	inline Il2CppObject ** get_address_of_LGBFNMECDHC_7() { return &___LGBFNMECDHC_7; }
	inline void set_LGBFNMECDHC_7(Il2CppObject * value)
	{
		___LGBFNMECDHC_7 = value;
		Il2CppCodeGenWriteBarrier(&___LGBFNMECDHC_7, value);
	}

	inline static int32_t get_offset_of_IMAGLIMLPFK_8() { return static_cast<int32_t>(offsetof(U3CfadeTextU3Ec__Iterator2_t3047042458, ___IMAGLIMLPFK_8)); }
	inline bool get_IMAGLIMLPFK_8() const { return ___IMAGLIMLPFK_8; }
	inline bool* get_address_of_IMAGLIMLPFK_8() { return &___IMAGLIMLPFK_8; }
	inline void set_IMAGLIMLPFK_8(bool value)
	{
		___IMAGLIMLPFK_8 = value;
	}

	inline static int32_t get_offset_of_EPCFNNGDBGC_9() { return static_cast<int32_t>(offsetof(U3CfadeTextU3Ec__Iterator2_t3047042458, ___EPCFNNGDBGC_9)); }
	inline int32_t get_EPCFNNGDBGC_9() const { return ___EPCFNNGDBGC_9; }
	inline int32_t* get_address_of_EPCFNNGDBGC_9() { return &___EPCFNNGDBGC_9; }
	inline void set_EPCFNNGDBGC_9(int32_t value)
	{
		___EPCFNNGDBGC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
