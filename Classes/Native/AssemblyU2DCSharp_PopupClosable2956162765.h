﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_NarrativeEffect3336735121.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopupClosable
struct  PopupClosable_t2956162765  : public NarrativeEffect_t3336735121
{
public:
	// System.String PopupClosable::popupMessage
	String_t* ___popupMessage_3;

public:
	inline static int32_t get_offset_of_popupMessage_3() { return static_cast<int32_t>(offsetof(PopupClosable_t2956162765, ___popupMessage_3)); }
	inline String_t* get_popupMessage_3() const { return ___popupMessage_3; }
	inline String_t** get_address_of_popupMessage_3() { return &___popupMessage_3; }
	inline void set_popupMessage_3(String_t* value)
	{
		___popupMessage_3 = value;
		Il2CppCodeGenWriteBarrier(&___popupMessage_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
