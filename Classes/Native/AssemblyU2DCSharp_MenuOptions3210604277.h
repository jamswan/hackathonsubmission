﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// CameraSettings
struct CameraSettings_t3536359094;
// TrackableSettings
struct TrackableSettings_t4265251850;
// MenuAnimator
struct MenuAnimator_t2049002970;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuOptions
struct  MenuOptions_t3210604277  : public MonoBehaviour_t1158329972
{
public:
	// CameraSettings MenuOptions::HHNJAPJPEON
	CameraSettings_t3536359094 * ___HHNJAPJPEON_2;
	// TrackableSettings MenuOptions::NKGJJGJKKIE
	TrackableSettings_t4265251850 * ___NKGJJGJKKIE_3;
	// MenuAnimator MenuOptions::KHNIGIIJIII
	MenuAnimator_t2049002970 * ___KHNIGIIJIII_4;

public:
	inline static int32_t get_offset_of_HHNJAPJPEON_2() { return static_cast<int32_t>(offsetof(MenuOptions_t3210604277, ___HHNJAPJPEON_2)); }
	inline CameraSettings_t3536359094 * get_HHNJAPJPEON_2() const { return ___HHNJAPJPEON_2; }
	inline CameraSettings_t3536359094 ** get_address_of_HHNJAPJPEON_2() { return &___HHNJAPJPEON_2; }
	inline void set_HHNJAPJPEON_2(CameraSettings_t3536359094 * value)
	{
		___HHNJAPJPEON_2 = value;
		Il2CppCodeGenWriteBarrier(&___HHNJAPJPEON_2, value);
	}

	inline static int32_t get_offset_of_NKGJJGJKKIE_3() { return static_cast<int32_t>(offsetof(MenuOptions_t3210604277, ___NKGJJGJKKIE_3)); }
	inline TrackableSettings_t4265251850 * get_NKGJJGJKKIE_3() const { return ___NKGJJGJKKIE_3; }
	inline TrackableSettings_t4265251850 ** get_address_of_NKGJJGJKKIE_3() { return &___NKGJJGJKKIE_3; }
	inline void set_NKGJJGJKKIE_3(TrackableSettings_t4265251850 * value)
	{
		___NKGJJGJKKIE_3 = value;
		Il2CppCodeGenWriteBarrier(&___NKGJJGJKKIE_3, value);
	}

	inline static int32_t get_offset_of_KHNIGIIJIII_4() { return static_cast<int32_t>(offsetof(MenuOptions_t3210604277, ___KHNIGIIJIII_4)); }
	inline MenuAnimator_t2049002970 * get_KHNIGIIJIII_4() const { return ___KHNIGIIJIII_4; }
	inline MenuAnimator_t2049002970 ** get_address_of_KHNIGIIJIII_4() { return &___KHNIGIIJIII_4; }
	inline void set_KHNIGIIJIII_4(MenuAnimator_t2049002970 * value)
	{
		___KHNIGIIJIII_4 = value;
		Il2CppCodeGenWriteBarrier(&___KHNIGIIJIII_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
