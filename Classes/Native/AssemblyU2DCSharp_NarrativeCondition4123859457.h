﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// NarrativeCondition
struct NarrativeCondition_t4123859457;
// NarrativeCondition[]
struct NarrativeConditionU5BU5D_t2447630556;
// NarrativeEffect[]
struct NarrativeEffectU5BU5D_t2993910028;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NarrativeCondition
struct  NarrativeCondition_t4123859457  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean NarrativeCondition::simulate
	bool ___simulate_2;
	// System.Boolean NarrativeCondition::reusable
	bool ___reusable_3;
	// System.Boolean NarrativeCondition::repeat
	bool ___repeat_4;
	// NarrativeCondition NarrativeCondition::narrativeObjectParent
	NarrativeCondition_t4123859457 * ___narrativeObjectParent_5;
	// NarrativeCondition[] NarrativeCondition::narrativeObjectChildren
	NarrativeConditionU5BU5D_t2447630556* ___narrativeObjectChildren_6;
	// NarrativeEffect[] NarrativeCondition::narrativeEffects
	NarrativeEffectU5BU5D_t2993910028* ___narrativeEffects_7;
	// System.String NarrativeCondition::HFBHJJDNKAF
	String_t* ___HFBHJJDNKAF_8;
	// System.Boolean NarrativeCondition::or
	bool ___or_9;
	// UnityEngine.GameObject NarrativeCondition::nextToEnable
	GameObject_t1756533147 * ___nextToEnable_10;
	// System.Boolean NarrativeCondition::BLKKEILFIPK
	bool ___BLKKEILFIPK_11;
	// System.Boolean NarrativeCondition::ECPKIOBFGJD
	bool ___ECPKIOBFGJD_12;

public:
	inline static int32_t get_offset_of_simulate_2() { return static_cast<int32_t>(offsetof(NarrativeCondition_t4123859457, ___simulate_2)); }
	inline bool get_simulate_2() const { return ___simulate_2; }
	inline bool* get_address_of_simulate_2() { return &___simulate_2; }
	inline void set_simulate_2(bool value)
	{
		___simulate_2 = value;
	}

	inline static int32_t get_offset_of_reusable_3() { return static_cast<int32_t>(offsetof(NarrativeCondition_t4123859457, ___reusable_3)); }
	inline bool get_reusable_3() const { return ___reusable_3; }
	inline bool* get_address_of_reusable_3() { return &___reusable_3; }
	inline void set_reusable_3(bool value)
	{
		___reusable_3 = value;
	}

	inline static int32_t get_offset_of_repeat_4() { return static_cast<int32_t>(offsetof(NarrativeCondition_t4123859457, ___repeat_4)); }
	inline bool get_repeat_4() const { return ___repeat_4; }
	inline bool* get_address_of_repeat_4() { return &___repeat_4; }
	inline void set_repeat_4(bool value)
	{
		___repeat_4 = value;
	}

	inline static int32_t get_offset_of_narrativeObjectParent_5() { return static_cast<int32_t>(offsetof(NarrativeCondition_t4123859457, ___narrativeObjectParent_5)); }
	inline NarrativeCondition_t4123859457 * get_narrativeObjectParent_5() const { return ___narrativeObjectParent_5; }
	inline NarrativeCondition_t4123859457 ** get_address_of_narrativeObjectParent_5() { return &___narrativeObjectParent_5; }
	inline void set_narrativeObjectParent_5(NarrativeCondition_t4123859457 * value)
	{
		___narrativeObjectParent_5 = value;
		Il2CppCodeGenWriteBarrier(&___narrativeObjectParent_5, value);
	}

	inline static int32_t get_offset_of_narrativeObjectChildren_6() { return static_cast<int32_t>(offsetof(NarrativeCondition_t4123859457, ___narrativeObjectChildren_6)); }
	inline NarrativeConditionU5BU5D_t2447630556* get_narrativeObjectChildren_6() const { return ___narrativeObjectChildren_6; }
	inline NarrativeConditionU5BU5D_t2447630556** get_address_of_narrativeObjectChildren_6() { return &___narrativeObjectChildren_6; }
	inline void set_narrativeObjectChildren_6(NarrativeConditionU5BU5D_t2447630556* value)
	{
		___narrativeObjectChildren_6 = value;
		Il2CppCodeGenWriteBarrier(&___narrativeObjectChildren_6, value);
	}

	inline static int32_t get_offset_of_narrativeEffects_7() { return static_cast<int32_t>(offsetof(NarrativeCondition_t4123859457, ___narrativeEffects_7)); }
	inline NarrativeEffectU5BU5D_t2993910028* get_narrativeEffects_7() const { return ___narrativeEffects_7; }
	inline NarrativeEffectU5BU5D_t2993910028** get_address_of_narrativeEffects_7() { return &___narrativeEffects_7; }
	inline void set_narrativeEffects_7(NarrativeEffectU5BU5D_t2993910028* value)
	{
		___narrativeEffects_7 = value;
		Il2CppCodeGenWriteBarrier(&___narrativeEffects_7, value);
	}

	inline static int32_t get_offset_of_HFBHJJDNKAF_8() { return static_cast<int32_t>(offsetof(NarrativeCondition_t4123859457, ___HFBHJJDNKAF_8)); }
	inline String_t* get_HFBHJJDNKAF_8() const { return ___HFBHJJDNKAF_8; }
	inline String_t** get_address_of_HFBHJJDNKAF_8() { return &___HFBHJJDNKAF_8; }
	inline void set_HFBHJJDNKAF_8(String_t* value)
	{
		___HFBHJJDNKAF_8 = value;
		Il2CppCodeGenWriteBarrier(&___HFBHJJDNKAF_8, value);
	}

	inline static int32_t get_offset_of_or_9() { return static_cast<int32_t>(offsetof(NarrativeCondition_t4123859457, ___or_9)); }
	inline bool get_or_9() const { return ___or_9; }
	inline bool* get_address_of_or_9() { return &___or_9; }
	inline void set_or_9(bool value)
	{
		___or_9 = value;
	}

	inline static int32_t get_offset_of_nextToEnable_10() { return static_cast<int32_t>(offsetof(NarrativeCondition_t4123859457, ___nextToEnable_10)); }
	inline GameObject_t1756533147 * get_nextToEnable_10() const { return ___nextToEnable_10; }
	inline GameObject_t1756533147 ** get_address_of_nextToEnable_10() { return &___nextToEnable_10; }
	inline void set_nextToEnable_10(GameObject_t1756533147 * value)
	{
		___nextToEnable_10 = value;
		Il2CppCodeGenWriteBarrier(&___nextToEnable_10, value);
	}

	inline static int32_t get_offset_of_BLKKEILFIPK_11() { return static_cast<int32_t>(offsetof(NarrativeCondition_t4123859457, ___BLKKEILFIPK_11)); }
	inline bool get_BLKKEILFIPK_11() const { return ___BLKKEILFIPK_11; }
	inline bool* get_address_of_BLKKEILFIPK_11() { return &___BLKKEILFIPK_11; }
	inline void set_BLKKEILFIPK_11(bool value)
	{
		___BLKKEILFIPK_11 = value;
	}

	inline static int32_t get_offset_of_ECPKIOBFGJD_12() { return static_cast<int32_t>(offsetof(NarrativeCondition_t4123859457, ___ECPKIOBFGJD_12)); }
	inline bool get_ECPKIOBFGJD_12() const { return ___ECPKIOBFGJD_12; }
	inline bool* get_address_of_ECPKIOBFGJD_12() { return &___ECPKIOBFGJD_12; }
	inline void set_ECPKIOBFGJD_12(bool value)
	{
		___ECPKIOBFGJD_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
