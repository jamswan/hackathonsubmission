﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.AudioSource
struct AudioSource_t1135106623;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IJDGMEKNLGL
struct  IJDGMEKNLGL_t3456803436  : public Il2CppObject
{
public:
	// UnityEngine.AudioSource IJDGMEKNLGL::FAMOEAKBKID
	AudioSource_t1135106623 * ___FAMOEAKBKID_0;
	// System.Single IJDGMEKNLGL::MKLFMGNDJBE
	float ___MKLFMGNDJBE_1;
	// System.Single IJDGMEKNLGL::CHFMJNJEGEC
	float ___CHFMJNJEGEC_2;
	// System.Boolean IJDGMEKNLGL::HMFINGPEEDN
	bool ___HMFINGPEEDN_3;

public:
	inline static int32_t get_offset_of_FAMOEAKBKID_0() { return static_cast<int32_t>(offsetof(IJDGMEKNLGL_t3456803436, ___FAMOEAKBKID_0)); }
	inline AudioSource_t1135106623 * get_FAMOEAKBKID_0() const { return ___FAMOEAKBKID_0; }
	inline AudioSource_t1135106623 ** get_address_of_FAMOEAKBKID_0() { return &___FAMOEAKBKID_0; }
	inline void set_FAMOEAKBKID_0(AudioSource_t1135106623 * value)
	{
		___FAMOEAKBKID_0 = value;
		Il2CppCodeGenWriteBarrier(&___FAMOEAKBKID_0, value);
	}

	inline static int32_t get_offset_of_MKLFMGNDJBE_1() { return static_cast<int32_t>(offsetof(IJDGMEKNLGL_t3456803436, ___MKLFMGNDJBE_1)); }
	inline float get_MKLFMGNDJBE_1() const { return ___MKLFMGNDJBE_1; }
	inline float* get_address_of_MKLFMGNDJBE_1() { return &___MKLFMGNDJBE_1; }
	inline void set_MKLFMGNDJBE_1(float value)
	{
		___MKLFMGNDJBE_1 = value;
	}

	inline static int32_t get_offset_of_CHFMJNJEGEC_2() { return static_cast<int32_t>(offsetof(IJDGMEKNLGL_t3456803436, ___CHFMJNJEGEC_2)); }
	inline float get_CHFMJNJEGEC_2() const { return ___CHFMJNJEGEC_2; }
	inline float* get_address_of_CHFMJNJEGEC_2() { return &___CHFMJNJEGEC_2; }
	inline void set_CHFMJNJEGEC_2(float value)
	{
		___CHFMJNJEGEC_2 = value;
	}

	inline static int32_t get_offset_of_HMFINGPEEDN_3() { return static_cast<int32_t>(offsetof(IJDGMEKNLGL_t3456803436, ___HMFINGPEEDN_3)); }
	inline bool get_HMFINGPEEDN_3() const { return ___HMFINGPEEDN_3; }
	inline bool* get_address_of_HMFINGPEEDN_3() { return &___HMFINGPEEDN_3; }
	inline void set_HMFINGPEEDN_3(bool value)
	{
		___HMFINGPEEDN_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
