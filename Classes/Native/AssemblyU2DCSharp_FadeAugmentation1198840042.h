﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_AugmentationEffect2696530045.h"

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeAugmentation
struct  FadeAugmentation_t1198840042  : public AugmentationEffect_t2696530045
{
public:
	// System.Single FadeAugmentation::fadeTime
	float ___fadeTime_2;
	// System.Single FadeAugmentation::JNEKLHNJBGA
	float ___JNEKLHNJBGA_3;
	// UnityEngine.Material FadeAugmentation::MEOPIOMLHMO
	Material_t193706927 * ___MEOPIOMLHMO_4;
	// UnityEngine.Coroutine FadeAugmentation::PMKAOIAMIEP
	Coroutine_t2299508840 * ___PMKAOIAMIEP_5;

public:
	inline static int32_t get_offset_of_fadeTime_2() { return static_cast<int32_t>(offsetof(FadeAugmentation_t1198840042, ___fadeTime_2)); }
	inline float get_fadeTime_2() const { return ___fadeTime_2; }
	inline float* get_address_of_fadeTime_2() { return &___fadeTime_2; }
	inline void set_fadeTime_2(float value)
	{
		___fadeTime_2 = value;
	}

	inline static int32_t get_offset_of_JNEKLHNJBGA_3() { return static_cast<int32_t>(offsetof(FadeAugmentation_t1198840042, ___JNEKLHNJBGA_3)); }
	inline float get_JNEKLHNJBGA_3() const { return ___JNEKLHNJBGA_3; }
	inline float* get_address_of_JNEKLHNJBGA_3() { return &___JNEKLHNJBGA_3; }
	inline void set_JNEKLHNJBGA_3(float value)
	{
		___JNEKLHNJBGA_3 = value;
	}

	inline static int32_t get_offset_of_MEOPIOMLHMO_4() { return static_cast<int32_t>(offsetof(FadeAugmentation_t1198840042, ___MEOPIOMLHMO_4)); }
	inline Material_t193706927 * get_MEOPIOMLHMO_4() const { return ___MEOPIOMLHMO_4; }
	inline Material_t193706927 ** get_address_of_MEOPIOMLHMO_4() { return &___MEOPIOMLHMO_4; }
	inline void set_MEOPIOMLHMO_4(Material_t193706927 * value)
	{
		___MEOPIOMLHMO_4 = value;
		Il2CppCodeGenWriteBarrier(&___MEOPIOMLHMO_4, value);
	}

	inline static int32_t get_offset_of_PMKAOIAMIEP_5() { return static_cast<int32_t>(offsetof(FadeAugmentation_t1198840042, ___PMKAOIAMIEP_5)); }
	inline Coroutine_t2299508840 * get_PMKAOIAMIEP_5() const { return ___PMKAOIAMIEP_5; }
	inline Coroutine_t2299508840 ** get_address_of_PMKAOIAMIEP_5() { return &___PMKAOIAMIEP_5; }
	inline void set_PMKAOIAMIEP_5(Coroutine_t2299508840 * value)
	{
		___PMKAOIAMIEP_5 = value;
		Il2CppCodeGenWriteBarrier(&___PMKAOIAMIEP_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
