﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_Boolean3825574718.h"

// System.Net.Sockets.TcpClient
struct TcpClient_t408947970;
// System.Threading.Thread
struct Thread_t241561612;
// Gvr.Internal.EmulatorManager
struct EmulatorManager_t3364249716;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorClientSocket
struct  EmulatorClientSocket_t2001911543  : public MonoBehaviour_t1158329972
{
public:
	// System.Net.Sockets.TcpClient Gvr.Internal.EmulatorClientSocket::ONPKLOLLMED
	TcpClient_t408947970 * ___ONPKLOLLMED_5;
	// System.Threading.Thread Gvr.Internal.EmulatorClientSocket::CMBKFJCMIIG
	Thread_t241561612 * ___CMBKFJCMIIG_6;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Gvr.Internal.EmulatorClientSocket::JHLHEGHOFHP
	bool ___JHLHEGHOFHP_7;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Gvr.Internal.EmulatorClientSocket::NOEALGHBFFL
	bool ___NOEALGHBFFL_8;
	// Gvr.Internal.EmulatorManager Gvr.Internal.EmulatorClientSocket::BAIPHHCGAPF
	EmulatorManager_t3364249716 * ___BAIPHHCGAPF_9;
	// System.Boolean Gvr.Internal.EmulatorClientSocket::<OPLNEPJFOLP>k__BackingField
	bool ___U3COPLNEPJFOLPU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_ONPKLOLLMED_5() { return static_cast<int32_t>(offsetof(EmulatorClientSocket_t2001911543, ___ONPKLOLLMED_5)); }
	inline TcpClient_t408947970 * get_ONPKLOLLMED_5() const { return ___ONPKLOLLMED_5; }
	inline TcpClient_t408947970 ** get_address_of_ONPKLOLLMED_5() { return &___ONPKLOLLMED_5; }
	inline void set_ONPKLOLLMED_5(TcpClient_t408947970 * value)
	{
		___ONPKLOLLMED_5 = value;
		Il2CppCodeGenWriteBarrier(&___ONPKLOLLMED_5, value);
	}

	inline static int32_t get_offset_of_CMBKFJCMIIG_6() { return static_cast<int32_t>(offsetof(EmulatorClientSocket_t2001911543, ___CMBKFJCMIIG_6)); }
	inline Thread_t241561612 * get_CMBKFJCMIIG_6() const { return ___CMBKFJCMIIG_6; }
	inline Thread_t241561612 ** get_address_of_CMBKFJCMIIG_6() { return &___CMBKFJCMIIG_6; }
	inline void set_CMBKFJCMIIG_6(Thread_t241561612 * value)
	{
		___CMBKFJCMIIG_6 = value;
		Il2CppCodeGenWriteBarrier(&___CMBKFJCMIIG_6, value);
	}

	inline static int32_t get_offset_of_JHLHEGHOFHP_7() { return static_cast<int32_t>(offsetof(EmulatorClientSocket_t2001911543, ___JHLHEGHOFHP_7)); }
	inline bool get_JHLHEGHOFHP_7() const { return ___JHLHEGHOFHP_7; }
	inline bool* get_address_of_JHLHEGHOFHP_7() { return &___JHLHEGHOFHP_7; }
	inline void set_JHLHEGHOFHP_7(bool value)
	{
		___JHLHEGHOFHP_7 = value;
	}

	inline static int32_t get_offset_of_NOEALGHBFFL_8() { return static_cast<int32_t>(offsetof(EmulatorClientSocket_t2001911543, ___NOEALGHBFFL_8)); }
	inline bool get_NOEALGHBFFL_8() const { return ___NOEALGHBFFL_8; }
	inline bool* get_address_of_NOEALGHBFFL_8() { return &___NOEALGHBFFL_8; }
	inline void set_NOEALGHBFFL_8(bool value)
	{
		___NOEALGHBFFL_8 = value;
	}

	inline static int32_t get_offset_of_BAIPHHCGAPF_9() { return static_cast<int32_t>(offsetof(EmulatorClientSocket_t2001911543, ___BAIPHHCGAPF_9)); }
	inline EmulatorManager_t3364249716 * get_BAIPHHCGAPF_9() const { return ___BAIPHHCGAPF_9; }
	inline EmulatorManager_t3364249716 ** get_address_of_BAIPHHCGAPF_9() { return &___BAIPHHCGAPF_9; }
	inline void set_BAIPHHCGAPF_9(EmulatorManager_t3364249716 * value)
	{
		___BAIPHHCGAPF_9 = value;
		Il2CppCodeGenWriteBarrier(&___BAIPHHCGAPF_9, value);
	}

	inline static int32_t get_offset_of_U3COPLNEPJFOLPU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(EmulatorClientSocket_t2001911543, ___U3COPLNEPJFOLPU3Ek__BackingField_10)); }
	inline bool get_U3COPLNEPJFOLPU3Ek__BackingField_10() const { return ___U3COPLNEPJFOLPU3Ek__BackingField_10; }
	inline bool* get_address_of_U3COPLNEPJFOLPU3Ek__BackingField_10() { return &___U3COPLNEPJFOLPU3Ek__BackingField_10; }
	inline void set_U3COPLNEPJFOLPU3Ek__BackingField_10(bool value)
	{
		___U3COPLNEPJFOLPU3Ek__BackingField_10 = value;
	}
};

struct EmulatorClientSocket_t2001911543_StaticFields
{
public:
	// System.Int32 Gvr.Internal.EmulatorClientSocket::IMGBFIPPOGO
	int32_t ___IMGBFIPPOGO_2;

public:
	inline static int32_t get_offset_of_IMGBFIPPOGO_2() { return static_cast<int32_t>(offsetof(EmulatorClientSocket_t2001911543_StaticFields, ___IMGBFIPPOGO_2)); }
	inline int32_t get_IMGBFIPPOGO_2() const { return ___IMGBFIPPOGO_2; }
	inline int32_t* get_address_of_IMGBFIPPOGO_2() { return &___IMGBFIPPOGO_2; }
	inline void set_IMGBFIPPOGO_2(int32_t value)
	{
		___IMGBFIPPOGO_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
